window.onload  = function(){

	//Evento validacion
	$("#btn-enviar").on('click', function (e) {
		e.preventDefault();

		//validacion para no enviar en caso de no seleccionar ninguno servicio
		if(tipe == 'desktop'){
			for (i = 0; i <= contadorSelects; i++) {

				categoria = $("#divAgenda"+i+" [data-categoria"+i+"]").data('categoria'+i);


			    if(document.getElementById("cantidad-"+categoria) != null){
			    	select = document.getElementById("cantidad-"+categoria).value;

			    	if(select != '0'){
			    		data[i] = select;
			    		cantSeparator++;
			    	}
			    }
			}

			data = startFromZero(data);

			if(data == ""){
				$('#modalnotificaciones').modal('show')
				$('#modalnotificaciones').on('shown.bs.modal', function () {
				$('.advertencia').html('Debes solicitar almenos un servicio, recuerda que el valor minimo de agendamiento es $20,000.');
				//formblanquito.direccion.focus();
				})
				return false;
			}
		}


		//Validacion general
		if(typeof(formblanquito.direccion) == "undefined"){
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes Ingresar una dirección.');
			//formblanquito.direccion.focus();
			})
			return false;
		}
		if(formblanquito.direccion.value == "") {
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes Ingresar una dirección.');
			//formblanquito.direccion.focus();
			})
			return false;
		}
		if(formblanquito.metodo.value == 0) {
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes ingresar un método de págo.');
			formblanquito.metodo.focus();
			})
			return false;
		}
		if(formblanquito.telefono.value == "") {
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes ingresar un teléfono de contacto.');
			formblanquito.telefono.focus();
			})
			return false;
		}
		if(formblanquito.telefono.value.length < 7) {
				$('#modalnotificaciones').modal('show')
				$('#modalnotificaciones').on('shown.bs.modal', function () {
				$('.advertencia').html('El campo teléfono debe contener al menos siete números.');
				formblanquito.telefono.focus();
				})
				return false;
			}
			if(formblanquito.telefono.value.length > 15) {
				$('#modalnotificaciones').modal('show')
				$('#modalnotificaciones').on('shown.bs.modal', function () {
				$('.advertencia').html('El campo teléfono debe contener menos de quince números.');
				formblanquito.telefono.focus();
				})
				return false;
			}
		if(formblanquito.fecha.value == "") {
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes ingresar una fecha para el servicio.');
			formblanquito.fecha.focus();
			})
			return false;
		}
		if(formblanquito.hora.value == "") {
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes ingresar una hora para el servicio.');
			formblanquito.hora.focus();
			})
			return false;
		}
		if(document.getElementById('dirSenuelo') && document.getElementById('dirSenuelo').value == "false") {
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes ingresar una dirección.');
			})
			return false;
		}
		//Validacion general
		//Cantidad de servicios
		var sel = 0;

		$(".select_servicios :selected").each(function(i, value){
			if ($(value).val() != '') {
				sel = sel + 1;
			}
		});
		var sel = 1;
		if (sel == 0) {
			$('#modalnotificaciones').modal('show')
			$('#modalnotificaciones').on('shown.bs.modal', function () {
			$('.advertencia').html('Debes seleccionar almenos un servicio.');
			})
			return false;
		}else{
			document.getElementById("formblanquito").submit();
			// $( "#formblanquito" ).submit();
		}
		//Fin Cantidad de servicios

	});
	//Fin Evento validacion

	//reset de posiciones de un array
	function startFromZero(arr) {
	    var newArr = [];
	    var count = 0;

	    for (var i in arr) {
	        newArr[count++] = arr[i];
	    }

	    return newArr;
	}

	//Recibe fecha en formato DD/MM/YYYY
	function dia_semana(fecha){
		fecha=fecha.split('/');
		if(fecha.length!=3){
						return null;
		}
		//Vector para calcular día de la semana de un año regular.
		var regular =[0,3,3,6,1,4,6,2,5,0,3,5];
		//Vector para calcular día de la semana de un año bisiesto.
		var bisiesto=[0,3,4,0,2,5,0,3,6,1,4,6];
		//Vector para hacer la traducción de resultado en día de la semana.
		var semana=['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
		//Día especificado en la fecha recibida por parametro.
		var dia=fecha[0];
		//Módulo acumulado del mes especificado en la fecha recibida por parametro.
		var mes=fecha[1]-1;
		//Año especificado por la fecha recibida por parametros.
		var anno=fecha[2];
		//Comparación para saber si el año recibido es bisiesto.
		if((anno % 4 == 0) && !(anno % 100 == 0 && anno % 400 != 0))
				mes=bisiesto[mes];
		else
				mes=regular[mes];
		//Se retorna el resultado del calculo del día de la semana.
		return semana[Math.ceil(Math.ceil(Math.ceil((anno-1)%7)+Math.ceil((Math.floor((anno-1)/4)-Math.floor((3*(Math.floor((anno-1)/100)+1))/4))%7)+mes+dia%7)%7)];
	}


//date picker con validacion para configuracion de time picker
	var input = $('#content_hour').html();
	$('.datepicker').datepicker({
		format: "dd-mm-yyyy",
		startDate: "date.getDate()",
		datesDisabled: ["01-01-2017","02-01-2017","03-01-2017","04-01-2017","05-01-2017","06-01-2017","07-01-2017","08-01-2017","09-01-2017","10-01-2017"],
		todayBtn: true,
		language: "es",
		calendarWeeks: false,
		autoclose: true,
		todayHighlight: true,
		toggleActive: true
	}).on('changeDate', function(e) {
		var diaActual = moment().format('DD-MM-YYYY')
		, diaCalendar = this.value
		, hourMin = '7:00 am'
		, hourMax = '7:00 pm'
		, hourActual = moment().format('hh:mm a');

		var diaCalendarSemana = diaCalendar.replace(/-/g,'/');

		var diaSemanaHora = dia_semana(diaCalendarSemana);

		if(diaSemanaHora == "Domingo"){
			hourMax = '4:00 pm';
		}

		diaActual = diaActual.replace('/','-');

		if(diaActual == diaCalendar){
				hourMilitary = moment().format('HH'); // 5 years ago
				minutos = moment().format('mm'); // 5 years ago

				hourMilitary*=1;        // parse int Hour 07,08,09
				if(hourMilitary >= 19){
						$('#modalnotificaciones').modal('show')
						$('#modalnotificaciones').on('shown.bs.modal', function () {
							$('.advertencia').html('Lo sentimos nuestro horario de atencion es hasta las 7:00 pm.');
						})
						this.value = '';
						return;
				}
				else if(hourMilitary >= 7){ //calculo de hora
						if(minutos *1 <= 30){ minutos = '30'; hourMilitary+=2; }
						else if(minutos *1 <= 59){ minutos = '00'; hourMilitary+=3; }
						hourMin = moment(hourMilitary+':'+minutos, "HH:mm").format('hh:mm a');
				}
		}

		var Fecha = document.getElementById("fecha").value;
		var urlPromocode = urlTo + "/festivos/" + Fecha;

		$.ajax({
			url      : urlPromocode,
			dataType : "json",
			type     : "GET",
			data     : {},
			success  : function (data) {

				if(data['festivo'] != false){
					hourMax = '3:00 pm';
				}

				if(diaCalendar != ''){

					$('#content_hour').html(input)

					$('.time_element').timepicker({
						timeFormat  : 'h:mm p',
						interval    : 30,
						minTime     : hourMin,
						maxTime     : hourMax,
						defaultTime : '11:00 pm',
						startTime   : hourMin,
						dynamic     : false,
						dropdown    : true,
						scrollbar   : true
					});
				}
		  	}
		});
	});
	//fin date and time picker

	// evento reasignar value de select
	$(".select_servicios").on('change', function () {
		var category = $(this).data('category');
		var valor = $(".select_servicios option:selected").data('valor');
		$("#"+category).val(valor);
	});


	//evento servicios vacios
	if(autobook != 'No'){
		if(/&ntilde;/.test(servicio)){
			servicio = servicio.replace("&ntilde;","ñ");
		}
		if(/ /.test(servicio)){
			servicio = servicio.replace(/ /g,"");
		}
		servicio = servicio.replace("[","");
		servicio = servicio.replace("]","-");
		var openDiv = servicio.split("-");
		//document.getElementById('cantidad-'+servicio+'-'+cantidad).value = urlInspirate;
		$('#cantidad-'+servicio+'-'+cantidad).val(urlInspirate);
		$('#cantidad-'+servicio+'-'+cantidad).change();

		categoriaDiv(category);
	}

	function focusFunction($category){
		alert($category);
	}

}

if(is_new_user == 1){
	$('#modalNotiPromoCode').modal('show')
	$('#modalNotiPromoCode').on('shown.bs.modal', function () { })
}

function modalservices(id){
	$('#modalServicios-'+id)
		.modal('show')
		.on('shown.bs.modal', function () { })
}

function cambiar(foto,id,nombre,descripcion) {
	document.getElementById('imgSel-'+id).src = servidor+'/media'+foto;
	document.getElementById('titleSel-'+id).innerHTML = nombre;
	document.getElementById('descSel-'+id).innerHTML = descripcion;

}

function selectService(cont,id,servicio,valor){

	var divService = document.getElementById('ContentServices-'+cont)
	,   divMicroService =  document.getElementById('ContentMicroservices'+cont+'-'+id)
	,   divCantService = document.getElementById("cantidad-"+cont+"-"+id+"-front")
	,   contParr = divService.innerHTML
	,   serSelect = document.getElementById('cantidad-'+cont+'-'+id).value
	,   boolean = (serSelect != '0' && serSelect != '')? !0: !1;

	if(boolean){//Agregar un servicio a la vista
		if(contParr.lastIndexOf('ContentMicroservices'+cont+'-'+id) != -1){//Si ya existe, eliminar
			$('#ContentMicroservices'+cont+'-'+id).remove();
		}

		//document.getElementById('cantidad-'+cont+'-'+id).disabled = false;

		if(contParr == '<p>Seleccione sus servicios</p>'){
			divService.innerHTML = "<div id='ContentMicroservices"+cont+"-"+id+"'>"+servicio+" por $"+valor+" <div id='cantidad-"+cont+"-"+id+"-front' style='display: inline-block;'>x 1</div></div>";
		}else{
			divService.innerHTML += "<div id='ContentMicroservices"+cont+"-"+id+"'>"+servicio+" por $"+valor+" <div id='cantidad-"+cont+"-"+id+"-front' style='display: inline-block;'>x 1</div></div>";
		}
		var cant = document.getElementById('cantidad-'+cont+'-'+id).value
		,   res = cant.split("/");
		document.getElementById('cantidad-'+cont+'-'+id+'-front').innerHTML = "x "+res[3];
	 // document.getElementById('foto-'+cont+'-'+id).style.display = 'block';

	}else{//eliminar un servicio en la vista
		document.getElementById('cantidad-'+cont+'-'+id).value = 0;
		//document.getElementById('cantidad-'+cont+'-'+id).disabled = true;

		if(contParr == '<p>Seleccione sus servicios</p>'){ return true; }

		//var cant = document.getElementById('cantidad-'+cont+'-'+id+'-front').innerHTML;
	 // var img = document.getElementById('foto-'+cont+'-'+id).style.display = 'none';
		$('#ContentMicroservices'+cont+'-'+id).remove();
		contParr = divService.innerHTML
		if(contParr == ""){
			divService.innerHTML = "<p>Seleccione sus servicios</p>";
		}
	}
}

function categoriaDiv(contCategory){
	var boolean = document.getElementById('divDropdownBoolean-'+contCategory).value;

	if(boolean != "true"){
		$('[data-category="category-'+contCategory+'"]').show();
		document.getElementById('divDropdownBoolean-'+contCategory).value="true";
	}else{
		$('[data-category="category-'+contCategory+'"]').hide();
		document.getElementById('divDropdownBoolean-'+contCategory).value="false";
	}
}
	// function Cambiarcantidad (categoria,id){
	//   var cant = document.getElementById('cantidad-'+categoria+'-'+id).innerHTML;
	//   document.getElementById('cantidad-'+categoria+'-'+id+'-front').innerHTML = "x "+cant;
	//   //document.getElementById('foto-'+categoria+'-'+id).style.display = 'block';
	// }

	// function newService(select, category){
	//   var exec   = select.getAttribute("data-exec")
	//   ,   parent = select.parentNode
	//   ,   html   = parent.innerHTML;


	//   if(select.value == "" && exec == 1){
	//     removeService(select);
	//     return;
	//   }
	//   else if(select.value == "" || exec == 1){ return; }
	//   select.setAttribute('data-exec', 1);

	//   var contChild = select.getAttribute('data-cont') * 1
	//   , contParent = contChild + 1;

	//   html = html.replace('servicio-'+category+'-'+contChild, 'servicio-'+category+'-'+contParent);
	//   html = html.replace('servicio-'+category+'-'+contChild, 'servicio-'+category+'-'+contParent);
	//   html = html.replace('ammount-'+category+'-'+contChild, 'ammount-'+category+'-'+contParent);
	//   html = html.replace('ammount-'+category+'-'+contChild, 'ammount-'+category+'-'+contParent);
	//   html = html.replace('data-cont="'+contChild+'"', 'data-cont="'+contParent+'"');

	//   parent.setAttribute('data-estado', 'enable');
	//   parent.insertAdjacentHTML('afterend', '<div class="content-category" data-estado="disabled">'+html+'</div>');
	// }

	// function removeService(element){
	//   var divRemove = element.parentNode;
	//   $(divRemove).remove();
	// }