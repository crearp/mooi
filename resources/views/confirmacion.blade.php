<link rel="shortcut icon" href="favicon.ico">
@extends('layouts.app')


@section('content')
<div>

<br><br><br><br><br><br><br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center">
				CONFIRMACION DE SERVICIO
			</h3>
			<div class="row">
				<div class="col-md-12">
				<P style="text-align: justify;">Tu servicio por el precio de <b>{{$valort_trans}}</b> pesos Colombianos, ha sido pagado con exito y agendado en tu historial de citas, puedes consultar su estado en tu agenda de servicios.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<br><br><br><br>
</div>
@include('front.template.foot')

@endsection

