@extends('layouts.app')
@section('content')

<div class="modal fade" tabindex="-1" role="dialog" id="modalConfirm">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Aviso!</h4>
			</div>
			<div class="modal-body">
				<p id="textModal"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="saveFormHistory">Aceptar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	window.addEventListener('load', function(){
		$('#content_historial [data-action="cancelar"], #content_historial [data-action="terminar"]').on('click', function(e){
			e.preventDefault();

			var action = $(this).data('action')
			,	cont   = $(this).data('cont')
			,	idForm = action+'_'+cont
			,	msj    = (action=="cancelar")? '¿Realmente deseas cancelar este servicio?': '¿Realmente deseas finalizar este servicio?';

			$('#textModal').html(msj);
			$('#modalConfirm').modal('show');
			$('#saveFormHistory').off();
			$('#saveFormHistory').on('click',function(e){
				e.preventDefault();
				$('#'+idForm).submit();
			})
		})

	}, false);

</script>

<div>
	<br><br><br>
	<h2 class="center">HISTORIAL</h2>
	<br><br>
	<div class="row" id="content_historial">
		<div class="col-md-12">
			@if($cantserv != 0)
			@include('flash::message')
			<div style="overflow-x:auto;">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>Dirección</th>
							<th>Hora y Fecha</th>
							<th>Servicio y Valor</th>
							<th>Descuento</th>
							<th>Total</th>
							<th>Profesional</th>
							<th>Método de pago</th>
							<th>Estado</th>
							<th>Calificación</th>
							<th>Comentarios</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($servicios as $idForm => $sus3)
						<tr>
							<td><a href="tiempo/{{ $sus3->date }}/{{ $sus3->hour }}/{{ $sus3->state }}">{{ $sus3->id }}</a></td>
							<td>{{ $sus3->address }}</td> <!-- Dirección -->
							<td>{{ $sus3->date }}&nbsp;{{ $sus3->hour }}</td> <!-- Hora Y Fecha -->
							<td>
								@foreach($sus3->services as $np)
								<?php $valor=number_format($np->price);?>
								{{ $np->name }}&nbsp;:&nbsp;${{ $valor }}<br> <!-- servicio --><!-- valor -->
								@endforeach

								<?php
									$fecha1=explode("/", $sus3->date);
									$fecha2='20'.$fecha1[2].'-'.$fecha1[1].'-'.$fecha1[0];
									$dt = Carbon\Carbon::parse($fecha2);
									$diadelasemana=$dt->dayOfWeek;

									if($diadelasemana != 0){
										echo $valor_domicilio='Domicilio Ordinario $3,000';
									}else{
										echo $valor_domicilio='Domicilio Dominical $5,000';
									}

								?>

							</td>
							<td>{{ $sus3->discount }}</td><!-- descuento -->
							<td>{{ $sus3->total }}</td> <!-- profesional -->
							<td>{{ $sus3->professional }}</td> <!-- profesional -->
							<td>{{ $sus3->payment }}</td> <!-- METODO DE 'PAGO' -->
							<td>{{ $sus3->state }}</td> <!-- estado -->
							<td>{{ $sus3->score }}</td> <!-- calificación -->
							<td>{{ $sus3->comments }}</td> <!-- comentarios -->
							<?php
								//condicional 1 TERMINAR SERVICIO-> que la fecha y hora de hoy sean mayores a la agendada
								$timeActual=date("d/m/y H:i");

								//condicional 2 TERMINAR SERVICIO-> que la fecha y hora de lo agendado + 2 sean menores a la hora actual
								$fecha1=explode("/", $sus3->date);
								$fecha2=$fecha1[2].'-'.$fecha1[1].'-'.$fecha1[0];
								$time=$fecha2.' '.$sus3->hour;
								$horaFutura=Carbon\Carbon::parse($time)->addHours(2);
								$horaFutura=$horaFutura->format("d/m/y H:i");

								//condiocional 3 TERMINAR SERVICIO-> que esta operacion solo se ejecute si la fecha de agendamiento es igual a la de hoy
								$today=date("d/m/y");
							?>

							<td>

								<?php if($sus3->state ==  'Reservado'){ ?>
									{{ Form::open(array('id' => 'cancelar_'.$idForm, 'url' => 'cancelar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Cancelar" data-action="cancelar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php }

								else if($sus3->state ==  'Reservado sin asignacion'){ ?>
									{{ Form::open(array('id' => 'cancelar_'.$idForm, 'url' => 'cancelar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Cancelar" data-action="cancelar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php }

								else if($sus3->state == 'Finalizado' && ($sus3->score) == NULL ){ ?>
									{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Calificar" class="btn btn-lg btn-color submit" />
									{{ Form::close() }}
								<?php }

								else if($sus3->state ==  'Cancelado por cliente'){ ?>
									{{ Form::open(array('url' => 'reprogramar', 'method' => 'post')) }}
									<input type="submit" value="Reprogramar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php }

								else if($sus3->state ==  'Cancelado por professional'){ ?>
									{{ Form::open(array('url' => 'reprogramar', 'method' => 'post')) }}
									<input type="submit" value="Reprogramar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php } ?>
								<br>
			<!-- </td>
			<td> -->
								<?php /*echo $sus3->date.' '.$sus3->hour; echo " menor que ";  echo  $timeActual; echo "<br>";
									echo $sus3->date.' '.$sus3->hour; echo " menor que ";  echo  $horaFutura; echo "<br>";
									echo $sus3->date; echo " igual que ";  echo  $today; echo "<br>";*/
								if($sus3->state ==  'Reservado' && $sus3->date.' '.$sus3->hour < $timeActual && $sus3->date.' '.$sus3->hour < $horaFutura && $sus3->date == $today){ ?>
									{{ Form::open(array('id' => 'terminar_'.$idForm, 'url' => 'terminar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Terminar" data-action="terminar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php }

								else if($sus3->state ==  'Reservado sin asignacion' && $sus3->date.' '.$sus3->hour < $timeActual && $sus3->date.' '.$sus3->hour < $horaFutura && $sus3->date == $today){ ?>
									{{ Form::open(array('id' => 'terminar_'.$idForm, 'url' => 'terminar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Terminar" data-action="terminar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php }

								else if($sus3->state ==  'Cancelado por cliente' && ($sus3->score) == NULL ){ ?>
									{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Calificar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php }

								else if($sus3->state ==  'Cancelado por professional' && ($sus3->score) == NULL ){ ?>
									{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Calificar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
								<?php } ?>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@else
			@include('flash::message')
			@endif
		</div>
	</div>

	<div class="row">
		<hr>
		<br><br>
		<div class="col-md-3"></div>
		<div class="col-md-6 center">
			<a href="{{ url('/agenda') }}">
				<button class="btn btn-lg btn-block btn-color" type="submit"><h4 id="historial">AGENDA UN NUEVO SERVICIO</h4></button>
			</a>
		</div>
		<div class="col-md-3"></div>
	</div>
	<br><br>
</div><!-- /.container -->
@include('front.template.foot')
@endsection