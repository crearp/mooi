<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="Access-Control-Allow-Origin" content="<origin>"/>
    <!--link rel="icon" href="../../favicon.ico">-->
    <title>Calificación</title>

    <style type="text/css">
      .center{text-align:-webkit-center;text-align:-moz-center}body{}.white{background-color:#fff;padding:20px 30px;border-radius:16px}@font-face{font-family:mooi;src:url(http://localhost:8000/static/css/font/brandon_med-webfont.ttf)}@font-face{font-family:mooi_titles;src:url(http://localhost:8000/static/css/font/brandon_med-webfont.ttf)}@font-face{font-family:mooi_txt;src:url(http://localhost:8000/static/css/font/Brandon_reg.otf)}h3{font-family:mooi;color:#645E5E;font-size:30px}h4{font-family:mooi;color:#a79f9f;font-size:20px}h2{font-family:mooi_titles;color:#645E5E}p{font-family:mooi_txt;font-size:20px;color:#645E5E}.size{width:40%}a{color:#EEBA8A;font-size:16px;letter-spacing:2px}a:focus,a:hover{color:#645E5E;text-decoration:underline}.line{width:8%;height:2px;background-color:#EEBA8A}hr{width:70%;height:1px;background-color:#EEBA8A}span{color:#EEBA8A;font-size:20px}.back{background-color:#f5f4f4;padding:20px 30px;border-radius:10px}.row h2{font-size:25px}.col-md-8{width:10 0%}.col-md-4{width: 33.3%;    float: left;}.col-md-2{width:33%}.ii a[href]{color: #eeba8a !important;} .m_style{text-align: -webkit-center;text-align: -moz-center; margin-bottom:50px}
      @media(max-width: 650px){.col-md-4{width: 100%}}

    </style>

  </head>
  <body>
    <div class="container m_style">
      <center>
      <div class="row center">
        <div class="col-md-2"></div>
        <div class="col-md-8 white">
          <div class="row">
          <img class="img-responsive size" src="http://mooi.com.co:8000/static/img/logo_mooi.png" style="width: 40%;">
          <br>
          <h2>{{ $mensaje }}</h2>
          <div class="line"></div>
          <br>
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              <p>Esperamos que te haya encantado el resultado y que hayas disfrutado tu experiencia MOOI.</p>
            </div>
            <div class="col-md-2"></div>
          </div>
          <br>
        </div>
        <br><br>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <P>Agendando con MOOI, ayudas a mujeres en condición de vulnerabilidad, víctimas de la pobreza extrema y/o de la guerra, que han salido de su realidad capacitándose como estilistas profesionales de belleza. Contamos con un riguroso proceso de selección para su vinculación a nuestra plataforma, para brindarte plena seguridad y satisfacción.</P>
            <br>
            <P>Para agendar otra experiencia MOOI, haz clic <a href="https://mooi.com.co/agenda"><span>aquí</span></a>.</P>
            <br>
            <h2>Gracias por agendar con MOOI, esperamos verte pronto de nuevo.</h2>
            <br><br>
            <img class="img-responsive" src="http://mooi.com.co:8000/static/img/icon_MOOI.png">
            <br>
            <a href="http://mooi.com.co">
              www.mooi.com.co
            </a>
          </div>
          <div class="col-md-2"></div>
        </div>

        </div>
        <div class="col-md-2"></div>
      </div>

      <br><br>

      <hr>

      <div id="footer" class="container" style="padding: 20px 0px;">
        <div class="row" style="">
        <div class="col-md-4">
            <a href="https://mooi.com.co/cancelacion">
              <p>Políticas de Cancelación</p>
            </a>
        </div>
        <div class="col-md-4">
          <a href="">
            <p>Agendar Nuevo Servicio</p>
          </a>
        </div>
        <div class="col-md-4 center" style="margin-top: 20px;">
          <a href="https://www.facebook.com/MooiBelleza/" target="blank">
            <img class="img-responsive" src="http://mooi.com.co:8001/media/icon_fb@2x.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </a>
          <a href="https://www.instagram.com/mooibelleza/" target="blank">
            <img class="img-responsive" src="http://mooi.com.co:8001/media/icon_inst@2x.png">
          </a>
        </div>
      </div>
      </div>
      </center>
    </div><!-- /.container -->
  </body>
</html>