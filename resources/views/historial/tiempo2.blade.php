@extends('layouts.app')


@section('content')

<br><br><br><br>

<div class="container">
    	<h2 class="center">TIEMPO PARA TU CITA</h2>
    	</br></br>
    <div class="row" style="text-align: -webkit-center">
    	<div class="col-md-3"></div>
    	<div class="col-md-6">
	    	<p>
	    		Tu servicio fue cancelado, te invitamos a seguir disfrutando de nuestros servicios.
	    	</p>
	    	</br></br>
	    	<div>			
				<a href="{{ url('/historial') }}">
  					<button class="btn btn-lg btn-primary btn-block btn-color"><h4>REGRESAR</h4></button>
  				</a>
			</div>
    	</div>
    	<div class="col-md-3"></div>
	</div>
	</br></br></br>
</div>

<br><br><br><br>
@include('front.template.foot')

@endsection

