<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>error...</title>

    <style>
        html, body, p {
            height     : 100%;
            width      : 100%;
            margin     : 0;
            padding    : 0;
            box-sizing : border-box;
        }
    </style>
</head>
<body>
    <p ALIGN="center"><a href="{{ url('/') }}"><img src="{{ url('image/errorpage.png') }}" WIDTH="100%"></a></p>
</body>
</html>
