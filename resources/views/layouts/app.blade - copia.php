<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="MOOI S.A.S.">
	<html lang="es">

	<meta name="Description" content="Pensamos en generar un enlace entre estilistas profesionales y un buen servicio, y mujeres que se preocupan por sentirse siempre hermosas, siempre MOOI.">

	<!--link rel="icon" href="../../favicon.ico">-->

	<title>MOOI</title>

	<!-- Bootstrap core CSS -->
	<link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="{{ url('css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="{{ url('mooi.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">
	<link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.standalone.css')}}">


	@if (Agent::isDesktop())
	<link rel="stylesheet" type="text/css" href="{{ url('css/raccordion.css') }}" />
	<style type="text/css">
		.accordion_style{ width: 100% !important; }
	</style>
	@else
	<link rel="stylesheet" type="text/css" href="{{ url('css/gallery/flexslider.css') }}" />
	@endif

	@yield('css', '')
</head>
<body>

	<div>
		<div class="row">
			@if (Agent::isDesktop())

				<div class="bar-top">
					<a href="https://www.instagram.com/mooibelleza/" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true" style="color: #EEBA8A"></i></a>&nbsp;&nbsp;&nbsp;
					<a href="https://www.facebook.com/MooiBelleza/" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true" style="color: #EEBA8A"></i></a>
				</div>
				<div class="bar-top">
					<!-- Authentication Links -->
					<?php $sessionuser=Session::get('sessionuserC'); $nombreuser=Session::get('nombreuserC');  ?>
					@if ($sessionuser != 'OK')
					<span>
						<a href="{{ url('/inicio-sesion') }}">
							<span>INICIAR SESIÓN</span>
						</a>&nbsp;&nbsp;&nbsp;
						<span style="color:#EEBA8A">|</span>&nbsp;&nbsp;&nbsp;
						<a href="{{ url('/registro') }}">
							<span>REGÍSTRATE</span>
						</a>
					</span>
					@else
					<div class="dropdown session">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<h4 style="text-transform: uppercase">Hola {{ $nombreuser }}<span class="caret"></span></h4>
						</a>
						<ul class="dropdown-menu">
							<li><a href="{{ url('/logout') }}"><h4>CERRAR SESIÓN</h4></a></li>
							<li><a href="{{ url('/changecard') }}"><h4>AGREGAR TARJETA</h4></a></li>
							<li><a href="{{ url('/deletecards') }}"><h4>ELIMINAR TARJETA</h4></a></li>
							<li><a href="{{ url('/historial') }}"><h4>HISTORIAL</h4></a></li>
							<!--<li><a href="{{ url('/perfil') }}"><h4>PERFIL</h4></a></li>-->
						</ul>
					</div>
					@endif
				</div>
			@else

				<!-- Authentication Links -->
				<?php $sessionuser=Session::get('sessionuserC'); $nombreuser=Session::get('nombreuserC');  ?>
				@if ($sessionuser != 'OK')
				<div class="btnMobile">
					<a href="{{ url('/inicio-sesion') }}">
						<span>INGRESA</span>
					</a>&nbsp;&nbsp;&nbsp;
				</div>
				@else
				<div class="dropdown session btnMobile">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<h4 style="text-transform: uppercase">{{ $nombreuser }}<span class="caret"></span></h4>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('/logout') }}"><h4>CERRAR SESIÓN</h4></a></li>
						<li><a href="{{ url('/changecard') }}"><h4>AGREGAR TARJETA</h4></a></li>
						<li><a href="{{ url('/deletecards') }}"><h4>ELIMINAR TARJETA</h4></a></li>
						<li><a href="{{ url('/historial') }}"><h4>HISTORIAL</h4></a></li>
						<!--<li><a href="{{ url('/perfil') }}"><h4>PERFIL</h4></a></li>-->
					</ul>
				</div>
				@endif

			@endif
		</div>
	</div>
   <!--
	<ul class="navcenter" id="navcenter" style="left:42%; float:left; position:absolute; z-index:1000;">
		<li><IMG SRC="{{ url('image/triangulo.PNG') }}" width="80px" heigth="60px" /></li>
	</ul> -->
	<?php $url= $_SERVER["REQUEST_URI"];?>
	@if($url != '/')
	@if($url == '/inicio' || $url == '/postdeletecards' || $url =='/logout')
	{{-- Menu --}}
	@include('front.template.menu')
	@else
	{{-- Menu --}}
	@include('front.template.menu2')
	@endif
	@endif

	@yield('content')

	<!-- JavaScripts -->
	<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<script src="{{ url('assets/js/jquery.min.js') }}"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	{{-- <script src="{{ elixir('js/app.js') }}"></script> --}} -->
	<script src="{{ url('js/jquery.min.js') }}" type="text/javascript"></script>
	<script src="{{ url('js/bootstrap.min.js')}}"></script>
	<script src="{{ url('js/ie-emulation-modes-warning.js')}}"></script>
	<script src="{{ url('js/ie10-viewport-bug-workaround.js')}}"></script>

	@if (Agent::isDesktop())
	<script src="{{ url('js/jquery.raccordion.js') }}" type="text/javascript"></script>
	@else
	<script src="{{ url('js/gallery/jquery.flexslider.js')}}"></script>
	<script src="{{ url('js/gallery/jquery.easing.js')}}"></script>
	<script src="{{ url('js/gallery/jquery.mousewheel.js')}}"></script>
	@endif

	@if($url != '/agenda')
	<script src="{{ url('js/jquery.animation.easing.js') }}" type="text/javascript"></script>
	@endif
	<!-- Jquery -->
	<!-- Datepicker Files -->
	<script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
	<!-- Languaje -->
	<script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
	<script type="text/javascript">
		$(window).load(function () {
			if ($('#accordion-wrapper').length > 0) {

				@if (Agent::isDesktop())
					$('#accordion-wrapper').raccordion({
						speed: 1000,
						sliderWidth: 940,
						sliderHeight: 600,
						autoCollapse: true
					});
				@else
					var width = window.screen.width <= 400? window.screen.width: 210
					$('.flexslider').flexslider({
						animation     : "slide",
						animationLoop : false,
						itemWidth     : width,
						// itemMargin : 5,
						start         : function(slider){
							$('body').removeClass('loading');
						}
					});
				@endif
			}

		});
	</script>

	@yield('js', '')

</body>
</html>