<!DOCTYPE html>
<html lang="es-CO">
<?php $url= $_SERVER["REQUEST_URI"];?>
<head>
	<meta name="google-site-verification" content="pevNC8mRImc6chP5mlOt7IP6iJ0wHzdKkxLu4xeQ2Vo" />
	<meta name="google-site-verification" content="2JBZN7CBhEYjPYazmz48vw0yVJ_H4vho99C0Zc2MsVU" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- meta para idioma -->
	<meta http-equiv="Content-Language" content="es"/>
	<!-- meta para lozalizacion-->
	<meta name="geo.region" content="CO" />
	<meta name="geo.position" content="4.570868;-74.297333" />
	<meta name="ICBM" content="4.570868, -74.297333" />

	<!--Meta para facebook-->
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	@include('front.template.metas')

	<!-- Bootstrap core CSS -->
	<link href="{{ asset('css/bootstrap/bootstrap.min.css') }}?{{ rand(0, 9000) }}" rel="stylesheet">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="{{ asset('css/ie10-viewport-bug-workaround.css') }}?{{ rand(0, 9000) }}" rel="stylesheet">

	@if(substr_count($url, 'agenda') == 1 || substr_count($url, 'servicios/autoservicios') == 1)
		<link rel="stylesheet" type="text/css" href="{{ url('css/timepicki.css') }}?{{ rand(0, 9000) }}">
		<link rel="stylesheet" type="text/css" href="{{ url('js/timepicker/jquery.timepicker.min.css') }}?{{ rand(0, 9000) }}">
		<link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css') }}?{{ rand(0, 9000) }}">
		<link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.standalone.css') }}?{{ rand(0, 9000) }}">
	@endif

	<!-- Custom styles for this template -->
	<link href="{{ asset('css/main.min.css') }}?{{ rand(0, 9000) }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}?{{ rand(0, 9000) }}">

	@yield('css', '')
</head>
<body>
	<input type="hidden" id="popUpPromoCode" value="false">
	<div>
		<div class="row">
			<div class="bar-top">
				<a href="https://www.instagram.com/mooibelleza/" target="_blank"><i class="fa fa-instagram fa-2x" aria-hidden="true" style="color: #EEBA8A"></i></a>&nbsp;&nbsp;&nbsp;
				<a href="https://www.facebook.com/MooiBelleza/" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true" style="color: #EEBA8A"></i></a>
			</div>
			<div class="bar-top">
				<!-- Authentication Links -->
				<?php $sessionuser=Session::get('sessionuserC'); $nombreuser=Session::get('nombreuserC');  $tipo_usuario=Session::get('tipo'); ?>
				@if ($sessionuser != 'OK')
					<span>
						<a href="{{ url('/inicio-sesion') }}">
							<span style="font-size: 14px;">INICIAR SESIÓN</span>
						</a>&nbsp;&nbsp;&nbsp;
						<span style="color:#EEBA8A">|</span>&nbsp;&nbsp;&nbsp;
						<a href="{{ url('/registro') }}">
							<span style="font-size: 14px;">REGÍSTRATE</span>
						</a>
					</span>
				@else
					<div class="dropdown session">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<h4 style="text-transform: uppercase; font-size: 14px; color: #EEBA8A;">Hola {{ $nombreuser }}<span class="caret"></span></h4>
						</a>
						<ul class="dropdown-menu">
							@if($tipo_usuario == 'CLIENT')
								<li><a href="{{ url('/logout') }}"><h4>CERRAR SESIÓN</h4></a></li>
								<li><a href="{{ url('/changecard') }}"><h4>AGREGAR TARJETA</h4></a></li>
								<li><a href="{{ url('/deletecards') }}"><h4>ELIMINAR TARJETA</h4></a></li>
								<li><a href="{{ url('/historial') }}"><h4>HISTORIAL</h4></a></li>
								<li><a href="{{ url('/perfil') }}"><h4>PERFIL</h4></a></li>
							@else
								<li><a href="{{ url('/logout') }}"><h4>CERRAR SESIÓN</h4></a></li>
							@endif
						</ul>
					</div>

				@endif
			</div>
			@if ($sessionuser != 'OK')
				<div class="btnMobile">
					<a href="{{ url('/inicio-sesion') }}">
						<span>INGRESA</span>
					</a>&nbsp;&nbsp;&nbsp;
				</div>
			@else
				<div class="dropdown session btnMobile">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<h4 style="text-transform: uppercase">{{ $nombreuser }}<span class="caret"></span></h4>
					</a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('/logout') }}"><h4>CERRAR SESIÓN</h4></a></li>
						<li><a href="{{ url('/changecard') }}"><h4>AGREGAR TARJETA</h4></a></li>
						<li><a href="{{ url('/deletecards') }}"><h4>ELIMINAR TARJETA</h4></a></li>
						<li><a href="{{ url('/historial') }}"><h4>HISTORIAL</h4></a></li>
						<!--<li><a href="{{ url('/perfil') }}"><h4>PERFIL</h4></a></li>-->
					</ul>
				</div>

			@endif
		</div>
	</div>
	@if($url != '/perfilprofessional')
		@if($url != '/')
			@if($url == '/inicio' || $url == '/postdeletecards' || $url =='/logout')
				{{-- Menu --}}
				@include('front.template.menu')
			@else
				{{-- Menu --}}
				@include('front.template.menu2')
			@endif
		@endif
	@endif

	@yield('content')

	<!-- Analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js?{{ rand(0, 9000) }}','ga');

		  ga('create', 'UA-85977106-1', 'auto');
		  ga('send', 'pageview');
		</script>
	<!-- Analytics -->

	<!-- JavaScripts -->
	<script src="{{ asset('js/jquery.min.js') }}?{{ rand(0, 9000) }}" type="text/javascript"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}?{{ rand(0, 9000) }}"></script>
	<script src="{{ asset('js/ie-emulation-modes-warning.js') }}?{{ rand(0, 9000) }}"></script>
	<script src="{{ asset('js/ie10-viewport-bug-workaround.js') }}?{{ rand(0, 9000) }}"></script>

	<script src="{{ asset('js/gallery/jquery.easing.js') }}?{{ rand(0, 9000) }}">alert(432)</script>
	<script src="{{ asset('js/gallery/jquery.mousewheel.js') }}?{{ rand(0, 9000) }}"></script>
	<script src="{{ asset('js/jquery.animation.easing.js') }}?{{ rand(0, 9000) }}" type="text/javascript"></script>

	@if(substr_count($url, 'agenda') == 1 || substr_count($url, 'servicios/autoservicios') == 1)
		<!-- Datepicker Files -->
		<script src="{{ asset('datePicker/js/bootstrap-datepicker.js') }}?{{ rand(0, 9000) }}"></script>
		<!-- Languaje -->
		<script src="{{ asset('datePicker/locales/bootstrap-datepicker.es.min.js') }}?{{ rand(0, 9000) }}"></script>

		<script src="{{ url('js/timepicki.js') }}?{{ rand(0, 9000) }}">alert(4321)</script>
		<script src="{{ url('js/validator.js') }}?{{ rand(0, 9000) }}"></script>
		<script src="{{ url('js/timepicker/jquery.timepicker.min.js') }}?{{ rand(0, 9000) }}"></script>
		<script src="{{ url('js/timepicker/moment.min.js') }}?{{ rand(0, 9000) }}"></script>

		<script src="{{ url('js/agenda.js') }}?{{ rand(0, 9000) }}"></script>

	@elseif(substr_count($url, 'servicios') == 1)
		<script src="{{ asset('js/gallery/jquery.flexslider.js') }}?{{ rand(0, 9000) }}"></script>

		{{-- RESPONSIVE SIN RACORDEON --}}
		<script type="text/javascript">

			window.onload  = function(){
				if ($('#accordion-wrapper').length > 0) {
					// var width = window.screen.width <= 400? window.screen.width: window.screen.width * 1 / 3
					var width = $(window).width();

					if(width <= 500){ }
					else if(width <= 700){ width = width * 1 / 2; }
					else{ width = 386.33 }

					$('.flexslider').flexslider({
						animation     : "slide",
						animationLoop : false,
						itemWidth     : width,
						// touch         : true,
						// itemMargin : 5,
						start         : function(slider){
							var heightP = 0;
							$('.description-autoview-style').each(function(index, element){
								if($(element).height() > heightP){
									heightP = $(element).height();
								}
							})
							$('.description-autoview-style').height(heightP);
							$('body').removeClass('loading');

						}
					});
					var myVar;

					myVar = setTimeout(showPage, 1000);


					function showPage() {
					  document.getElementById("loader").style.display = "none";
					  document.getElementById("accordion-wrapper").style.display = "block";
					}
				}
			}
		</script>
	@elseif(substr_count($url, 'regPromoCode') == 1)

		<script type="text/javascript">
			function modalRegistro(){
				var codePromoCode = '{{ $promoCode }}';
				if(codePromoCode != ""){
					//codigo resolución pequeña
					$('#modalNotiPromoCode').modal('show')
					$('#modalNotiPromoCode').on('shown.bs.modal')
				}
			}
		</script>

	@elseif(substr_count($url, '/') == 1)

		<script type="text/javascript">
			window.onload  = function(){
				var ancho = $( window ).width();
				var statePopUp = localStorage.getItem("popUpPromoCode");
				if(statePopUp == null){
					if(ancho<=750){
						//codigo resolución pequeña
						$('#modalPromoCodeMin').modal('show')
						$('#modalPromoCodeMin').on('shown.bs.modal')
						localStorage.setItem("popUpPromoCode", "true");
					}else{
						//codigo resolución mediana
						$('#modalPromoCodeGrand').modal('show')
						$('#modalPromoCodeGrand').on('shown.bs.modal')
						localStorage.setItem("popUpPromoCode", "true");
					}
				}
				var url = '{{ $url }}';
				if(/regPromoCode/.test(url)){
					//modalRegistro();
				}

			}
		</script>
	@endif

</body>
</html>