<link rel="shortcut icon" href="favicon.ico">
@extends('layouts.app')

@section('content')
<br><br><br><br><br><br><br>
<div class="container">
   <div class="row">
       <div class="col col-md-6 col-md-offset-3"   >
           <div class="panel panel-default">
             <div class="panel-heading"><h3 class="panel-title">Atencion!!!</h3></div>
             <div class="panel-body">
               <h4>Tu mensaje ha sido enviado, pronto responderemos a tu solicitud.</h4>
             </div>
             <div class="panel-footer">
                 <a href="{{ route('contacto') }}" class="btn btn-primary btn-xs">Volver</a>
               </div>
           </div>
        </div>
   </div>
</div>
<br><br><br><br>
@include('front.template.foot')

@endsection

