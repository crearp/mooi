@extends('layouts.app')
@section('content')
<script type="text/javascript">/*courtesy of onlineclock.net*/

countdown_dateFuture=new Date({{$ano}},{{$mes-01}},{{$dia}},{{$hora}},{{$min}},00);


	console.log({{$ano}});
	console.log({{$mes-01}});
	console.log({{$dia}});
	console.log({{$hora}});
	console.log({{$min}});
	console.log(countdown_dateFuture.getTime());
function countdown_UpdateCount(){

dateNow=new Date();

timediff=Math.abs(countdown_dateFuture.getTime() - dateNow.getTime());

delete dateNow;

if(timediff<0){

	document.getElementById('countdown_container').style.display="none";

	}else {

	days=0;
	hours=0;
	mins=0;
	secs=0;
	out="";
	timediff=Math.floor(timediff/1000);
	days=Math.floor(timediff/86400);
	timediff=timediff % 86400;
	hours=Math.floor(timediff/3600);
	timediff=timediff % 3600;
	mins=Math.floor(timediff/60);
	timediff=timediff % 60;
	secs=Math.floor(timediff);

		if(document.getElementById('countdown_container')){
		document.getElementById('countdown_days').innerHTML=days;
		document.getElementById('countdown_hours').innerHTML=hours;
		document.getElementById('countdown_mins').innerHTML=mins;
		document.getElementById('countdown_secs').innerHTML=secs;
		}

	setTimeout("countdown_UpdateCount()", 1000);

	}
}
window.onload=function(){
countdown_UpdateCount();
}

</script>

<div class="container">
    </br></br></br>
    	<h2 class="center">TIEMPO PARA TU CITA</h2>
    	</br></br>
    <div class="row" style="text-align: -webkit-center">
    	<div class="col-md-3"></div>
    	<div class="col-md-6">
	    	<div id="countdown_container">
	    		<div id="countdown_toppart">
					<div class="countdown_square_container">
						<div class="countdown_number" id="countdown_days"></div>
						<div class="countdown_number_name">Dias</div>
					</div>
					<div class="countdown_square_container">
						<div class="countdown_number" id="countdown_hours"></div>
						<div class="countdown_number_name">Horas</div>
					</div>
					<div class="countdown_square_container">
						<div class="countdown_number" id="countdown_mins"></div>
						<div class="countdown_number_name">Min</div>
					</div>
					<div class="countdown_square_container">
						<div class="countdown_number" id="countdown_secs"></div>
						<div class="countdown_number_name">Seg</div>
					</div>
				</div>
	    	</div>
	    	<div>
				<a href="{{ url('/historial') }}">
  					<button class="btn btn-lg btn-primary btn-block btn-color"><h4>REGRESAR</h4></button>
  				</a>
			</div>
    	</div>
    	<div class="col-md-3"></div>
	</div>
	</br></br></br>
</div>

@include('front.template.foot')

@endsection