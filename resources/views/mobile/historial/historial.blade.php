	@extends('mobile.layouts.app')
	@section('content')
	<?php $servidor=webservice(true);?>
	<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">

	<style type="text/css">
	.btnMobile {
		position: fixed;
	}

	.table>tbody>tr>td {
		 /* padding: 8px; */
  		line-height: 1;
  		vertical-align: middle;
  		border-bottom: none;
  		border-top: none;
  		padding-top: 3px;
	}
	</style>

	<div class="row" style="height: 90px;">
		<div class="col-md-12" style="position:relative;">

			<div class="col-md-12 img_nav" style="position: fixed; z-index: 1;">
				@include('mobile.front.template.menu')
				<!-- @include('mobile.layouts.menu') -->
			</div>
			@include('flash::message')
		</div>

	</div>

	<!-- Modal mensajes estandar historial-->
	  <div class="modal fade" id="modalConfirm" name="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	    <div class="modal-dialog" role="document">
	      <div class="modal-content" style="background: #ffffff;">
	        <div class="modal-header" style="padding: 0px 10px 0px 10px;border-bottom: 0px solid #e5e5e5;">
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 15px;"><span aria-hidden="true">&times;</span></button>
	          <h4 class="modal-title" id="myModalLabel"><h1 style="font-size: 18px;">¡Aviso!</h1></h4>
	        </div>
	        <hr style="border-top: 1px solid #eeba8a;">
	        <div class="modal-body" style="color: #65534f;">
	          <p id="textModal"></p>
	        </div><!--Aqui va el mensaje-->
	        <div class="modal-footer" style="border-top: 1px solid #cea791;">
	          <a><button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" style="color: #65534f; background-color: #ffffff; border-color: #cea791;">ACEPTAR</button></a>
	        </div>
	      </div>
	    </div>
	  </div>
	<!--Fin Modal -->

	<script type="text/javascript">
	window.addEventListener('load', function(){
		$('#content_historial [data-action="cancelar"], #content_historial [data-action="terminar"]').on('click', function(e){
			e.preventDefault();

			var action = $(this).data('action')
			,	cont   = $(this).data('cont')
			,	idForm = action+'_'+cont
			,	msj    = (action=="cancelar")? '¿Realmente deseas cancelar este servicio?': '¿Realmente deseas finalizar este servicio?';

			$('#textModal').html(msj);
			$('#modalConfirm').modal('show');
			$('#saveFormHistory').off();
			$('#saveFormHistory').on('click',function(e){
				e.preventDefault();
				$('#'+idForm).submit();
			})
		})

	}, false);

	</script>
	<div>
		<div>
			<div class="container-fluid">
				<div class="row center">
					<div class="col-md-12">
					@if($cantserv != 0)
					@include('flash::message')
						<div class="row center" style="  margin-top: 20px;">
							<div class="col-xs-5"><b>Servicio(s)</b>
							</div>
							<div class="col-xs-4"><b>Valor</b>
							</div>
							<div class="col-xs-3"><b>Total</b>
							</div>
							{{-- <div class="col-xs-1"><b>Ver más</b>
							</div> --}}
						</div>
						<hr style="border-color: #ecbb9d;">
						@foreach($servicios as $idForm => $sus3)
							<div class="row" style="margin-top: 15px; margin-bottom: 0px;" onclick="modalHistorial('{{ $sus3->id }}');">
								<div class="col-xs-9">
									<table class="table table-striped" style="background: white; font-size: 15px; margin-bottom: 0px;">
										{{-- <thead>
											<tr>
												<th></th>
												<th></th>
											</tr>
										</thead> --}}
										<tbody>
											@foreach($sus3->services as $np)
												<tr style="background: white;">
													<?php $valor=number_format($np->price);?>
													<td>{{ $np->name }}</td> <!-- servicio -->
													<td style="text-align: right;">${{ $valor }}</td><!-- valor -->
												</tr>
											@endforeach
											<tr style="background: white;">
											<?php
												$fecha1=explode("/", $sus3->date);
												$fecha2='20'.$fecha1[2].'-'.$fecha1[1].'-'.$fecha1[0];
												$dt = Carbon\Carbon::parse($fecha2);
												$diadelasemana=$dt->dayOfWeek;

												if($diadelasemana != 0){
													echo "<td>Domicilio Ordinario</td>";
													echo '<td style="text-align: right;">$3,000</td>';
												}else{
													echo "<td>Domicilio Dominical</td>";
													echo '<td style="text-align: right;">$5,000</td>';
												}
											?>
											</tr>
										</tbody>
									</table>

								</div>
								<div class="col-xs-3 center">
									{{ $sus3->total }}
								</div>
								{{-- <div class="col-xs-1" onclick="modalHistorial('{{ $sus3->id }}');">
								 	(+)
								</div> --}}
							</div>
							<div class="col-md-12">
								<button type="button" onclick="modalHistorial('{{ $sus3->id }}');" class="btn btn-default" style="color: #65534f; background-color: #ffffff; border-color: #cea791;">Ver más</button>
							</div>
							<hr style="border-color: #65534f;">
							<!-- modal de descripcion del servicio en historial -->
							<div class="modal fade" id="modalHistorial-{{ $sus3->id }}" name="modalHistorial-{{ $sus3->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog view-services" role="document" style="margin: 0px; width: 100%;/*padding-top: 115px;*/">
									<div class="modal-content" style="border-radius: 0px;">
										{{-- <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">SERVICIOS MOOI</h4>
										</div> --}}
										<div class="modal-body">
											<div class="container-fluid">
												<div class="row" style="font-size: 14px;">
													<div class="col-md-12">
														<div class="row">
															<div class="col-xs-2">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true"><a><</a></span>
																</button>
															</div>
															<div class="col-xs-10" style="margin-bottom: 30px;">
																<h4 style="text-transform: uppercase; color: #65534f;text-align: center;">HISTORIAL</h4>
															</div>
														</div>
														<div class="row">
															<div class="col-xs-4">
																No. Servicio
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->id }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Dirección
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->address }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Hora y Fecha
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->date }}&nbsp;{{ $sus3->hour }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Servicio y Valor
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																@foreach($sus3->services as $np)
																	<?php $valor=number_format($np->price);?>
																	{{ $np->name }}&nbsp;:&nbsp;${{ $valor }}<br> <!-- servicio --><!-- valor -->
																	@endforeach

																	<?php
																	$fecha1=explode("/", $sus3->date);
																	$fecha2='20'.$fecha1[2].'-'.$fecha1[1].'-'.$fecha1[0];
																	$dt = Carbon\Carbon::parse($fecha2);
																	$diadelasemana=$dt->dayOfWeek;

																	if($diadelasemana != 0){
																		echo $valor_domicilio='Domicilio Ordinario $3,000';
																	}else{
																		echo $valor_domicilio='Domicilio Dominical $5,000';
																	}

																	?>
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Descuento
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->discount }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Total
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->total }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Profesional
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->professional }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Método de pago
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->payment }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Estado
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->state }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Calificación
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->score }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Comentarios
															</div>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																{{ $sus3->comments }}
															</div>
														</div>
														<hr style="border-color: #65534f;">
														<div class="row">
															<div class="col-xs-4">
																Acción
															</div>
															<?php
																//condicional 1 TERMINAR SERVICIO-> que la fecha y hora de hoy sean mayores a la agendada
																$timeActual=date("d/m/y H:i");

																	//condicional 2 TERMINAR SERVICIO-> que la fecha y hora de lo agendado + 2 sean menores a la hora actual
																$fecha1=explode("/", $sus3->date);
																$fecha2=$fecha1[2].'-'.$fecha1[1].'-'.$fecha1[0];
																$time=$fecha2.' '.$sus3->hour;
																$horaFutura=Carbon\Carbon::parse($time)->addHours(2);
																$horaFutura=$horaFutura->format("d/m/y H:i");

																	//condiocional 3 TERMINAR SERVICIO-> que esta operacion solo se ejecute si la fecha de agendamiento es igual a la de hoy
																$today=date("d/m/y");
															?>
															<div class="col-xs-8" style="border-left: 1px solid #eeba8a; padding-left: 5px;">
																<?php if($sus3->state ==  'Reservado'){ ?>
																{{ Form::open(array('id' => 'cancelar_'.$idForm, 'url' => 'cancelar', 'method' => 'post')) }}
																<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
																<input type="submit" style="font-size: 14px;" value="Cancelar" data-action="cancelar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
																{{ Form::close() }}
																<?php }

																else if($sus3->state ==  'Reservado sin asignacion'){ ?>
																{{ Form::open(array('id' => 'cancelar_'.$idForm, 'url' => 'cancelar', 'method' => 'post')) }}
																<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
																<input type="submit" style="font-size: 14px;" value="Cancelar" data-action="cancelar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
																{{ Form::close() }}
																<?php }

																else if($sus3->state == 'Finalizado' && ($sus3->score) == NULL ){ ?>
																{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
																<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
																<input type="submit" style="font-size: 14px;" value="Calificar" class="btn btn-lg btn-color submit" />
																{{ Form::close() }}
																<?php }

																else if($sus3->state ==  'Cancelado por cliente'){ ?>
																{{ Form::open(array('url' => 'reprogramar-mobile', 'method' => 'post')) }}
																<input type="submit" style="font-size: 14px;" value="Reprogramar" class="btn btn-lg btn-color submit"/>
																{{ Form::close() }}
																<?php }

																else if($sus3->state ==  'Cancelado por professional'){ ?>
																{{ Form::open(array('url' => 'reprogramar-mobile', 'method' => 'post')) }}
																<input type="submit" style="font-size: 14px;" value="Reprogramar" class="btn btn-lg btn-color submit"/>
																{{ Form::close() }}
																<?php } ?>
																<br>
											<!-- </td>
											<td> -->
																<?php /*echo $sus3->date.' '.$sus3->hour; echo " menor que ";  echo  $timeActual; echo "<br>";
																	echo $sus3->date.' '.$sus3->hour; echo " menor que ";  echo  $horaFutura; echo "<br>";
																	echo $sus3->date; echo " igual que ";  echo  $today; echo "<br>";*/
																//if($sus3->state ==  'Reservado' && $sus3->date.' '.$sus3->hour < $timeActual && $sus3->date.' '.$sus3->hour < $horaFutura && $sus3->date == $today){
																	?>
																	{{ Form::open(array('id' => 'terminar_'.$idForm, 'url' => 'terminar', 'method' => 'post')) }}
																	<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
																	{{-- <input type="submit" style="font-size: 14px;" value="Terminar" data-action="terminar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/> --}}
																	{{ Form::close() }}
																<?php //}

																//else if($sus3->state ==  'Reservado sin asignacion' && $sus3->date.' '.$sus3->hour < $timeActual && $sus3->date.' '.$sus3->hour < $horaFutura && $sus3->date == $today){
																?>
																{{ Form::open(array('id' => 'terminar_'.$idForm, 'url' => 'terminar', 'method' => 'post')) }}
																<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
																{{-- <input type="submit" style="font-size: 14px;" value="Terminar" data-action="terminar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/> --}}
																{{ Form::close() }}
																<?php //}

																if($sus3->state ==  'Cancelado por cliente' && ($sus3->score) == NULL ){ ?>
																{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
																<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
																<input type="submit" style="font-size: 14px;" value="Calificar" class="btn btn-lg btn-color submit"/>
																{{ Form::close() }}
																<?php }

																else if($sus3->state ==  'Cancelado por professional' && ($sus3->score) == NULL ){ ?>
																{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
																<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
																<input type="submit" style="font-size: 14px;" value="Calificar" class="btn btn-lg btn-color submit"/>
																{{ Form::close() }}
																<?php } ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div><!--Aqui va el mensaje-->
									</div>
								</div>
							</div>
							<!-- fin modal de descripcion del servicio en historial -->
						@endforeach
					</div>
				</div>
			</div>
			@else
				@include('flash::message')
			@endif
	</div>
</div><!-- /.container -->
@endsection

<?php

	if(\Session::get('msjHistorial') !== null && \Session::get('msjHistorial') != ''){
		$msjTextHistorial = \Session::get('msjTextHistorial');
		$msjHistorial=\Session::get('msjHistorial');
	}else{
		$msjTextHistorial = 'No info';
		$msjHistorial = false;
	}

?>

<script type="text/javascript">

	window.onload = function() {
  		var msjHistorial = '{{ $msjHistorial }}';
		if(msjHistorial == 1){
				$('#modalConfirm')
					.modal('show')
					.on('shown.bs.modal', function () { document.getElementById('textModal').innerHTML = '{{$msjTextHistorial}}'; })
		}
	};



	function modalHistorial(id){
		$('#modalHistorial-'+id)
			.modal('show')
			.on('shown.bs.modal', function () { })
	}


</script>