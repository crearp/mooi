	@extends('mobile.layouts.app')
	@section('content')
	<?php $servidor=webservice(true);?>
	<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">

	<style type="text/css">
	.btnMobile {
		position: fixed;
	}
	</style>

	<div class="row" style="height: 90px;">
		<div class="col-md-12" style="position:relative;">

			<div class="col-md-12 img_nav" style="position: fixed; z-index: 1;">
				@include('mobile.front.template.menu')
				<!-- @include('mobile.layouts.menu') -->
			</div>
			@include('flash::message')
		</div>

	</div>

    <div class="row">
	    <div class="col-md-12 center">
		    <div class="col-xs-1"></div>
			    <div class="col-xs-10">
				    </br>
				    <h2 class="center">CALIFICACIÓN DEL SERVICIO</h2>
				    </br></br>
				    <p>Para nosotros es importante que califiques el servicio y todo lo referente a su ejecución con esta pequeña encuesta.</p>
				    {{ Form::open(array('url' => 'calificarfinal', 'method' => 'post'))}}
				    </br>
				    <p>Califica de 1 a 5 el servicio, siendo 1 el más bajo y 5 el más alto.</p>
				    </br></br>

			      	<p align=center class="clasificacion">
						  <input id="radio1" type="radio" name="estrellas" value="5"><!--
						  --><label for="radio1">★</label><!--
						  --><input id="radio2" type="radio" name="estrellas" value="4"><!--
						  --><label for="radio2">★</label><!--
						  --><input id="radio3" type="radio" name="estrellas" value="3"><!--
						  --><label for="radio3">★</label><!--
						  --><input id="radio4" type="radio" name="estrellas" value="2"><!--
						  --><label for="radio4">★</label><!--
						  --><input id="radio5" type="radio" name="estrellas" value="1"><!--
						  --><label for="radio5">★</label>
					</p>

			      	</br></br>
			     	<p>Déjanos tu comentario sobre todo lo referente al servicio.</p>
			        <textarea class="form-control" id="comentario" name="comentario" rows="6" cols="50"></textarea>
			        <input type="hidden" name="id" id="id" value="{{$id}}">
			     	</br></br></br>
			      	<button class="btn btn-lg btn-primary btn-block btn-color" type="submit"><h4>CALIFICAR</h4></button>
			      	{{ Form::close() }}
			      </br></br></br></br>
		      </div>
	       <div class="col-xs-1"></div>
     	</div>
   	</div>

@endsection

