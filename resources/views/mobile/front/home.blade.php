@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<style type="text/css">
	.back {
	   display: table;
	}

	.back [class*="col-"] {
	  display: table-cell;
	  float: none;
	  vertical-align: middle;
	}

	html {
	   width: 100%;
	   height: 100%;
	}
</style>

<div class="row" style="height: 90px;">
	<div class="col-md-12" style="position:relative;">

		<div class="col-md-12 img_nav" style="position: fixed; z-index: 1;">
			@include('mobile.front.template.menu')
			@include('mobile.layouts.menu')
		</div>
		@include('flash::message')
	</div>

</div>
<div class="row">
	<div class="col-md-12">
		{!! Form::open(['route' => 'agenda_Mobile', 'method' => 'post', 'class' => 'form-signing', 'id' => 'formblanquito']) !!}
		<form action="agenda_Mobile" method="post" id="formblanquito" style="padding-top: 10px;">

		<?php
			foreach ($menustyles->styles as $value) {
				//if($value->category_name != 'Combos'){
					//$stylesValue1 = explode("[", $value->category_name);
					$stylesValue2 = explode("-", $value->category_name);
					$cantStyles[]=$stylesValue2[0];
				//}
			}
			$cantStyles=array_unique($cantStyles);
			$cantStyles=array_values($cantStyles);
			$i=0;
			$c=0;
			$m=1;
		?>
		@foreach($cantStyles as $cant)
			<input type="hidden" name="show-{{ $c }}-{{ $i }}" id="show-{{ $c }}-{{ $i }}" value="false">
			<div class="col-md-12" style="margin-top: 40px; display: none" id="categoria-{{ $c }}-{{ $i }}">
				@foreach ($menustyles->styles as $styles)
					<?php
						$category=strpos($styles->category_name, $cant);
					?>
					@if($category !== false)
						<div class="col-md-12" style="height: 138px;background-color:#cea791;border-top: solid;border-bottom: solid;border-color: white;border-width: 1px;" onclick="modalservices('{{ $m }}');">
							<div class="col-xs-5" style="background-color: #cea791;height: 100%;">
							<?php
								if($styles->category_name == 'mujeres-Peinados'){
									$imgCat = url('img/Peinados_app.png');
								}elseif ($styles->category_name == 'mujeres-Maquillaje') {
									$imgCat = url('img/Maquillaje_app.png');
								}elseif ($styles->category_name == 'Hombres-manicure y pedicure') {
									$imgCat = url('img/Hombres_app.png');
								}elseif ($styles->category_name == 'mujeres-Depilacion') {
									$imgCat = url('img/Depilacion_app.png');
								}elseif ($styles->category_name == 'mujeres-Masaje') {
									$imgCat = url('img/Masaje_app.png');
								}elseif ($styles->category_name == 'Hombres-Masaje') {
									$imgCat = url('img/Masaje_app.png');
								}elseif ($styles->category_name == 'mujeres-manicure y pedicure') {
									$imgCat = url('img/Manicure_Pedicure_app.png');
								}elseif ($styles->category_name == 'Combos') {
									$imgCat = url('img/combos_app.png');
								}else{
									$imgCat = url('img/Maquillaje_app.png');
								}
							?>
							<img src="{{ $imgCat }}" class="img-responsive" alt="" height="150px" width="150px" style="max-width: 130%; height: 100%; width: 115%;">
							</div>
							<div class="col-xs-7" style="height: 100%;">
							<?php $servicios = str_replace($cant."-", "", $styles->category_name); ?>
								<h4 style="font-size:18px; color:#65534f; text-align:left; padding:20% 15%; text-transform:uppercase; font-weight: bold;">{{ $servicios }}</h4>
							</div>
						</div>
						<?php $m++; $serviciosOrden[$styles->category_name] = [$m]; ?>
					@endif
				@endforeach
			</div>
			<?php $i++; $c++; //var_dump($serviciosOrden);
			?>
		@endforeach

		<?php
			$contStyle = 0;
			$contadorSelects=0;
		?>
		@foreach ($arrayStyle as $category => $arrOption)
				<?php
					if (array_key_exists($category, $serviciosOrden)) {
    					$contStyle=$serviciosOrden[$category][0]-1;
					}
				?>
				<div class="modal fade" id="modalServicios-{{$contStyle}}" name="modalServicios-{{$contStyle}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog view-services" role="document" style="margin: 0px; width: 100%;/*padding-top: 115px;*/">
						<div class="modal-content" style="border-radius: 0px;">
							{{-- <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">SERVICIOS MOOI</h4>
							</div> --}}
							<div class="modal-body">
								<div class="container-fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-12">
													<div class="row back" style="width: 100%;">
														</br></br>
														<div class="col-xs-2 col-md-2">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true"><h4 style="font-size: 30px;"><a><</a></h4></span>
															</button>
														</div>
														<div class="col-xs-10 col-md-10">
															<h4 style="text-transform: uppercase; color: #65534f;text-align: center;" for="servicio-{{$contStyle}}">{{$category}}</h4>
														</div>
													</div>
													{{-- Aqui estaba el foreach principal--}}
													<br>
													<?php
														$contador=0;
													?>
													<div class="row">
														<div class="col-xs-12 col-sm-12 col-md-12">
															<div class="col-xs-6 col-sm-6 col-md-6" style="cursor: pointer;">
																<h4 class="size_popup Color_mooi_ppl">Servicio</h4>
															</div>
															<div class="col-xs-3 col-sm-6 col-md-6 Color_mooi_ppl" style="cursor: pointer;">
																<h4 class="size_popup " style="color:white">Cant</h4>
															</div>
															<div class="col-xs-3 col-sm-6 col-md-6" style="">
																<h4 class="size_popup Color_mooi_ppl">Cantidad</h4>
															</div>
														</div>
													</div>
													<br>
													@foreach($arrOption as $option)
															<?php
																$contador=$contador+1;
															?>
															@foreach ($option->specify_styles as $element)
																<?php

																	if($element->specify_style_photo != 'null'){
																		$foto = $element->specify_style_photo;
																		$foto = str_replace('./','/',$foto);
																	}else{
																		 $foto=$foto;
																	}
																	$nombre = $option->style_name;
																	$descripcion = $element->specify_style_description;

																?>
															@endforeach
													<div class="row">
														<div class="col-xs-6 col-sm-6 col-md-6" style="cursor: pointer;">
															<h4 class="size_popup" style="margin:0;">{{ $option->style_name }}</h4>
															<a onclick="showImage('{{ $foto }}','{{ $option->style_name }}')" id="detailPhoto{{ $option->style_name }}">(+) Detalle</a>
														</div>

														<div class="col-xs-3 col-sm-6 col-md-3" style="cursor: pointer; text-align: center;">
															<a>${{ number_format($option->style_ammount) }}
															</a>
															<br>
														</div>

														<div id="divAgenda{{ $contadorSelects }}" class="col-xs-3 col-sm-6 col-md-3" style="text-align: center;">
															{{-- <a onclick="cambiar('{{ $foto }}',{{$contStyle}},'{{ $nombre }}','{{ $descripcion }}');"> --}}
																<select class="form-control Color_mooi_ppl" data-categoria{{ $contadorSelects }}="{{$contStyle}}-{{ $contador }}" name="cantidad-{{$contStyle}}-{{ $contador }}" id="cantidad-{{$contStyle}}-{{ $contador }}" style="border-radius: 0px;box-shadow: none;border-left: none;border-right: none;border-top: none">
																	<option value="0">0</option>
																	<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/1">1</option>
																	<!-- <option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/2">2</option>
																	<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/3">3</option>
																	<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/4">4</option>
																	<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/5">5</option> -->
																</select>
															{{-- </a> --}}
														</div>
														<?php
															$contadorSelects=$contadorSelects+1;
														?>

													</div>
													<br>
													<input type="hidden" name="showImage-{{ $foto }}" id="showImage-{{ $foto }}" value="false">
													<div class="row" id="{{ $foto }}" style="background-color: #65534f;padding: 6px; display: none; margin-bottom: 15px;">
														<div class="col-md-12" style="position:relative;">
															<div class="col-xs-6">
																<div class="col-xs-12" style="bottom: 0; padding-right: 5px; width: 100%">
																	<h4 id="titleSel-{{$contStyle}}" class="Color_mooi_ppl" style="text-transform: uppercase;letter-spacing: 1px;">{{$nombre}}</h4>
																	<p id="descSel-{{$contStyle}}" style="color:white; font-size: 14px;">{{ $descripcion }}</p>
																</div>
															</div>
															<div class="col-xs-6">
																<img id="imgSel-{{$contStyle}}" src="{{ $servidor }}/media{{ $foto }}" style="width:100%;"/>
															</div>
														</div>
													</div>
													@endforeach
													<br><br>
													{{-- Aqui termina el foreach principal --}}
												</div>
												<div class="col-md-12 center">
													<a><button class="btn btn-lg btn-block btn-color" type="button" onclick="modalPreAgenda();"><h4>Agregar</h4></button>
												</div>
												<br><br>
											</div>
										</div>
									</div>
								</div>
							</div><!--Aqui va el mensaje-->
						</div>
					</div>
				</div>
			@endforeach
			@endsection
		{!! Form::close() !!}
		</form>
	</div>
</div>

<input type="hidden" name="modalActualOpen" id="modalActualOpen" value="">

<!-- Modal ciudad -->
	<div class="modal fade" id="modalServicios-400" name="modalServicios-400" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document" style="margin: 5px;padding-top: 25%;">
			<div class="modal-content" style="color: white; background-color: #62514d;">
				<!-- <div class="modal-header">
					<h4 class="modal-title" id="myModalLabel" style="color: white;"">Selecciona tu ciudad</h4>
				</div> -->
				<div class="modal-body advertencia">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close" class="Color_mooi_ppl"><span aria-hidden="true">&times;</span></button> -->
				<h4 class="modal-title" id="myModalLabel" style="color: white;text-align: center;">ELIJE TU CIUDAD</h4>
					<hr class="Color_mooi_ppl" style="border-top: 1px solid">
					<div class="row">
						<div class="col-md-4 center" onclick="modalservicesClose(400,1);">
							<div class="col-xs-5"><img src="{{url('img/Bogota_.png')}}" style="height: 15%;"/></div>
							<div class="col-xs-7"><br><br>Bogotá</div>
							{{-- <div class="col-xs-2"><br><br><input type="radio" name="ciudadUser" id="ciudadUser" value="1"  onclick="modalservicesClose(400);" style="display: block;"></div> --}}
						</div>
					</div>
					<hr class="Color_mooi_ppl" style="border-top: 1px solid">
					<div class="row">
						<div class="col-md-4 center">
							<div class="col-xs-5"><img src="{{url('img/Cartagena_.png')}}" style="height: 15%;"/></div>
							<div class="col-xs-7">
								<br>Cartagena
								<br>Próximamente
							</div>
							<!--div class="col-xs-2"><br><br><input type="radio" name="ciudadUser" id="ciudadUser" value="2"  onclick="modalservicesClose(400);" style="display: block;"></div-->
						</div>
					</div>
					<hr class="Color_mooi_ppl" style="border-top: 1px solid">
					<div class="row">
						<div class="col-md-4 center">
							<div class="col-xs-5"><img src="{{url('img/Cali_.png')}}" style="height: 15%;"/></div>
							<div class="col-xs-7">
								<br>Cali
								<br>Próximamente
							</div>
							<!--div class="col-xs-2"><br><br><input type="radio" name="ciudadUser" id="ciudadUser" value="3"  onclick="modalservicesClose(400);" style="display: block;"></div-->
						</div>
					</div>
				</div><!--Aqui va el mensaje-->
				{{--<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Seleccionar</button>
				</div>--}}
			</div>
		</div>
	</div>
<!--Fin Modal ciudad-->

<!-- Modal finalizacion agendamiento-->
<div class="modal fade" id="modalAlertAgendamiento" name="modalAlertAgendamiento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog view-services" role="document" style="margin: 0px; width: 100%;/*padding-top: 115px;*/">
		<div class="modal-content" style="border-radius: 0px; height: 100%;">
			<div class="modal-header" style="border-bottom: 0px solid #e5e5e5; margin-top: 20px;">
			  {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 15px;"><span aria-hidden="true">&times;</span></button> --}}
			  <h4 style="text-transform: uppercase; color: #65534f;text-align: center;">Servicios para Agendar</h4>
			</div>
			{{-- <hr class="Color_mooi_ppl" style="border-top: 1px solid"> --}}
			<div class="modal-body" style="color: #65534f;">
	
				<div class="col-md-12">
					<div class="row">
						<div class="col-xs-6 center">
							<h4 class="size_popup Color_mooi_ppl" style=" text-align: -webkit-left;">Servicio(s)</h4>
						</div>
						<div class="col-xs-3 center">
							<h4 class="size_popup Color_mooi_ppl"></h4>
						</div>
						<div class="col-xs-3 center">
							<h4 class="size_popup Color_mooi_ppl" style="text-align: -webkit-left;">Valor</h4>
						</div>
					</div>

					<div class="row">	
						<div class="col-xs-6 center">
							<h4 class="size_popup" style="text-align: -webkit-left;" id="textModalAgendamientoServicio"></h4>
						</div>
						<div class="col-xs-3 center">
							<p id="textModalAgendamientoCantidad"></p>
						</div>
						<div class="col-xs-3 center">
							<p id="textModalAgendamientoValor"></p>
						</div>
					</div>

					<hr>

					<div class="row">
						<div class="col-xs-6 center">
							<h4 class="size_popup" style="margin-top: 4px;">Total</h4>
						</div>
						<div class="col-xs-3 center">
							<p id="textModalAgendamientoCantidadTotal"></p>
						</div>
						<div class="col-xs-3 center">
							<p id="textModalAgendamientoValorTotal"></p>
						</div>
					</div>
				</div>
			</div>

			</div><!--Aqui va el mensaje-->
			<div class="modal-footer" style="border-top: none; position: absolute; bottom: 10px;">
				<div class="col-md-12">
					<div class="col-xs-6" style="padding-left: 10;">
			  			<a style="color: white;">
			  				<button style="font-size: 12px; text-transform: uppercase;" type="button" class="btn btn-lg btn-color" data-dismiss="modal" aria-label="Close">
			  					<h4>Regresar</h4>
			  				</button>
			  			</a>
					</div>
					<div class="col-xs-6" style="padding-left: 10;">
			  			<a onclick="$('#formblanquito').submit();" style="color: white;">
			  				<button style="font-size: 12px; float: right; text-transform: uppercase;" type="button" class="btn btn-lg btn-color">
			  				<h4>Check Out</h4>
			  				</button>
			  			</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin Modal -->

<!-- Modal finalizacion agendamiento sin seleccion de servicios-->
<div class="modal fade" id="modalAlertAgendamientoNulo" name="modalAlertAgendamientoNulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog view-services" role="document" style="margin: 0px; width: 100%;/*padding-top: 115px;*/">
		<div class="modal-content" style="border-radius: 0px; height: 100%;">
			<div class="modal-header" style="border-bottom: 0px solid #e5e5e5;">
			  {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 15px;"><span aria-hidden="true">&times;</span></button> --}}
			  <h4 style="text-transform: uppercase; color: #65534f;text-align: center;">Servicios para Agendar</h4>
			</div>
			{{-- <hr class="Color_mooi_ppl" style="border-top: 1px solid"> --}}
			<div class="modal-body" style="color: #65534f;">
				<p>No has seleccionado ningun servicio MOOI,</p>
				<p>Para continuar, selecciona alguno de los ubicados en las categorias.</p>

			</div><!--Aqui va el mensaje-->
			<div class="modal-footer" style="border-top: none; position: absolute; bottom: 10px;">
				<div class="col-md-12">
					<div class="col-xs-12" style="padding-left: 10;">
			  			<a style="color: white;"><button style="font-size: 12px; text-transform: uppercase;" type="button" class="btn btn-lg btn-block btn-color" data-dismiss="modal" aria-label="Close">< Agendar</button></a>
					</div>
					{{-- <div class="col-xs-6" style="padding-left: 10;">
			  			<a onclick="$('#formblanquito').submit();" style="color: white;"><button style="font-size: 12px; float: right; text-transform: uppercase;" type="button" class="btn btn-lg btn-block btn-color">Check Out ></button></a>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin Modal -->

<!-- Modal finalizacion agendamiento-->
  <div class="modal fade" id="modalAlertAgendamientoFinalizado" name="modalAlertAgendamientoFinalizado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="background: #ffffff;">
        <div class="modal-header" style="padding: 0px 10px 0px 10px;border-bottom: 0px solid #e5e5e5;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 15px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"><h1 style="font-size: 18px;">¡Aviso!</h1></h4>
        </div>
        <hr class="Color_mooi_ppl" style="border-top: 1px solid">
        <div class="modal-body" style="color: #65534f;">
          <p id="textModal">Tu pedido aún no está confirmado, debes recibir un correo adicional con la confirmación y la información de tu estilista asignada.</p>
        </div><!--Aqui va el mensaje-->
        <div class="modal-footer" style="border-top: 1px solid #cea791;">
          <a href="{{ url('/historial-mobile') }}"><button type="button" class="btn btn-default" style="color: #65534f; background-color: #ffffff; border-color: #cea791;">Historial</button></a>
          <a href="{{ url('/cancelacion') }}"><button type="button" class="btn btn-default" style="color: #65534f; background-color: #ffffff; border-color: #cea791;">Políticas de Cancelación</button></a>
        </div>
      </div>
    </div>
  </div>
<!--Fin Modal -->

	<script type="text/javascript">
		window.onload = function() {
			var alto = screen.height;
			var agendamientoMobile = {{$agendamientoMobile}}
			//alert(alto);
			document.getElementById("navbar").style.height = alto+"px";

			display(0,0,'false');

			if(sessionStorage.getItem('ciudad') == null){
				modalservices(400);
			}

			if(agendamientoMobile == 1){
				modalAgendamiento();
			}

		};

		function display(categoria,num,bool){

			if(bool == 'true'){
				resetShow();
			}

			var show = document.getElementById("show-"+categoria+"-"+num).value;
			if(show == "false"){
				document.getElementById("categoria-"+categoria+"-"+num).style.display = "block";
				document.getElementById("show-"+categoria+"-"+num).value = "true";

				document.getElementById("div-"+categoria+"-"+num+"-categorias").style.borderTop = "solid 1px";
				document.getElementById("div-"+categoria+"-"+num+"-categorias").style.borderTopColor = "#ccae98";
				document.getElementById("div-"+categoria+"-"+num+"-categorias").style.fontWeight = "bold";
			}
			if(show == "true"){
				document.getElementById("categoria-"+categoria+"-"+num).style.display = "none";
				document.getElementById("show-"+categoria+"-"+num).value = "false";

				document.getElementById("div-"+categoria+"-"+num+"-categorias").style.borderTop = "none";
				document.getElementById("div-"+categoria+"-"+num+"-categorias").style.borderTopColor = "transparent";
				document.getElementById("div-"+categoria+"-"+num+"-categorias").style.fontWeight = "normal";
			}
		}

		function resetShow (){
			for (var i = 0; i <= cantCat; i++) {
				console.log(i);
				document.getElementById("categoria-"+i+"-"+i).style.display = "none";
				document.getElementById("show-"+i+"-"+i).value = "false";

				document.getElementById("div-"+i+"-"+i+"-categorias").style.borderTop = "none";
				document.getElementById("div-"+i+"-"+i+"-categorias").style.borderTopColor = "transparent";
				document.getElementById("div-"+i+"-"+i+"-categorias").style.fontWeight = "normal";
			}
		}

		//procesamiento para popUpPreagenda

		function modalPreAgenda(){
			var servicio = document.getElementById("textModalAgendamientoServicio");
			var valor = document.getElementById("textModalAgendamientoValor");
			var cantidad = document.getElementById("textModalAgendamientoCantidad");

			servicio.innerHTML = "";
			valor.innerHTML = "";
			cantidad.innerHTML = "";

			var contadorSelects = '{{ $contadorSelects }}';
			var data = [];
			var cantSeparator = 0;

			for (i = 0; i <= contadorSelects; i++) {

				categoria = $("#divAgenda"+i+" > select").data('categoria'+i);

			    if(document.getElementById("cantidad-"+categoria) != null){
			    	select = document.getElementById("cantidad-"+categoria).value;

			    	if(select != '0'){
			    		data[i] = select;
			    		cantSeparator++;
			    	}
			    }
			}

			data = startFromZero(data);
			if(data != ""){
				// //Generacion de informacion
				var valorTotal = 0;
				var cantidadTotal = 0;

				for (x = 0; x < cantSeparator; x++) {
					var response = data[x].split("/");

					servicio.innerHTML += "-"+response[2]+"<br>";
					valor.innerHTML += "$"+number_format(response[1],0)+"<br>";
					cantidad.innerHTML += response[3]+"<br>";

					valorTotal = valorTotal*1+response[1]*1;
					cantidadTotal += response[3]*1;
				}

				document.getElementById("textModalAgendamientoValorTotal").innerHTML = "$"+number_format(valorTotal,0);
				document.getElementById("textModalAgendamientoCantidadTotal").innerHTML = cantidadTotal;

				//Fin

				modalServicioGlobalActual = document.getElementById("modalActualOpen").value;

				$('#modalServicios-'+modalServicioGlobalActual)
					.modal('hide')
					.on('shown.bs.modal', function () { })

				//document.getElementById("textModalAgendamientoServicio").innerHTML = data;

				$('#modalAlertAgendamiento')
					.modal('show')
					.on('shown.bs.modal', function () { })

			}else{
				modalServicioGlobalActual = document.getElementById("modalActualOpen").value;

				$('#modalServicios-'+modalServicioGlobalActual)
					.modal('hide')
					.on('shown.bs.modal', function () { })

				$('#modalAlertAgendamientoNulo')
					.modal('show')
					.on('shown.bs.modal', function () { })
			}
		}

		function startFromZero(arr) {
		    var newArr = [];
		    var count = 0;

		    for (var i in arr) {
		        newArr[count++] = arr[i];
		    }

		    return newArr;
		}

		function number_format(amount, decimals) {

		    amount += ''; // por si pasan un numero en vez de un string
		    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

		    decimals = decimals || 0; // por si la variable no fue fue pasada

		    // si no es un numero o es igual a cero retorno el mismo cero
		    if (isNaN(amount) || amount === 0)
		        return parseFloat(0).toFixed(decimals);

		    // si es mayor o menor que cero retorno el valor formateado como numero
		    amount = '' + amount.toFixed(decimals);

		    var amount_parts = amount.split('.'),
		        regexp = /(\d+)(\d{3})/;

		    while (regexp.test(amount_parts[0]))
		        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

		    return amount_parts.join('.');
		}

		//fin procesamiento para popUpPreagenda

		function modalservices(id){
			document.getElementById("modalActualOpen").value = id;

			$('#modalServicios-'+id)
				.modal('show')
				.on('shown.bs.modal', function () { })
		}

		function modalAgendamiento(){
			$('#modalAlertAgendamientoFinalizado')
				.modal('show')
				.on('shown.bs.modal', function () { })
		}

		function modalservicesClose(id,ciudad){
			//var ciudad = document.getElementById("ciudadUser").value;
			sessionStorage.setItem('ciudad', ciudad);

			$('#modalServicios-'+id)
				.modal('hide')
				.on('shown.bs.modal', function () { })
		}

		function showImage(foto,id){

			var showImg = document.getElementById("showImage-"+foto).value;

			console.log(showImg);
			if(showImg == "false"){
				document.getElementById("detailPhoto"+id).innerHTML = "(-) Detalle";
				document.getElementById(foto).style.display = "block";
				document.getElementById("showImage-"+foto).value = "true";
			}
			if(showImg == "true"){
				document.getElementById("detailPhoto"+id).innerHTML = "(+) Detalle";
				document.getElementById(foto).style.display = "none";
				document.getElementById("showImage-"+foto).value = "false";
			}
		}

	</script>
