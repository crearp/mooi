@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<style type="text/css">
  .btnMobile {
    position: fixed;
  }
</style>

<div class="row" style="height: 90px;">
  <div class="col-md-12" style="position:relative;">

    <div class="col-md-12 img_nav" style="position: fixed; z-index: 1;">
      @include('mobile.front.template.menu')
      {{-- @include('mobile.layouts.menu') --}}
    </div>
    @include('flash::message')
  </div>

</div>

  <div class="container">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 center">
        </br></br></br>
        <h1>POLÍTICAS DE CANCELACIÓN</h1>
        </br>
        <hr>
          <ul style="text-align: justify">
          <p>Si necesitas cambiar tu cita o cancelarla, por favor contacta al equipo MOOI cuanto antes a info@mooibelleza.com con tu nombre, la hora original de tu cita y el cambio que necesitas, pero ten en cuenta las siguientes políticas de cancelación:
          <br><br>

          <ol tyoe="1">

          <li><p>Si cancelas tu cita entre 4 horas y una hora de anticipación, te será cargado el 50% del valor del servicio.
               Citas canceladas con una hora de anticipación o no cancelación del servicio serán cargadas en su totalidad.
               </p>
          </li>
          <br><br>
          <li><p>Para citas agendadas entre las 8:00 am o antes, necesitamos que notifiques tu cambio a más tardar a las 8:00 pm del día anterior, de lo contrario será cargado el 50% del valor del servicio.
               </p>
          </li>
          <br><br>
          <li><p>Todos los servicios reservados con bonos de regalo o cupones se perderán por cancelaciones.
               </p>
          </li>

          </ol>

          </ul>

      </div>
      <div class="col-sm-1"></div>
    </div><!-- /.container -->

  </br></br>
@endsection