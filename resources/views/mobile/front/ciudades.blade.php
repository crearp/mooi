@extends('mobile.layouts.app')
@section('content')
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<style type="text/css" media="screen">
	
	/*slider de ciudades*/

	#loader {
	  position: absolute;
	  left: 50%;
	  top: 50%;
	  z-index: 1;
	  width: 40px;
	  height: 40px;
	  margin: -35px 0 0 -35px;
	  border: 7px solid #f3f3f3;
	  border-radius: 50%;
	  border-top: 7px solid #eeba8a;
	  width: 50px;
	  height: 50px;
	  -webkit-animation: spin 2s linear infinite;
	  animation: spin 2s linear infinite;
	}

	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}

	/* Add animation to "page content" */
	.animate-bottom {
	  position: relative;
	  -webkit-animation-name: animatebottom;
	  -webkit-animation-duration: 1s;
	  animation-name: animatebottom;
	  animation-duration: 1s
	}

	@-webkit-keyframes animatebottom {
	  from { bottom:-100px; opacity:0 }
	  to { bottom:0px; opacity:1 }
	}

	@keyframes animatebottom {
	  from{ bottom:-100px; opacity:0 }
	  to{ bottom:0; opacity:1 }
	}

	#accordion-wrapper {
	  display: none;
	  text-align: center;
	}
	/*estilo nuevo slider*/

	* {
    box-sizing: border-box;
}

body {
    text-align: center;
}

input[name="slider-select-element"] {
    display: none;
}

#slider-arrows {
    margin: -72% auto 0 auto;
    width: 80%;
}

#slider-box {
    height: 100%;
    width: 300%;
}

#slider-container {
    height: 20%;
    margin: 0 auto;
    overflow: hidden;
    text-align: left;
    width: 80%;
}

.element-blue,
.element-green,
.element-red {
    min-height: 200px;
    max-height: 100%;
    width: 100%;
}

.element-blue {
    background-image: url({{ url('img/Cali_.png') }});
    background-repeat:no-repeat;
    background-position: center center;
}

.element-green {
    background-image: url({{ url('img/Cartagena_.png') }});
    background-repeat:no-repeat;
    background-position: center center;
}

.element-red {
    background-image: url({{ url('img/Bogota_.png') }});
    background-repeat:no-repeat;
    background-position: center center;
}

.slider-element {
    float: left;
    width: 33.333%;
}

#element1:checked ~ #slider-arrows label:nth-child(2),
#element2:checked ~ #slider-arrows label:nth-child(3),
#element3:checked ~ #slider-arrows label:nth-child(1) {
    display: block;
    float: right;
}

#element1:checked ~ #slider-arrows label:nth-child(3),
#element2:checked ~ #slider-arrows label:nth-child(1),
#element3:checked ~ #slider-arrows label:nth-child(2) {
    display: block;
    float: left;
}

#element1:checked ~ #slider-arrows label:nth-child(2):before,
#element2:checked ~ #slider-arrows label:nth-child(3):before,
#element3:checked ~ #slider-arrows label:nth-child(1):before {
    color: black;
    content: "\f054";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: antialiased;
}

#element1:checked ~ #slider-arrows label:nth-child(3):before,
#element2:checked ~ #slider-arrows label:nth-child(1):before,
#element3:checked ~ #slider-arrows label:nth-child(2):before {
    color: black;
    content: "\f053";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: antialiased;
}

#element1:checked ~ #slider-container #slider-box {
    margin-left: 0;
}

#element2:checked ~ #slider-container #slider-box {
    margin-left: -100%;
}

#element3:checked ~ #slider-container #slider-box {
    margin-left: -200%;
}

#slider-arrows label {
    color: transparent;
    cursor: pointer;
    display: none;
    font-size: 3rem;
    height: 20px;
    width: 30px;
}

#slider-box {
    -webkit-transition: all 0.75s ease;
    -moz-transition: all 0.75s ease;
    -ms-transition: all 0.75s ease;
    -o-transition: all 0.75s ease;
    transition: all 0.75s ease;
}
</style>

<link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
{{-- RESPONSIVE SIN RACORDEON --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/flexslider.css') }}?<?= rand(0, 9000); ?>" />

<div class="row" style="height: 90px;">
	<div style="position:relative;">
		<div class="img_nav">
		@include('mobile.front.template.menu')
		<!-- @include('mobile.layouts.menu') -->
		</div>
		@include('flash::message')
	</div>
	<div class="container"></div><!-- /.container -->
</div>
<div class="col-md-12" style="padding: 50px 0px 0px 0px;">
		<h3>DÓNDE ESTAMOS</h3>
	</br>
    <div id="page">
        <section>
            <input type="radio" name="slider-select-element" id="element1" checked="checked" />
            <input type="radio" name="slider-select-element" id="element2" />
            <input type="radio" name="slider-select-element" id="element3" />

            <div id="slider-container">
                <div id="slider-box">
                    <div class="slider-element">
                        <article class="element-red">

                        </article>
                        <div class="caption center" style=" width: 100%; float: left;">
                        	</br>
		                    <h4 style="font-size:15px; color: #65534f; text-transform: uppercase; font-weight: bold;">Estamos en Bogotá</h4>
		                    <hr style="width: 90%; float: inherit; border-color: #b5b0ab; margin: 8px;">
		                    <p style="padding: 10px; color: #65534f;font-weight: bold;">Lunes a sábado de 6am a 6pm, domingos y festivos de 8am a 4pm.</p>
		                </div>
                    </div>
                    <div class="slider-element">
                        <article class="element-green">

                        </article>
                        <div class="caption center" style=" width: 100%; float: left;">
                        	</br>
		                    <h4 style="font-size:15px; color: #65534f; text-transform: uppercase; font-weight: bold;">Próximamente en Cartagena</h4>
		                    <hr style="width: 90%; float: inherit; border-color: #b5b0ab; margin: 8px;">
		                    <p style="padding: 10px; color: #65534f;font-weight: bold;">Lunes a sábado de 6am a 6pm, domingos y festivos de 8am a 4pm.</p>
		                </div>
                    </div>
                    <div class="slider-element">
                        <article class="element-blue">

                        </article>
                        <div class="caption center" style=" width: 100%; float: left;">
                        	</br>
		                    <h4 style="font-size:15px; color: #65534f; text-transform: uppercase; font-weight: bold;">Próximamente en Cali</h4>
		                    <hr style="width: 90%; float: inherit; border-color: #b5b0ab; margin: 8px;">
		                    <p style="padding: 10px; color: #65534f;font-weight: bold;">Lunes a sábado de 6am a 6pm, domingos y festivos de 8am a 4pm.</p>
		                </div>
                    </div>
                </div>
            </div>

            <div id="slider-arrows">
                <label for="element1"></label>
                <label for="element2"></label>
                <label for="element3"></label>
                </div>
        </section>
    </div>
</div>
</div>
	@endsection