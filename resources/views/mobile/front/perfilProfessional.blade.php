@extends('layouts.app')
@section('content')

<script type="text/javascript">

  	function cancel(){

	    if (confirm('¿Realmente deseas cancelar este servicio?'))
        {
                //document.form.submit();//NO hace falta
           	return true; //retornamos true para que se envíe
               // document.eliminartarjeta.submit()
        }
        return false; //si llegamos aquí devolvemos false;
	}

	function finish(){

		if (confirm('¿Realmente deseas finalizar este servicio?'))
	    {
	                //document.form.submit();//NO hace falta
	        return true; //retornamos true para que se envíe
	               // document.eliminartarjeta.submit()
	    }
	    return false; //si llegamos aquí devolvemos false;
	}

</script>

<div class="container">
      </br></br></br>
        <h1 class="center">PERFIL DE PROFESIONAL</h1>
        </br></br>

	<div class="row">
		<div class="col-md-4 text-center" style="width:280px;">
			<img style="max-width:100%;" alt="Bootstrap Image Preview" src="<?php echo webservice().'/media/'?><?php echo $foto;?>" class="img-rounded" />
		</div>
		<div class="col-md-8 center">
			<table class="table text-center">
				<tbody>
				<tr class="active">
					<td>
						<b>NOMBRES:</b>
					</td>
					<td>
						{{$nombre}}
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>APELLIDOS:</b>
					</td>
					<td>
						{{$apellido}}
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>TELÉFONO:</b>
					</td>
					<td>
						{{$telefono}}
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>CORREO:</b>
					</td>
					<td>
						{{$correo}}
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br><br><br>
	<div class="row">
		<div class="col-md-12">
			@if($cantserv != 0)
				@include('flash::message')
				<div style="overflow-x:auto;">
					<table class="table table-striped">
						<thead>
						<tr>
							<th>Tiempo</th>
							<th>Cliente</th>
							<th>Dirección</th>
							<th>Hora y Fecha</th>
							<th>Servicio</th>
							<!-- <th>Valor</th> -->
							<th>Descuento</th>
							<th>Total</th>
							<th>Iva</th>
							<th>Método de pago</th>
							<th>Comentarios</th>
							<th>Estado</th>
							<th>Calificación</th>
							<th>Acción</th>
						</tr>
						</thead>
						<tbody>
						@foreach($servicios as $idForm => $sus3)
							<tr>
								{{-- <td><a href="tiempo/{{ //$sus3->date }}/{{ //$sus3->hour }}/{{ //$sus3->state }}">{{ //$sus3->id }}</a></td> --}}
								<td>{{ $sus3->id }}</td>
								<td>{{ $sus3->client }}</td> <!-- cliente -->
								<td>{{ $sus3->address }}</td> <!-- Dirección -->
								<td>{{ $sus3->date }}&nbsp;{{ $sus3->hour }}</td> <!-- Hora Y Fecha -->
								<td>
									@foreach($sus3->services as $np)
										{{ $np->name }}<br> <!-- servicio -->
									@endforeach
								</td>
								<!-- <td>
									@foreach($sus3->services as $np)
										<?php $valor=number_format($np->price);?>
										${{ $valor }}<br> --> <!-- valor -->
									<!-- @endforeach
									$3,000
								</td> -->
								<td>Esperando WS descuento</td>
								<td>{{ $sus3->total }}</td> <!-- total -->
								<td>
									<?php
										$total_separado=explode('$', $sus3->total);
										$total_limpio=trim($total_separado[1]);
										$iva_parcial=number_format($total_limpio*16/100,3);
										//este para el valor total con iva -> $iva=number_format($total_limpio-$iva_parcial,3);
										$iva=number_format($iva_parcial,3);
									?>
									${{ $iva }}
								</td> <!-- IVA -->
								<td>{{ $sus3->payment }}</td> <!-- METODO DE 'PAGO' -->
								<td>{{ $sus3->comments }}</td> <!-- comentarios -->
								<td>{{ $sus3->state }}</td> <!-- estado -->
								<td>{{ $sus3->score }}</td> <!-- calificación -->
								<?php
								//condicional 1 TERMINAR SERVICIO-> que la fecha y hora de hoy sean mayores a la agendada
								$timeActual=date("d/m/y H:i");

								//condicional 2 TERMINAR SERVICIO-> que la fecha y hora de lo agendado + 2 sean menores a la hora actual
								$fecha1=explode("/", $sus3->date);
								$fecha2=$fecha1[2].'-'.$fecha1[1].'-'.$fecha1[0];
								$time=$fecha2.' '.$sus3->hour;
								$horaFutura=Carbon\Carbon::parse($time)->addHours(2);
								$horaFutura=$horaFutura->format("d/m/y H:i");

								//condiocional 3 TERMINAR SERVICIO-> que esta operacion solo se ejecute si la fecha de agendamiento es igual a la de hoy
								$today=date("d/m/y");
								?>

								<td>

									<?php /*if($sus3->state ==  'Reservado'){ ?>
									{{ Form::open(array('id' => 'cancelar_'.$idForm, 'url' => 'cancelarProfessional', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Cancelar" data-action="cancelar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
									<?php }

									else if($sus3->state ==  'Reservado sin asignacion'){ ?>
									{{ Form::open(array('id' => 'cancelar_'.$idForm, 'url' => 'cancelarProfessional', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Cancelar" data-action="cancelar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
									<?php }*/

									/*else if($sus3->state == 'Finalizado' && ($sus3->score) == NULL ){ ?>
									{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Calificar" class="btn btn-lg btn-color submit" />
									{{ Form::close() }}
									<?php }

									else if($sus3->state ==  'Cancelado por cliente'){ ?>
									{{ Form::open(array('url' => 'reprogramar', 'method' => 'post')) }}
									<input type="submit" value="Reprogramar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
									<?php }*/

									//else if($sus3->state ==  'Cancelado por professional'){ ?>
									<!-- {{ Form::open(array('url' => 'reprogramar', 'method' => 'post')) }}
									<input type="submit" value="Reprogramar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }} -->
									<?php //} ?>
									<br>
									<!-- </td>
                                    <td> -->
									<?php /*echo $sus3->date.' '.$sus3->hour; echo " menor que ";  echo  $timeActual; echo "<br>";
									echo $sus3->date.' '.$sus3->hour; echo " menor que ";  echo  $horaFutura; echo "<br>";
									echo $sus3->date; echo " igual que ";  echo  $today; echo "<br>";*/
									if($sus3->state ==  'Reservado' && $sus3->date.' '.$sus3->hour < $timeActual && $sus3->date.' '.$sus3->hour < $horaFutura && $sus3->date == $today){ ?>
									{{ Form::open(array('id' => 'terminar_'.$idForm, 'url' => 'terminar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Terminar" data-action="terminar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
									<?php }

									else if($sus3->state ==  'Reservado sin asignacion' && $sus3->date.' '.$sus3->hour < $timeActual && $sus3->date.' '.$sus3->hour < $horaFutura && $sus3->date == $today){ ?>
									{{ Form::open(array('id' => 'terminar_'.$idForm, 'url' => 'terminar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Terminar" data-action="terminar" data-cont="{{ $idForm }}" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
									<?php }

									/*else if($sus3->state ==  'Cancelado por cliente' && ($sus3->score) == NULL ){ ?>
									{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Calificar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }}
									<?php }*/

									//else if($sus3->state ==  'Cancelado por professional' && ($sus3->score) == NULL ){ ?>
									<!--{{ Form::open(array('url' => 'calificar', 'method' => 'post')) }}
									<input type="hidden" name="id" id="id" value="{{ $sus3->id }}">
									<input type="submit" value="Calificar" class="btn btn-lg btn-color submit"/>
									{{ Form::close() }} -->
									<?php //} ?>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			@else
				@include('flash::message')
			@endif
		</div>
	</div>
	<br><br>
    <div class="col-md-3"></div>
</div><!-- /.container -->
@include('front.template.foot')
@endsection