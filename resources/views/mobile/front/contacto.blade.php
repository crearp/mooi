@extends('mobile.layouts.app')
@section('content')
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<style type="text/css" media="screen">

	p {
		color: white;
	}
}
</style>

<div class="row" style="height: 100%; background-color: #65534f;">
	<div style="position:relative;">

		<div class="img_nav">
			@include('mobile.front.template.menu')
			{{-- @include('mobile.layouts.menu') --}}
		</div>
		@include('flash::message')
	</div>

	<div class="container"></div><!-- /.container -->
	<div class="row">
	<div class="col-md-12 center" style="background-color: #65534f;">

		<div class="col-md-12">
			<h3 style="color: white;margin-top: 100px">CONTÁCTANOS</h3>
		</div>
		<div class="col-md-12">
			<br>
            <a href="{{ url('/mobile') }}"><img class="img-responsive img_footer" style="width:70%" src="{{ url('image/logo_MOOI.png') }}?<?= rand(0, 9000); ?>"></a>
			<br>
		</div>

		<div class="col-md-12">
            <p>CALLE 79 # 10 - 80 OFC 205</p>
            <p>BOGOTÁ - COLOMBIA</p>
            <p>TEL. +57 316 5253993</p>
		</div>
		<br>
		<hr style="float: none; width: 60%;">
		<br>

		<div class="col-md-12">
			<a href="{{ url('/trabaja-con-nosotros-mobile') }}">
				<!--span style="text-decoration: underline;">TRABAJA CON NOSOTROS</spam-->
			</a>
			<a href="{{ url('/cancelacion') }}">
				<span style="text-decoration: underline;">CANCELACIÓN</spam>
			</a>
			</br></br>
		</div>

		<div class="col-md-12">
		 	<a href="https://www.instagram.com/mooibelleza/" target="_blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://www.facebook.com/MooiBelleza/" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></br></br>
		</div>

		<div class="col-md-12">
			<a>Copyright © 2016   |  </a>
			<a href="http://creardigital.com/" target="blank">Desarrollado por: Crear Digital.</a>
			</p>
			<br>
		</div>
	</div>
	</div>
</div>

	{{-- @include('front.template.foot') --}}
	@endsection

	<script type="text/javascript">
		window.onload = function() {
			var alto = screen.height;
			document.getElementById("navbar").style.height = alto+"px";
		};

	</script>
