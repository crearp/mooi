     @extends('mobile.layouts.app')
     @section('content')
     <?php $servidor=webservice(true);?>
     <link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
     <style type="text/css">
          .back {
               display: table;
          }

          .back [class*="col-"] {
              display: table-cell;
              float: none;
              vertical-align: middle;
          }
     </style>

     <div class="row" style="height: 90px;">
          <div class="col-md-12" style="position:relative;">

               <div class="col-md-12 img_nav">
                    @include('mobile.front.template.menu')
                    {{-- @include('mobile.layouts.menu') --}}
               </div>
               @include('flash::message')
          </div>

     </div>

       <div class="container">
           <div class="col-xs-12 center">

               <ul class="nav nav-tabs" style="border-bottom-style: none;">
                    <li class="active" style="width:50%">
                         <a data-toggle="tab" href="#tyc" class="tyc_tabs">
                              <h4>T&C</h4>
                         </a>
                    </li>
                    <li style="width:50%">
                         <a data-toggle="tab" href="#Politics">
                              <h4>Políticas</h4>
                         </a></li>
               </ul>

               <div class="tab-content">
                   <div id="tyc" class="tab-pane fade in active">
                    <br>
                   <div class="row back">
                         <div class="col-xs-2"><a onclick="window.history.back();" style="font-size: 30px;"><&nbsp;</a>&nbsp;</div>
                         <div class="col-xs-8"><h3>TÉRMINOS Y CONDICIONES</h3></div>
                         <div class="col-xs-2"></div>
                   </div>

               <ul style="text-align: justify; padding-left: 0;">
               <p>POLÍTICAS DE USO DE MOOI
               POR FAVOR LEA ESTE DOCUMENTO CON CUIDADO. CONTIENE INFORMACIÓN MUY IMPORTANTE RESPECTO DE SUS DERECHOS Y OBLIGACIONES AL IGUAL QUE LAS LIMITACIONES Y EXCLUSIONES QUE LE PUEDEN SER APLICABLES.</p>
               <p>Para acceder a los servicios de nuestro sitio web (www.mooi.com.co), es necesario que el Usuario acepte los presentes términos y condiciones al momento de registrarse y al efectuarse cualquier actualización de los mismos.</p>

               <br>

               <ol tyoe="1">

               <li><h4 style="text-transform: uppercase">Definiciones.</h4></li>

               <p>1.1.  Cliente”: Es el usuario que acepta estos términos y condiciones</p>
               <p>1.2.  “MOOI”: Es la sociedad GRUPO MOOI SAS identificada con el Nit No. 900965823-0.</p>
               <p>1.3.   “Plataforma”: Integra el sitio web [www.mooi.com.co] </p>
               <p>1.4.  “Servicio(s) de belleza individual”: Es el servicio prestado por profesionales de la belleza vinculados a nuestra plataforma, a los clientes y/o usuarios.</p>
               <p>1.5.  “Usuario(s)”: Integra a las personas que se han registrado debidamente en nuestra Plataforma.</p>

               <br>
               <li><h4 style="text-transform: uppercase">Descarga, instalación y actualización de la APP.</h4></li>

               <p>2.1.  La descarga, instalación y actualización de la APP MOOI puede realizarse desde las principales tiendas de aplicaciones para dispositivos móviles (App Store, Google Play, Windows Phone, Black Berry World, Firefox MarketPlace); el uso de versiones desactualizadas de la aplicación podrían afectar su correcto funcionamiento, en caso de requerir soporte técnico relacionado con la aplicación puede escribirnos al correo electrónico info@mooibelleza.com.</p>

               <p>2.2.  El Usuario debe verificar, antes de efectuar cualquier descarga o instalación que la Plataforma sea compatible con sus dispositivos. En todo caso, el Usuario es el único responsable de cualquier daño causado a sus dispositivos por la instalación o uso de versiones no compatibles de la APP MOOI, por ejemplo la instalación desde páginas o tiendas no oficiales.</p>

               <br>
               <li><h4 style="text-transform: uppercase">Registro de usuarios.</h4></li>

               <p>3.1.  Para registrarse como Usuario en la Plataforma es necesario ser mayor edad y gozar de capacidad plena para contraer las obligaciones que se señalan en los presentes términos y condiciones.</p>

               <p>3.2.  El Usuario actuando en nombre propio o por medio de su representante legal, reconoce tener capacidad legal para contratar y contraer obligaciones. Además declara carecer de impedimentos legales que le prohíban vincularse al presente contrato.</p>

               <p>3.3.  El registro se realiza de manera virtual a través de la Plataforma, ingresando datos del Usuario tales como nombre, apellido, número celular, correo electrónico, entre otros.</p>

               <p>3.4.  El Usuario declara y garantiza que la información y datos suministrados a la Plataforma es veraz y actualizada.</p>

               <p>3.5.  Los derechos que se adquieren con el registro son personales e intransferibles. La contraseña de ingreso registrada por el Usuario no debe ser informada a terceros y debe ser usada con la mayor prudencia y diligencia, pues cualquier daño o perjuicio que se ocasione por el robo o mal uso de la misma será responsabilidad exclusiva del Usuario.</p>

               <p>3.6.  En todo caso MOOI se reserva el derecho de verificar por cualquier medio lícito la veracidad de la información registrada por los Usuarios y de aceptar o negar el registro de los mismos.</p>

               <br>
               <li><h4 style="text-transform: uppercase">Acceso a la plataforma.</h4></li>

               <p>4.1.  Para ingresar a la Plataforma se debe ingresar el correo electrónico y contraseña registradas por el Usuario, en caso de actualización de los presentes términos y condiciones el Usuario sólo podrá ingresar a la Plataforma cuando los haya leído y aceptado.</p>

               <p>4.2.  MOOI no puede hacerse responsable de los servicios proveídos por terceros que sean necesarios para el buen funcionamiento de la Plataforma, tales como el servicio de internet, telefonía, datos móviles, servicios de ubicación en el sistema de posicionamiento global (GPS) y similares.</p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Uso de la plataforma.</h4></li>

               <p>5.1.  Una vez registrados, los Usuarios podrán usar la Plataforma para contactar los profesionales de la belleza y solicitar la prestación de los servicios que estos prestan; las obligaciones que nazcan de dichos contratos, por ejemplo la de pagar el precio de acuerdo con la tarifa vigente, son responsabilidad exclusiva de sus contratantes no de MOOI como portal de contacto y proveedor de servicios de comunicaciones.</p>

               <p>5.2.  MOOI podrá prestar su colaboración para ayudar a solucionar cualquier inconveniente o diferencia que surja entre los Usuarios, pero ello no significa que MOOI sea responsable directo o solidario de las obligaciones que estos adquieran.</p>

               <p>5.3.  El contenido de los mensajes de datos que se compartan a través de la Plataforma son responsabilidad exclusiva del Usuario emisor.</p>

               <p>5.4.  MOOI actuará exclusivamente en su rol de portal de contacto y prestador de servicios de comunicaciones para relacionar usuarios con profesionales de belleza que se encuentren autorizadas para ofrecer y prestar servicios de belleza. </p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Evaluación de los usuarios.</h4></li>

               <p>Al finalizar cada servicio, la Plataforma permite que el Usuario califique al profesional de la belleza que le prestó el servicio; las calificaciones y comentarios que se hagan por parte de los usuarios podrán ser consultados de forma anónima por los demás Usuarios, a fin de incentivar el buen uso de la Plataforma.</p>

               <p>6.1.  MOOI no avala la idoneidad, la salud física o mental de los Usuarios registrados, la veracidad de los datos proporcionados por estos es de su exclusiva responsabilidad; por ello recomendamos a todos los Usuarios obrar con especial prudencia al ponerse en contacto con otros Usuarios, no compartir información confidencial o delicada, informar a las autoridades de cualquier irregularidad e informar a MOOI el mal comportamiento de otros Usuarios para ayudar a mejorar la calidad de los servicios que se ofrecen a través de la Plataforma.</p>

               <p>6.2.  MOOI se reserva el derecho de suspender o bloquear de manera permanente el acceso a la Plataforma de cualquier Usuario, además, MOOI podrá iniciar acciones legales en contra de cualquier Usuario que utilice la Plataforma para cometer u ocultar delitos. En todo caso, el Usuario renuncia a reclamar alguna indemnización o compensación por la cancelación o suspensión de su cuenta en la Plataforma.</p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Reversión de pagos.</h4></li>

               <p>7.1.  Cuando de conformidad con la Ley el Usuario tenga derecho de solicitar la reversión de un pago efectuado mediante una tarjeta de crédito, débito o cualquier otro instrumento de pago electrónico utilizado, MOOI realizará sus mejores esfuerzos ante los Usuarios para facilitar el proceso de reversión de pago siempre y cuando se cumpla con las siguientes condiciones:
               <br><br>
               (i) Que el Usuario haya sido objeto de fraude, que la transacción corresponda a una operación no solicitada, o que el servicio no haya sido prestado realmente por el profesional de la belleza contactado a través de nuestro portal MOOI, estos eventos siempre y cuando haya sido cobrado a través de la Plataforma mediante un instrumento de pago electrónico.
               <br>
               (ii) El Usuario presente reclamación a través del procedimiento habilitado por la Plataforma, o personalmente ante el profesional de la belleza que le prestó el servicio.
               <br>
               (iii) El reclamo se presente ante el emisor del instrumento de pago electrónico dentro de los cinco (5) días hábiles siguientes a la fecha en que el Usuario tuvo noticia de la operación fraudulenta o no solicitada.</p>

               <p>7.2.  MOOI realizará sus mejores esfuerzos ante los Usuarios o ante el proveedor de pagos para gestionar la devolución del dinero al Usuario que presenta la reclamación, el Usuario entiende y acepta que la devolución del dinero no depende de MOOI, y que el proceso de devolución comenzará a partir de realizada la solicitud en debida forma e irá hasta tanto el emisor del instrumento de pago electrónico utilizado lo autorice y reintegre.</p>

               <p>7.3.  En el evento en que hubiere alguna controversia derivada de la reclamación de reversión del pago y siempre que hubiere pronunciamiento de una autoridad jurisdiccional o administrativa en firme que determine que la misma no era procedente, el Usuario que presenta la reclamación será responsable por todos los costos en que se haya incurrido con ocasión de la solicitud de reversión. En este caso, el emisor del instrumento de pago, en conjunto con los demás participantes del proceso de pago, una vez notificada la decisión de la autoridad jurisdiccional o administrativa en firme, cargará definitivamente la transacción reclamada al Usuario reclamante y el dinero será puesto a disposición de MOOI como Portal de Contacto quien lo transferirá al beneficiario del pago, siempre que en la cuenta ahorros, tarjeta de crédito o instrumento de pago utilizado para realizar la compra objetada, existan recursos para efectuarla.</p>

               <p>7.4.  La entidad financiera verificará por una sola vez la existencia de recursos y el cargo puede ser parcial en el evento que estos no sean suficientes. En estos casos, el Usuario reclamante deberá reembolsar directamente a MOOI como Portal de Contacto el valor de la transacción, o el monto faltante, y los demás costos incurridos con ocasión de la solicitud de reversión para que MOOI proceda a transferir dichos recursos al beneficiario del pago.</p>

               <br>
               <li><h4 style="text-transform: uppercase">Protección y tratamiento de datos personales.</h4></li>

               <p>8.1.  En cumplimiento de la Ley 1581 de 2012 y su decreto reglamentario, MOOI ha adoptado un Manual Interno de Políticas y Procedimientos para Protección de Datos Personales que puede ser consultado aquí Política de privacidad.</p>

               <p>8.2.  MOOI, en condición de Responsable, efectúa el tratamiento de los datos personales suministrados por los Usuarios únicamente dentro de las finalidades contempladas en su política.</p>

               <p>8.3.  Al aceptar los presentes términos y condiciones, el Usuario también acepta el contenido del Política de Privacidad y del Manual Interno de Políticas y Procedimientos para Protección de Datos Personales adoptado por MOOI.</p>

               <p>8.4.  De acuerdo con los presentes términos y condiciones, el Usuario otorga su autorización expresa para el tratamiento de sus datos personales, incluida la autorización expresa de transferencia y transmisión internacional de sus datos personales incluyendo el nombre, imagen, edad, número de teléfono, correo electrónico, localización y dirección, dentro de las finalidades comunicadas en la Política de Privacidad y en el manual de políticas mencionado.</p>

               <p>8.5.  Si hubiere lugar a una venta, fusión, escisión, consolidación, integración empresarial, cambio en el control societario, transferencia de activos sustancial o transferencia global de activos, reorganización o liquidación de MOOI, entonces, MOOI, podrá discrecionalmente y bajo cualquier título, transferir, transmitir, vender o asignar los datos personales recabados, a cualquier tercero vinculado con algunas de las operaciones descritas o a una o más partes relevantes.</p>

               <p>8.6.  Con ocasión del uso de la Plataforma, los Usuarios podrían compartir entre ellos datos personales necesarios o pertinentes para usar adecuadamente dicha Plataforma, tales como la dirección de solicitud del servicio, nombres, números de teléfono, etc. En virtud de los presentes términos y condiciones el Usuario se obliga a no usar o tratar los datos personales que reciba o comparta para propósitos distintos a solicitar o prestar un servicio de transporte por medio de la Plataforma.</p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Licencia de uso.</h4></li>

               <p>9.1.  MOOI brindará al Usuario una licencia limitada, personal, no exclusiva, intransferible, no comercial y totalmente revocable para utilizar la Plataforma, de conformidad con los términos contenidos en este documento. La licencia será vigente únicamente mientras se acceda a la Plataforma y en caso que el acceso sea otorgado por MOOI.</p>

               <p>9.2.  MOOI se reserva todos los derechos sobre la Plataforma no expresamente concedidos aquí. Se prohíbe expresamente la distribución, reproducción, modificación o transformación, creación de obras derivadas, scrapping o extracción de información y comunicación pública de la Plataforma.</p>

               <p>9.3.  MOOI se reserva la facultad de no permitir el uso, o suspender o cancelar el uso de la Plataforma a cualquier Usuario, sin aviso previo y sin lugar a indemnización alguna.</p>

               <p>El Usuario acepta que los logotipos, marcas, fotos, imágenes, descripciones, textos, plantillas, símbolos, señales distintivas, manual(es) y cualquier otro material o contenido relacionado con la Plataforma, constituye, conforme al caso, derechos de propiedad intelectual, de negocio y/o derechos de propiedad de MOOI.</p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Cláusula compromisoria.</h4></li>

               <p>Toda diferencia o controversia relativa a estos términos y condiciones se someterá a la decisión en Derecho de UN (1) árbitro que se sujetará al reglamento de procedimiento de arbitraje del Centro de Arbitraje y Conciliación de la Cámara de Comercio de Bogotá D.C. El árbitro será designado por las partes de común acuerdo y en su defecto, será designado por el Centro de Arbitraje y Conciliación de la Cámara de Comercio de Bogotá D.C. y decidirá en derecho, a solicitud de cualquiera de las partes. El tribunal de arbitramento tendrá su sede en la ciudad de Bogotá, la ley sustancial aplicable será la colombiana. La secretaría del tribunal estará integrada por un (1) miembro de la lista oficial de secretarios del Centro de Arbitraje y Conciliación de la Cámara de Comercio de Bogotá D.C.</p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Comunicaciones y notificaciones.</h4></li>

               <p>Cualquier información que deba ser dada a conocer a los Usuarios podrá llevarse a cabo a través de la publicación de comunicados en la Plataforma, por medio de un mensaje de datos enviado al Usuario o por medio de correo electrónico a la dirección registrada por el Usuario, a elección de MOOI.
               Las comunicaciones y notificaciones dirigidas a MOOI deben enviarse a la Calle 79 # 10-80 Oficina 301 en la ciudad de Bogotá D.C.</p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Alcance.</h4></li>

               <p>Los presentes Términos y Condiciones serán aplicables a todo acto o contrato que el Usuario realice en la Plataforma y permanecerán vigentes mientras dichos actos o contratos produzcan efectos o generen obligaciones y podrán ser modificados por MOOI sin previo aviso a los usuarios. Asimismo serán aplicables cada vez que el Usuario utilice la Plataforma. El acuerdo que resulte de la aceptación de los presentes términos y condiciones sustituye todos los acuerdos, representaciones, declaraciones de garantía pactadas entre las partes y sustituye expresamente los términos de cualquier oferta mercantil que se haya comunicado.</p>

               <br><br>
               <li><h4 style="text-transform: uppercase">Ley aplicable.</h4></li>

               <p>Los presentes Términos y Condiciones se regirán e interpretaran conforme a la Ley de la Republica de Colombia.
               Todos los valores y/o precios anunciados en nuestro portal ya incluyen impuestos.</p>

               </ol>
               </ul>

          </br></br>

                   </div>

                   <div id="Politics" class="tab-pane fade">
                    </br>
                    <div class="row back">
                         <div class="col-xs-2"><a onclick="window.history.back();" style="font-size: 30px;"><&nbsp;</a>&nbsp;</div>
                         <div class="col-xs-8"><h3 class="center">POLÍTICAS DE PRIVACIDAD MOOI S.A.S.</h3></div>
                         <div class="col-xs-2"></div>
                   </div>
                    </br>
                     <ol type="1" style="text-align: -webkit-left;">
          <h4><li>RESPONSABLE DE LOS DATOS</li></h4>
          <p>1.1.  GRUPO MOOI SAS., empresa colombiana identificada con el NIT 900965823-0 ubicada en la calle 79 no 10-80 oficina 203, teléfono: 3175951031 correo electrónico: info@mooibelleza.com</p>
        </br>

          <h4><li>DEFINICIONES:</li></h4>
          <p>2.1.  “Cliente”: Es el usuario que acepta estos términos y condiciones</p>
          <p>2.2.  “MOOI”: Es la sociedad GRUPO MOOI SAS identificada con el Nit No. 900965823-0.</p>
          <p>2.3.  “Plataforma”: Integra el sitio web www.mooi.com.co</p>
          <p>2.4.  “Servicio(s) de belleza individual”: Es el servicio prestado por profesionales de la belleza vinculados a nuestra plataforma, a los clientes y/o usuarios.</p>
          <p>2.5.  “Usuario(s)”: Integra a las personas que se han registrado debidamente en nuestra Plataforma.</p>
        </br>

          <h4><li>INFORMACIÓN Y FINALIDAD DE SU TRATAMIENTO</li></h4>
          <p>3.1.  MOOI con el objeto desarrollar adecuadamente su actividad, cuenta con una base de datos de vinculados, clientes y usuarios de los servicios, quienes para el para el acceso correcto a nuestra plataforma deben suministrar una serie de datos personales tales como nombre, identificación e información de contacto como número de teléfono y dirección electrónica y eventualmente otra información relevante para facilitar el ejercicio de las labores propias de MOOI.</p>
          <p>3.2.  La información personal de los usuarios será utilizada por MOOI con las siguientes finalidades:</p>
            <ol type="a">
              <li><p>Contacto a usuarios y/o clientes de los servicios a través de diferentes canales como son mensajes de texto, correo electrónico, redes sociales, entre otros;</p></li>
              <li><p>Desarrollo de actividades de análisis históricos y estudios estadísticos; </p></li>
              <li><p>Realización de actividades comerciales, de mercadeo y publicitarias de la compañía, aliados comerciales u otras entidades con las que se realicen acuerdos comerciales para el efecto;</p></li>
              <li><p>Envío de información institucional de MOOI o publicaciones de interés.</p></li>
            </ol>

          <p>3.3.  La información personal no será utilizada con propósitos diferentes a los aquí manifestados, sin embargo, podrá ser divulgada por MOOI cuando así sea requerido por la ley, una orden judicial o una autoridad competente en ejercicio de sus funciones.</p>
          <p>3.4.  MOOI podrá llevar a cabo tratamiento de los datos personales en sus propios  servidores o en aquellos provistos por un tercero especializado en la materia con fundamento en los acuerdos que se realicen por parte de la entidad para tal objeto.</p>
          <p>3.5.  En caso de que se comparta la información con terceras personas para el cumplimiento de los fines aquí establecidos, tales como call y contact centers y empresas dedicadas a la realización de estudios de mercado, estadísticos o encuestas, estos terceros estarán igualmente sujetos a las mismas obligaciones de confidencialidad en el manejo de la información a que está sujeto MOOI con las limitaciones legales impuestas por las leyes aplicables sobre la materia en Colombia.</p>
          </br>

          <h4><li>ENLACES DE TERCEROS</li></h4>
          <p>4.1.  MOOI, en su página de Internet o a través de las redes sociales podrá poner a disposición de sus usuarios enlaces hacia direcciones de terceros, en este caso MOOI no se hace responsable por las prácticas de privacidad de esas otras páginas de Internet o quienes las manejan y MOOI se exime expresamente de cualquier responsabilidad por las acciones de estos terceros.</p>
          </br>

          <h4><li>INFORMACIÓN DE TERCEROS</li></h4>
          <p>5.1.  La entrega, cesión, divulgación o uso de información personal de terceras personas requiere ser autorizada por el titular de los datos.</p>
          <p>5.2.  En caso de estar entregando la información de un tercero a MOOI a través de cualquier medio, quien comparte los datos manifiesta contar con todas las autorizaciones por parte del titular de la misma, incluyendo las finalidades para las cuales se comparte, en particular realización de contacto futuro por parte de MOOI y en ese sentido, MOOI no asumirá ningún tipo de responsabilidad por el uso que le dé a los datos de acuerdo con las finalidades indicadas en esta política.</p>
          </br>

          <h4><li>CAMPAÑAS CON TERCEROS</li></h4>
          <p>6.1.  El usuario, acepta ser seleccionado por MOOI para participar en campañas de activación de marcas, ejercicios de estudios de mercado, y/o asistencia a eventos para marcas que se interesen en colaborar con la comunidad de usuarios de MOOI. Según la campaña específica, el usuario podrá recibir beneficios ofrecidos por las marcas que colaboren con MOOI.</p>
          </br>

          <h4><li>COMENTRAIOS, ARTÍCULOS, FOTOS, VIDEOSY OTROS CONTENIDOS</li></h4>
          <p>7.1.  Los afiliados y usuarios de MOOI pueden en muchas ocasiones y a través de diferentes medios compartir comentarios, artículos, blogs y otros datos. La entrega de información de cualquiera de las formas mencionadas u otras, tales como asistencia a eventos, es de entera responsabilidad de la persona que la comparte y, en ese sentido, si incluye cualquier tipo de información correspondiente a terceros tendrá que asegurarse de contar con todas las autorizaciones por parte de sus titulares, de tal manera que MOOI no se hará responsable por ninguno de los contenidos que se reciban, aunque si se reserva los derechos para publicarlos o eliminarlos.</p>
          <p>7.2.  Al subir contenido correspondiente a imágenes propias o de terceros, comentarios, fotografías, videos etc. a la página de Internet de MOOI o entregarlo por otros medios a la entidad, se autoriza a MOOI para presentarlo en su página de internet, Facebook, Twitter y otros medios de comunicación y utilizarlo como parte de sus campañas comerciales o de mercadeo dirigidas al público en general a través de distintos medios de comunicación.</p>
          </br>

          <h4><li>DERECHOS Y SU EJERCICIO</li></h4>
          <p>8.1.  Toda persona natural que actúe en tal condición y no en razón de ser representante de una persona jurídica o en su calidad de comerciante en relación con los actos propios de sus labores cotidianas, tiene derecho a verificar al menos una vez al mes, corregir, actualizar o solicitar la eliminación de cualquier información personal solicitada y proveída a MOOI.</p>
          <p>8.2.  Es de advertir que en caso en que la información esté siendo objeto de tratamiento por parte de MOOI en razón de existir una relación de tipo legal o contractual entre el titular de los datos y la entidad, el derecho a solicitar la supresión se verá limitado hasta la finalización de las relaciones que se mantengan.</p>
          <p>8.3.  Para el ejercicio de los derechos aquí establecidos los titulares de la información podrán contactarse con MOOI a través del área de servicio al cliente en la siguiente página dirección de correo eléctrónico: info@mooibelleza.com y las solicitudes serán atendidas de acuerdo con los tiempos establecidos para el efecto en las leyes de protección de datos vigentes en el momento.</p>
          </br>

          <h4><li>DATOS DE MENORES Y SENSIBLES</li></h4>
          <p>9.1.  MOOI en razón del servicio que presta a sus usuarios podrá realizar el tratamiento de datos de menores de edad en desarrollo de sus actividades. MOOI, ante la imposibilidad de realizar las verificaciones pertinentes, entiende que cada vez que un menor se inscribe como usuario de los servicios de la entidad y proporciona su información personal, la actualiza o la modifica, se encuentra acompañado de un adulto responsable que aprueba su actuación.</p>
          <p>9.2.  Sin embargo MOOI hace claridad en que el manejo de datos de menores se hará siempre en su beneficio y en todo caso con respeto por el interés superior de los niños, niñas y adolescentes y sus derechos fundamentales.
          <p>9.3.  MOOI, en lo posible, no realiza el tratamiento de datos sensibles, sin embargo, en caso de ser requerido, tiene establecida la posibilidad para el titular de los datos de no responder a preguntas o circunstancias que puedan involucrar esta categoría de datos.</p>
          </br>

          <h4><li>VIGENCIA DE LOS DATOS</li></h4>
          <p>10.1. MOOI en razón del servicio que presta a sus usuarios y afiliados, conservará los datos de registro de unos y otros de manera indefinida en sus bases de datos, sin embargo estos serán suprimidos en cualquier momento en que así sea requerido por el usuario o el afiliado. En tal caso no se recibirá ningún tipo de información adicional de la empresa y no habrá posibilidad para la prestación de los servicios por parte de la compañía a menos que se realice una nueva inscripción.</p>
          </br>

          <h4><li>MODIFICACIONES A LAS POLÍTICAS</li></h4>
          <p>11.1. MOOI se reserva el derecho de modificar la política de privacidad de la información de carácter personal en cualquier momento. Para el efecto realizará la publicación de un aviso en la página de internet con diez (10) días hábiles de antelación a su entrada en vigencia. El uso de la página de internet de MOOI  o de la aplicación dispuesta para usuarios y trasportadores afiliados después de la fecha de entrada en vigencia de la última modificación, indicará la aceptación a los cambios realizados.</p>
          </br>

          <h4><li>LEY Y JURISDICCIÓN</li></h4>
          <p>12.1. Toda interpretación, actuación judicial o administrativa derivada del tratamiento de los datos personales estará sujeta a las normas de protección personal establecidas en la República de Colombia y las autoridades administrativas o jurisdiccionales competentes para la resolución de cualquier inquietud, queja o demanda sobre las mismas serán las de la República de Colombia.</p>
          </br>

          <H4 class="center">Fecha de publicación y entrada en vigencia: 05 de mayo de 2016. </H4>
        </p>
        </ol>

   </br></br>
                   </div>
               </div>




           </div>
         </div><!-- /.container -->

       </br></br>
     @endsection