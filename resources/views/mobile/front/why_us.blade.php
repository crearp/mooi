@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<style type="text/css" media="screen">
  p {
      padding-left: 12px;
    }
</style>

<div class="row" style="height: 90px;">
  <div class="col-md-12" style="position:relative;">

    <div class="col-md-12 img_nav">
      @include('mobile.front.template.menu')
      {{-- @include('mobile.layouts.menu') --}}
    </div>
    @include('flash::message')
  </div>

</div>
<div class="container">
      <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6 center">
          </br></br></br>
          <h1>¿POR QUÉ MOOI?</h1>
          </br>
          <hr>
        </div>
        <div class="col-sm-3"></div>
      </div>
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8 center">
          <div class="row">
            <div class="col-sm-4 cont_1">
              <h2>
                ¿Qué somos?
              </h2>
            </div>
            </br>
            <div class="col-sm-8 border_">
              <p style="text-align: -webkit-left; text-align: -wmoz-left">
                ¿Preparada para conocer más sobre tu experiencia MOOI? Aquí va. Bello, hermoso, esplendoroso, glamuroso, eso es MOOI en afrikáans. Nacimos pensando en la necesidad de generar un enlace entre estilistas profesionales que cumplen los más altos estándares de calidad y buen servicio, y mujeres que se preocupan por sentirse siempre hermosas, siempre MOOI.
              </p>
            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-sm-4 cont_1">
              <h2>
                Respaldo
              </h2>
            </div>
            </br>
            <div class="col-sm-8 border_">
              <p style="text-align: -webkit-left; text-align: -wmoz-left">
                En MOOI estamos comprometidos con nuestro país por lo que promovemos una plataforma de belleza con sentido social. En ese sentido, y en alianza con L’OREAL y la Fundación Belleza por un Futuro, buscamos acercar a las mujeres graduadas de esta fundación las cuales se encuentran en condición de vulnerabilidad por ser víctimas de la violencia y de la pobreza extrema, a una oportunidad laboral que beneficie su futuro y el de su familia. Las profesionales de la belleza graduadas de la Fundación Belleza por un Futuro son nuestro principal motor y la principal fuente de estilistas que tenemos en nuestra página.
              </p>

              <p style="text-align: -webkit-left; text-align: -wmoz-left">
                Igualmente, cualquier profesional de belleza puede afiliarse a nuestra plataforma entrando al link de Trabaja con Nosotros y registrando sus datos. Una vez registrados, realizamos un estricto proceso de selección donde se verifican todos los datos al igual que realizamos un riguroso chequeo de seguridad. Una vez aprobado este paso, realizamos pruebas en nuestras instalaciones para asegurarnos que nuestras clientas tengan los mejores profesionales a su servicio.
              </p>

            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-sm-4 cont_1">
              <h2>
                Calidad
              </h2>
            </div>
          </br>
            <div class="col-sm-8 border_">
              <p style="text-align: -webkit-left; text-align: -wmoz-left">
                Así mismo, los productos utilizados por los estilistas profesionales vinculados a nuestra plataforma son de las mejores marcas de belleza y cuidado profesional. Puedes estar segura del alto estándar de servicio al mejor precio.</p>
              <p style="text-align: -webkit-left; text-align: -wmoz-left">¿Cómo una mujer se convierte en una mujer MOOI? Gracias a los filtros rigurosos de selección y seguridad para vinculación a nuestra plataforma, puedes estar tranquila a la hora de recibir la experiencia MOOI. Los estilistas profesionales vinculados a nuestra plataforma, tienen como fin ofrecerte un top service que incluye recibir el servicio en la comodidad de tu hogar, oficina u hotel, evitándote molestos desplazamientos y largas esperas en un salón.</p>
            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-sm-4 cont_4">
              <h2>
                Disfruta Mooi
              </h2>
            </div>
            </br>
            <div class="col-sm-8 border_">
              <p style="text-align: -webkit-left; text-align: -wmoz-left">
                ¡Es sencillo! Ingresas con tus datos a nuestra web, agendas una cita para peinado, maquillaje y/o uñas. Realizas el pago de forma online o si lo prefieres con datáfono o efectivo en el momento del servicio, luego obtienes el servicio el día y a la hora programado, y calificas tu experiencia, así aseguramos tu satisfacción. Agendando con MOOI ayudas a estilistas que buscan salir de su situación de vulnerabilidad, combatiendo una de las problemáticas sociales de nuestro país.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-2"></div>
      </div>
    </div><!-- /.container -->

  </br></br>
@endsection

