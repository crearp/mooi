@extends('mobile.layouts.app')
@section('content')

<style type="text/css" media="screen">
	.navbar-header {/*tamaño del menu*/
  		height: 90px;
	}
	.navbar-inverse .navbar-toggle .icon-bar {/*fondo rayas boton menu*/
	  background-color: #d8af98;
	}
	.navbar-toggle .icon-bar {/*rayas del boton menu*/
	  display: block;
	  width: 50px;
	  height: 8px;
	  border-radius: 1px;
	}
	.navbar-inverse .navbar-toggle {/*bordes del boton*/
	  border-color: transparent;
	}
	.navbar-toggle {/*cuerpo boton dem menu*/
	  left: 10px;
	  float: left;
	  position: relative;
	  margin-top: 8px;
	  border-radius: 0;
	  border-color: transparent;
	  background-color: transparent;
	}
	.navbar-inverse .navbar-toggle:focus, .navbar-inverse .navbar-toggle:hover {/*fondo boton oprimido*/
	  background-color: transparent;
	}

	.navbar .navbar-nav {/*color de fondo menu desplegable*/
	  width: 100%;
	  background-color: rgba(101, 83, 79, 0.90);
	}
	.navbar .navbar-collapse {/*tamaño menu desplegable*/
	  padding: 0;
	  width: 70%;
	  overflow: hidden;
	  text-align: right;
	}

}
</style>

<div class="row" style="height: 90px;">
	<div style="position:relative;">

		<div class="img_nav">
			@include('mobile.front.template.menu')
			@include('mobile.layouts.menu')
		</div>
		@include('flash::message')
	</div>
	claro
	<div class="container"></div><!-- /.container -->

</div>
	{{-- @include('front.template.foot') --}}
	@endsection

	<script type="text/javascript">
		window.onload = function() {
			var alto = screen.height;
			alert(alto);
			document.getElementById("navbar").style.height = alto+"px";
		};

	</script>
