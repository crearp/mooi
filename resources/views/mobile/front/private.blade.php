 @extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<style type="text/css" media="screen">
    h4 {
      float: left;
      width: 100%;
    }
}
</style>

<div class="row" style="height: 90px;">
  <div class="col-md-12" style="position:relative;">

    <div class="col-md-12 img_nav">
      @include('mobile.front.template.menu')
      {{-- @include('mobile.layouts.menu') --}}
    </div>
    @include('flash::message')
  </div>

</div>
  <div class="container">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
        </br></br>  
        <h3 class="center">POLÍTICAS DE PRIVACIDAD MOOI S.A.S.</h3>
        </br>
        <hr>
        </br>
        <ol type="1">
          <h4><li>RESPONSABLE DE LOS DATOS</li></h4>
          <p>1.1.  GRUPO MOOI SAS., empresa colombiana identificada con el NIT 900965823-0 ubicada en la calle 79 no 10-80 oficina 203, teléfono: 3175951031 correo electrónico: info@mooibelleza.com</p>
        </br>

          <h4><li>DEFINICIONES:</li></h4>
          <p>2.1.  “Cliente”: Es el usuario que acepta estos términos y condiciones</p>
          <p>2.2.  “MOOI”: Es la sociedad GRUPO MOOI SAS identificada con el Nit No. 900965823-0.</p>
          <p>2.3.  “Plataforma”: Integra el sitio web www.mooi.com.co</p>
          <p>2.4.  “Servicio(s) de belleza individual”: Es el servicio prestado por profesionales de la belleza vinculados a nuestra plataforma, a los clientes y/o usuarios.</p>
          <p>2.5.  “Usuario(s)”: Integra a las personas que se han registrado debidamente en nuestra Plataforma.</p>
        </br>

          <h4><li>INFORMACIÓN Y FINALIDAD DE SU TRATAMIENTO</li></h4>
          <p>3.1.  MOOI con el objeto desarrollar adecuadamente su actividad, cuenta con una base de datos de vinculados, clientes y usuarios de los servicios, quienes para el para el acceso correcto a nuestra plataforma deben suministrar una serie de datos personales tales como nombre, identificación e información de contacto como número de teléfono y dirección electrónica y eventualmente otra información relevante para facilitar el ejercicio de las labores propias de MOOI.</p>
          <p>3.2.  La información personal de los usuarios será utilizada por MOOI con las siguientes finalidades:</p>
            <ol type="a">
              <li><p>Contacto a usuarios y/o clientes de los servicios a través de diferentes canales como son mensajes de texto, correo electrónico, redes sociales, entre otros;</p></li>
              <li><p>Desarrollo de actividades de análisis históricos y estudios estadísticos; </p></li>
              <li><p>Realización de actividades comerciales, de mercadeo y publicitarias de la compañía, aliados comerciales u otras entidades con las que se realicen acuerdos comerciales para el efecto;</p></li>
              <li><p>Envío de información institucional de MOOI o publicaciones de interés.</p></li>
            </ol>

          <p>3.3.  La información personal no será utilizada con propósitos diferentes a los aquí manifestados, sin embargo, podrá ser divulgada por MOOI cuando así sea requerido por la ley, una orden judicial o una autoridad competente en ejercicio de sus funciones.</p>
          <p>3.4.  MOOI podrá llevar a cabo tratamiento de los datos personales en sus propios  servidores o en aquellos provistos por un tercero especializado en la materia con fundamento en los acuerdos que se realicen por parte de la entidad para tal objeto.</p>
          <p>3.5.  En caso de que se comparta la información con terceras personas para el cumplimiento de los fines aquí establecidos, tales como call y contact centers y empresas dedicadas a la realización de estudios de mercado, estadísticos o encuestas, estos terceros estarán igualmente sujetos a las mismas obligaciones de confidencialidad en el manejo de la información a que está sujeto MOOI con las limitaciones legales impuestas por las leyes aplicables sobre la materia en Colombia.</p>
          </br>

          <h4><li>ENLACES DE TERCEROS</li></h4>
          <p>4.1.  MOOI, en su página de Internet o a través de las redes sociales podrá poner a disposición de sus usuarios enlaces hacia direcciones de terceros, en este caso MOOI no se hace responsable por las prácticas de privacidad de esas otras páginas de Internet o quienes las manejan y MOOI se exime expresamente de cualquier responsabilidad por las acciones de estos terceros.</p>
          </br>

          <h4><li>INFORMACIÓN DE TERCEROS</li></h4>
          <p>5.1.  La entrega, cesión, divulgación o uso de información personal de terceras personas requiere ser autorizada por el titular de los datos.</p>
          <p>5.2.  En caso de estar entregando la información de un tercero a MOOI a través de cualquier medio, quien comparte los datos manifiesta contar con todas las autorizaciones por parte del titular de la misma, incluyendo las finalidades para las cuales se comparte, en particular realización de contacto futuro por parte de MOOI y en ese sentido, MOOI no asumirá ningún tipo de responsabilidad por el uso que le dé a los datos de acuerdo con las finalidades indicadas en esta política.</p>
          </br>

          <h4><li>CAMPAÑAS CON TERCEROS</li></h4>
          <p>6.1.  El usuario, acepta ser seleccionado por MOOI para participar en campañas de activación de marcas, ejercicios de estudios de mercado, y/o asistencia a eventos para marcas que se interesen en colaborar con la comunidad de usuarios de MOOI. Según la campaña específica, el usuario podrá recibir beneficios ofrecidos por las marcas que colaboren con MOOI.</p>
          </br>

          <h4><li>COMENTRAIOS, ARTÍCULOS, FOTOS, VIDEOSY OTROS CONTENIDOS</li></h4>
          <p>7.1.  Los afiliados y usuarios de MOOI pueden en muchas ocasiones y a través de diferentes medios compartir comentarios, artículos, blogs y otros datos. La entrega de información de cualquiera de las formas mencionadas u otras, tales como asistencia a eventos, es de entera responsabilidad de la persona que la comparte y, en ese sentido, si incluye cualquier tipo de información correspondiente a terceros tendrá que asegurarse de contar con todas las autorizaciones por parte de sus titulares, de tal manera que MOOI no se hará responsable por ninguno de los contenidos que se reciban, aunque si se reserva los derechos para publicarlos o eliminarlos.</p>
          <p>7.2.  Al subir contenido correspondiente a imágenes propias o de terceros, comentarios, fotografías, videos etc. a la página de Internet de MOOI o entregarlo por otros medios a la entidad, se autoriza a MOOI para presentarlo en su página de internet, Facebook, Twitter y otros medios de comunicación y utilizarlo como parte de sus campañas comerciales o de mercadeo dirigidas al público en general a través de distintos medios de comunicación.</p>
          </br>

          <h4><li>DERECHOS Y SU EJERCICIO</li></h4>
          <p>8.1.  Toda persona natural que actúe en tal condición y no en razón de ser representante de una persona jurídica o en su calidad de comerciante en relación con los actos propios de sus labores cotidianas, tiene derecho a verificar al menos una vez al mes, corregir, actualizar o solicitar la eliminación de cualquier información personal solicitada y proveída a MOOI.</p>
          <p>8.2.  Es de advertir que en caso en que la información esté siendo objeto de tratamiento por parte de MOOI en razón de existir una relación de tipo legal o contractual entre el titular de los datos y la entidad, el derecho a solicitar la supresión se verá limitado hasta la finalización de las relaciones que se mantengan.</p>
          <p>8.3.  Para el ejercicio de los derechos aquí establecidos los titulares de la información podrán contactarse con MOOI a través del área de servicio al cliente en la siguiente página dirección de correo eléctrónico: info@mooibelleza.com y las solicitudes serán atendidas de acuerdo con los tiempos establecidos para el efecto en las leyes de protección de datos vigentes en el momento.</p>
          </br>

          <h4><li>DATOS DE MENORES Y SENSIBLES</li></h4>
          <p>9.1.  MOOI en razón del servicio que presta a sus usuarios podrá realizar el tratamiento de datos de menores de edad en desarrollo de sus actividades. MOOI, ante la imposibilidad de realizar las verificaciones pertinentes, entiende que cada vez que un menor se inscribe como usuario de los servicios de la entidad y proporciona su información personal, la actualiza o la modifica, se encuentra acompañado de un adulto responsable que aprueba su actuación.</p>
          <p>9.2.  Sin embargo MOOI hace claridad en que el manejo de datos de menores se hará siempre en su beneficio y en todo caso con respeto por el interés superior de los niños, niñas y adolescentes y sus derechos fundamentales.
          <p>9.3.  MOOI, en lo posible, no realiza el tratamiento de datos sensibles, sin embargo, en caso de ser requerido, tiene establecida la posibilidad para el titular de los datos de no responder a preguntas o circunstancias que puedan involucrar esta categoría de datos.</p>
          </br>

          <h4><li>VIGENCIA DE LOS DATOS</li></h4>
          <p>10.1. MOOI en razón del servicio que presta a sus usuarios y afiliados, conservará los datos de registro de unos y otros de manera indefinida en sus bases de datos, sin embargo estos serán suprimidos en cualquier momento en que así sea requerido por el usuario o el afiliado. En tal caso no se recibirá ningún tipo de información adicional de la empresa y no habrá posibilidad para la prestación de los servicios por parte de la compañía a menos que se realice una nueva inscripción.</p>
          </br>

          <h4><li>MODIFICACIONES A LAS POLÍTICAS</li></h4>
          <p>11.1. MOOI se reserva el derecho de modificar la política de privacidad de la información de carácter personal en cualquier momento. Para el efecto realizará la publicación de un aviso en la página de internet con diez (10) días hábiles de antelación a su entrada en vigencia. El uso de la página de internet de MOOI  o de la aplicación dispuesta para usuarios y trasportadores afiliados después de la fecha de entrada en vigencia de la última modificación, indicará la aceptación a los cambios realizados.</p>
          </br>

          <h4><li>LEY Y JURISDICCIÓN</li></h4>
          <p>12.1. Toda interpretación, actuación judicial o administrativa derivada del tratamiento de los datos personales estará sujeta a las normas de protección personal establecidas en la República de Colombia y las autoridades administrativas o jurisdiccionales competentes para la resolución de cualquier inquietud, queja o demanda sobre las mismas serán las de la República de Colombia.</p>
          </br>

          <H4 class="center">Fecha de publicación y entrada en vigencia: 05 de mayo de 2016. </H4>
        </p>
        </ol>
      </div>
      <div class="col-sm-1"></div>
    </div><!-- /.container -->

  </br>
  @endsection

