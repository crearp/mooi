@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<style type="text/css">
	.btnMobile {
	  position: fixed;
	}
	.inputMod {
		width: 100%;
	    text-align: left;
	    border: none;
	    box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
	    -webkit-box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
	    -moz-box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
	    border-bottom: solid 2px #d8af98;
	    border-radius: 0px;
	}
</style>

<div class="row" style="height: 90px;">
	<div class="col-md-12" style="position:relative;">

		<div class="col-md-12 img_nav" style="position: fixed; z-index: 1;">
			@include('mobile.front.template.menu')
			<!-- @include('mobile.layouts.menu') -->
		</div>
		@include('flash::message')
	</div>

</div>

<script type="text/javascript">

	window.addEventListener('load', function(){
		$('#optDeleteCard').on('click', function(e){
			e.preventDefault();
			$('#modalAlert').modal('show');
			$('#saveDeleteCard').off();
			$('#saveDeleteCard').on('click',function(e){
				e.preventDefault();
				$('#formDeleteCard').submit();
			})
		})

	var medio = {{$IntMedComuni}};
	document.formulario_envio.comunicaciones.value=medio;
	}, false);

  	function cancel(){
	    if (confirm('¿Realmente deseas cancelar este servicio?'))
        {
                //document.form.submit();//NO hace falta
           	return true; //retornamos true para que se envíe
               // document.eliminartarjeta.submit()
        }
        return false; //si llegamos aquí devolvemos false;
	}

	function finish(){
		if (confirm('¿Realmente deseas finalizar este servicio?'))
	    {
	                //document.form.submit();//NO hace falta
	        return true; //retornamos true para que se envíe
	               // document.eliminartarjeta.submit()
	    }
	    return false; //si llegamos aquí devolvemos false;
	}
</script>

<div class="container">
      </br>
        <h3 class="center">PERFIL</h3>
        </br>
      @include('flash::message')

	<div class="row">
		<!-- <div class="col-md-4 text-center">
			<img alt="Bootstrap Image Preview" src="https://t3.ftcdn.net/jpg/01/05/72/60/500_F_105726077_mMZzmT08rVm2pckrBfecs6l3QNjvGnrt.jpg" WIDTH=280 HEIGHT=210 class="img-rounded" />
		</div> -->
		<div class="col-md-12 center">
		{!! Form::open(['route' => 'actualizar_perfil', 'method' => 'post', 'name' =>'formulario_envio', 'id' =>'formulario_envio']) !!}
			<div class="col-md-12">
				<div class="col-md-12 left"><h4>NOMBRES:</h4></div>
				<div class="col-md-12">
					<input type="text" class="form-control inputMod input_profile" name="nombre" id="nombre" value="{{$nombre}}"/>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6 left"><h4>APELLIDOS:</h4></div>
				<div class="col-md-6">
					<input type="text" class="form-control inputMod " name="apellido" id="apellido" value="{{$apellido}}"/>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6 left"><h4>TELÉFONO:</h4></div>
				<div class="col-md-6">
					<input type="text" class="form-control inputMod "  name="telefono" id="telefono" value="{{$telefono}}"/>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6 left"><h4>DIRECCIÓN:</h4></div>
				<div class="col-md-6">
					<input type="text" class="form-control inputMod " name="direccion" id="direccion" value="{{$direccion}}"/>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-6 left"><h4>CORREO:</h4></div>
				<div class="col-md-6">
					<input type="text" class="form-control inputMod " name="correo" id="correo" value="{{$correo}}" disabled/>
				</div>
			</div>
		</br></br>
			<div class="col-md-12 left">
				<div class="col-md-6"><h4>¿Por qué medio desea recibir promociones y publicidad?:</h4></div>
				<div class="col-md-6">
					<p>Tu medio actual es: {{$StrMedComuni}}</p>
					<select name="comunicaciones" id="comunicaciones" class="form-control inputMod">
		                <option value=0 >E-mail y SMS</option>
		                <option value=1 >E-mail</option>
		                <option value=2 >SMS</option>
		                <option value=3 >Ninguno</option>
		            </select>
				</div>
			</div>
			<br>
			<button type="button" class="btn btn-lg btn-color center" style="color: white;" onclick="modalNotificacion();">
				<h4>GUARDAR</h4>
			</button>
			{!! Form::close() !!}
		</div>
	</div>
		<div class="row">
			<div class="col-md-12 center" >
				<br>
				<hr>
				<br><br>
				<h3>ELIMINAR TARJETAS</h3>
					<br>
					{!! Form::open(['route' => 'posteliminar_cards', 'method' => 'post', 'id' => 'formDeleteCard', 'name' => 'formDeleteCard']) !!}
						<br>
						<select name="cards" id="cards" class="form-control inputMod" required="required">
							@for($x=0;$x<count($cards);$x++)
							<option value="{{ $cards[$x]->card_reference }}" data-valor="{{ $cards[$x]->card_reference }}">{{ $cards[$x]->card_number_encrypted }}
							</option>
							@endfor
						</select>
						<input type="hidden" name="id" id="id" value="{{ $id }}">
					</br>
					<button type="button" id="optDeleteCard" class="btn btn-lg btn-color" style="color: white;" onclick="modalNotificacionTarjeta();">
						<h4>ELIMINAR</h4>
					</button>
				{!! Form::close() !!}

			</div>
		</div>
		<div class="row">
			<div class="col-md-12 center">
				<br>
				<hr>
				<br><br>
				<h3>AGREGAR TARJETAS</h3>
				<br><br>
				<a href="/changecard">
					<button type="button" class="btn btn-lg btn-color" style="color: white;">
						<h4>AGREGAR TARJETA</h4>
					</button><a>
				<br><br>
			</div>
		</div>
	</div>
	<br><br>
    <div class="col-md-3"></div>
</div><!-- /.container -->


	<div class="modal fade" tabindex="-1" role="dialog" id="modalConfirm"><!-- /.modal delete -->
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">¡Aviso!</h4>
				</div>
				<div class="modal-body">
					<p id="textModal1" style="color: #464041;">¿Quieres guardar los cambios?</p>
				</div>
				<div class="modal-footer">
					<a><button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" style="color: #65534f; background-color: #ffffff; border-color: #cea791;" onclick="document.formulario_envio.submit();">ACEPTAR</button></a>
		        	<a><button type="button" class="btn btn-default" data-dismiss="modal" style="color: #65534f; background-color: #ffffff; border-color: #cea791;">CERRAR</button></a>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal delete-->

<div class="modal fade" tabindex="-1" role="dialog" id="modalTarjetas"><!-- /.modal delete -->
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">¡Aviso!</h4>
			</div>
			<div class="modal-body">
				<p id="textModal">¿Quieres eliminar esta tarjeta?</p>
			</div>
			<div class="modal-footer">
				<a><button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close" style="color: #65534f; background-color: #ffffff; border-color: #cea791;" onclick="document.formDeleteCard.submit();">ACEPTAR</button></a>
	        	<a><button type="button" class="btn btn-default" data-dismiss="modal" style="color: #65534f; background-color: #ffffff; border-color: #cea791;">CERRAR</button></a>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal delete-->
@endsection

<script type="text/javascript">
	window.onload  = function(){
		var x = {{ $x }};

		if(x <= 0){
			document.getElementById("optDeleteCard").disabled = true;
		}
	}

	function modalNotificacion(){
		$('#modalConfirm')
			.modal('show')
			.on('shown.bs.modal', function () { })
	}
	function modalNotificacionTarjeta(){
		$('#modalTarjetas')
			.modal('show')
			.on('shown.bs.modal', function () { })
	}
</script>