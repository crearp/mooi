@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<div id="servicios" class="servicios"><!--servicios-->
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6 center">
          </br></br></br>
          <h1>SERVICIOS</h1>
          </br>
          <hr>
        </div>
        <div class="col-sm-3"></div>
      </div>
      </br></br></br>

    <div class="row">
      <div class="col-md-4 center space how_work_pad">
          <a href="{{ url('/servicios/Uñas') }}">
            <div class="service_nails"></div>
          </a>
        </br></br>
          <h4>UÑAS</h4>
          </br>
          <p>Colores claros, naturales y fuertes, múltiples opciones de acuerdo a tu gusto y a las últimas tendencias.</p>
        </div>

        <div class="col-md-4 center space how_work_pad">
          <a href="{{ url('/servicios/Peinados') }}">
            <div class="service_hair"></div>
          </a>
        </br></br>
          <h4>CABELLO</h4>
          </br>
          <p>Tu mejor accesorio como más te gusta: liso, ondulado, recogido, voluminoso o relajado, en manos expertas.</p>
        </div>

        <div class="col-md-4 center space how_work_pad">
        <a href="{{ url('/servicios/Maquillaje') }}">
          <div class="service_makeup"></div>
        </a>
        </br></br>
          <h4>MAQUILLAJE</h4>
          </br>
          <p>Mirada impactante y natural con looks únicos.</p>
        </div>
      </div>
</div><!--servicios-->

  </br></br>
@include('front.template.foot')

@endsection

