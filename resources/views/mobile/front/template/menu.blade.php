<nav class="navbar navbar-inverse">
	<div class="header">
		<div class="navbar-header center">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="center responsive" title="inicio" style="left:47%">
				<a href="{{ url('/mobile') }}" style="display:inline-block;">
					<img alt="icono mooi" src="{{ url('img/icon_m.png') }}?<?= rand(0, 9000); ?>" style="">
				</a>
			</div>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<a href="{{ url('/mobile') }}">
				<img class="img-responsive" alt="icono menu mooi" src="{{ url('img/logo_mooi_footer.png') }}?<?= rand(0, 9000); ?>" style="width:300px;">
			</a>
			<?php $sessionuser=Session::get('sessionuserC'); $nombreuser=Session::get('nombreuserC');  $tipo_usuario=Session::get('tipo'); ?>
			<ul class="nav navbar-nav menu_align" style="text-align: -webkit-left; padding-left: 10px;">

				<!--li><a class="color" href="{{ url('/porque-nosotros-mobile') }}"><h4>¿PORQUé NOSOTROS?</h4></a></li-->
				@if ($sessionuser == 'OK')
				<li><a class="color" href="{{ url('perfil-mobile') }}"><h4>PERFIL</h4></a></li>
				<li><a class="color" href="{{ url('/historial-mobile') }}"><h4>HISTORIAL</h4></a></li>
				@endif
				<li><a class="color" href="{{ url('/mobile') }}"><h4>SERVICIOS</h4></a></li>
				@if ($sessionuser == 'OK')
				<li><a href="{{ url('/changecard') }}"><h4>AGREGAR TARJETA</h4></a></li>
				<li><a href="{{ url('/deletecards') }}"><h4>ELIMINAR TARJETA</h4></a></li>
				@endif
				<li><a class="color" href="{{ url('/aliados') }}"><h4>ALIADOS</h4></a></li>
				<li><a class="color" href="{{ url('/donde-estamos') }}"><h4>¡DÓNDE ESTAMOS!</h4></a></li>
				<li><a class="color" href="{{ url('/contacto-movil') }}"><h4>CONTÁCTANOS</h4></a></li>
				<li><a class="color" href="{{ url('/terminos-y-condiciones-mobile') }}"><h4>T&C</h4></a></li>
				<li><a class="color" href="https://www.instagram.com/mooibelleza/"><h4>PRENSA</h4></a></li>
				@if ($sessionuser == 'OK')
				<li><a href="{{ url('/logout') }}"><h4>CERRAR SESIÓN</h4></a></li>
				@endif
				<!--li><a class="color" href="{{ url('/privacidad-mobile') }}"><h4>POLÍTICAS DE PRIVACIDAD</h4></a></li-->
				<!--li><a class="color" href="{{ url('/como-funciona-mobile') }}"><h4>¿CóMO FUNCIONA?</h4></a></li-->
				<!--<li class="dropdown">
					<a class="color" href="{{ url('/inicio') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><H4>NOVIAS<span class="caret"></span></H4> </a>
					<ul class="dropdown-menu">
						<li><a class="color" href="{{ url('/inicio') }}"><h4>INSPIRACIÓN</h4></a></li>
						<li><a class="color" href="{{ url('/inicio') }}"><h4>SOLUCITUDES</h4></a></li>
					</ul>
				</li>
				<li><a class="color" href="https://www.blogger.com"><h4>BLOG</h4></a></li>-->
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>

