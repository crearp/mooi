<?php $url= $_SERVER["REQUEST_URI"];?>

@if($url == '/')
	<!--home-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Solicita tu cita de Manicure, Pedicure, Peinado y Maquillaje a domicilio a través de nuestra plataforma. Tendencias y asesoría personalizada">
	<title>Mooi | Servicios de belleza a domicilio</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/inicio-sesion')
	<!--login-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Iniciar Sesión. ¿Olvidaste tu contraseña? ¿Aún no estás registrada?">
	<title>Iniciar sesión | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/inicio-sesion" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/inicio-sesion",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/registro')
	<!--registro-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Registra tus datos para poder agendar tu cita de servicios de belleza a domicilio en Mooi.">
	<title>Registro | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/registro" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/registro",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/agenda')
	<!--agenda-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Agenda un servicio de belleza a domicilio a través de nuestra plataforma.">
	<title>Agenda tu cita | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/agenda" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/agenda",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/porque-nosotros')
	<!--porque-nosotros-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="¿Qué somos?. Respaldo. Calidad. Disfruta Mooi.">
	<title>Porqué Mooi | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/porque-nosotros" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/porque-nosotros",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/novias')
	<!--novias-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Enviaremos un equipo experto en maquillaje, manicure, pedicure y peinados para bodas en Bogotá y sus alrededores.">
	<title>Novias Belleza | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/novias" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/novias",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/contacto')
	<!--contacto-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Contáctate con nosotros. Pregúntanos lo que quieras.">
	<title>Contacto| Mooi </title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/contacto" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/contacto",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/trabaja-con-nosotros')
	<!--trabaja-con-nosotros-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Envíanos tus datos para trabajar en esta gran empresa.">
	<title>Trabaja con Nosotros | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/trabaja-con-nosotros" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/trabaja-con-nosotros",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/politicas-privacidad')
	<!--politicas-privacidad-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Políticas de privacidad de Mooi S.A.S">
	<title>Políticas de privacidad | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/politicas-privacidad" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/politicas-privacidad",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/terminos-y-condiciones')
	<!--terminos-y-condiciones-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Términos y Condiciones de Mooi S.A.S">
	<title>Términos y Condiciones | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/terminos-y-condiciones" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/terminos-y-condiciones",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/cancelacion')
	<!--cancelacion-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Políticas de cancelación de agendamientos.">
	<title>Políticas de cancelación | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/cancelacion" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/cancelacion",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/servicios')
	<!--servicios-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Servicios MOOI.">
	<title>Servicios | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/servicios" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/servicios",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/contrasena/cambiar')
	<!--/contrasena/cambiar-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Recuperación de Contraseña.">
	<title>Recuperación de Contraseña | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/contrasena/cambiar" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/contrasena/cambiar",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/deletecards')
	<!--/contrasena/cambiar-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Recuperación de Contraseña.">
	<title>Eliminacion de tarjetas | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/contrasena/cambiar" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/contrasena/cambiar",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/changecard')
	<!--/contrasena/cambiar-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Recuperación de Contraseña.">
	<title>Agregar tarjetas | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/contrasena/cambiar" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/contrasena/cambiar",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/historial')
	<!--/contrasena/cambiar-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Recuperación de Contraseña.">
	<title>Historial de usuario | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/contrasena/cambiar" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/contrasena/cambiar",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if($url == '/perfil')
	<!--/contrasena/cambiar-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content="Recuperación de Contraseña.">
	<title>Perfil de usuario | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co/contrasena/cambiar" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->
	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/contrasena/cambiar",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif
@if(substr_count($url, 'servicios') == 1)

	@php
		$title = explode("-", $url);
		$titulo = str_replace("%20", " ", $title[1]);
	@endphp
	<!--inspírate-->
	<!--meta estandar-->
	<meta name="author" content="MOOI S.A.S.">
	<meta name="developer" content="Crear Digital Merchandising S.A.S.">
	{{-- <meta name="developer name" content="JAOS"> --}}
	<meta name="description" content=" Selecciona uno de los peinados especiales de Mooi, maquillaje o una de las opciones de manicure y/o pedicure y te enviaremos un especialista a tu casa u oficina.">
	<title>{{ $titulo }} a Domicilio | Mooi</title>
	<!--meta para facebook -->
	<meta property="og:url" content="https://www.mooi.com.co{{ $url }}" />
	<meta property="og:type" content="product.group" />
	<meta property="og:title" content="MOOI | Belleza con sentido social." />
	<meta property="og:description" content="Brindar un servicio de belleza capacitado y profesional a domicilio para facilitarle la vida a las mujeres modernas que tienen poco tiempo." />
	<meta property="og:image" content="https://www.mooi.com.co/img/logo_mooi_footer.png" />

	<!-- meta script datos estructurados google -->

	<script type="application/ld+json">
		{
		  "@context":"http://schema.org",
		  "@type":"HealthAndBeautyBusiness ",
		  "image": "https://www.mooi.com.co/img/logo_mooi_footer.png",
		  "@id":"https://www.mooi.com.co/",
		  "name":"MOOI Belleza",
		  "sameAs" : [
		    "https://www.instagram.com/mooibelleza/",
		    "https://www.facebook.com/MooiBelleza/"
		  ],
		  "address":{
		    "@type":"PostalAddress",
		    "streetAddress":"Calle 79 # 10 - 80 Oficina 205",
		    "addressLocality":"Bogotá",
		    "addressRegion":"CUN",
		    "postalCode":"110221",
		    "addressCountry":"CO"
		  },
		  "geo":{
		    "@type":"GeoCoordinates",
		    "latitude":4.663576,
		    "longitude":-74.054232
		  },
		  "telephone":"+573165253993",
		  "potentialAction":{
		    "@type":"ReserveAction",
		    "target":{
		      "@type":"EntryPoint",
		      "urlTemplate":"https://www.mooi.com.co/{{ $url }}",
		      "inLanguage":"es-CO",
		      "actionPlatform":[
		        "http://schema.org/DesktopWebPlatform",
		        "http://schema.org/IOSPlatform",
		        "http://schema.org/AndroidPlatform"
		      ]
		    },
		    "result":{
		      "@type":"Reservation",
		      "name":"Book a class"
		    }
		  }
		}
	</script>
@endif