@extends('mobile.layouts.app')
@section('content')
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">

<div class="row" style="height: 90px; background-color: #65534f;">
	<div style="position:relative;">

		<div class="img_nav">
			@include('mobile.front.template.menu')
			{{-- @include('mobile.layouts.menu') --}}
		</div>
		@include('flash::message')
	</div>
</div>
<div class="container" style="height: calc(100% - 1px)!important; padding-right: 0px; padding-left: 0px;">
	<div class="col-md-12 center container" style="background-color: #65534f;">
		<div class="col-md-12 center">
		</br>
		<h3>NUESTROS ALIADOS</h3>
		</br>
            <img class="img-responsive" alt="Belleza por un futuro" src="{{ url('img/Beleza_Futuro.png') }}?<?= rand(0, 9000); ?>">
        </div>
		<div class="col-md-12 center fundacion" style="padding: 0 30px">
        </br>
        <h4 style="color:white">Belleza con conciencia</h4>
        <p>En MOOI estamos comprometidos con nuestro país por lo que promovemos una plataforma de belleza con sentido social. En ese sentido, y en alianza con L’OREAL y la Fundación Belleza por un Futuro, buscamos acercar a las mujeres graduadas de esta fundación las cuales se encuentran en condición de vulnerabilidad por ser víctimas de la violencia y de la pobreza extrema, a una oportunidad laboral que beneficie su futuro y el de su familia. Las profesionales de la belleza graduadas de la Fundación Belleza por un Futuro son nuestro principal motor y la principal fuente de estilistas que tenemos en nuestra página.</p>

        <p>Año tras año, a través de su programa de Responsabilidad Social Belleza por un Futuro, el grupo L'Oréal Colombia brinda educación en peluquería a cientos de colombianas que han hecho parte de este programa y desde el 2009 hasta hoy ha graduado a más de 750 mujeres.</p>
		</div>
		<div class="col-md-12 center" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px;">
        	<img src="{{ url('img/fundacion.png') }}?<?= rand(0, 9000); ?>" alt="fundación L´oreal mooi" class="img-responsive" style="border-style: solid;border-width: 10px;border-color: #d8af98;">
     	</div>
	</div>

	</div><!-- /.container -->
	{{-- @include('front.template.foot') --}}
	@endsection

	<script type="text/javascript">
		window.onload = function() {
			var alto = screen.height;
			document.getElementById("navbar").style.height = alto+"px";
		};

	</script>
