@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<style type="text/css" media="screen">
	.navbar-header {/*tamaño del menu*/
  		height: 90px;
	}
	.navbar-inverse .navbar-toggle .icon-bar {/*fondo rayas boton menu*/
	  background-color: #d8af98;
	}
	.navbar-toggle .icon-bar {/*rayas del boton menu*/
	  display: block;
	  width: 50px;
	  height: 8px;
	  border-radius: 1px;
	}
	.navbar-inverse .navbar-toggle {/*bordes del boton*/
	  border-color: transparent;
	}
	.navbar-toggle {/*cuerpo boton dem menu*/
	  left: 10px;
	  float: left;
	  position: relative;
	  margin-top: 8px;
	  border-radius: 0;
	  border-color: transparent;
	  background-color: transparent;
	}
	.navbar-inverse .navbar-toggle:focus, .navbar-inverse .navbar-toggle:hover {/*fondo boton oprimido*/
	  background-color: transparent;
	}

	.navbar .navbar-nav {/*color de fondo menu desplegable*/
	  width: 100%;
	  background-color: rgba(101, 83, 79, 0.90);
	}
	.navbar .navbar-collapse {/*tamaño menu desplegable*/
	  padding: 0;
	  width: 70%;
	  overflow: hidden;
	  text-align: right;
	}

	.modal-backdrop {
	  background-color: transparent;
	}
	.close {
	  float: right;
	  font-size: 30px;
	  font-weight: 100 !important;
	  line-height: 1;
	  color: #65534f;
	  text-shadow: 0 1px 0 #fff;
	  filter: alpha(opacity=20);
	  opacity: 1;
	}

}
</style>
	<!-- Modal ciudad -->
		<div class="modal fade" id="modalServicios-Calificar" name="modalServicios-Calificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document" style="margin: 0px;">
				<div class="modal-content" style="color: white; background-color: #62514d;">
					<!-- <div class="modal-header">
						<h4 class="modal-title" id="myModalLabel" style="color: white;"">Selecciona tu ciudad</h4>
					</div> -->
					<div class="modal-body advertencia">
						<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #eeba8a;"><span aria-hidden="true">&times;</span></button> -->
					<!-- <h4 class="modal-title" id="myModalLabel" style="color: white;text-align: center;">CALIFICA TU SERVICIO</h4>
						<hr style="border-top: 1px solid #eeba8a;"> -->
					<div class="row">
						<div class="col-md-3 center"></div>
						<div class="col-md-6 center">
							<hr style="border-top: 1px solid #eeba8a;">
						    <h2 class="center">CALIFICACIÓN DEL SERVICIO</h2>
						    </br></br>
						    <p>Para nosotros es importante que califiques el servicio y todo lo referente a su ejecución con esta pequeña encuesta.</p>
						    {{ Form::open(array('url' => 'calificarfinal', 'method' => 'post'))}}
						    </br>
						    <p>Califica de 1 a 5 el servicio, siendo 1 el más bajo y 5 el más alto.</p>
						    </br></br>
						    <p>Tú(s) servicio(s):</br>
						    	@foreach($serviciosCal[0] as $serv)
						    		{{$serv->name}}</br>
						    	@endforeach
						    </p>
						    <p>Fecha del servicio:</br>
						    	{{$fecha_servicio}}-{{$hora_servicio}}
						    </p>
						    <p>Profesional del Servicio:</br>
						    	{{$profesional_servicio}}
						    </p>

					      	<p align=center class="clasificacion">
								  <input id="radio1" type="radio" name="estrellas" value="5"><!--
								  --><label for="radio1">★</label><!--
								  --><input id="radio2" type="radio" name="estrellas" value="4"><!--
								  --><label for="radio2">★</label><!--
								  --><input id="radio3" type="radio" name="estrellas" value="3"><!--
								  --><label for="radio3">★</label><!--
								  --><input id="radio4" type="radio" name="estrellas" value="2"><!--
								  --><label for="radio4">★</label><!--
								  --><input id="radio5" type="radio" name="estrellas" value="1"><!--
								  --><label for="radio5">★</label>
							</p>

					      	</br></br>
					     	<p>Déjanos tu comentario sobre todo lo referente al servicio.</p>
					        <textarea class="form-control" id="comentario" name="comentario" rows="6" cols="50"></textarea>
					        <input type="hidden" name="id" id="id" value="{{$id_servicio}}">
					        <input type="hidden" name="is_mobile" id="is_mobile" value="true">
					     	</br></br></br>
					      	<button class="btn btn-lg btn-primary btn-block btn-color" type="submit"><h4>CALIFICAR</h4></button>
					      	{{ Form::close() }}
						</div>
						<hr style="border-top: 1px solid #eeba8a;">
						<div class="col-md-3 center"></div>
					</div><!--Aqui va el mensaje-->
					{{--<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Seleccionar</button>
					</div>--}}
				</div>
			</div>
		</div>	
	<!--Fin Modal ciudad-->

	<script type="text/javascript">
		window.onload = function() {
			var alto = screen.height;
			//alert(alto);
			//document.getElementById("navbar").style.height = alto+"px";

				$('#modalServicios-Calificar')
					.modal('show')
					.on('shown.bs.modal', function () { })
		};
	</script>
