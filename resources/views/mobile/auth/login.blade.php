@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">

<div class="row" style="height: 90px;">
  <div class="col-md-12" style="position:relative;">

    <div class="col-md-12 img_nav">
      @include('mobile.front.template.menu')
      {{-- @include('mobile.layouts.menu') --}}
    </div>
    @include('flash::message')
  </div>

</div>

<?php $sessionagenda=Session::get('agendalogout'); ?>

<script type="text/javascript">

  var agenLogout   = '{{ $sessionagenda }}';
console.log(agenLogout);
  if(agenLogout == "OK"){
    window.onload = function(){ $("#modalAlert").modal("show"); };
  }

</script>

<div class="container">
  <div class="col-sm-3"></div>
  <div class="col-sm-6 center">
   @include('flash::message')
   </br>
   <h3>INICIAR SESIÓN</h3>
   </br>
   <form class="form-signin" action="{{ url('/inicio-sesion') }}" method="POST">
    {{ csrf_field() }}
    <label for="inputEmail" class="sr-only">Correo Electronico</label>
    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required="" autofocus="" placeholder="Correo Electrónico*">

    @if ($errors->has('email'))
    <span class="help-block">
      <strong>{{ $errors->first('email') }}</strong>
    </span>
    @endif
  </br>
  <label for="inputPassword" class="sr-only">password</label>
  <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña*">

  @if ($errors->has('password'))
  <span class="help-block">
    <strong>{{ $errors->first('password') }}</strong>
  </span>
  @endif


  <div class="checkbox">
    <label>
      <input type="checkbox" name="remember"> No cerrar sesión
    </label>
    <label>
      <p><a href="{{ url('contrasena/cambiar-mobile') }}">¿Olvidaste tu contraseña?</a></p>
    </label>
  </div>

  <button class="btn btn-lg btn-block btn-color_2" type="submit"><h4>ENTRAR</h4></button>

  </br>
    <hr>
  </br>
  <h4>¿AÚN NO ESTÁS REGISTRADA?</h4>
  </br>
  <a href="{{ url('/registro-movil') }}">
    <button type="button" class="btn btn-lg btn-block btn-color_2"><h4>REGÍSTRATE</h4></button>
  </a>

</br>

</form>
</div>
<div class="col-sm-3"></div>
</div><!-- /.container -->

</br></br>

<!-- Modal -->
  <div class="modal fade" id="modalAlert" name="modalAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="background: #65534f;">
        <div class="modal-header" style="padding: 0px 10px 0px 10px;border-bottom: 0px solid #e5e5e5;">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 15px;"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"><h1 style="font-size: 18px;">¡Aviso!</h1></h4>
        </div>
        <hr style="border-top: 1px solid #eeba8a;">
        <div class="modal-body" style="color: white;">
          <p id="textModal">Para terminar el agendamiento de tu servicio, debes iniciar sesión ó registrarte si aún no lo has hecho.</p>
        </div><!--Aqui va el mensaje-->
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div> -->
      </div>
    </div>
  </div>
<!--Fin Modal -->

@endsection