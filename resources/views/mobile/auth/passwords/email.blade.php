@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">

<div class="row" style="height: 90px;">
    <div class="col-md-12" style="position:relative;">

        <div class="col-md-12 img_nav" style="position: fixed; z-index: 1;">
            @include('mobile.front.template.menu')
           <!--  @include('mobile.layouts.menu') -->
        </div>
        @include('flash::message')
    </div>

</div>

<div class="container">
    <div class="col-sm-2"></div>
    <div class="col-md-8 center">
        <br>
        <h3>RECUPERACIÓN DE CONTRASEÑA</h3>
        <hr>
            </br>
        @include('flash::message')
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
            <form class="center" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <br>
                    <h4 style="float: left;">CORREO ELECTRÓNICO</h4>
                    <div>
                        <input id="email" type="email" class="form-control" placeholder="Ingresa el Correo Electrónico registrado" name="email" value="{{ old('email') }}" required="" autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <button class="btn btn-lg btn-block btn-color_2" id="submit" type="submit"><h4>RESTABLECER</h4></button>
            </form>
    </div>
    <div class="col-sm-2"></div>
</div>

<br><br>

@endsection
