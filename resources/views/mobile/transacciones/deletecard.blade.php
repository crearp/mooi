@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice(true);?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<style type="text/css">
  .btnMobile {
    position: fixed;
  }
</style>

<div class="row" style="height: 90px;">
	<div class="col-md-12" style="position:relative;">

		<div class="col-md-12 img_nav" style="position: fixed; z-index: 1;">
			@include('mobile.front.template.menu')
			{{-- @include('mobile.layouts.menu') --}}
		</div>
		@include('flash::message')
	</div>

</div>

<!-- Modal -->
	<div class="modal fade" id="modalAlert" name="modalAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="background: #65534f;">
				<div class="modal-header" style="padding: 0px 10px 0px 10px;border-bottom: 0px solid #e5e5e5;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 15px;"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><h1 style="font-size: 18px;">¡Aviso!</h1></h4>
				</div>
				<hr style="border-top: 1px solid #eeba8a;">
				<div class="modal-body" style="color: white;">
					<p id="textModal">¿Realmente deseas eliminar esta tarjeta?</p>
				</div><!--Aqui va el mensaje-->
				<hr style="border-top: 1px solid #eeba8a;">
				<div class="modal-footer" style="border-top: 0px solid #e5e5e5;">
					<button type="button" class="btn btn-primary" id="saveDeleteCard" style=" background-color: #eeba8a;border-color: #eeba8a;">Aceptar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
				</div>
			</div>
		</div>
	</div>
<!--Fin Modal -->

<script language="Javascript">

	window.addEventListener('load', function(){
		$('#optDeleteCard').on('click', function(e){
			e.preventDefault();

			$('#modalAlert').modal('show');
			$('#saveDeleteCard').off();
			$('#saveDeleteCard').on('click',function(e){
				e.preventDefault();
				$('#formDeleteCard').submit();
			})
		})
	}, false);

</script>

<br><br>
<div class="container" style="width: 100%;">
	<div class="col-md-2"></div>
	<div class="col-md-8 center">
		<h3>ELIMINAR TARJETAS</h3>
		<br>
		<hr>
		<br>
		<form action="postdeletecards" method="post" id="formDeleteCard">
			{{ csrf_field() }}
			<h4>SELECCIONA UNA TARJETA</h4>
			<br><br>
			<select name="cards" id="cards" class="form-control"  required>
				@for($x=0;$x<count($cards);$x++)
				<option value="{{ $cards[$x]->card_reference }}" data-valor="{{ $cards[$x]->card_reference }}">{{ $cards[$x]->card_number_encrypted }}
				</option>
				@endfor
			</select>
			<input type="hidden" name="id" id="id" value="{{ $id }}">
		</br>
		<button type="submit" id="optDeleteCard" class="btn btn-lg btn-color" style="color: white;"><h4>ELIMINAR</h4></button>
	</form>
	<br><br><br><br>
</div>
<div class="col-md-2"></div>
</div>

@endsection

