@extends('mobile.layouts.app')
@section('content')
<?php $servidor=webservice();?>
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
	<style>
	#body, #body2 {
		height: 400px;
		width: 100%;
		/*margin: 0;*/
		padding: 15px;
		display: none;
		/*left: 15%;*/
	}
	#map, #map2 {
		height: 100%;
	}

	.controls {
		margin-top: 10px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		border: 3px solid #d8af98;
	}

	.controls2 {
		margin-top: 50px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		z-index: 1;
		position: absolute;
		left: 310px;
		top: 10px;
	}

	#pac-input {
		position: absolute;
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 400;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 70%;
	}
	#pac-input2 {
		position: absolute;
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 120px;
	}

	#pac-input:focus {
		border-color: #4d90fe;
	}

	.pac-container {
		font-family: Roboto;
	}

	#type-selector {
		color: #fff;
		background-color: #4d90fe;
		padding: 5px 11px 0px 11px;
	}

	#type-selector label {
		font-family: Roboto;
		font-size: 13px;
		font-weight: 300;
	}

	#target {
		width: 345px;
	}

	.gmnoprint{
		display: none;
	}
	#pac-input:focus {
	  border-color: #eeba8a;
	}
	</style>
	<div class="row" style="height: 90px;">
	<div style="position:relative;">

		<div class="img_nav">
			@include('mobile.front.template.menu')
			<!-- @include('mobile.layouts.menu') -->
		</div>
		@include('flash::message')
	</div>
	<div class="container"></div><!-- /.container -->

</div>

	{!! Form::open(['route' => 'info_pago', 'method' => 'post', 'class' => 'form-signing', 'id' => 'formblanquito']) !!}
	<div class="container">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 center">
		@include('flash::message')
	</br>
	<h4><a onclick="window.history.back();"><&nbsp;</a>&nbsp;AGENDA TU CITA</h4>
	<input type="hidden" name="agendaMobile" id="agendaMobile" value="true">
	<hr>
	</br></br>
	<h4>DIRECCIÓN DEL SERVICIO</h4>
	</br>
	<div id="divDireccion" class="col-sm-12 center">
		@if(Session::get('iduser') != '')
		<input type="hidden" name="login" id="login" value="true">
		<div class="radio">
			<label><input type="radio" name="direccion" id="direccion2" required="required" value="{{ $direccion }}"  checked="checked" style="display: block !important"><a><p id="UbicacionRegistrada">Direccion registrada: {{ $direccion }}</p></a></label>
		</div>
		<button class="btn btn-lg btn-block btn-color_2" onclick="modalGooglePlace();" style="width: 72%;"><h4>AGREGAR NUEVA</h4></button>
	</br>
	@else
	<input type="hidden" name="login" id="login" value="false">
	<input type="hidden" name="dirSenuelo" id="dirSenuelo" value="false">
	<button class="btn btn-lg btn-block btn-color_2" onclick="modalGooglePlace();" required='required'><h4>AGREGAR NUEVA</h4></button>
	</br>
	@endif
	</div>
	</br>
	<div class="col-sm-12 center" id="body" style="border-style: solid;border-color: #d8af98;border-width: 10px;">
		<input type="hidden" name="dirSenuelo" id="dirSenuelo" value="{{ $direccion }}">
		<input type="hidden" name="boolMap" id="boolMap" value="false">
		<input id="pac-input" class="controls" type="text" placeholder="Ingrese aqui">
		<input id="latlng" type="hidden" value="">
		<input id="boolFirstMap" type="hidden" value="false">
		{{-- <input id="pac-input2" class="controls2" type="button" value="Mi Ubicación" onclick="initMap();"  style="display: block !important"> --}}
		<div id="map"></div>
	</div>
	</br>
	<div class="col-md-12">
		<h4>INDICACIONES</h4>
		<textarea placeholder="Bloque, Apartamento, Casa, etc." class="form-control" name="direccionIndicaciones" id="direccionIndicaciones" style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial"></textarea>
	</div>
	</br>
	<p style="float: left; font-size: 14px;padding-top: 20px;padding-bottom: 20px; margin: 0% 10%;">*En este campo puedes ingresar ademas de la dirección, indicaciones detalladas para llegar al lugar de tu servicio</p>
	</br></br>
	<hr>
			</br></br>
			<div class="col-md-12">
				<h4>TELÉFONO DE CONTACTO</h4>
				<div style="display:inline-block; width: 100%;">
					@if(Session::get('iduser') != '')
					<input class="form-control" id="telefono" name="telefono" value="{{ $tel }}" type="number" required style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial">
					@else
					<input class="form-control" id="telefono" name="telefono" value="" type="number" required style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial">
					@endif
				</div>
			</div>
	</br></br>
	<hr>
	</br></br>
	<div class="col-md-12">
		<h4>AGENDAR FECHA Y HORA</h4>
	</br>
	<!--<input id="fecha" type="date" name="fecha" required=”required” min="<?php echo date("d-m-Y");?>" novalidate="false" onclick="valida(fecha.value)">-->
	<h4 for="fecha">Fecha</h4>
	<div style="display:inline-block; width: 40%;">
		<input id="fecha" name="fecha" type="text" class="form-control datepicker" readonly style="border-radius: 0px; border-color: #eeba8a; border-width: medium; background-color: white;">
	</div>
	</br></br>
	<h4 for="fecha">Hora</h4>
	<div id="content_hour" style="display:inline-block; width: 100%;">
		<input type="text" class="time_element form-control" id="hora" name="hora" required=”required” min="<?php echo time()+1;?>" novalidate="true" readonly style="border-radius: 0px; border-color: #eeba8a; border-width: medium; background-color: white; width: 40%;">
	</div>
	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
	</div>
	<br><br>
	<hr>
	</br></br>

	<div class="col-md-12">
		<h4>MÉTODO DE PAGO</h4>
	</br>
	<select name="metodo" id="metodo" class="form-control"  required style="box-shadow: none; width: 70%; display: none;">
		<option value="0" disabled selected>Selecciona</option>
		@foreach($forms as $fpag)
		<option value="{{ $fpag->payment_id }}">{{ $fpag->payment_name }}</option>
		@endforeach
	</select>
		<div class="row">
			<div class="col-xs-4" id="fpga1">
				<img class="img-responsive" alt="tarjeta de crédito" id="tarjeta_de_crédito" src="{{ url('img/tarjeta_credito.png') }}" onclick="selectMethodPay(1);">
				<p>Tarjeta de</p>
				<p>Crédito</p>
			</div>
			<div class="col-xs-4" id="fpga2">
				<img class="img-responsive" alt="efectivo" id="efectivo" src="{{ url('img/efectivo.png') }}" onclick="selectMethodPay(2);">
				<p>Efectivo</p>
			</div>
			<div class="col-xs-4" id="fpga3">
				<img class="img-responsive" alt="datáfono" id="datafono" src="{{ url('img/datafono.png') }}" onclick="selectMethodPay(3);">
				<p>Datáfono</p>
			</div>
		</div>
	</div>

	</br>
	<div class="col-md-12">
		<br>
	<hr>
	</br></br>
		<h4>COMENTARIOS PARA EL SERVICIO</h4>
		<textarea class="form-control" name="comments" style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial"></textarea>
	</br>
	<p style="float: left; font-size: 14px;padding-top: 20px;padding-bottom: 20px; margin: 0% 10%;">*En éste campo puedes ingresar sugerencias para tu servicio, desde solicitar una estilista en particular como pedir pestañas postizas, extensiones o cualquier requerimiento especial.</p>
	</br>
	</div>
	</br></br></br></br></br></br>

	<input type="hidden" name="dirLat" id="dirLat" value="">
	<input type="hidden" name="dirLng" id="dirLng" value="">
	<input type="hidden" name="arrIdServices" id="arrIdServices" value="<?php echo htmlentities(serialize($arrIdServices)); ?>">
	<input type="hidden" name="valoryservicios" id="valoryservicios" value="<?php echo htmlentities(serialize($valoryservicios)); ?>">
	<input type="hidden" name="valort_trans" id="valort_trans" value="{{$valort_trans}}">
	<button class="btn btn-lg btn-block btn-color" type="submit" id="btn-enviar"><h4>RESUMEN SERVICIO</h4></button>
	<!--  </form> -->
	{!! Form::close() !!}
	</br></br>
	</div>
	<div class="col-sm-3"></div>
	</div><!-- /.container -->



	</br></br></br></br>

<!-- Modal PromoCode-->
<div class="modal fade" id="modalNotiPromoCode" name="modalNotiPromoCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">¡FELICITACIONES!</h4>
      </div>
      <div class="modal-body">
      	Por ser un nuevo usuario MOOI tienes el {{ $promoPopUp['discount'] }}% de descuento en tu primer agendamiento.
      </div><!--Aqui va el mensaje-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">ACEPTAR</button>
      </div>
    </div>
  </div>
</div>
<!--Fin Modal PromoCode-->
<!-- Modal Google Place-->
	<div class="modal fade" id="modalGooglePlaceMap" name="modalGooglePlaceMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body" id="body">
					<input id="pac-input" class="controls" type="text" placeholder="Ingrese aquí">
					<div class="col-md-12" id="map"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
	<!--Fin Modal Google Place-->
	<!-- Modal -->
	<div class="modal fade" id="modalnotificaciones" name="modalnotificaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content" style="background: #65534f;">
				<div class="modal-header" style="padding: 0px 10px 0px 10px;border-bottom: 0px solid #e5e5e5;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 15px;"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><h1 style="font-size: 18px;">ADVERTENCIA</h1></h4>
				</div>
				<hr style="border-top: 1px solid #eeba8a;">
				<div class="modal-body advertencia" style="color: white;"></div><!--Aqui va el mensaje-->
				<!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div> -->
			</div>
		</div>
	</div>
<!--Fin Modal -->
</div>

	<script>


		// variables globales
		var urlTo      = '{{ URL::to("/") }}'
		, autobook     = '{{ $autobook }}'
		, cantidad     = '{{ $cant }}'
		, servicio     = '{{ $styleDefault }}'
		, urlInspirate = '{{ $id }}/{{ $ammount }}/{{ $namestyle }}/1'
		, servidor     = '{{ $servidor }}'
		, category     = '0'
		, idDivParent  = document.getElementById("divStyle-"+servicio)
		, is_new_user  = '{{ $is_new_user }}'
		, contadorSelects = 0
		, data = []
		, cantSeparator = 0
		, categoria = ''
		, select = ''
		, tipe = 'mobile'
		, verify = [];

		if(servicio != '0') category = idDivParent.parentNode.getAttribute("data-category");
		category = category.replace('category-', '');

	//seleccion metodo de pago
	function selectMethodPay(value){
		document.getElementById('metodo').value=value;

  		switch(value) {
		    case 1:
		    	document.getElementById('tarjeta_de_crédito').src="{{ url('img/tarjeta_credito_.png') }}";
		    	document.getElementById('efectivo').src="{{ url('img/efectivo.png') }}";
		    	document.getElementById('datafono').src="{{ url('img/datafono.png') }}";

		        // document.getElementById('fpga1').style.border='solid';
		        // document.getElementById('fpga1').style.borderColor='#eeba8a';


		        // document.getElementById('fpga2').style.border='none';

		        // document.getElementById('fpga3').style.border='none';
		        break;
		    case 2:
		    	document.getElementById('tarjeta_de_crédito').src="{{ url('img/tarjeta_credito.png') }}";
		    	document.getElementById('efectivo').src="{{ url('img/efectivo_.png') }}";
		    	document.getElementById('datafono').src="{{ url('img/datafono.png') }}";

		        // document.getElementById('fpga1').style.border='none';

		        // document.getElementById('fpga2').style.border='solid';
		        // document.getElementById('fpga2').style.borderColor='#eeba8a';

		        // document.getElementById('fpga3').style.border='none';
		        break;
		    case 3:
		    	document.getElementById('tarjeta_de_crédito').src="{{ url('img/tarjeta_credito.png') }}";
		    	document.getElementById('efectivo').src="{{ url('img/efectivo.png') }}";
		    	document.getElementById('datafono').src="{{ url('img/datafono_.png') }}";
		        // document.getElementById('fpga1').style.border='none';

		        // document.getElementById('fpga2').style.border='none';

		        // document.getElementById('fpga3').style.border='solid';
		        // document.getElementById('fpga3').style.borderColor='#eeba8a';
		        break;
		}

	}

	//mapa
	function initAutocomplete() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 4.6097100, lng: -74.0817500},
			zoom: 11,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		var input = document.getElementById('pac-input');
		var searchBox = new google.maps.places.SearchBox(input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});

		var markers = [];
		// [START region_getplaces]
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			var places = searchBox.getPlaces();
			if (places.length == 0) {
				return;
			}

			// Clear out the old markers.
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];
			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();

			places.forEach(function(place) {
				//asignacion de direccion para agendamiento
				console.log(place['geometry']['lan']);
				var dir = document.getElementById('direccion').value = place['formatted_address'];
				console.log(dir);
				document.getElementById('UbicacionManual').innerHTML = 'Dirección agregada: '+dir;
				var icon = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				markers.push(new google.maps.Marker({
					map: map,
					icon: icon,
					title: place.name,
					position: place.geometry.location

				}));

				var lat = place.geometry.location.lat();
				document.getElementById('dirLat').value = lat;
				var lng = place.geometry.location.lng();
				document.getElementById('dirLng').value = lng;

				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
			});
	map.fitBounds(bounds);
	});
		// [END region_getplaces]
	}
	//fin mapa
	function modalGooglePlace(){
		var login = document.getElementById("login").value;
		var boolMap = document.getElementById("boolMap").value;
		var boolFirstMap = document.getElementById("boolFirstMap").value;
		if(boolFirstMap == "false"){
			if(login == "false"){
				document.getElementById("divDireccion").innerHTML="<input type='hidden' name='login' id='login' value='false'><div class='radio'><label onclick='modalGooglePlace();'><input type='radio' name='direccion' id='direccion' required='required' value='' checked='checked' style='display: block !important'><a onclick='modalGooglePlace();'><p id='UbicacionManual'>Agregar dirección</p></a></label></div>";
			}else{
				var dirRegistrada = document.getElementById("dirSenuelo").value;
				document.getElementById("divDireccion").innerHTML="<div class='radio'><label><input type='radio' name='direccion' id='direccion2' required=' value='"+dirRegistrada+"'  checked='checked' style='display: block !important'><a><p id='UbicacionRegistrada'>Direccion registrada: "+dirRegistrada+"</p></a></label></div> <input type='hidden' name='login' id='login' value='true'><div class='radio'><label onclick='modalGooglePlace();'><input type='radio' name='direccion' id='direccion' required='required' value='' checked='checked' style='display: block !important'><a onclick='modalGooglePlace();'><p id='UbicacionManual'>Agregar dirección</p></a></label></div>";
			}
		}
		if(boolMap != "true"){
			document.getElementById("body").style.display = "block";
			document.getElementById("boolMap").value = "true";
			if( boolFirstMap == "false"){
				initAutocomplete();
				document.getElementById("boolFirstMap").value = "true";
			}
		}else{
			document.getElementById("body").style.display = "none";
			document.getElementById("boolMap").value = "false";
		}

		//$('#modalGooglePlaceMap').modal('show')
		//$('#modalGooglePlaceMap').on('shown.bs.modal', function () {})

	}
	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBne-d1CpIF_VBeQQkvE0kdObX-F9cAVX0&libraries=places&callback=initAutocomplete"
	async defer></script>
	@endsection