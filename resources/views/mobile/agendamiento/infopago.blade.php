@extends('mobile.layouts.app')
@section('content')
<link rel="stylesheet" href="{{ asset('mooiMobile.css') }}?{{ rand(0, 9000) }}">
<div class="row" style="height: 90px;">
	<div style="position:relative;">

		<div class="img_nav">
			@include('mobile.front.template.menu')
			<!-- @include('mobile.layouts.menu') -->
		</div>
		@include('flash::message')
	</div>
	<div class="container"></div><!-- /.container -->

</div>
	<br><br>
	<div class="container">
			<div class="col-md-2"></div>
			<div class="col-md-8 center">
			@include('flash::message')
				<h3><a onclick="window.history.back();"><&nbsp;</a>&nbsp;INFORMACIÓN DE PAGO</h3> 
				<hr>
				<br>
				<div class="row">
					<p>*Tu servicio <b>NO ESTÁ AGENDADO</b> en este momento, recuerda que debes finalizarlo presionando el botón AGENDAR SERVICIO.</p>
					<br> 

				</div>
				<?php $session=Session::get('sessionuserC');?>
				@if($session != 'OK')
					<!-- <form action="agendaserviciologout" method="post" onSubmit="return pregunta();"> -->
					{!! Form::open(['route' => 'agenda_servicioslogout', 'method' => 'post', 'id' => 'formInfoPago', 'onsubmit' => 'return confirm("¿Realmente deseas Agendar?");']) !!}
				@else
					<!-- <form action="agendaservicio" method="post" onSubmit="return pregunta();"> -->
					{!! Form::open(['route' => 'agenda_servicios', 'id' => 'formInfoPago', 'method' => 'post']) !!}
				@endif

				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-hover">
							<thead>
								<tr class="bacTitle">
									<th>
										<h4 class="title_table_infopago bacTitle">Detalle</h4>
									</th>
									<th>
										<h4 class="title_table_infopago bacTitle">Cantidad</h4>
									</th>
									<th>
										<h4 class="title_table_infopago bacTitle">Valor</h4>
									</th>
								</tr>
							</thead>
							<tbody>
							@foreach($valoryservicios as $valser)
								<tr>
									<td style="border:none">
										<p class="p_table_infopago">{{$valser[2]}}</p>
									</td>
									<td style="border:none">
										<p class="p_table_infopago">{{$valser[3]}}</p>
									</td>
									<td style="border:none">
										<p class="p_table_infopago">
											<?php
												$valor=number_format($valser[1]*$valser[3]);
											?>
											${{$valor}}
										</p>
									</td>
								</tr>
							@endforeach
								<tr>
									<td>
										<p class="p_table_infopago">
											Recargo fijo por domicilio
										</p>
									</td>
									<td>
										<p class="p_table_infopago">
											1
										</p>
									</td>
									<td>
										<p class="p_table_infopago">
											<?php $festRecargo=number_format($recargo);?>
											${{$festRecargo}}
										</p>
									</td>
								</tr>
								<tr class="bacTitle">
									<td>
										<p class="p_table_infopago">
											SubTotal a pagar
										</p>
									</td>
									<td></td>
									<td>
										<p class="p_table_infopago">
											<?php $valortotal=number_format($valort_trans+$recargo);?>
											${{$valortotal}}
										</p>
									</td>
								</tr>
								@if ($session == 'OK')
							</tbody>
						</table>

					</div>
				</div>
				<hr>
				</br></br></br>
					<div class="col-sm-12">
						<h4 style="text-align: left;">CUPÓN DE DESCUENTO</h4>
						<div class="col-xs-7">
							<input id="cupon" name="cupon" type="text" class="form-control" placeholder="Ingresa tu cupón">
						</div>
						<div class="col-xs-5">
							<button class="btn btn-lg btn-block btn-color" type="button" id="btn-aplicar" onclick="promoCode()"; style="color: white; width: 80%; padding: 5px 0px;">
								<p style="    margin: 0;">APLICAR</p>
							</button>
						</div>
				        
				        </br>
				        <p style="float: left; font-size: 14px;">*Ten en cuenta que una vez sea utilizado el código promocional, no lo podrás volver a usar.</p>
				        <br>
				        </br></br></br></br>
				        
				    </div>
				    <div class="col-sm-12">
						<table class="table table-striped table-hover">
							<tbody>
							@endif
								<tr>
									<td style="background-color:white">
										<p class="p_table_infopago">Descuento por cupón promocional</p>
									</td>
									<td style="background-color:white"></td>
									<td id="descuento" style="background-color:white">
										<p class="p_table_infopago">
											@if($msj_promocode_done == "NO")
											$ 0
											@else
												${{number_format($valorPromoCode)}}
											@endif
										</p>
									</td>
								</tr>
								<tr class="warning">
									<td class="bacTitle">
										<p class="p_table_infopago">Total a pagar</p>
									</td>
									<td class="bacTitle"></td>
									<td class="bacTitle" id="totalPagar">
										<p class="p_table_infopago">
											${{number_format($valortotalcondescuento+$recargo)}}
											<?php $totalPagar=$valortotalcondescuento+$recargo;?>
										</p>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="row">
						
						<div class="col-xs-6">
							@if($valort_trans >= 17000)
								<button class="btn btn-lg btn-block btn-color" type="button" onclick="submitFormInfoPago()">
									<h4>AGENDAR</h4>
								</button>
							@else
								<label>PARA AGENDAR, EL VALOR DEL SERVICIO DEBE SER MAYOR A $20,000 PESOS</label>
							@endif
							<br>
							<hr>
							<br>
						</div>
						<div class="col-xs-6">
							<a href="javascript:history.back(1)"><button class="btn-color" style="padding: 6% 7%;" type="button" id="btn-cancelar"><h4>CANCELAR</h4></button></a>
						</div>
					</div>

					<p>*Todos los valores expuestos ya incluyen iva.</p>
					<p>*Para tu comodidad cobramos 3.000 pesos por domicilios en días hábiles y 5.000 pesos por domingos y festivos.</p>
					<p>*En brevedad buscaremos un profesional para la hora y el día que elegiste ó una persona de MOOI se pondrá en contacto contigo para re programar tu cita. 
	            			Si tu servicio fue agendado con éxito recibirás un correo de confirmación.</p>
			        <!-- <h2>INGRESE SU CODIGO DE PROMOCIÓN</h2>
			        <br>
			        <input type="text" name="discount" id="discount" class="form-control" placeholder="Aqui su codigo de promoción">
			        <br> -->


				</div>
				</br>
				<input type="hidden" name="valort_trans" id="valort_trans" value="{{$valort_trans}}">
				<input type="hidden" name="direccion" id="direccion" value="{{$direccion}}">
				<input type="hidden" name="metodo" id="metodo" value="{{$metodo}}">
				<input type="hidden" name="telefono" id="telefono" value="{{$telefono}}">
				<input type="hidden" name="fecha" id="fecha" value="{{$fecha}}">
				<input type="hidden" name="hora" id="hora" value="{{$hora}}">
				<input type="hidden" name="comments" id="comments" value="{{$comments}}">
				<input type="hidden" name="num" id="num" value="{{$num}}">
				<input type="hidden" name="style" id="style" value="<?php echo htmlentities(serialize($arrIdServices)); ?>">
				<input type="hidden" name="msjcode" id="msjcode" value="{{$msj_promocode_done}}">
				<input type="hidden" name="valcode" id="valcode" value="{{$valorPromoCode}}">

				<!-- </form> -->
				{!! Form::close() !!}
				<br><br><br>
			</div>
			<div class="col-md-2"></div>
	</div>
<!--Modal de confirmacion-->
 <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>

                <div class="modal-body">
                    <p>You are about to delete one track, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                    <p class="debug-url"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
 <!--Fin Modal de confirmacion-->

<script type="text/javascript">

	function submitFormInfoPago(){

		//Facebook Pixel Code -->

		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '733330003482400');
		fbq('track', 'PageView'); fbq('track', 'Lead');

		document.getElementById("formInfoPago").submit();
	}

	function pregunta(){

	    if (confirm('¿Realmente deseas Agendar?'))
        {
            //document.form.submit();//NO hace falta
            return true; //retornamos true para que se envíe
           // document.eliminartarjeta.submit()
        }
        return false; //si llegamos aquí devolvemos false;
	}

	function guardarCupon(){

		var cupon = document.getElementById("cuponHid").value;

		if(cupon != "NADA" && cupon != "null" && cupon != "" ){
			<?php
				$cupon = "<script>document.writeln(cupon);</script>";
				//Session::put('cuponutilizado',$cupon);
			?>
		}
	}

	function promoCode() {

		var strCupon = document.getElementById("cupon").value;

		if(strCupon != ""){

			document.getElementById("btn-aplicar").disabled = true;
			document.getElementById("cupon").disabled = true;

			<?php
	    	$arrStyles=json_encode($arrIdServices);
	    	$intIduser=$id_authuser;
	    	?>

	    	var urlPromocode = "{{URL::to("/")}}/cupon/AquiCupon/{{$intIduser}}/{{$arrStyles}}/{{$totalPagar}}/{{$fecha}}";
	    	var promoCode = document.getElementById("cupon").value;
	    	var urlPromocode = urlPromocode.replace("AquiCupon", promoCode);

	    	$.ajax({
					url: urlPromocode,
					dataType: "json",
					type: "GET",
					data: {},
					success: function (data) {
						if(data['status'] != 'failed'){

							console.log(data['status'])
							console.log(data['data'])
							console.log(data['descuento'])
							console.log(data['total'])
							console.log(data['nombreCupon'])

							if(data['descuento'] > "0"){
								document.getElementById("descuento").innerHTML = "$"+data['descuento'];
								document.getElementById("totalPagar").innerHTML = "$"+data['total'];
								document.getElementById("valort_trans").value = data['total'];
								document.getElementById("valcode").value = data['descuento'];
								document.getElementById("msjcode").value = data['data'];
								document.getElementById("cupon").value = "";

								alert(data['data']);
							}else{
								alert("El valor de descuento es igual o inferior a cero, ingresa otro codigo de descuento");
								document.getElementById("btn-aplicar").disabled = false;
								document.getElementById("cupon").value = "";
								document.getElementById("cupon").disabled = false;
							}
						}else{
							console.log(data['status'])
							console.log(data['data'])

							document.getElementById("btn-aplicar").disabled = false;
							document.getElementById("cupon").value = "";
							document.getElementById("cupon").disabled = false;

							alert(data['data']);
						}


					}
				});
    	}

		//confirm('¿Vamos de putas o que ?');
	}

</script>
<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=733330003482400&ev=PageView&noscript=1"
		/></noscript>
@endsection