<link rel="shortcut icon" href="favicon.ico">
@extends('layouts.app')


@section('content')
<div>

<br><br><br><br><br><br><br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3 class="text-center">
				PAGO DE SERVICIO
			</h3>
			<div class="row">
			@include('flash::message')
				<div class="col-md-3"></div>
				<div class="col-md-6">
						{!! Form::open(array('url' => 'pagos', 'method' => 'post', 'id'=>'formPagos')) !!}
						{!! Form::label('cards', 'Elegir Tarjeta' ) !!}<span class="text-danger"></span>
							<select name="cards" id="cards" class="form-control" required onsubmit="validarSiNumero()">
								@for($x=0;$x<count($cards);$x++)
									<option value="{{ $cards[$x]->card_reference }}" data-valor="{{ $cards[$x]->card_reference }}">{{ $cards[$x]->card_number_encrypted }}
									</option>
								@endfor
							</select>
							<br>
							{!! Form::label('cuotas', 'Número de Cuotas' ) !!}<span class="text-danger"></span>
							<select class="form-control" name="cuotas" id="cuotas">
							  <option value=1>1</option>
							  <option value=2>2</option>
							  <option value=3>3</option>
							  <option value=4>4</option>
							  <option value=5>5</option>
							  <option value=6>6</option>
							  <option value=7>7</option>
							  <option value=8>8</option>
							  <option value=9>9</option>
							  <option value=10>10</option>
							  <option value=11>11</option>
							  <option value=12>12</option>
							</select>
							<br>
							<!-- <label>*Se te notificará de un cobro a tu tarjeta por $100 pesos</label> -->
							<br>
							<input type="hidden" name="address" id="address" value="{{ $address }}">
							<input type='hidden' name='style' value="<?php echo htmlentities(serialize($style)); ?>" />
							<input type="hidden" name="hour" id="hour" value="{{ $hour }}">
							<input type="hidden" name="user_id" id="user_id" value="{{ $user_id }}">
							<input type="hidden" name="metodo" id="metodo" value="{{ $metodo }}">
							<input type="hidden" name="tel" id="tel" value="{{ $tel }}">
							<input type="hidden" name="comments" id="comments" value="{{ $comments }}">
							<input type="hidden" name="valorcupon" id="valorcupon" value="{{ $valorPromoCode }}">
							<input type="hidden" name="lat" id="lat" value="{{$lat}}">
							<input type="hidden" name="lng" id="lng" value="{{$lng}}">
							<p align=center>
								<br>
								<a class="btn btn-link" href="#">
									<button class="btn btn-lg btn-color" id="btnFormPagos" type="button">
										<h4 style="color: white">PAGAR</h4>
									</button>
								</a>
							</p>
							{{ Form::close() }}
							<br>
							<p class="center">
								<a href="{{ url('/changecard') }}">
									<h4 style="color: #EEBA8A; font-size:12px;">ó agregar una tarjeta de crédito.</h4>
								</a>
							</p>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
</div>
<br><br><br><br>
</div>
<div id="modalConfig" style="position:absolute; width:100%; height:100%; top:0; left:0; display:none"></div>

<script type="text/javascript">
	window.addEventListener('load', function(){
		$("#btnFormPagos").on('click', function (e) {
			e.preventDefault();
			//Facebook Pixel Code -->

			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '733330003482400');
			fbq('track', 'PageView'); fbq('track', 'Lead');

			$('#modalConfig').css('display', 'block');
			$('#formPagos').submit();
		})

	}, false);

</script>
<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=733330003482400&ev=PageView&noscript=1"
	/></noscript>
@include('front.template.foot')
@endsection
