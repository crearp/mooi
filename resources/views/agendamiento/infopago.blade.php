@extends('layouts.app')
@section('content')
	<br><br>
	<div class="container">
			<div class="col-md-2"></div>
			<div class="col-md-8 center">
			@include('flash::message')
				<h1>INFORMACIÓN DE PAGO</h1>
				<br>
				<hr>
				<br>
				<div class="row">
					<p>*Tu servicio <b>NO ESTÁ AGENDADO</b> en este momento, recuerda que debes finalizarlo presionando el botón AGENDAR SERVICIO.</p>
					<p>En brevedad buscaremos un profesional para la hora y el día que elegiste. Sin embargo, de no haber disponibilidad en esa franja horaria, te llegará un correo de cancelación y una persona de MOOI se pondrá en contacto contigo vía teléfono o whastapp para re programar tu cita.
						Si tu servicio fue agendado con éxito recibirás un correo de confirmación con el nombre de la profesional asignada y los datos de tu servicio.</p><br>
					<div class="col-md-6">
						@if($valort_trans >= 17000)
							<button class="btn btn-lg btn-block btn-color" type="button" id="btn-enviar" onclick="submitFormInfoPago()"><h4>AGENDAR SERVICIO</h4></button>
						@else
							<label>PARA AGENDAR, EL VALOR DEL SERVICIO DEBE SER MAYOR A $20,000 PESOS</label>
						@endif
						<br>
						<hr>
						<br>
					</div>
					<div class="col-md-6">
						<a href="javascript:history.back(1)"><button class="btn btn-lg btn-block btn-color" type="button" id="btn-cancelar"><h4>CANCELAR</h4></button></a>
						<br>
						<hr>
						<br>
					</div>
				</div>
				<?php $session=Session::get('sessionuserC');?>
				@if($session != 'OK')
					<!-- <form action="agendaserviciologout" method="post" onSubmit="return pregunta();"> -->
					{!! Form::open(['route' => 'agenda_servicioslogout', 'method' => 'post', 'id' => 'formInfoPago', 'onsubmit' => 'return confirm("¿Realmente deseas Agendar?");']) !!}
				@else
					<!-- <form action="agendaservicio" method="post" onSubmit="return pregunta();"> -->
					{!! Form::open(['route' => 'agenda_servicios', 'id' => 'formInfoPago', 'method' => 'post']) !!}
				@endif

				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>
										Detalle
									</th>
									<th>
										Cantidad
									</th>
									<th>
										Valor
									</th>
								</tr>
							</thead>
							<tbody>
							@foreach($valoryservicios as $valser)
								<tr class="active">
									<td>
										{{$valser[2]}}
									</td>
									<td>
										{{$valser[3]}}
									</td>
									<td>
										<?php
											$valor=number_format($valser[1]*$valser[3]);
										?>
										${{$valor}}
									</td>
								</tr>
							@endforeach
							@if($is_new_user == true)
									<tr>
										<td>
											Descuento por primera compra
										</td>
										<td>
											{{ $porcentajePromoCode }}%
										</td>
										<td>
											- ${{number_format($valorPromoCode)}}
										</td>
									</tr>
								@endif
								<tr>
									<td>
										Recargo fijo por domicilio
									</td>
									<td>
										1
									</td>
									<td>
										<?php $festRecargo=number_format($recargo);?>
										${{$festRecargo}}
									</td>
								</tr>
								<tr class="warning">
									<td>
										SubTotal a pagar
									</td>
									<td></td>
									<td>
										<?php $valortotal=number_format($valort_trans+$recargo);?>
										${{$valortotal}}
									</td>
								</tr>
								@if ($session == 'OK')
								@if($is_new_user != true)
							</tbody>
						</table>
							<br>
					        <h4>SI TIENES UN CUPON DE PROMOCIÓN, INGRÉSALO AQUÍ</h4>
					        <br>
					        <input id="cupon" name="cupon" type="text" class="form-control" placeholder="Ingresa tu cupón de descuento aquí">
					        </br>
					        <p style="float: left; font-size: 17px;">*Ten en cuenta que una vez sea agendado el código, no lo podrás volver a utilizar.</p>
					        <br>
							<button class="btn btn-lg btn-block btn-color" type="button" id="btn-aplicar" onclick="promoCode()"><h4>APLICAR</h4></button>
					        </br></br>
					        <hr>
					        </br>

						<table class="table table-striped table-hover">
							<tbody>
							@endif
							@endif
							@if($is_new_user != true)
								<tr>
									<td>
										Descuento por cupón de promoción
									</td>
									<td></td>
									<td id="descuento">
									@if($msj_promocode_done == "NO")
										$ 0
									@else
										${{number_format($valorPromoCode)}}
									@endif
									</td>
								</tr>
							@endif
								<tr class="warning">
									<td>
										Total a pagar
									</td>
									<td></td>
									<td id="totalPagar">
										${{number_format($valortotalcondescuento+$recargo)}}
										<?php $totalPagar=$valortotalcondescuento+$recargo;?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<p>*Todos los valores expuestos ya incluyen iva.</p>
					<p>*Para tu comodidad cobramos 3.000 pesos por domicilios en días hábiles y 5.000 pesos por domingos y festivos.</p>

					<br>
					<hr>
					<br>
			        <!-- <h2>INGRESE SU CODIGO DE PROMOCIÓN</h2>
			        <br>
			        <input type="text" name="discount" id="discount" class="form-control" placeholder="Aqui su codigo de promoción">
			        <br> -->
				</div>
				</br>
				<input type="hidden" name="valort_trans" id="valort_trans" value="{{$valort_trans}}">
				<input type="hidden" name="direccion" id="direccion" value="{{$direccion}}">
				<input type="hidden" name="metodo" id="metodo" value="{{$metodo}}">
				<input type="hidden" name="telefono" id="telefono" value="{{$telefono}}">
				<input type="hidden" name="fecha" id="fecha" value="{{$fecha}}">
				<input type="hidden" name="hora" id="hora" value="{{$hora}}">
				<input type="hidden" name="comments" id="comments" value="{{$comments}}">
				<input type="hidden" name="num" id="num" value="{{$num}}">
				<input type="hidden" name="style" id="style" value="<?php echo htmlentities(serialize($arrIdServices)); ?>">
				<input type="hidden" name="msjcode" id="msjcode" value="{{$msj_promocode_done}}">
				<input type="hidden" name="valcode" id="valcode" value="{{$valorPromoCode}}">
				<input type="hidden" name="lat" id="lat" value="{{$lat}}">
				<input type="hidden" name="lng" id="lng" value="{{$lng}}">

				<!-- </form> -->
				{!! Form::close() !!}
				<br><br><br>
			</div>
			<div class="col-md-2"></div>
	</div>
<!--Modal de confirmacion-->
 <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>

                <div class="modal-body">
                    <p>You are about to delete one track, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                    <p class="debug-url"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok">Delete</a>
                </div>
            </div>
        </div>
    </div>
 <!--Fin Modal de confirmacion-->
@include('front.template.foot')

<script type="text/javascript">

	function submitFormInfoPago(){

		//Facebook Pixel Code -->

		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '733330003482400');
		fbq('track', 'PageView'); fbq('track', 'Lead');

		document.getElementById("formInfoPago").submit();
	}

	function pregunta(){

	    if (confirm('¿Realmente deseas Agendar?'))
        {
            //document.form.submit();//NO hace falta
            return true; //retornamos true para que se envíe
           // document.eliminartarjeta.submit()
        }
        return false; //si llegamos aquí devolvemos false;
	}

	function guardarCupon(){

		var cupon = document.getElementById("cuponHid").value;

		if(cupon != "NADA" && cupon != "null" && cupon != "" ){
			<?php
				$cupon = "<script>document.writeln(cupon);</script>";
				//Session::put('cuponutilizado',$cupon);
			?>
		}
	}

	function promoCode() {

		var strCupon = document.getElementById("cupon").value;

		if(strCupon != ""){

			document.getElementById("btn-aplicar").disabled = true;
			document.getElementById("cupon").disabled = true;

			<?php
	    	$arrStyles=json_encode($arrIdServices);
	    	$intIduser=$id_authuser;
	    	?>

	    	var urlPromocode = "{{URL::to("/")}}/cupon/AquiCupon/{{$intIduser}}/{{$arrStyles}}/{{$totalPagar}}/{{$fecha}}";
	    	var promoCode = document.getElementById("cupon").value;
	    	var urlPromocode = urlPromocode.replace("AquiCupon", promoCode);

	    	$.ajax({
					url: urlPromocode,
					dataType: "json",
					type: "GET",
					data: {},
					success: function (data) {
						if(data['status'] != 'failed'){

							console.log(data['status'])
							console.log(data['data'])
							console.log(data['descuento'])
							console.log(data['total'])
							console.log(data['nombreCupon'])

							if(data['descuento'] > "0"){
								document.getElementById("descuento").innerHTML = "$"+data['descuento'];
								document.getElementById("totalPagar").innerHTML = "$"+data['total'];
								document.getElementById("valort_trans").value = data['total'];
								document.getElementById("valcode").value = data['descuento'];
								document.getElementById("msjcode").value = data['data'];
								document.getElementById("cupon").value = "";

								alert(data['data']);
							}else{
								alert("El valor de descuento es igual o inferior a cero, ingresa otro codigo de descuento");
								document.getElementById("btn-aplicar").disabled = false;
								document.getElementById("cupon").value = "";
								document.getElementById("cupon").disabled = false;
							}
						}else{
							console.log(data['status'])
							console.log(data['data'])

							document.getElementById("btn-aplicar").disabled = false;
							document.getElementById("cupon").value = "";
							document.getElementById("cupon").disabled = false;

							alert(data['data']);
						}


					}
				});
    	}

		//confirm('¿Vamos de putas o que ?');
	}

</script>
<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=733330003482400&ev=PageView&noscript=1"
		/></noscript>
@endsection