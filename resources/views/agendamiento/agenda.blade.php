	@extends('layouts.app')
	@section('js')
	<?php $servidor=url('/');?>

	@endsection

	@section('content')

	<style>
	#body, #body2 {
		height: 400px;
		width: 100%;
		/*margin: 0;*/
		padding: 15px;
		display: none;
		/*left: 15%;*/
	}
	#map, #map2 {
		height: 100%;
	}

	.controls {
		margin-top: 10px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		border: 3px solid #d8af98;
	}

	.controls2 {
		margin-top: 50px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		z-index: 1;
		position: absolute;
		left: 310px;
		top: 10px;
	}

	#pac-input {
		position: absolute;
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 400;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 70%;
	}
	#pac-input2 {
		position: absolute;
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 120px;
	}

	#pac-input:focus {
		border-color: #4d90fe;
	}

	.pac-container {
		font-family: Roboto;
	}

	#type-selector {
		color: #fff;
		background-color: #4d90fe;
		padding: 5px 11px 0px 11px;
	}

	#type-selector label {
		font-family: Roboto;
		font-size: 13px;
		font-weight: 300;
	}

	#target {
		width: 345px;
	}

	.gmnoprint{
		display: none;
	}

	#pac-input:focus {
	  border-color: #eeba8a;
	}
	</style>

	{!! Form::open(['route' => 'info_pago', 'method' => 'post', 'class' => 'form-signing', 'id' => 'formblanquito']) !!}
	<div class="container">
		<div class="col-sm-3"></div>
		<div class="col-sm-6 center">
		@include('flash::message')
	</br>
	<h1>AGENDA TU CITA</h1>
	<input type="hidden" name="agendaMobile" id="agendaMobile" value="false">
	</br>
	<hr>
	</br></br>
	<h4>DIRECCIÓN DEL SERVICIO</h4>
	</br>
	<div id="divDireccion" class="col-sm-12 center">
		@if(Session::get('iduser') != '')
			<input type="hidden" name="login" id="login" value="true">
			<div class="radio">
				<label><input type="radio" name="direccion" id="direccion2" required="required" value="{{ $direccion }}"  checked="checked" style="display: block !important"><a><p id="UbicacionRegistrada">Direccion registrada: {{ $direccion }}</p></a></label>
			</div>
			<button class="btn btn-lg btn-block btn-color_2" onclick="modalGooglePlace();"><h4>AGREGAR NUEVA</h4></button>
			</br>
		@else
			<input type="hidden" name="login" id="login" value="false">
			<input type="hidden" name="dirSenuelo" id="dirSenuelo" value="false">
			<button class="btn btn-lg btn-block btn-color_2" onclick="modalGooglePlace();" required='required'><h4>AGREGAR NUEVA</h4></button>
			</br>
		@endif
	</div>
	</br>
	<div class="col-sm-12 center" id="body" style="border-style: solid;border-color: #d8af98;border-width: 10px;">
		<input type="hidden" name="dirSenuelo" id="dirSenuelo" value="{{ $direccion }}">
		<input type="hidden" name="boolMap" id="boolMap" value="false">
		<input id="pac-input" class="controls" type="text" placeholder="Ingrese dirección aqui">
		<input id="latlng" type="hidden" value="">
		<input id="boolFirstMap" type="hidden" value="false">
		{{-- <input id="pac-input2" class="controls2" type="button" value="Mi Ubicación" onclick="initMap();"  style="display: block !important"> --}}
		<div id="map"></div>
	</div>
	</br>
	<div class="col-md-12">
		<h4>INDICACIONES</h4>
		<textarea class="form-control" name="direccionIndicaciones" id="direccionIndicaciones" style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial"></textarea>
	</div>
	</br>
	<p style="float: left; font-size: 14px;padding-top: 20px;padding-bottom: 20px; margin: 0% 10%;">*En este campo puedes ingresar ademas de la dirección, indicaciones detalladas para llegar al lugar de tu servicio</p>
	</br></br>
	<hr>
	</br></br>
	<div class="col-md-12">
		<h4>ESCOGER SERVICIO(S)</h4>
	</br>
	<?php
	foreach ($arrayStyle as $category => $arrOption) {
							//$category=str_replace("[","",$category);
							//$category=str_replace("/","-",$category);
		$categoryValue=explode("-", $category);
		$categoryArrValue[]=$categoryValue[0];
		$categoryArrValue=array_unique($categoryArrValue);
		$categoryArrValue=array_values($categoryArrValue);
	}

	$contCategory = 0;
	?>
	@foreach ($categoryArrValue as $categoryVal)
	<?php
	$i=0;
	$contCategory++;
	?>
	<div class="col-md-12" id="divDropdown-{{$contCategory}}" style="border-style: solid; border-width: thin; border-color: #eeeeee;">
		<input type="hidden" name="divDropdownBoolean-{{$contCategory}}" id="divDropdownBoolean-{{$contCategory}}" value="false">
		<h4 style="cursor: pointer; text-transform: uppercase; color: #c3c0c0;" for="servicio-{{$contCategory}}" onclick="categoriaDiv('{{$contCategory}}');">{{$categoryVal}}</h4>
		<div data-category="category-{{$contCategory}}" style="display:none;">

			<?php $contStyle = 0; ?>
			@foreach ($arrayStyle as $category => $arrOption)
			<?php
			$contStyle++;
									//$category=str_replace("[","",$category);
									//$category=str_replace("/","-",$category);
			$categoryValue=explode("-", $category);
			$index=str_replace($categoryValue[0]."-", "", $category);
			?>
			@if($categoryValue[0] == $categoryVal)
			<div class="col-md-12" onclick="modalservices({{$contStyle}});" id="divStyle-{{$contStyle}}" name="divStyle-{{$contStyle}}" style="cursor: pointer;">
				<h4 style="text-transform: uppercase; color: #c3c0c0;" for="servicio-{{$contStyle}}">{{$index}}</h4>
				<div id="ContentServices-{{$contStyle}}"><p>Seleccione sus servicios</p></div>
				<br><br>
			</div>
			@endif
			@endforeach
		</div>
	</div>
	@endforeach

	<!-- Modal Servicios -->
	<?php $contStyle = 0; $contadorSelects=0; ?>
	@foreach ($arrayStyle as $category => $arrOption)
	<?php $contStyle++; ?>
	@if($category != 'Combos')
	<?php
								//$category=str_replace("[","",$category);
								//$category=str_replace("/","-",$category);
	?>
	<div class="modal fade" id="modalServicios-{{$contStyle}}" name="modalServicios-{{$contStyle}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog view-services" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">SERVICIOS MOOI</h4>
				</div>
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">
										{{-- Aqui estaba el foreach principal--}}
										<h4 style="text-transform: uppercase; color: #c3c0c0;" for="servicio-{{$contStyle}}">{{$category}}</h4>
										<br>
										<?php
										$contador=0;
										?>
										<div class="row">
											<div class="col-xs-10 col-md-10" style="width: calc(100% - 55px); cursor: pointer;">
												<p>Servicio</p>
											</div>
											<div class="col-xs-2 col-md-2" style="width: 55px;">
												<p>Cantidad</p>
											</div>
										</div>
										<br>
										@foreach($arrOption as $option)
										<?php
										$contador=$contador+1;
										?>
										@foreach ($option->specify_styles as $element)
										<?php

										if($element->specify_style_photo != 'null'){
											$foto = $element->specify_style_photo;
											$foto = str_replace('./','/',$foto);
										}else{
											$foto=$foto;
										}
										$nombre = $option->style_name;
										$descripcion = $element->specify_style_description;

										?>
										@endforeach
										<div class="row">
											<div class="col-xs-10 col-md-10" style="width: calc(100% - 55px); cursor: pointer;">
												<a onclick="cambiar('{{ $foto }}',{{$contStyle}},'{{ $nombre }}','{{ $descripcion }}');"><img id="imgSel" src="{{ $servidor}}/media{{ $foto }}"/>{{ $option->style_name }} - ${{ number_format($option->style_ammount) }}
												</a>
											</div>
											<div id="divAgenda{{ $contadorSelects }}" class="col-xs-2 col-md-2" style="width: 55px;">
												<a onclick="cambiar('{{ $foto }}',{{$contStyle}},'{{ $nombre }}','{{ $descripcion }}');">
													<select class="form-control" data-categoria{{ $contadorSelects }}="{{$contStyle}}-{{ $contador }}" name="cantidad-{{$contStyle}}-{{ $contador }}" id="cantidad-{{$contStyle}}-{{ $contador }}" onchange="selectService('{{$contStyle}}','{{ $contador }}','{{$option->style_name}}','{{number_format($option->style_ammount)}}');">
														<option value="0">0</option>
														<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/1">1</option>
																						<!-- <option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/2">2</option>
																						<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/3">3</option>
																						<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/4">4</option>
																						<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/5">5</option> -->
																					</select>
																				</a>
																			</div>
																			<?php
																				$contadorSelects=$contadorSelects+1;
																			?>
																		</div>
																		<br>
																		@endforeach
																		<br><br>
																		{{-- Aqui termina el foreach principal --}}
																	</div>
																	<div class="col-md-1">
																	</div>
																	<div class="col-md-5" style="position:relative;">
																		<br>
																		<img id="imgSel-{{$contStyle}}" src="{{ $servidor }}/media{{ $foto }}" style="width:100%; height:100%;"/>
																		<div class="col-md-12" style="position:absolute; bottom: 0; background-color: rgba(100, 94, 94, 0.5); width: 100%">
																			<h4 id="titleSel-{{$contStyle}}" style="text-transform: uppercase; color: #EEBA8A;">{{$nombre}}</h4>
																			<p id="descSel-{{$contStyle}}" style="color:white;">{{ $descripcion }}</p>
																		</div>
																	</div>
															<!-- <div class="col-md-1">
														</div> -->
													</div>
												</div>
											</div>
										</div>
									</div><!--Aqui va el mensaje-->
								</div>
							</div>
						</div>
						@else
						<?php
								//$category=str_replace("[","",$category);
								//$category=str_replace("/","-",$category);
						?>
						<div class="modal fade" id="modalServicios-{{$contStyle}}" name="modalServicios-{{$contStyle}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog view-services" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">SERVICIOS MOOI</h4>
									</div>
									<div class="modal-body">
										<div class="container-fluid">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-11">
															{{-- Aqui estaba el foreach principal--}}
															<h4 style="text-transform: uppercase; color: #c3c0c0;" for="servicio-{{$contStyle}}">{{$category}}</h4>
															<br>
															<?php
															$contador=0;
															?>
															<div class="row">
																<div class="col-xs-10 col-md-10" style="width: calc(100% - 55px); cursor: pointer;">
																	<p>Servicio</p>
																</div>
																<div class="col-xs-2 col-md-2" style="width: 55px;">
																	<p>Cantidad</p>
																</div>
															</div>
															<br>
															@foreach($arrOption as $option)
															<?php
															$contador=$contador+1;
															?>
															@foreach ($option->specify_styles as $element)
															<?php

															if($element->specify_style_photo != 'null'){
																$foto = $element->specify_style_photo;
																$foto = str_replace('./','/',$foto);
															}else{
																$foto=$foto;
															}
															$nombre = $option->style_name;
															$descripcion = $element->specify_style_description;

															?>
															@endforeach
															<div class="row">
																<div class="col-xs-10 col-md-10" style="width: calc(100% - 55px); cursor: pointer;">
																	<a><img id="imgSel" src="{{ $servidor}}/media{{ $foto }}"/>{{ $option->style_name }} - ${{ number_format($option->style_ammount) }}
																	</a>
																</div>
																<div id="divAgenda{{ $contadorSelects }}" class="col-xs-2 col-md-2" style="width: 55px;">
																	<select class="form-control" data-categoria{{ $contadorSelects }}="{{$contStyle}}-{{ $contador }}" name="cantidad-{{$contStyle}}-{{ $contador }}" id="cantidad-{{$contStyle}}-{{ $contador }}" onchange="selectService('{{$contStyle}}','{{ $contador }}','{{$option->style_name}}','{{number_format($option->style_ammount)}}');">
																		<option value="0">0</option>
																		<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/1">1</option>
																						<!-- <option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/2">2</option>
																						<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/3">3</option>
																						<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/4">4</option>
																						<option value="{{ $option->style_id }}/{{$option->style_ammount}}/{{$option->style_name}}/5">5</option> -->
																					</select>
																				</div>
																				<?php
																					$contadorSelects=$contadorSelects+1;
																				?>
																			</div>
																			<br>
																			@endforeach
																			<br><br>
																			{{-- Aqui termina el foreach principal --}}
																		</div>
																		<div class="col-md-1">
																		</div>
															<!-- <div class="col-md-1">
														</div> -->
													</div>
												</div>
											</div>
										</div>
									</div><!--Aqui va el mensaje-->
								</div>
							</div>
						</div>
						@endif
						@endforeach
						<!--Fin Modal Servicios-->
					</div>
				</br></br></br></br>
				<hr>
			</br></br>
			<div class="col-md-12">
				<h4>TELÉFONO DE CONTACTO</h4>
				<div style="display:inline-block; width: 100%;">
					@if(Session::get('iduser') != '')
					<input class="form-control" id="telefono" name="telefono" value="{{ $tel }}" type="number" required style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial">
					@else
					<input class="form-control" id="telefono" name="telefono" value="" type="number" required style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial">
					@endif
				</div>
			</div>
	</br></br>
	<hr>
	</br></br>
	<div class="col-md-12">
		<h4>AGENDAR FECHA Y HORA</h4>
	</br>
	<!--<input id="fecha" type="date" name="fecha" required=”required” min="<?php echo date("d-m-Y");?>" novalidate="false" onclick="valida(fecha.value)">-->
	<h4 for="fecha">Fecha</h4>
	<div style="display:inline-block; width: 40%;">
		<input id="fecha" name="fecha" type="text" class="form-control datepicker" readonly style="border-radius: 0px; border-color: #eeba8a; border-width: medium; background-color: white;">
	</div>
	</br></br>
	<h4 for="fecha">Hora</h4>
	<div id="content_hour" style="display:inline-block; width: 100%;">
		<input type="text" class="time_element form-control" id="hora" name="hora" required=”required” min="<?php echo time()+1;?>" novalidate="true" readonly style="border-radius: 0px; border-color: #eeba8a; border-width: medium; background-color: white; width: 40%;">
	</div>
	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
	</div>
	<br><br>
	<hr>
	</br></br>
	<div class="col-md-12">
		<h4>MÉTODO DE PAGO</h4>
	</br>
	<select name="metodo" id="metodo" class="form-control"  required style="box-shadow: none; width: 70%;">
		<option value="0" disabled selected>Selecciona</option>
		@foreach($forms as $fpag)
		<option value="{{ $fpag->payment_id }}">{{ $fpag->payment_name }}</option>
		@endforeach
	</select>
	</div>
	</br>
	<div class="col-md-12">
		<br>
	<hr>
	</br></br>
		<h4>COMENTARIOS PARA EL SERVICIO</h4>
		<textarea class="form-control" name="comments" style="border-left: 0px;border-right: none;border-top: none;-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none; border-radius: 0px; width: 70%; border-color: #eeba8a; border-bottom-width: initial"></textarea>
	</br>
	<p style="float: left; font-size: 14px;padding-top: 20px;padding-bottom: 20px; margin: 0% 10%;">*En éste campo puedes ingresar sugerencias para tu servicio, desde solicitar una estilista en particular como pedir pestañas postizas, extensiones o cualquier requerimiento especial.</p>
	</br>
	</div>
	</br></br></br></br></br></br>

	<input type="hidden" name="dirLat" id="dirLat" value="">
	<input type="hidden" name="dirLng" id="dirLng" value="">

	<button class="btn btn-lg btn-block btn-color" type="submit" id="btn-enviar"><h4>RESUMEN SERVICIO</h4></button>
	<!--  </form> -->
	{!! Form::close() !!}
	</br></br>
	</div>
	<div class="col-sm-3"></div>
	</div><!-- /.container -->



	</br></br></br></br>

<!-- Modal PromoCode-->
<div class="modal fade" id="modalNotiPromoCode" name="modalNotiPromoCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">¡FELICITACIONES!</h4>
      </div>
      <div class="modal-body">
      	Por ser un nuevo usuario MOOI tienes el {{ $promoPopUp['discount'] }}% de descuento en tu primer agendamiento.
      </div><!--Aqui va el mensaje-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">ACEPTAR</button>
      </div>
    </div>
  </div>
</div>
<!--Fin Modal PromoCode-->
<!-- Modal Google Place-->
	<div class="modal fade" id="modalGooglePlaceMap" name="modalGooglePlaceMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body" id="body">
					<input id="pac-input" class="controls" type="text" placeholder="Ingrese aquí">
					<div class="col-md-12" id="map"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
	<!--Fin Modal Google Place-->
	<!-- Modal -->
	<div class="modal fade" id="modalnotificaciones" name="modalnotificaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">ADVERTENCIA</h4>
				</div>
				<div class="modal-body advertencia"></div><!--Aqui va el mensaje-->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
<!--Fin Modal -->
</div>

	<script>


		// variables globales
		var urlTo      = '{{ URL::to("/") }}'
		, autobook     = '{{ $autobook }}'
		, cantidad     = '{{ $cant }}'
		, servicio     = '{{ $styleDefault }}'
		, urlInspirate = '{{ $id }}/{{ $ammount }}/{{ $namestyle }}/1'
		, servidor     = '{{ $servidor }}'
		, category     = '0'
		, idDivParent  = document.getElementById("divStyle-"+servicio)
		, is_new_user  = '{{ $is_new_user }}'
		, contadorSelects = '{{ $contadorSelects }}'
		, data = []
		, cantSeparator = 0
		, categoria = ''
		, select = ''
		, tipe = 'desktop'
		, verify = [];

		if(servicio != '0') category = idDivParent.parentNode.getAttribute("data-category");
		category = category.replace('category-', '');
	//mapa
	function initAutocomplete() {
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 4.6097100, lng: -74.0817500},
			zoom: 11,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		var input = document.getElementById('pac-input');
		var searchBox = new google.maps.places.SearchBox(input);
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		// Bias the SearchBox results towards current map's viewport.
		map.addListener('bounds_changed', function() {
			searchBox.setBounds(map.getBounds());
		});

		var markers = [];
		// [START region_getplaces]
		// Listen for the event fired when the user selects a prediction and retrieve
		// more details for that place.
		searchBox.addListener('places_changed', function() {
			var places = searchBox.getPlaces();
			if (places.length == 0) {
				return;
			}

			// Clear out the old markers.
			markers.forEach(function(marker) {
				marker.setMap(null);
			});
			markers = [];
			// For each place, get the icon, name and location.
			var bounds = new google.maps.LatLngBounds();

			places.forEach(function(place) {
				//asignacion de direccion para agendamiento
				console.log(place['geometry']['lan']);
				var dir = document.getElementById('direccion').value = place['formatted_address'];
				console.log(dir);
				document.getElementById('UbicacionManual').innerHTML = 'Dirección agregada: '+dir;
				var icon = {
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				markers.push(new google.maps.Marker({
					map: map,
					icon: icon,
					title: place.name,
					position: place.geometry.location

				}));

				var lat = place.geometry.location.lat();
				document.getElementById('dirLat').value = lat;
				var lng = place.geometry.location.lng();
				document.getElementById('dirLng').value = lng;

				if (place.geometry.viewport) {
					// Only geocodes have viewport.
					bounds.union(place.geometry.viewport);
				} else {
					bounds.extend(place.geometry.location);
				}
			});
	map.fitBounds(bounds);
	});
		// [END region_getplaces]
	}
	//fin mapa
	function modalGooglePlace(){
		var login = document.getElementById("login").value;
		var boolMap = document.getElementById("boolMap").value;
		var boolFirstMap = document.getElementById("boolFirstMap").value;
		if(boolFirstMap == "false"){
			if(login == "false"){
				document.getElementById("divDireccion").innerHTML="<input type='hidden' name='login' id='login' value='false'><div class='radio'><label onclick='modalGooglePlace();'><input type='radio' name='direccion' id='direccion' required='required' value='' checked='checked' style='display: block !important'><a onclick='modalGooglePlace();'><p id='UbicacionManual'>Agregar dirección</p></a></label></div>";
			}else{
				var dirRegistrada = document.getElementById("dirSenuelo").value;
				document.getElementById("divDireccion").innerHTML="<div class='radio'><label><input type='radio' name='direccion' id='direccion2' required=' value='"+dirRegistrada+"'  checked='checked' style='display: block !important'><a><p id='UbicacionRegistrada'>Direccion registrada: "+dirRegistrada+"</p></a></label></div> <input type='hidden' name='login' id='login' value='true'><div class='radio'><label onclick='modalGooglePlace();'><input type='radio' name='direccion' id='direccion' required='required' value='' checked='checked' style='display: block !important'><a onclick='modalGooglePlace();'><p id='UbicacionManual'>Agregar dirección</p></a></label></div>";
			}
		}
		if(boolMap != "true"){
			document.getElementById("body").style.display = "block";
			document.getElementById("boolMap").value = "true";
			if( boolFirstMap == "false"){
				initAutocomplete();
				document.getElementById("boolFirstMap").value = "true";
			}
		}else{
			document.getElementById("body").style.display = "none";
			document.getElementById("boolMap").value = "false";
		}

		//$('#modalGooglePlaceMap').modal('show')
		//$('#modalGooglePlaceMap').on('shown.bs.modal', function () {})

	}
	</script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBne-d1CpIF_VBeQQkvE0kdObX-F9cAVX0&libraries=places&callback=initAutocomplete"
	async defer></script>
	@include('front.template.foot')
	@endsection