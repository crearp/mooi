@extends('layouts.app')

@section('content')

<script type="text/javascript">
function checkForm(form)
  {
    if(form.password.value == "") {
      alert("Error: Password no puede estar en blanco");
      form.password.focus();
      return false;
    }

    if(form.password.value != "" && form.password.value == form.password_confirmation.value) {
      if(form.password.value.length < 6) {
        alert("Error: La contraseña debe contener al menos seis caracteres !");
        form.password.focus();
        return false;
      }
    } else {
      alert("Error: Por favor, compruebe la confirmacion de su contraseña");
      form.password.focus();
      return false;
    }

    return true;
  }

</script>


<div class="container">
    <div class="col-sm-2"></div>
    <div class="col-sm-8 center">
    </br></br></br>
    <h2>RECUPERACIÓN DE CONTRASEÑA</h2>
        </br>
        <hr>
        </br>
        <form class="form-signin" role="form" method="POST" action="{{ url('/password/reset') }}" onsubmit="return checkForm(this);">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id" value="{{ $id }}">

            <div class="col-md-12 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <br>
                <h4 style="float: left;">INGRESE SU NUEVA CONTRASEÑA</h4>
                <div class="col-md-12 center">
                    <input id="password" type="password" class="form-control" name="password">
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-md-12 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <br>
                <h4 style="float: left;">CONFIRME SU NUEVA CONTRASEÑA</h4>
                <div class="col-md-12 center">
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <br>
            <br>
        <button class="btn btn-lg btn-primary btn-block btn-color_2" type="submit"><h4>RESTABLECER</h4></button>
        </form>
    </div>
    <div class="col-sm-2"></div>
</div><!-- /.container -->

@endsection
