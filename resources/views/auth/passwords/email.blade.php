@extends('layouts.app')

<!-- Main Content -->
@section('content')

<div class="container">
    <div class="col-sm-2"></div>
    <div class="col-md-8 center">
        <br>
        <h2>RECUPERACIÓN DE CONTRASEÑA</h2>
            </br>
        <hr>
            </br>
        @include('flash::message')
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
            <form class="center" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <br>
                    <h4 style="float: left;">CORREO ELECTRÓNICO</h4>
                    <div>
                        <input id="email" type="email" class="form-control" placeholder="Ingresa el Correo Electrónico registrado" name="email" value="{{ old('email') }}" required="" autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <button class="btn btn-lg btn-block btn-color_2" id="submit" type="submit"><h4>RESTABLECER</h4></button>
            </form>
    </div>
    <div class="col-sm-2"></div>
</div>

<br><br>
@include('front.template.foot')

@endsection
