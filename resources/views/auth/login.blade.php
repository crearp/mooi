@extends('layouts.app')
@section('content')

<?php $sessionagenda=Session::get('agendalogout'); ?>

<script type="text/javascript">

  var agenLogout   = '{{ $sessionagenda }}';
console.log(agenLogout);
  if(agenLogout == "OK"){
    window.onload = function(){ $("#modalAlert").modal("show"); };
  }

</script>

<div class="container">
  <div class="col-sm-3"></div>
  <div class="col-sm-6 center">
   @include('flash::message')
   </br>
   <h4>INICIAR SESIÓN</h4>
   </br>
   <form class="form-signin" action="{{ url('/inicio-sesion') }}" method="POST">
    {{ csrf_field() }}
    <label for="inputEmail" class="sr-only">Correo Electronico</label>
    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required="" autofocus="" placeholder="Correo Electrónico*">

    @if ($errors->has('email'))
    <span class="help-block">
      <strong>{{ $errors->first('email') }}</strong>
    </span>
    @endif
  </br>
  <label for="inputPassword" class="sr-only">password</label>
  <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña*">

  @if ($errors->has('password'))
  <span class="help-block">
    <strong>{{ $errors->first('password') }}</strong>
  </span>
  @endif


  <div class="checkbox">
    <label>
      <input type="checkbox" name="remember"> No cerrar sesión
    </label>
    <label>
      <p><a href="{{ url('/contrasena/cambiar') }}">¿Olvidaste tu contraseña?</a></p>
    </label>
  </div>

  <button class="btn btn-lg btn-block btn-color_2" type="submit"><h4>ENTRAR</h4></button>

  </br>
    <hr>
  </br>
  <h4>¿AÚN NO ESTÁS REGISTRADA?</h4>
  </br>
  <a href="{{ url('/registro') }}">
    <button type="button" class="btn btn-lg btn-block btn-color_2"><h4>REGÍSTRATE</h4></button>
  </a>

</br>

</form>
</div>
<div class="col-sm-3"></div>
</div><!-- /.container -->

</br></br>

<div class="modal fade" tabindex="-1" role="dialog" id="modalAlert">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">¡Aviso!</h4>
      </div>
      <div class="modal-body">
        <p id="textModal">Para terminar el agendamiento de tu servicio, debes iniciar sesión ó registrarte si aún no lo has hecho.</p>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@include('front.template.foot')
@endsection