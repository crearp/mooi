@extends('layouts.app')

@section('content')

<script type="text/javascript">

  function checkForm(form)
  {
    //Validacion terminos y condiciones
    var elemento = document.getElementById("tyc");
    if( !elemento.checked ) {
      /*alert("Error: Para continuar debes leer y aceptar los terminos y condiciones.");*/
      $('#modalnotificaciones').modal('show')
      $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('Para continuar debes leer y aceptar los terminos y condiciones.');
        form.tyc.focus();
      })
      return false;
    }
    //Fin validacion terminos y condiciones
    if(form.email.value == "") {
      $('#modalnotificaciones').modal('show')
      $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('El Email no puede estar en blanco.');
        form.email.focus();
      })
      return false;
    }
    if(form.email.value == form.confirEmail.value) {

    }else {
      $('#modalnotificaciones').modal('show')
      $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('Los correos electrónicos ingresados no coinciden.');
        form.confirEmail.focus();
      })
      return false;
    }
    //validacion campo telefono
    re = /[a-z]/;
      if(re.test(form.phone.value)) {
        $('#modalnotificaciones').modal('show')
        $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('El campo teléfono solo debe contener números.');
        form.phone.focus();
        })
        return false;
      }
      re = /[A-Z]/;
      if(re.test(form.phone.value)) {
        $('#modalnotificaciones').modal('show')
        $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('El campo teléfono solo debe contener números.');
        form.phone.focus();
        })
        return false;
      }
      if(form.phone.value.length < 7) {
        $('#modalnotificaciones').modal('show')
        $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('El campo teléfono debe contener al menos siete números.');
        form.phone.focus();
        })
        return false;
      }
      if(form.phone.value.length > 15) {
        $('#modalnotificaciones').modal('show')
        $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('El campo teléfono debe contener menos de quince números.');
        form.phone.focus();
        })
        return false;
      }
//fin validacion campo telefono
    if(form.password.value != "" && form.password.value == form.password_confirmation.value) {
      if(form.password.value.length < 6) {
        $('#modalnotificaciones').modal('show')
        $('#modalnotificaciones').on('shown.bs.modal', function () {
        $('.advertencia').html('La contraseña debe contener al menos seis caracteres.');
        form.password.focus();
        })
        return false;
      }
      /*if(form.password.value == form.email.value) {
        alert("Error: La contraseña debe ser diferente del nombre de usuario !");
        form.password.focus();
        return false;
      }
      re = /[0-9]/;
      if(!re.test(form.password.value)) {
        alert("Error: La contraseña debe contener al menos un número ( 0-9 ) !");
        form.password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password.value)) {
        alert("Error: La contraseña debe contener al menos una letra minúscula ( a-z ) !");
        form.password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password.value)) {
        alert("Error: La contraseña debe contener al menos una letra mayúscula ( A- Z ) !");
        form.password.focus();
        return false;
      }*/
    } else {
      $('#modalnotificaciones').modal('show')
      $('#modalnotificaciones').on('shown.bs.modal', function () {
      $('.advertencia').html('Las contraseñas no coinciden.');
      form.password.focus();
      })
      return false;
    }

    return true;
  }


  //Facebook Pixel Code -->
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '733330003482400');
  fbq('track', 'PageView');
  fbq('track', 'Lead');

  function FacePixel(){
    // CompleteRegistration
    // Track when a registration form is completed (ex. complete subscription, sign up for a service)
    fbq('track', 'CompleteRegistration');
  }

</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=733330003482400&ev=PageView&noscript=1"
  /></noscript>
<div class="container">
  <div class="col-sm-3"></div>
  <div class="col-sm-6 center">
    @include('flash::message')
</br></br></br>
<h2>REGISTRO</h2>
</br>
<hr>
</br><!--
<form class="form-signin" role="form" method="POST" action="{{ url('/registro') }}" onsubmit="return checkForm(this);"> -->
{!! Form::open(['route' => 'post-registro', 'method' => 'post', 'class' => 'form-signing', 'role' => 'form', 'onsubmit' => 'return checkForm(this);']) !!}

    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
        <h4 style="float: left;">CONTACTO</h4>
        <input type="text" id="first_name" class="form-control" placeholder="Nombre *" name="first_name" value="{{ old('first_name') }}" required autofocus="">
        @if ($errors->has('first_name'))
        <span class="help-block">
            <strong>{{ $errors->first('first_name') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('first_lastname') ? ' has-error' : '' }}">
        <input id="first_lastname" type="text" class="form-control" placeholder="Apellido *" name="first_lastname" value="{{ old('first_lastname') }}" required>
        @if ($errors->has('first_lastname'))
        <span class="help-block">
            <strong>{{ $errors->first('first_lastname') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
        <input id="phone" type="" class="form-control" placeholder="Teléfono *" name="phone" value="{{ old('phone') }}" required>
        @if ($errors->has('phone'))
        <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
            <input id="direccion" type="text" class="form-control" placeholder="Dirección *" name="direccion" value="{{ old('direccion') }}" required>
            @if ($errors->has('direccion'))
            <span class="help-block">
                <strong>{{ $errors->first('direccion') }}</strong>
            </span>
            @endif
    </div>

<h4 style="float: left;">EMAIL</h4>
</br>
<p style="float: left; font-size: 17px;">*Este correo electrónico lo utilizaremos para enviarte las confirmaciones de tus servicios y recuperar tu contraseña</p>
</br>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          @if($email != "")
            <input id="email" type="email" class="form-control" placeholder="Correo Electrónico *" name="email" value="{{ $email }}" required>
          @else
            <input id="email" type="email" class="form-control" placeholder="Correo Electrónico *" name="email" value="{{ old('email') }}" required>
          @endif
            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
    </div>

@if($email != "")
  <input type="email" id="confirEmail" name="confirEmail" class="form-control" placeholder="Confirmación Correo Electrónico *" required="" value="{{ $email }}">
@else
  <input type="email" id="confirEmail" name="confirEmail" class="form-control" placeholder="Confirmación Correo Electrónico *" required="">
@endif
</br>

<h4 style="float: left;">CONTRASEÑA</h4>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" placeholder="Contraseña *" class="form-control" name="password" required>
        @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
        <input id="password_confirmation" type="password" placeholder="Confirmar Contraseña *" class="form-control" name="password_confirmation">
        @if ($errors->has('password_confirmation'))
        <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
        @endif
    </div>


<h4 style="float: left;">¿POR QUÉ MEDIO DESEA RECIBIR INFORMACIÓN DE PROMOCIONES, NOVEDADES Y EVENTOS?</h4>

    <div class="form-group{{ $errors->has('medio') ? ' has-error' : '' }}">
        <select id="medio" class="form-control" name="medio">
            <option value="0">Ninguno</option>
            <option value="1">Correo Electrónico</option>
            <option value="2">Mensaje de texto</option>
        </select>
        @if ($errors->has('medio'))
        <span class="help-block">
            <strong>{{ $errors->first('medio') }}</strong>
        </span>
        @endif
    </div>

    <div class="checkbox">
        <label><input type="checkbox" id="tyc" name="tyc"><a target="_blank" href="{{ url('/terminos-y-condiciones') }}">Acepto Términos y Condiciones</a></label>
    </div>

    </br>
    <h4>CAMPOS REQUERIDOS *</h4>
    </br>
    <button class="btn btn-lg btn-block btn-color_2" onclick="FacePixel();" type="submit" id="submit"><h4>REGISTRARME</h4></button>
    </br></br>
<!-- </form> -->
{!! Form::close() !!}
</div>
<div class="col-sm-3"></div>
</div><!-- /.container -->


<!-- Modal -->
<div class="modal fade" id="modalnotificaciones" name="modalnotificaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADVERTENCIA</h4>
      </div>
      <div class="modal-body advertencia"></div><!--Aqui va el mensaje-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--Fin Modal-->

@include('front.template.foot')
@endsection