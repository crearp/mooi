<link rel="shortcut icon" href="favicon.ico">
@extends('layouts.app')


@section('content')

<br><br><br><br><br><br><br>
<div class="panel panel-default">
                <div class="panel-heading"> <p align=center>CANCELACION DEL SERVICIO</p></div>
                    <div class="panel-heading">
                    		<p align=center>¿Realmente desea cancelar el servicio <b>@foreach($nombre as $sus1) {{ $sus1->nombre_servicio }} @endforeach </b> por el increible valor de <b> @foreach($valor as $sus2) {{ $sus2->valor_total }} @endforeach </b> pesos Colombianos?</p> 

							{{ Form::open(array('url' => 'cancelarconfir', 'method' => 'post'))}}
								
									<input type="hidden" name="id" id="id"  value="{{ $id }}" >
									<input type="hidden" name="id2" id="id2"  value="{{ $id2 }}" >
								
	                            <p align=center><button type="submit" id="submit" class="btn btn-primary">
	                                Cancelar
	                            </button></p>

	                        {{ Form::close() }}
                    </div>
            </div>
<br>

<br><br><br><br>
@include('front.template.foot')

@endsection

