<html>
	<head>
		<h4>Futur@ MOOI</h4>
	</head>
	<body>
	<br><br>

		<p>Nombres y Apellidos: {{$request->get('nya')}}</p>

		<p>Teléfono: {{$request->get('tel')}}</p>

		<p>Dirección: {{$request->get('dir')}}</p>

		<p>Ciudad: {{$request->get('ciudad')}}</p>

		<p>Correo Electrónico: {{$request->get('email')}}</p>

		<p>¿Tiene Vehículo?: {{$request->get('vehiculo')}}</p>

		<p>¿Tiene Licencia?: {{$request->get('licencia')}}</p>

		<p>¿Cómo te enteraste?: </p>
		{{$request->get('como')}}

		<p>Por quien fue refereid@: {{$request->get('amigo')}}</p>

		<p>Perfil Profesional: </p>
		{{$request->get('perfil')}}

		<p>Área en la que {{$request->get('nya')}} quisiera trabajar: </p>
		<?php
			foreach ($areas as $area) {
				echo "-> ";
				echo $area."<br>";
			}
		?>
	</body>
</html>