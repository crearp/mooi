@extends('layouts.app')

@section('content')
@include('flash::message')

<?php
	$cardlogout = Session::get('agendacards');

	if(isset($cardlogout)){ $cardlogout=$cardlogout; }
	else{ $cardlogout='NO'; }

	$cardloginagendando = Session::get('agendaaddcardenlogin');

	if(isset($cardloginagendando)){ $cardloginagendando=$cardloginagendando; }
	else{ $cardloginagendando='NO'; }
?>
<script type="text/javascript">

	var contador     = 0
	,	iniciado     = false
	,	fin_contador = 1 					// Tiempo en en el que deseas que redireccione la funcion.
	,	cardlogout   = '{{$cardlogout}}'
	,	cardloginagendando = '{{$cardloginagendando}}';

	function load(){
		if(contador == fin_contador){

			$('#modalAlert').modal('show');

			$('#modalAlert').on('hidden.bs.modal', function (e) {
				if(cardlogout != 'OK' ){
					if(cardloginagendando == 'NO'){
						window.location.href = "{{ url('/') }}";
					}
					else{window.location.href = "{{ url('/kjhsuy43uy78x') }}";}
				}
				else{ window.location.href = "{{ url('/kjhsuy43uy78x') }}"; }
			})
		}
		else{ fin_contador--; }
	}

</script>

<div class="container">
	<div class="col-md-2"></div>
	<div class="col-md-8 center">
		<h2>AGREGAR TARJETAS DE CRÉDITO</h2>
		<br>
		<hr>
		<br>
		<p class="text-justify">
			Estamos comprometidos contigo a cumplir nuestra misión: ser el enlace oficial entre estilistas profesionales, aliados y tú. De esta manera obtienes belleza al instante en el lugar que prefieras y sin esfuerzos, para eso requerimos los datos de tu tarjeta de crédito, así los pagos podrán ser online o con datafono el día de tu cita si lo deseas. Tus datos siempre estarán protegidos. MOOI no tiene acceso a tus números de tarjetas ni contraseñas.
		</p>
		<br>
		<div class="row">
			<div class="col-md-12">
				<iframe id="paymentezadd" class="embed-responsive-item" src="{{$UrlAnadirTarjeta}}" onLoad="load(this)" width=100% height=100% marginwidth="0" marginheight="0" border="0" style="background:#E7E6E6;height: 300px;"></iframe>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>

<br><br><br><br>
<!-- Modal -->
<div class="modal fade" id="modalnotificaciones" name="modalnotificaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ADVERTENCIA</h4>
      </div>
      <div class="modal-body advertencia"></div><!--Aqui va el mensaje-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--Fin Modal-->

<div class="modal fade" tabindex="-1" role="dialog" id="modalAlert">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">¡Aviso!</h4>
			</div>
			<div class="modal-body">
				<p id="textModal">Tu tarjeta ha sido agregada correctamente, te invitamos a disfrutar de nuestros servicios.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="saveFormHistory" data-dismiss="modal">Aceptar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('front.template.foot')
@endsection