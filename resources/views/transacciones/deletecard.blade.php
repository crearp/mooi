@extends('layouts.app')
@section('content')

<div class="modal fade" tabindex="-1" role="dialog" id="modalAlert">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">¡Aviso!</h4>
			</div>
			<div class="modal-body">
				<p id="textModal">¿Realmente deseas eliminar esta tarjeta?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="saveDeleteCard">Aceptar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script language="Javascript">

	window.addEventListener('load', function(){
		$('#optDeleteCard').on('click', function(e){
			e.preventDefault();

			$('#modalAlert').modal('show');
			$('#saveDeleteCard').off();
			$('#saveDeleteCard').on('click',function(e){
				e.preventDefault();
				$('#formDeleteCard').submit();
			})
		})
	}, false);

</script>

<br><br>
<div class="container">
	<div class="col-md-2"></div>
	<div class="col-md-8 center">
		<h2>ELIMINAR TARJETAS</h2>
		<br>
		<hr>
		<br>
		<form action="postdeletecards" method="post" id="formDeleteCard">
			{{ csrf_field() }}
			<h4>SELECCIONA UNA TARJETA</h4>
			<br><br>
			<select name="cards" id="cards" class="form-control"  required>
				@for($x=0;$x<count($cards);$x++)
				<option value="{{ $cards[$x]->card_reference }}" data-valor="{{ $cards[$x]->card_reference }}">{{ $cards[$x]->card_number_encrypted }}
				</option>
				@endfor
			</select>
			<input type="hidden" name="id" id="id" value="{{ $id }}">
		</br>
		<button type="submit" id="optDeleteCard" class="btn btn-lg btn-color" style="color: white;"><h4>ELIMINAR</h4></button>
	</form>
	<br><br><br><br>
</div>
<div class="col-md-2"></div>
</div>

@include('front.template.foot')

@endsection

