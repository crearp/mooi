@extends('layouts.app')
@section('content')

  <div class="container">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 center">
        <br><br><br>
        <h1>BLOG MOOI</h1>
        <br>
        <hr>
        @foreach($blog as $info)
          <div class="row">
            <div class="col-md-12">
            <br><br>
            <h4>{{$info->title}}</h4>
            <br><br>
              <div class="row">
                <div class="col-md-6">
                 <img class="img-responsive" src="<?php echo $servidor; ?>/media<?php echo $info->photo; ?>">
                </div>
                <div class="col-md-6" style="text-align: -webkit-left;text-align: -moz-left;">
                {{$info->content}}
                </div>
              </div>
            </div>
          </div>
        <br><br>
        <hr>
        @endforeach
        <br><br>
      </div>
      <div class="col-sm-1"></div>
    </div>
  <br><br><br>
@include('front.template.foot')
@endsection