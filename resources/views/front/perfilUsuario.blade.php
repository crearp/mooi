@extends('layouts.app')
@section('content')

<script type="text/javascript">

	window.addEventListener('load', function(){
		$('#optDeleteCard').on('click', function(e){
			e.preventDefault();

			$('#modalAlert').modal('show');
			$('#saveDeleteCard').off();
			$('#saveDeleteCard').on('click',function(e){
				e.preventDefault();
				$('#formDeleteCard').submit();
			})
		})

	var medio = {{$IntMedComuni}};
	document.formulario_envio.comunicaciones.value=medio;

	}, false);

  	function cancel(){

	    if (confirm('¿Realmente deseas cancelar este servicio?'))
        {
                //document.form.submit();//NO hace falta
           	return true; //retornamos true para que se envíe
               // document.eliminartarjeta.submit()
        }
        return false; //si llegamos aquí devolvemos false;
	}

	function finish(){

		if (confirm('¿Realmente deseas finalizar este servicio?'))
	    {
	                //document.form.submit();//NO hace falta
	        return true; //retornamos true para que se envíe
	               // document.eliminartarjeta.submit()
	    }
	    return false; //si llegamos aquí devolvemos false;
	}

</script>

<div class="container">
      </br></br></br>
        <h1 class="center">PERFIL DE USUARIO</h1>
        </br></br>
      @include('flash::message')

	<div class="row">
		<!-- <div class="col-md-4 text-center">
			<img alt="Bootstrap Image Preview" src="https://t3.ftcdn.net/jpg/01/05/72/60/500_F_105726077_mMZzmT08rVm2pckrBfecs6l3QNjvGnrt.jpg" WIDTH=280 HEIGHT=210 class="img-rounded" />
		</div> -->
		<div class="col-md-12 center">
		{!! Form::open(['route' => 'actualizar_perfil', 'method' => 'post', 'name' =>'formulario_envio','onsubmit' => 'return confirm("¿Realmente deseas Editar esta información?");']) !!}
			<table class="table text-center">
				<tbody>
				<tr class="active">
					<td>
						<b>NOMBRES:</b>
					</td>
					<td>
						<input type="text" class="form-control" name="nombre" id="nombre" value="{{$nombre}}"/>
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>APELLIDOS:</b>
					</td>
					<td>
						<input type="text" class="form-control" name="apellido" id="apellido" value="{{$apellido}}"/>
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>TELÉFONO:</b>
					</td>
					<td>
						<input type="text" class="form-control"  name="telefono" id="telefono" value="{{$telefono}}"/>
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>DIRECCIÓN:</b>
					</td>
					<td>
						<input type="text" class="form-control" name="direccion" id="direccion" value="{{$direccion}}"/>
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>CORREO:</b>
					</td>
					<td>
						<input type="text" class="form-control" name="correo" id="correo" value="{{$correo}}" disabled/>
					</td>
				</tr>
				<tr class="active">
					<td>
						<b>¿Por qué medio desea recibir promociones y publicidad?:</b>
					</td>
					<td>
						Tu medio actual es: {{$StrMedComuni}}
						<br>
						<select name="comunicaciones" id="comunicaciones" class="form-control">
			                <option value=0 >Ninguno</option>
			                <option value=1 >Correo electrónico</option>
			                <option value=2 >Mensaje de texto</option>
			                <option value=3 >Email y SMS</option>
			            </select>
					</td>
				</tr>
				</tbody>
			</table>
			<button type="submit" class="btn btn-lg btn-color" style="color: white;">
				GUARDAR
			</button>
		{!! Form::close() !!}
		</div>
	</div>
		<div class="row">
			<div class="col-md-12 text-center" >
				<br><br>
				<hr>
				<br><br>
				<h2>ELIMINAR TARJETAS</h2>
					<br>

					<br>
					{!! Form::open(['route' => 'posteliminar_cards', 'method' => 'post', 'id' => 'formDeleteCard']) !!}

						<br><br>
						<select name="cards" id="cards" class="form-control"  required>
							@for($x=0;$x<count($cards);$x++)
							<option value="{{ $cards[$x]->card_reference }}" data-valor="{{ $cards[$x]->card_reference }}">{{ $cards[$x]->card_number_encrypted }}
							</option>
							@endfor
						</select>
						<input type="hidden" name="id" id="id" value="{{ $id }}">
					</br>
					<button type="submit" id="optDeleteCard" class="btn btn-lg btn-color" style="color: white;"><h4>ELIMINAR</h4></button>
				{!! Form::close() !!}
				<br><br>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<br><br>
				<hr>
				<br><br>
				<h2>AGREGAR TARJETAS</h2>
				<br><br>
				<a href="/changecard"><button type="button" class="btn btn-lg btn-color" style="color: white;">
				AGREGAR TARJETA
				</button><a>
				<br><br>
			</div>
		</div>
	</div>
	<br><br>
    <div class="col-md-3"></div>
</div><!-- /.container -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalAlert">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">¡Aviso!</h4>
			</div>
			<div class="modal-body">
				<p id="textModal">¿Realmente deseas eliminar esta tarjeta?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="saveDeleteCard">Aceptar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@include('front.template.foot')
@endsection