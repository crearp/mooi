@extends('layouts.app')
@section('content')

  <div class="container">
      <div class="col-sm-1"></div>
      <div class="col-sm-10 center">
        <br><br><br>
        <h1>NOVIAS</h1>
        <br>
        <hr>
        <div class="col-md-5">
          <img class="img-responsive" src="{{ url('img/Novias_mooi.png') }}">
        </div>
        <div class="col-md-1">
        </div>
        <div class="col-md-6" style="text-align: -webkit-left;text-align: -moz-left;">
          <h4 id="novias">Ni locos nos perderíamos el mejor día de tu vida</h4>
          <hr>
          <p>Por eso en Bogotá y a las afueras de la ciudad llegará un equipo de profesionales expertos en matrimonios, así tu peinado, uñas y maquillaje serán de ensueño el día de tu boda. Para garantizar tu satisfacción, ellos te realizarán una prueba de maquillaje y peinado previa al gran día.</p>

          <p>Dependiendo de las necesidades que tengas, podrás armar un paquete personalizado para ti, tu familia y amigas, con precios especiales y productos de las mejores marcas como Dior, Chanel, Makeup, Bobby Brown, MAC, Forever, entre otras.</p>

          <p>Y no te preocupes por más, en los paquetes también puedes incluir servicios para la fiesta de compromiso, shower, despedida de soltera, día de la boda y los que se te ocurran.</p>

          <p>Ya diste el SÍ más importante, ahora dile SÍ a ser una novia MOOI.</p>

          <p>Para más información sobre nuestros servicios de novia llámanos al +57 (316) 525-3993, escríbenos por WhatsApp o envíanos un email a info@mooibelleza.com</p>
        </div>
      </div>
      <div class="col-sm-1"></div>
    </div>

  <br><br><br>
@include('front.template.foot')
@endsection