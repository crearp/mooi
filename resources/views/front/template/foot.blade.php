@yield('content')
    <footer class="footer" style="padding: 30px 0 0 0;">
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <br><br>
                <p>MOOI</p>
                <p>CALLE 79 # 10 - 80 OFC 205</p>
                <p>BOGOTÁ - COLOMBIA</p>
                <p>TEL. +57 316 5253993</p>
            </div>
            <div class="col-sm-4 col-md-4 center" >
                <p>PARTNERS</p>
                <img class="img-responsive" alt="Belleza por un futuro" src="{{ url('img/Beleza_Futuro.png') }}?<?= rand(0, 9000); ?>">
            </div>
            <?php
                $url= $_SERVER["REQUEST_URI"];
                $mystring = $url;
                $findme   = 'perfilprofessional';
                $pos = strpos($mystring, $findme);
            ?>
            @if($pos === false )
                <div class="col-sm-4 col-md-4 center">
                    <br><br>
                    <a href="{{ url('/') }}"><img class="img-responsive img_footer" src="{{ url('image/logo_MOOI.png') }}?<?= rand(0, 9000); ?>"></a>
                </div>
            @else
                <div class="col-sm-4 col-md-4 center">
                    <br><br>
                    <img class="img-responsive img_footer" src="{{ url('image/logo_MOOI.png') }}">
                </div>
            @endif
        </div>
        <br><br>
        <div class="row" style="padding-bottom: 0;">
        @if($pos === false )
            <div class="col-xs-12 col-sm-8 col-md-8 center">
                <a href="{{ url('contacto') }}"><span>CONTACTO&nbsp;&nbsp;&nbsp;&nbsp;</spam></a>
                <!--<a href="{{ url('/trabaja-con-nosotros') }}"><span>TRABAJA CON NOSOTROS&nbsp;&nbsp;&nbsp;&nbsp;</spam></a>-->
                <a href="{{ url('/politicas-privacidad') }}"><span>PRIVACIDAD&nbsp;&nbsp;&nbsp;&nbsp;</spam></a>
                <a href="{{ url('/terminos-y-condiciones') }}"><span>TÉRMINOS Y CONDICIONES&nbsp;&nbsp;&nbsp;&nbsp;</spam></a>
                <a href="{{ url('/politicas-cancelacion') }}"><span>CANCELACIÓN&nbsp;&nbsp;&nbsp;&nbsp;</spam></a>
            </div>
        @endif
            <div class="col-xs-12 col-sm-4 col-md-4 center">
                <a href="https://www.instagram.com/mooibelleza/" target="_blank"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="https://www.facebook.com/MooiBelleza/" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="row center" style="
    background-color: #eeba8a;
    padding-bottom: 5px;
    padding-top: 15px;
    margin-top: 20px;
">
            <div class="col-md-12 left">
                <p style="
    text-transform: uppercase;
    font-size: 12px;
">Copyright © 2016   |  <a href="http://creardigital.com/" target="blank" style="
    color: white;
">
                Desarrollado por: Crear Digital.
                </a></p>
            </div>
        </div>
    </footer>
    @yield('js', '')