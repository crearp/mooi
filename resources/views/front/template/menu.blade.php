<nav class="navbar navbar-inverse">
	<div class="header">
		<div class="navbar-header center">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="center responsive" title="inicio">
				<a href="{{ url('/') }}" style="display:inline-block;">
					<img alt="icono mooi" src="{{ url('img/icon_MOOI.png') }}?<?= rand(0, 9000); ?>">
				</a>
			</div>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<a href="{{ url('/') }}">
				<img class="img-responsive" alt="icono menu mooi" src="{{ url('img/logo_mooi_footer.png') }}?<?= rand(0, 9000); ?>" style="width:300px;">
			</a>
			<ul class="nav navbar-nav">
				<li><a class="color" href="{{ url('/agenda') }}"><h4>AGENDA TU CITA</h4></a></li>
				<?php
					foreach ($menustyles->styles as $value) {
						if($value->category_name != 'Combos' && $value->category_name != 'Bonos'){
							//$stylesValue1 = explode("[", $value->category_name);
							$stylesValue2 = explode("-", $value->category_name);
							$cantStyles[]=$stylesValue2[0];
						}
					}
					$cantStyles=array_unique($cantStyles);
					$cantStyles=array_values($cantStyles);
				?>
				@foreach($cantStyles as $cant)
					<li class="dropdown">
						<a class="color" href="{{ url('/inicio') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><H4>{{$cant}}<span class="caret"></span></H4> </a>
						<ul class="dropdown-menu">
							@foreach ($menustyles->styles as $styles)
								<?php
									$category=strpos($styles->category_name, $cant);
								?>
								@if($styles->category_name != 'Combos' &&  $category !== false && $styles->category_name != 'Bonos')
									<li><a href="{{ url('/servicios/'.$styles->category_name) }}"><h4 style="text-transform: uppercase">{{ str_replace("$cant-","",$styles->category_name) }}</h4></a></li>

								@endif
							@endforeach
						</ul>
					</li>
				@endforeach
				<!-- <li><a class="color" href="#servicios"><h4>SERVICIOS</h4></a></li> -->
				<li><a class="color" href="{{ url('/porque-nosotros') }}"><h4>¿POR QUÉ MOOI?</h4></a></li>
				<!--<li><a class="color" href="{{ url('/inicio') }}"><h4>REGALOS</h4></a></li>-->
				<!--<li><a class="color" href="#how_work"><h4>CÓMO FUNCIONA</h4></a></li>-->
				<li><a class="color" href="{{ url('/novias') }}"><h4>NOVIAS</h4></a></li>
				<!--<li class="dropdown">
					<a class="color" href="{{ url('/inicio') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><H4>NOVIAS<span class="caret"></span></H4> </a>
					<ul class="dropdown-menu">
						<li><a class="color" href="{{ url('/inicio') }}"><h4>INSPIRACIÓN</h4></a></li>
						<li><a class="color" href="{{ url('/inicio') }}"><h4>SOLUCITUDES</h4></a></li>
					</ul>
				</li>
				<li><a class="color" href="https://www.blogger.com"><h4>BLOG</h4></a></li>-->
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>

