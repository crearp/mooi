@extends('layouts.app')
@section('content')

<div id="how_work">
  <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6 center">
      </br></br></br>
      <h1>CÓMO FUNCIONA</h1>
      </br>
      <hr>
    </div>
    <div class="col-sm-3"></div>
  </div>
  </br></br>

  <div class="row">

      <div id="" class="col-sm-6 col-md-3 center how_work_pad">
        <div class="how_work_one"></div>
        <br>
        <h4 class="gris">PASO 1</h4>
        <h4>ELIGE Y AGENDA</h4>
        <hr>
        <p class="gris">Elige el servicio que más te guste, selecciona el método de pago que prefieres y agenda tu cita. Recuerda ingresar el día, la hora, la dirección y todos tus datos en el agendamiento.</p>
        <br><br>
      </div>
      <div class="col-sm-6 col-md-3 center how_work_pad">
        <div class="how_work_three"></div>
        <br>
        <h4 class="gris">PASO 2</h4>
        <h4>¡YA ESTÁ!</h4>
        <hr>
        <p class="gris">Recibirás un mail de confirmación con los datos de tu reserva y 8 horas previas a la cita te enviaremos un email de recordatorio.</p>
        <br><br>
      </div>
      <div class="col-sm-6 col-md-3 center how_work_pad">
        <div class="how_work_four"></div>
        <br>
        <h4 class="gris">PASO 3</h4>
        <h4>VIVE MOOI</h4>
        <hr>
        <p class="gris">El día de tu cita, los profesionales llegarán al destino que elegiste puntualmente. ¡Ahora disfruta tu experiencia!</p>
        <br><br>
      </div>
        <div class="col-sm-6 col-md-3 center how_work_pad">
        <div class="how_work_two"></div>
        <br>
        <h4 class="gris">PASO 4</h4>
        <h4>CALIFICA TU EXPERIENCIA</h4>
        <hr>
        <p class="gris">Para nosotros es muy importante darte siempre el mejor servicio, con tu calificación nos ayudas a mejorar.</p>
        <br><br>
      </div>
    </div>


</div><!-- como funciona -->

  </br></br>
@include('front.template.foot')
@endsection

