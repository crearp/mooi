@extends('layouts.app')
@section('content')

<div class="row">
	<div style="position:relative;">
		<div class="img_nav">
			@include('front.template.menu')
		</div>
		<img class="img-responsive img_banner" style="width:100%; margin:auto;" alt="banner home mooi" src="{{ url('img/home_banner_with_stores.png') }}?<?= rand(0, 9000); ?>">
		<div class="img_nav2">
			<span>Belleza con</span>
			<br>
			<span>conciencia</span>
			<br>
			<span>a un click</span>
			<hr>
			<P>Tu equipo de profesionales en belleza donde quieras, cuando quieras.</P>
			<a href="{{ url('/agenda') }}">
				<button class="btn btn-lg btn-block btn-color_2" type="submit">
					<h4 style="color:white" >AGENDA</h4>
				</button>
			</a>
		</div>
		@include('flash::message')
	</div>

	<div class="container"></div><!-- /.container -->

	<div id="servicios" class="servicios"><!--servicios-->
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 center">
				<br><br>
				<h1>SERVICIOS</h1>
				<hr>
			</div>
			<div class="col-sm-3"></div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-1">
			</div>
			<div class="col-md-2 center space how_work_pad">
				<a>
					<div class="sprite_pasos_servicios service_nails"></div>
				</a>
				<br><br>
				<h4>UÑAS</h4>
				<br>
				<p>Múltiples opciones de acuerdo a tu gusto y tendencias.</p>
			</div>

			<div class="col-md-2 center space how_work_pad">
				<a>
					<div class="sprite_pasos_servicios service_hair"></div>
				</a>
				<br><br>
				<h4>CABELLO</h4>
				<br>
				<p>Tu mejor accesorio como más te gusta, en manos expertas.</p>
			</div>

			<div class="col-md-2 center space how_work_pad">
				<a>
					<div class="sprite_pasos_servicios service_makeup"></div>
				</a>
				<br><br>
				<h4>MAQUILLAJE</h4>
				<br>
				<p>Mirada impactante y natural con looks únicos.</p>
			</div>

			<div class="col-md-2 center space how_work_pad">
				<a>
					<div class="sprite_pasos_servicios service_hair_removal"></div>
				</a>
				<br><br>
				<h4>DEPILACIÓN</h4>
				<br>
				<p>Depilación con hilo y cera.</p>
			</div>
			<div class="col-md-2 center space how_work_pad">
				<a>
					<div class="sprite_pasos_servicios service_masaje"></div>
				</a>
				<br><br>
				<h4>MASAJE</h4>
				<br>
				<p>Masaje tailandes y reflexología.</p>
			</div>
			<div class="col-md-1">
			</div>
		</div>
	</div><!--servicios-->
	<div id="fundacion" class="b_color"><!--fundacion-->
      <div class="row">

        <div class="col-md-6 center" style="padding-left: 0px;">
          <img src="{{ url('img/fundacion.png') }}?<?= rand(0, 9000); ?>" alt="fundación L´oreal mooi" class="img-responsive">
        </div>

        <div class="col-md-6 center fundacion">
          <h2>Belleza con conciencia</h2>
          </br>
          <p>En MOOI estamos comprometidos con nuestro país por lo que promovemos una plataforma de belleza con sentido social. En ese sentido, y en alianza con L’OREAL y la Fundación Belleza por un Futuro, buscamos acercar a las mujeres graduadas de esta fundación las cuales se encuentran en condición de vulnerabilidad por ser víctimas de la violencia y de la pobreza extrema, a una oportunidad laboral que beneficie su futuro y el de su familia. Las profesionales de la belleza graduadas de la Fundación Belleza por un Futuro son nuestro principal motor y la principal fuente de estilistas que tenemos en nuestra página.</p>

          <p>Año tras año, a través de su programa de Responsabilidad Social Belleza por un Futuro, el grupo L'Oréal Colombia brinda educación en peluquería a cientos de colombianas que han hecho parte de este programa y desde el 2009 hasta hoy ha graduado a más de 750 mujeres.</p>
        </div>
      </div>
    </div><!--fundacion-->

	<div class="section" id="how_work">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 center">
				<br><br><br>
				<h2 class="gris">CÓMO FUNCIONA</h2>
				<br>
				<hr>
			</div>
			<div class="col-sm-3"></div>
		</div>
		<br><br>

		<div class="row">

			<div id="" class="col-sm-6 col-md-3 center how_work_pad">
				<div class="sprite_pasos_servicios how_work_one"></div>
				<br>
				<h4 class="gris">PASO 1</h4>
				<h4>ELIGE Y AGENDA</h4>
				<hr>
				<p class="gris">Elige el servicio que más te guste, selecciona el método de pago que prefieres y agenda tu cita. Recuerda ingresar el día, la hora, la dirección y todos tus datos en el agendamiento.</p>
				<br><br>
			</div>
			<div class="col-sm-6 col-md-3 center how_work_pad">
				<div class="sprite_pasos_servicios how_work_three"></div>
				<br>
				<h4 class="gris">PASO 2</h4>
				<h4>¡YA ESTÁ!</h4>
				<hr>
				<p class="gris">Recibirás un mail de confirmación con los datos de tu reserva y 2 horas previas a la cita te enviaremos un email de recordatorio.</p>
				<br><br>
			</div>
			<div class="col-sm-6 col-md-3 center how_work_pad">
				<div class="sprite_pasos_servicios how_work_four"></div>
				<br>
				<h4 class="gris">PASO 3</h4>
				<h4>VIVE MOOI</h4>
				<hr>
				<p class="gris">El día de tu cita, los profesionales llegarán al destino que elegiste puntualmente. ¡Ahora disfruta tu experiencia!</p>
				<br><br>
			</div>
				<div class="col-sm-6 col-md-3 center how_work_pad">
				<div class="sprite_pasos_servicios how_work_two"></div>
				<br>
				<h4 class="gris">PASO 4</h4>
				<h4>CALIFICA TU EXPERIENCIA</h4>
				<hr>
				<p class="gris">Para nosotros es muy importante darte siempre el mejor servicio, con tu calificación nos ayudas a mejorar.</p>
				<br><br>
			</div>
		</div>
	</div><!-- como funciona -->


	<div style="margin-top:100px"><!--Disponibilidad-->
		<div class="row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 center">
				<div class="col-sm-2"><hr></div>
				<div class="col-sm-8">
					<h4>ESTAMOS EN BOGOTÁ - LUNES A SÁBADO DE 6AM A 6PM , DOMINGOS Y FESTIVOS DE 8AM A 4PM.</h4>
					<br>
					<img class="img-responsive center" alt="bogotá Bogotá mooi" src="{{ url('img/Bogota_.png') }}?<?= rand(0, 9000); ?>">
					<br>
					<h4>BOGOTÁ</h4>
					<br><br><br><br>
				</div>
				<div class="col-sm-2"><hr></div>
				<br>
			</div>
			<div class="col-sm-1"></div>
		</div>
	</div><!--Disponibilidad-->

</div>

<?php $urlBackend = webservice(true);?>
<!-- Modal -->
<div class="modal fade" id="modalPromoCodeGrand" name="modalPromoCodeGrand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" style="width: 850px; margin: 100px auto;" role="document">
		<div class="modal-content" style="width: 902px;">
			{{-- <div class="modal-header" style="width: 900px;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">PROMOCODE-GRANDE</h4>
			</div> --}}
			<div class="modal-body" style=" position: relative; height: 400px; width: 900px; background: url('{{ $urlBackend }}/media{{ $promoPopUp['photo'] }}');">
			<!-- <div class="modal-body" style=" position: relative; height: 400px; width: 900px; background: url('img/promoEscritorio.jpg');" onclick="location.href='/agenda';">-->
				<div class="col-md-12">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<form action="regPromoCode" method="post" class="form-signing">
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<div class="col-md-8" style="top: 227px; left: 392px;">
						<input style="border: none; width: 40.5%; height: 37px;" type="text" name="email_promoCode" id="email_promoCode" class="form-control" placeholder="Ingresa tú email aquí" required="required">
					</div>
					<div class="col-md-4" style="top: 225px; left: 60px;">
						<button style="height: 43px; padding: 1px 18px;" class="btn btn-lg btn-block btn-color" type="button" onclick="location.href='/agenda';"><h4>AGENDA</h4></button>
					</div>
				</form>
			</div><!--Aqui va el mensaje-->
			{{-- <div class="modal-footer" style="width: 900px;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div> --}}
		</div>
	</div>
</div>
<!--Fin Modal-->

<!-- Modal -->
<div class="modal fade" id="modalPromoCodeMin" name="modalPromoCodeMin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" style="margin: 15% 0%; text-align: -webkit-center; text-align: -moz-center;" role="document">
		<div class="modal-content" style="width: 320px;">
			{{-- <div class="modal-header" style="width: 900px;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">PROMOCODE-GRANDE</h4>
			</div> --}}
			<div class="modal-body" style=" position: relative; height: 320px; width: 320px; background: url('{{ $urlBackend }}/media{{ $promoPopUp['photo_responsive'] }}');">
			<!-- <div class="modal-body" style=" position: relative; height: 530px; width: 320px; background: url('img/promoResponsive.png');" onclick="location.href='/agenda';"> -->
				<div class="col-md-12">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<form action="regPromoCode" method="post" class="form-signing">
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<div class="col-md-8" style="top: 215px; float: left; text-align: left; width: 170px;">
						<input style="border: none; width: 100%; height: 30px;" type="text" name="email_promoCode" id="email_promoCode" class="form-control" placeholder="Ingresa tú email aquí" required="required">
					</div>
					<div class="col-md-4" style="top: 196px; float: left; margin-right: 1px;">
						<button style="height: 29px; padding: 0; width: 43%; min-width: 110px;" class="btn btn-lg btn-block btn-color" type="button" onclick="location.href='/agenda';"><h4 style="font-size: 12px; margin-top: 8px;">AGENDA</h4></button>
					</div>
				</form>
			</div><!--Aqui va el mensaje-->
			{{-- <div class="modal-footer" style="width: 900px;">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div> --}}
		</div>
	</div>
</div>
<!--Fin Modal-->
	@include('front.template.foot')
	@endsection