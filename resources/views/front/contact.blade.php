@extends('layouts.app')
@section('content')
<div class="container">
      <div class="col-sm-3"></div>
      <div class="col-sm-6 center">
        @include('flash::message')
        </br></br></br>
        <h1>CONTÁCTATE CON NOSOTROS</h1>
        </br>
        <hr>
        </br>
        <form class="form-signin" action="contacmail" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <h4>Correo Electrónico:</h4>
        <input type="email" id="correo" name="correo" class="form-control" placeholder="Correo Electrónico" required="" autofocus="">
        </br>
        <h4>Asunto:</h4>
        <input type="text" id="asunto" name="asunto" class="form-control" placeholder="Asunto" required="" autofocus="">
        </br>
        <h4>Mensaje:</h4>
        <textarea class="form-control" name="cuerpo" cols="50" rows="10" id="cuerpo"></textarea>
        </br></br></br>
        <button class="btn btn-lg btn-block btn-color_2" type="submit"><h4>ENVIAR</h4></button>
      </form>
      </div>
      <div class="col-sm-3"></div>
    </div><!-- /.container -->

  </br></br>
@include('front.template.foot')
@endsection

