@extends('layouts.app')
@section('content')

<div class="container">
      <div class="col-sm-3"></div>
      <div class="col-sm-6 center">
        @include('flash::message')
        </br></br></br>
        <h1>TRABAJA CON NOSOTROS</h1>
        </br>
        <hr>
        </br>

        <form class="form-signin" action="contacmailtra" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <h4 style="text-transform: uppercase">Nombres y Apellidos</h4>
        <input type="text" id="nya" name="nya" class="form-control" placeholder="Nombres y Apellidos" required="" autofocus="">
        </br></br></br>
        <h4 style="text-transform: uppercase">Teléfono</h4>
        <input type="tel" id="tel" name="tel" class="form-control" placeholder="Teléfono" required="">
        </br></br></br>
        <h4 style="text-transform: uppercase">Dirección</h4>
        <input type="text" id="dir" name="dir" class="form-control" placeholder="Dirección" required="">
        </br></br></br>
        <h4 style="text-transform: uppercase">Ciudad</h4>
        <input type="text" id="ciudad" name="ciudad" class="form-control" placeholder="Ciudad" required="">
        </br></br></br>
        <h4 style="text-transform: uppercase">Correo Electrónico</h4>
        <input type="email" id="email" name="email" class="form-control" placeholder="Correo Electrónico" required="">
        </br></br></br>

        <div>
          <h4 style="text-transform: uppercase">Tienes vehículo</h4>
          <!--<input type="radio" id="vehiculo" name="vehiculo" value="si" checked> Si
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="radio" id="vehiculo" name="vehiculo" value="no"> No<br>
          </br></br></br>-->
          <select style="width: 30%;" name="vehiculo" id="vehiculo" class="form-control"  required>
            <option value="" disabled selected>Selecciona</option>
            <option value="si">Si</option>
            <option value="no">No</option>
          </select>
          </br></br></br>
        </div>

        <div>
          <h4 style="text-transform: uppercase">¿Cuentas con licencia cosmetológica?</h4>
          <!--<input type="radio" id="vehiculo" name="vehiculo" value="si" checked> Si
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="radio" id="vehiculo" name="vehiculo" value="no"> No<br>
          </br></br></br>-->
          <select style="width: 30%;" name="licencia" id="licencia" class="form-control"  required>
            <option value="" disabled selected>Selecciona</option>
            <option value="si">Si</option>
            <option value="no">No</option>
          </select>
          </br></br></br>
        </div>

        <h4 style="text-transform: uppercase">¿Cómo te enteraste de Mooi?</h4>
        <textarea id="como_se" name="como" rows="6" cols="70"></textarea>
        </br></br></br>
        <h4 style="text-transform: uppercase">Si fuiste referida por un amigo, indica su nombre aquí</h4>
        <input type="text" id="amigo" name="amigo" class="form-control" placeholder="Escribe su nombre aquí" required="" autofocus="">
        </br></br></br>
        <h4 style="text-transform: uppercase">Por favor, copia y pega tu perfil aquí</h4>
        <textarea id="perfil_you" name="perfil" rows="6" cols="70" required=""></textarea>
        </br></br></br>
        <h4 style="text-transform: uppercase">Área en la que quisieras trabajar</h4>
        <div class="checkbox">
          <p><input type="checkbox" value="Manicure y pedicure" name="areas[]">Manicure y pedicure</p>
        </div>
        <div class="checkbox">
          <p><input type="checkbox" value="Manicure y pedicure semipermanete" name="areas[]">Manicure y pedicure semipermanete</p>
        </div>
        <div class="checkbox">
          <p><input type="checkbox" value="Peinados" name="areas[]">Peinados</p>
        </div>
        <div class="checkbox">
          <p><input type="checkbox" value="Maquillaje" name="areas[]">Maquillaje</p>
        </div>
        </br></br></br>
        <h4 style="text-transform: uppercase">Adjunta una foto de tu trabajo (requerido)</h4>
        <input id="imagen1" name="imagen1" type="file" required="">
        </br></br></br>
        <h4 style="text-transform: uppercase">Adjunta una segunda foto de tu trabajo (requerido)</h4>
        <input id="imagen2" name="imagen2" type="file" required="">
        </br></br></br>
        <h4 style="text-transform: uppercase">Adjunta una tercera foto de tu trabajo (requerido)</h4>
        <input id="imagen3" name="imagen3" type="file" required="">
        </br>
        </br></br>
        <div class="checkbox">
          <label><input type="checkbox" required><a href="{{ url('/terminos-y-condiciones') }}" target="_blank">Acepto Términos y Condiciones</a></label>
        </div>
        </br></br></br>
        <button class="btn btn-lg btn-block btn-color_2" type="submit"><h4>ENVIAR SOLICITUD</h4></button>
        </br></br></br>
      </form>

      </div>
      <div class="col-sm-3"></div>
    </div><!-- /.container -->

  </br></br>
@include('front.template.foot')
@endsection

