@extends('layouts.app')
@section('content')
<link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
    <div class="wrapper col-md-12">
            <br>
            <?php
                $stylesValue1 = explode("[", $i);
                $stylesValue2 = explode("]", $stylesValue1[1]);
                //$cantStyles[]=$stylesValue2[0];
            ?>
        <h2 style="text-transform: uppercase" class="text-center">{{ $stylesValue2[1] }}</h2>
            <br><br>
        <div id="accordion-wrapper" class="accordion_style" style="width:100%;">
            @foreach($content as $t => $ca)
                @foreach($ca->specify_styles as $cb)
                    <div class="slide">
                        <img class="" src="<?php echo $servidor; ?>/media/<?php echo $cb->specify_style_photo; ?>"/>
                        <div class="caption center" style=" width: 100%;">
                            <h4 style="font-size: 40px; text-transform: capitalize;">{{$cb->specify_style_name}}</h4>
                            <p>{{$cb->specify_style_description}}</p>
                        </br>
                            <?php $valor=number_format($ca->style_ammount); ?>
                            <p>PRECIO: ${{$valor}}</p>
                            <hr>

                            <a href="autobook/{{$i}}/{{$ca->style_id}}/{{$ca->style_ammount}}/{{$ca->style_name}}">
                                <p>
                                    <!-- <h4>-AGENDAR-</h4> -->
                                    <button class="btn btn-lg btn-block btn-color">AGENDAR</button>
                                </p>
                            </a>
                        </div>
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>

<div class="container" style="margin-bottom: 80px;"></div>
@include('front.template.foot')
@endsection