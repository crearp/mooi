@extends('layouts.app')
@section('content')
<style>
/* Center the loader */
#loader {
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 1;
  width: 40px;
  height: 40px;
  margin: -35px 0 0 -35px;
  border: 7px solid #f3f3f3;
  border-radius: 50%;
  border-top: 7px solid #eeba8a;
  width: 50px;
  height: 50px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 }
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom {
  from{ bottom:-100px; opacity:0 }
  to{ bottom:0; opacity:1 }
}

#accordion-wrapper {
  display: none;
  text-align: center;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
{{-- RESPONSIVE SIN RACORDEON --}}
<link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/flexslider.css') }}?<?= rand(0, 9000); ?>" />
    <div class="flexslider carousel">
        <div id="loader"></div>
        <ul id="accordion-wrapper" class="accordion_style slides animate-bottom" style="display:none;">
        <?php
            $x=1;
        ?>
            @foreach($content as $t => $ca)
                @foreach($ca->specify_styles as $cb)
                    <li style="width: 386.33px; margin-right: 0px; float: left; display: block;">
                        <img class="" src="{{url('/')}}/media/<?php echo $cb->specify_style_photo;?>" alt="{{$cb->specify_style_name}}" title="{{$cb->specify_style_name}}" draggable="false" style="background: -webkit-linear-gradient(top, rgba(255, 194, 0, 0) 10%, rgba(255,255,255,0.45) 0%, rgb(255, 224, 191) 100%);" onmouseover="this.style.opacity=0.7" onmouseout="this.style.opacity=1"/>
                        <div class="caption center" style=" width: 100%; float: left;">
                            <h4 style="font-size:30px; text-transform: uppercase;">{{$cb->specify_style_name}}</h4>
                            <p style="padding: 10px;">{{$cb->specify_style_description}}</p>
                            <hr style="width: 90%; float: inherit; border-color: #b5b0ab; margin: 8px;">
                            <?php $valor=number_format($ca->style_ammount); ?>
                            @if ($cb->specify_style_name == "Depilación en hilo" || $cb->specify_style_name == "Depilación en cera")
                                <p style="font-size: small;">DESDE: ${{$valor}}</p>
                            @else
                                <p style="font-size: small;">PRECIO: ${{$valor}}</p>
                            @endif

                            <a href="autoservicios/{{$i}}/{{$ca->style_id}}/{{$ca->style_ammount}}/{{$ca->style_name}}/{{$x}}">
                                <p>
                                    <!-- <h4>-AGENDAR-</h4> -->
                                    <button class="btn btn-lg btn-block btn-color_2" style="min-width:140px;">AGENDAR</button>
                                </p>
                            </a>
                        </div>
                    </li>
                @endforeach
                <?php $x=$x+1; ?>
            @endforeach
        </ul>
    </div>
@include('front.template.foot')
@endsection