<?php

namespace App\Http\Controllers\Pruebas;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\servicios;
use DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
use App\Encryption;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use View;
use Mail;

class PruebasController extends Controller
{
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->urlBackend = webservice();

		$url = url()->current();

		if(strpos($url, 'localhost') === false) $this->urlBackend = 'http://localhost:'.ENV('WEBSERVICE_PORT');
		//$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}


    public function getMakeRequest(Request $request){
		$comments = "";
		$client6  = new Client;

		$res6 = $client6->post($this->urlBackend."/api/book",[
		   'json' => ['address' => "Calle 22", 'styles' => [12], 'hour' => "10/10/2016 23:23", "user_id" => 93, "phone" => "3044957760", "card_reference" => '14555120245102687919','payment_way' => 1,'comments' => $comments]
		]);

		$arr6 = json_decode($res6->getBody());

   /* $data = ['foo' => 'bar'];
	Mail::send('emails.welcome', $data, function($message) {
		$message->to('jaos1991@gmail.com', 'Jaos')->subject('This is a demo!');
	});*/
/*Mail::raw('Prueba Texto de la Futura Profesional MOOI', function ($message) {

	$pathToFile1='C:\Users\JAOS\Desktop\bd1.PNG';
	$pathToFile2='C:\Users\JAOS\Desktop\bd2.PNG';


	$message->to('jaos1991@gmail.com', 'Jaos')->subject('Futur@ Profesional MOOI');
	$message->cc('bryan.canon@creardigital.com', $name = 'MOOI');
	$message->attach($pathToFile1);
	$message->attach($pathToFile2);
	//$message->attach($pathToFile3);
});*/
/*$pathToFile1='C:\Users\JAOS\Desktop\bd1.PNG';
	$pathToFile2='C:\Users\JAOS\Desktop\bd2.PNG';
Mail::send('auth.emails.emailtrabaje',compact('request'), function ($message) {
			$message->from('info@mooi.com.co', 'MOOI');
			$message->to('jaos1991@gmail.com', 'Jaos')->subject('Formulario para nuev@ Profesional Mooi');
			$message->cc('bryan.canon@creardigital.com', $name = 'MOOI');
			$message->attach($pathToFile1);
			$message->attach($pathToFile2);
			});*/
		//GET request
		//$client = new Client;

		//Services call (GET)
		//$res = $client->get('http://159.203.42.145:8000/api/styles');
		//echo $res->getBody();

		//Login call
		/*
		* De acá debes almacenar toda la data que viene acá, en una tabla de usuarios (puede ser tuya)
		* Para almacenar las TC debes coger las referencias y los numeros encriptados para mostrar en un select
		* La estructura debe ser: <option value="referencia">****-****-****-1111</option>
		*
		*/

		//$res = $client->post($this->urlBackend."/api/login",[
		   //   'json' => ['username' => 'jaos1991@yahoo.com11', 'password' => '123456']
		//]);

		/*
		* La idea de acá es que debes validar la url "https://ccapi-stg.paymentez.com/api/cc/save" cuando sea,
		* debes hacer el login nuevamente, para refrescar la vista con la tarjeta nueva que el cliente
		*
		*/

		/*
		* Acá la idea es que cojas la url para agregar tarjeta (solo si el usuario desea) y hacer el despliegue
		* sobre otra UI, y hacer las validaciones que ya hablamos.
		*/

	   // $res = $client->post($this->urlBackend."/api/availability",[
	   //      'json' => ['address' => 'Cra 9 #47-75', 'styles' => [1,2], 'hour' => "30/10/2016 08:00", "user_id" => 63]
		//]);


		/*
		*  Para este request no debes hacer nada, el pago se realiza desde el api
		*/
		//$client90 = new Client;
		//$res90 = $client90->post($this->urlBackend."/api/book",[
		//      'json' => ['address' => 'Calle 44 No. 25 g3-22', 'styles' => [7], 'hour' => '23/08/2016 01:59', 'user_id' => 16, 'card_reference' => '15157742884624709557']
		//]);
		//$arr90 = json_decode($res90->getBody());
		//dd($arr90);
		//Esto imprime el JSON
		//echo $res->getBody();


		//$arr = json_decode($res->getBody());
		//dd($arr);

		//$client30 = new Client;
		//$res30 = $client30->get($this->urlBackend."/api/detailed_styles");
		//$arr30 = json_decode($res30->getBody());
		//$cabello=$arr30->detailed_styles[0]->specify_styles;
		//$precio=$arr30->detailed_styles[0]->style_ammount;
		//dd($cabello);

		//$client28 = new Client;

		//$res28 = $client28->get($this->urlBackend."/api/detailed_styles");

		//$arr28 = json_decode($res28->getBody());

		//dd($arr28);

		//$client12 = new Client;

		//$res12 = $client12->post($this->urlBackend."/api/history",[
		  //   'json' => ['user_id' => 16]
		//]);

		//$arr12 = json_decode($res12->getBody());

		//$servicios=$arr12;
		//dd($servicios);




	   /* //debug($arr);
		//echo $arr['name'];
		//dd($arr);
		//var_dump($arr);
		echo "Nombre: ".$arr->name;
		echo "<br>";
		echo "Apellido: ".$arr->lastname;
		echo "<br>";
		echo "Id: ".$arr->id;
		echo "<br>";
		echo "Direccion 1: ".$arr->address_one;
		echo "<br>";
		echo "Telefono: ".$arr->phone;
		echo "<br>";
		echo "Estado: ".$arr->status;
		echo "<br>";
		echo "Email: ".$arr->email;
		echo "<br>";
		echo "Direccion 2: ".$arr->address_two;
		echo "<br>";

		for($x=0;$x<count($arr->cards);$x++){

			echo "No.".$x." Numero Tarjeta: ".$arr->cards[$x]->card_number_encrypted;
			echo "<br>";
			echo "No.".$x." Referencia Tarjeta: ".$arr->cards[$x]->card_reference;
			echo "<br>";
			echo "No.".$x." Franquicia: ".$arr->cards[$x]->card_type;
			echo "<br>";

		}*/

	}
}
