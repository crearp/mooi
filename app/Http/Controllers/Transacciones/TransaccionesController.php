<?php

namespace App\Http\Controllers\Transacciones;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\servicios;
use DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
use App\Encryption;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use View;
use Mail;

class TransaccionesController extends Controller
{
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->urlBackend = webservice();

		// $url = url()->current();
		// if(strpos($url, 'localhost') === false) $this->urlBackend = 'http://localhost:'.ENV('WEBSERVICE_PORT');
		// $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}

    public function getEliminarTarjetas(Request $request){
		//obtención de datos para login interno
		$id_user = Session::get('iduser');//\Auth::user()->id;//Usuario
		$user    = Session::get('emailuser');//\Auth::user()->email;//correo usuario
		$pass    = Session::get('pass');

		$request->session()->forget('agendacards');
		$request->session()->forget('num');
		$request->session()->forget('style');
		$request->session()->forget('val_trans');
		$request->session()->forget('fecha');
		$request->session()->forget('direccion');
		$request->session()->forget('hora');
		$request->session()->forget('metodo');
		$request->session()->forget('telefono');
		$request->session()->forget('comments');
		$request->session()->forget('discount');
		$request->session()->forget('agendalogout');

		$WsLogin = new Client;

		$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
			'json' => ['username' => $user, 'password' => $pass]
		]);

		$Login = json_decode($ResponseLogin->getBody());

		$estado21=$Login->status;
		$cards=$Login->cards;
		$id=$Login->id;

		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			return view('mobile.transacciones.deletecard', compact('id','cards'));
		}else{
			return view('transacciones.deletecard', compact('id','cards'));
		}

	}

	public function postEliminarTarjetas(Request $request){

		$id = $request->get('id');//Id de usuario
		$reference = $request->get('cards');//Id de transaccion

		$WsEliminarTarjetas = new Client;

		$ResponseEliminarTarjetas = $WsEliminarTarjetas->post($this->urlBackend."/api/payment/delete",[
			 'json' => ['user_id' => $id, 'card_reference' => $reference]
		]);

		$EliminarTarjetas = json_decode($ResponseEliminarTarjetas->getBody());

		$estado=$EliminarTarjetas->status;

		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			if($estado != 'FAILED'){

			Flash::success('Tu tarjeta ha sido eliminada correctamente.');
			return redirect('/mobile');

			}else{

				Flash::message('Tu tarjeta no se pudo eliminar, consulta con el administrador del sistema');
				return redirect('/mobile');
			}
		}else{
			if($estado != 'FAILED'){

			Flash::success('Tu tarjeta ha sido eliminada correctamente.');
			return redirect()->route('/');

			}else{

				Flash::message('Tu tarjeta no se pudo eliminar, consulta con el administrador del sistema');
				return redirect()->route('/');
			}
		}
	}

	public function getAgregarTarjeta(Request $request){

		//obtención de datos para login interno
		$id_user   = Session::get('iduser');//\Auth::user()->id;//Usuario
		$pass   = Session::get('pass');
		$user = Session::get('emailuser');//\Auth::user()->email;//correo usuario

		// if($request->get('infopago') === null){

		// 	$request->session()->forget('agendacards');
		// 	$request->session()->forget('num');
		// 	$request->session()->forget('style');
		// 	$request->session()->forget('val_trans');
		// 	$request->session()->forget('fecha');
		// 	$request->session()->forget('direccion');
		// 	$request->session()->forget('hora');
		// 	$request->session()->forget('metodo');
		// 	$request->session()->forget('telefono');
		// 	$request->session()->forget('comments');
		// 	$request->session()->forget('discount');
		// 	$request->session()->forget('agendalogout');
		// }

		$WsLogin = new Client;

		$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
			'json' => ['username' => $user, 'password' => $pass]
		]);

		$Login = json_decode($ResponseLogin->getBody());

		$estado2=$Login->status;
		$cards1=$Login->cards;
		$cards11=sizeof($cards1);
		$UrlAnadirTarjeta=$Login->cc_add_form;
		$tipousuario=$Login->type;

		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			return view('mobile.transacciones.addcard',compact('UrlAnadirTarjeta'));
		}else{
			return view('transacciones.addcard',compact('UrlAnadirTarjeta'));
		}
	}

	public function getValidAddCardLogout(){
		$valid=Session::get('agendacards');

		if(isset($valid) && $valid == 'OK'){
			return redirect()->action('Agendamiento\AgendaController@postAgenda');
		}else{
			if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
				return redirect('/mobile');
			}else{
				return redirect('inicio');
			}
		}
	}
}
