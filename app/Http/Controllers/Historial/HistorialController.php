<?php

namespace App\Http\Controllers\Historial;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\servicios;
use DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
use App\Encryption;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use View;
use Mail;

class HistorialController extends Controller
{

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->urlBackend = webservice();

		// $url = url()->current();
		// if(strpos($url, 'localhost') === false) $this->urlBackend = 'http://localhost:'.ENV('WEBSERVICE_PORT');
		// $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}

    public function getHistorial(Request $request){

		//$user=\Auth::user()->id;//Usuario

		//$correo=\Auth::user()->email;//correo usuario

		//$id_user = DB::table('api_userapp')->where('mail',$correo)->get();
		//    foreach($id_user as $h){
		//    $id_authuser = $h->id;
		//    }
		$id_authuser=Session::get('iduser');

		$request->session()->forget('agendacards');
		$request->session()->forget('num');
		$request->session()->forget('style');
		$request->session()->forget('val_trans');
		$request->session()->forget('fecha');
		$request->session()->forget('direccion');
		$request->session()->forget('hora');
		$request->session()->forget('metodo');
		$request->session()->forget('telefono');
		$request->session()->forget('comments');
		$request->session()->forget('discount');
		$request->session()->forget('agendalogout');
		//$servicios = DB::table('api_book')->where('user_id', $id_authuser)->get();
		//    foreach($servicios as $s){
		//    $s_book = $s->id;
		//    }

		//$servicios2 = DB::table('api_book_services')->where('book_id', $s_book)->get();

		$WsHistorial = new Client;

		$ResponseHistorial = $WsHistorial->post($this->urlBackend."/api/history",[
			 'json' => ['user_id' => $id_authuser]
		]);

		$Historial = json_decode($ResponseHistorial->getBody());

		if(isset($Historial->status)){
			$serviciossinordenar=$Historial->services;
			$servicios=array_reverse($serviciossinordenar);
			$cantserv=sizeof($servicios);
		}else{
			$serviciossinordenar=$Historial;
			$servicios=array_reverse($serviciossinordenar);
			$cantserv=sizeof($servicios);
		}

		if(Session::get('is_mobile') && Session::get('is_mobile') == true){
			if($cantserv != 0){
				return view('mobile.historial.historial', compact('servicios','cantserv'));
			}else{
				$msjTextHistorial = 'Actualmente no tienes servicios agendados';

				$msjHistorial = true;
				//return redirect()->route('agenda_historial_mobile')->with(compact('msjTextHistorial','msjHistorial'));
				//\Flash::success("Actualmente no tienes servicios agendados");
				return view('mobile.historial.historial', compact('servicios','cantserv','msjTextHistorial','msjHistorial'));
			}
		}else{
			if($cantserv != 0){
				return view('historial.historial', compact('servicios','cantserv'));
			}else{
				\Flash::success("Actualmente no tienes servicios agendados");

				return view('historial.historial', compact('servicios','cantserv'));
			}
		}

	}

	public function getTiempo(Request $request,$dia,$mes,$ano,$hora,$estado){

	    $horas = explode(":", $hora);
        $hora=$horas[0];
        $min=$horas[1];

        $dia=$dia;
        $mes=$mes;
        $ano='20'.$ano;
        $hora=$hora;
        $min=$min;
        $estado=$estado;

		$horayfechaactual=Carbon::now();
        $horayfechaactual=$horayfechaactual->format("Y-m-d H:i:s");

        $horayfechacita=$ano.'-'.$mes.'-'.$dia.' '.$hora.':'.$min.':00';

        /*dd($horayfechaactual, $horayfechacita);*/

		if(($estado == 'Reservado sin asignacion' || $estado == 'Reservado') && ($horayfechaactual <= $horayfechacita)){

		return view('historial.tiempo', compact('id','nombre','valor','ano','mes','dia','hora','min','estado'));

		}else{
			/*$ano=0;
			$mes=0;
			$dia=0;
			$hora=0;
			$min=0;*/

			return view('historial.tiempo2');
		}

	}

	public function postCancelar(Request $request){

		$id = $request->get('id');//Id de transaccion del book

		$WsCancelarServicio = new Client;

		$ResponseCancelarServicio = $WsCancelarServicio->post($this->urlBackend."/api/cancel",[
			 'json' => ['book_id' => $id, 'state' => 3]
		]);

		$CancelarServicio = json_decode($ResponseCancelarServicio->getBody());

		if(Session::get('is_mobile') && Session::get('is_mobile') == true){
			if(isset($CancelarServicio->status) && $CancelarServicio->status == 'OK'){
				//\Flash::success("Tu servicio ha sido cancelado");
				//return redirect()->route('agenda_historial_mobile');
				$msjTextHistorial = 'Tu servicio ha sido cancelado';

				$msjHistorial = true;

				return redirect()->route('agenda_historial_mobile')->with(compact('msjTextHistorial','msjHistorial'));
			}else{
				//flash('No pudimos cancelar tu servicio, consulta con el administrador del sistema', 'danger');
				//return redirect()->route('agenda_historial_mobile');

				$msjTextHistorial = 'No pudimos cancelar tu servicio, consulta con el administrador del sistema';

				$msjHistorial = true;

				return redirect()->route('agenda_historial_mobile')->with(compact('msjTextHistorial','msjHistorial'));
			}
		}else{
			if(isset($CancelarServicio->status) && $CancelarServicio->status == 'OK'){
				\Flash::success("Tu servicio ha sido cancelado");
				return redirect()->route('agenda_historial');
			}else{
				flash('No pudimos cancelar tu servicio, consulta con el administrador del sistema', 'danger');
				return redirect()->route('agenda_historial');
			}
		}
	}

	public function postCancelarProfessional(Request $request){
		$id = $request->get('id');//Id de transaccion del book

		$WsCancelarServicio = new Client;

		$ResponseCancelarServicio = $WsCancelarServicio->post($this->urlBackend."/api/cancel",[
			 'json' => ['book_id' => $id, 'state' => 4]
		]);

		$CancelarServicio = json_decode($ResponseCancelarServicio->getBody());

		if(isset($CancelarServicio->status) && $CancelarServicio->status == 'OK'){
			\Flash::success("Tu servicio ha sido cancelado");
			return redirect()->route('perfil_professional');
		}else{
			flash('No pudimos cancelar tu servicio, consulta con el administrador del sistema', 'danger');
			return redirect()->route('perfil_professional');
		}
	}

	/*
	public function postCancelarConfirmacion(Request $request){

		$user=\Auth::user()->id;//Usuario

		DB::table('info_transacciones')
			->where('id', $request->get('id'))
			->where('id_usuario', $user)
			->update(['estado' => 'cancelado']);

		DB::table('api_book')
			->where('id', $request->get('id2'))
			->update(['state' => '2']);

		return redirect()->route('agenda_historial');
	}

	public function postIniciar(Request $request){

		$user=\Auth::user()->id;//Usuario

		DB::table('info_transacciones')
			->where('id', $request->get('id'))
			->where('id_usuario', $user)
			->update(['estado' => 'en curso']);

		return redirect()->route('agenda_historial');
	}

	public function postIncumplir(Request $request){

		$user=\Auth::user()->id;//Usuario

		DB::table('info_transacciones')
			->where('id', $request->get('id'))
			->where('id_usuario', $user)
			->update(['estado' => 'incumplido']);

		DB::table('api_book')
			->where('id', $request->get('id2'))
			->update(['state' => '2']);

		return redirect()->route('agenda_historial');
	}
	*/

	public function postTerminar(Request $request){

		$id = $request->get('id')*1;//Id de transaccion del book

		$WsTerminarPagandoServicio = new Client;

		$ResponseTerminarPagandoServicio = $WsTerminarPagandoServicio->post($this->urlBackend."/api/payment/debit",[
			 'json' => ['book_id' => $id]
		]);

		$TerminarPagandoServicio = json_decode($ResponseTerminarPagandoServicio->getBody());

		if(Session::get('is_mobile') && Session::get('is_mobile') == true){
			if(isset($TerminarPagandoServicio->status) && $TerminarPagandoServicio->status == 'OK'){
				$msjTextHistorial = 'Tu servicio ha finalizado correctamente';
				$msjHistorial = true;
				return redirect()->route('agenda_historial_mobile')->with(compact('msjTextHistorial','msjHistorial'));
				//\Flash::success("Tu servicio ha finalizado correctamente");
				//return redirect()->route('agenda_historial_mobile');
			}else{
				$msjTextHistorial = 'No se finalizo o pagó tu servicio, consulta con el administrador del sistema';
				$msjHistorial = true;
				return redirect()->route('agenda_historial_mobile')->with(compact('msjTextHistorial','msjHistorial'));
				//flash('No se finalizo o pagó tu servicio, consulta con el administrador del sistema', 'danger');
				//return redirect()->route('agenda_historial_mobile');
			}
		}else{
			if(isset($TerminarPagandoServicio->status) && $TerminarPagandoServicio->status == 'OK'){
			\Flash::success("Tu servicio ha finalizado correctamente");
			return redirect()->route('agenda_historial');
			}else{
				flash('No se finalizo o pagó tu servicio, consulta con el administrador del sistema', 'danger');
				return redirect()->route('agenda_historial');
			}
		}
	}

	public function postCalificar(Request $request){

		$user=Session::get('iduser');//iD DEL USUARIO EN LA PLATAFORMA

		$id = $request->get('id');//Id de transaccion

		/*$nombre1 = DB::table('api_book_services')->where('id', $request->get('id'))->pluck('style_id');
		//$nombre2->id_servicio = $nombre1;
		$nombre = DB::table('api_style')->select('name')->where('id', $nombre1)->get();*/

		/*$valor = DB::table('info_transacciones')->select('valor_total')->where('id', $id)->get();*/
		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			return view('mobile.historial.calificar', compact('id'));
		}else{
			return view('historial.calificar', compact('id'));
		}
	}

	public function postCalificarFinal(Request $request){

		$user    = Session::get('iduser');
		$id      = $request->get('id');//Id de transaccion
		$score   = $request->get('estrellas');//calificacion
		$coments = $request->get('comentario');//comentarios

		/*DB::table('info_transacciones')
			->where('id', $request->get('id'))
			->where('id_usuario', $user)
			->update(['cservicio' => $request->get('estrellas'),
					 'ccumplimiento' => $request->get('select'),
					 'ccomentario' => $request->get('comentario')]);

		DB::table('api_book')
			->where('id', $request->get('id2'))
			->update(['score' => $request->get('estrellas')]);*/

		$WsCalificarServicio = new Client;

		$ResponseCalificarServicio = $WsCalificarServicio->post($this->urlBackend."/api/score",[
			 'json' => ['book_id' => $id, 'score' => $score, 'comments' => $coments]
		]);

		$CalificarServicio = json_decode($ResponseCalificarServicio->getBody());

		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			//Flash::message('Gracias por tu calificación, es muy importante para nosotros');

			$msjTextHistorial = 'Gracias por tu calificación, es muy importante para nosotros';

			$msjHistorial = true;

			return redirect()->route('agenda_historial_mobile')->with(compact('msjTextHistorial','msjHistorial'));

			//return redirect('/mobile');
		}else{
			Flash::message('Gracias por tu calificación, es muy importante para nosotros');

			return redirect()->route('agenda_historial');
		}


	}

	public function getCalificacion($id){
		$id  = $id*1;
		$url = $_SERVER["REQUEST_URI"];
		$url = explode("/", $url);

		$calificacion = explode("=", $url[2]);
		$calificacion = $calificacion[1]*1;

		$WsCalificarServicio = new Client;

		$ResponseCalificarServicio = $WsCalificarServicio->post($this->urlBackend."/api/score",[
			 'json' => ['book_id' => $id, 'score' => $calificacion, 'comments' => ""]
		]);


		$CalificarServicio = json_decode($ResponseCalificarServicio->getBody());

		if(isset($CalificarServicio->status) && $CalificarServicio->status == 'OK'){
			session::put('id_book_calificacion', $id);
			session::put('score_calificacion', $calificacion);
			$mensaje = "Si";
			$estado = "success";
		}else{
			$mensaje="No";
			$estado = "success";
		}

		return redirect()->route('url_fin_calificacion', ['mensaje' => $mensaje, "estado" => $estado]);

	}

	public function getCalificacionRedirect(Request $request){
		$url = $_SERVER["REQUEST_URI"];
		$url = explode("/", $url);

		if($url[3] != "No"){
			$mensaje="Gracias por tu calificación";
			$estado = "Si";
			$id_book=session::get('id_book_calificacion');
			$score=session::get('score_calificacion');
		}else{
			$mensaje="No pudimos calificar tu servicio, te invitamos a hacerlo en el historial de nuestra pagina principal";
			$estado = "No";
			$id_book="No";
			$score="No";
		}
		return view('historial.fincalificar',compact('mensaje'));
	}

	public function postCalificacion(Request $request){
		$url   = $_SERVER["REQUEST_URI"];
		$url   = explode("=", $url);
		$texto = str_replace("+"," ",$url[1]);
		$texto = utf8_decode($texto);

		$id = session::get('id_book_calificacion');
		$calificacion=session::get('score_calificacion');

		$WsCalificarServicio = new Client;
		$ResponseCalificarServicio = $WsCalificarServicio->post($this->urlBackend."/api/score",[
			 'json' => ['book_id' => $id, 'score' => $calificacion, 'comments' => $texto]
		]);

		$CalificarServicio = json_decode($ResponseCalificarServicio->getBody());

		if(isset($CalificarServicio->status) && $CalificarServicio->status == 'OK'){
			$request->session()->forget('id_book_calificacion');
			$request->session()->forget('score_calificacion');
			$mensaje = "Gracias por tu calificación";
			$estado = "success";
		}else{
			$mensaje="No pudimos calificar tu servicio, te invitamos a hacerlo en el historial de nuestra pagina principal";
			$estado = "success";
		}

		return view('historial.fincalificar_terminado',compact('mensaje'));
	}
}