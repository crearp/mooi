<?php

namespace App\Http\Controllers\Agendamiento;

use App\Http\Controllers\Controller;

use App\User;
use App\servicios;
use App\Encryption;

use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Jenssegers\Agent\Agent;

use Illuminate\Http\Request;
use Illuminate\Support\Collection as Collection;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use DB;
use View;
use Mail;
use Validator;

class AgendaController extends Controller
{

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->urlBackend = webservice();
	}

	public function getListas(){
		$servidor    = $this->urlBackend;
		$sessionuser = Session::get('sessionuserC');

		//ws para si es nuevo usuario
	    $user = Session::get('emailuser');
	    $wsNewUser = new Client;
	    $ResponseNewUser = $wsNewUser->post($this->urlBackend."/api/is_new",[
	      'json' => ['email' => $user]
	    ]);

	    $NewUser = json_decode($ResponseNewUser->getBody());

	    if($NewUser->status == "OK"){
	      $is_new_user = $NewUser->is_new_user;
	    }else{
	      $is_new_user = false;
	    }
	    //fin ws para si es nuevo usuario

		if ($sessionuser == 'OK'){

			$user = Session::get('emailuser');//\Auth::user()->email;//correo usuario
			$pass = Session::get('pass');

			$WsLogin = new Client;
			$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
				'json' => ['username' => $user, 'password' => $pass]
			]);

			$Login = json_decode($ResponseLogin->getBody());

			$tel              = $Login->phone;
			$UrlAnadirTarjeta = $Login->cc_add_form;
			$tipousuario      = $Login->type;

			$direccion = Session::get('direccion1user');

			// formas de pago
			$WsFormasPago = new Client;
			$ResponseFormasPago = $WsFormasPago->get("http://mooi.com.co:8000/api/get_payments");

			$FormasPago = json_decode($ResponseFormasPago->getBody());
			$forms=$FormasPago->payment_types;

			// Servicios
			$WsEstilos = new Client;
			$ResponseEstilos = $WsEstilos->get($this->urlBackend."/api/detailed_styles");

			$Estilos  = json_decode($ResponseEstilos->getBody());
			$jsonData = json_encode(["specify_style_name" => "null", "specify_style_photo" => "null", "specify_style_description" => "null"]);

			foreach ($Estilos->detailed_styles as $key => $value) {
				//$name = str_ireplace([" ", "ñ"], ["","n"], $value->category_name);
				$name = $value->category_name;

				if(COUNT($value->specify_styles) == 0){
					$value->specify_styles[0] = json_decode($jsonData);
				}

				$arrayStyle[$name][] = $value;
			}
			//dd($Estilos);
			$styleDefault = 0;
			$autobook     = "No"; // indicar que se viene de autobook
			$name         = ""; // Variable vacia necesaria para menu autobook
			$id           = ""; // Variable vacia necesaria para menu autobook
			$ammount      = ""; // Variable vacia necesaria para menu autobook
			$namestyle    = ""; // Variable vacia necesaria para menu autobook
			$cant         = ""; // Variable vacia necesaria para menu autobook

			return view('agendamiento.agenda', compact('is_new_user','cant','arrayStyle','servidor','direccion','forms','count','tel','autobook','name','id','ammount','namestyle', 'styleDefault'));
		}

		//formas de pago
		$WsFormasPago = new Client;
		$ResponseFormasPago = $WsFormasPago->get("http://mooi.com.co:8000/api/get_payments");

		$FormasPago = json_decode($ResponseFormasPago->getBody());
		$forms      = $FormasPago->payment_types;

		$WsEstilos = new Client;
		$ResponseEstilos = $WsEstilos->get($this->urlBackend."/api/detailed_styles");

		$Estilos = json_decode($ResponseEstilos->getBody());

		foreach ($Estilos->detailed_styles as $key => $value) {
			//$name = str_ireplace([" ", "ñ"], ["","n"], $value->category_name);
			$name = $value->category_name;
			$arrayStyle[$name][] = $value;
		}

		$styleDefault = 0; //indicar que se viene de autobook
		$autobook     = "No"; //indicar que se viene de autobook
		$name         = ""; //Variable vacia necesaria para menu autobook
		$id           = ""; //Variable vacia necesaria para menu autobook
		$ammount      = ""; //Variable vacia necesaria para menu autobook
		$namestyle    = ""; //Variable vacia necesaria para menu autobook
		$cant         = ""; // Variable vacia necesaria para menu autobook
		$direccion    = "";

		return view('agendamiento.agenda', compact('is_new_user','cant','arrayStyle','direccion','forms','count','autobook', 'name', 'id', 'ammount', 'namestyle', 'styleDefault'));

	}

	public function postAgenda(Request $request){
			//dd($request->all());
			$sessionagenda=Session::get('agendalogout');

		      //ws para si es nuevo usuario
		      $user = Session::get('emailuser');
		      $wsNewUser = new Client;
		      $ResponseNewUser = $wsNewUser->post($this->urlBackend."/api/is_new",[
		        'json' => ['email' => $user]
		      ]);

		      $NewUser = json_decode($ResponseNewUser->getBody());

		      if($NewUser->status == "OK"){
		        $is_new_user = $NewUser->is_new_user;
		      }else{
		        $is_new_user = false;
		      }
		      //fin ws para si es nuevo usuario

		      //consulta descuento is_new_user
		      if($is_new_user == true){
		        $imgPopUp = new Client;

		            $resimgPopUp = $imgPopUp->get(webservice(true)."/api/popup");

		            $arrimgPopUp = json_decode($resimgPopUp->getBody());

		        $discount=$arrimgPopUp->discount;
		      }else{
		        $discount = "0";
		      }

			if($sessionagenda != 'OK'){//Con Login

				$id_authuser=Session::get('iduser');
				$cantserv=$request->get('num');//validacion y distincion segun login

				$style=unserialize($request->get('style'));
				$valort_trans=$request->get('valort_trans');
				$valort_trans=str_replace(",", "", $valort_trans);

				$fecha = Carbon::now(); //hora y fecha actual
				$fecha = $fecha->format('d-m-Y'); //fecha actual

				$hora = Carbon::now(); //hora y fecha actual
				$hora = $hora->toTimeString(); //hora actual

				$fechapet = new Carbon($request->get('fecha'));
				$fechapet= $fechapet->format('d/m/Y');

				$prehour=new Carbon($request->get('hora'));
				$prehour=$prehour->format('H:i');

				$hour=$fechapet." ".$prehour;

				$referencia_pago= "0";

				$WsDisponibilidadCitas = new Client;
				$ResponseDisponibilidadCitas = $WsDisponibilidadCitas->post($this->urlBackend."/api/availability",[
				  	'json' => ['address' => $request->get('direccion'),
									'styles'  => $style,
									'hour'    => $hour,
									"user_id" => $id_authuser
								]
				]);

				$DisponibilidadCitas = json_decode($ResponseDisponibilidadCitas->getBody());

				$estado=$DisponibilidadCitas->status;


				$tel      = $request->get('telefono');
				$metodo   = $request->get('metodo')*1;
				$address  = $request->get('direccion');
				$lat      = $request->get('lat');
        		$lng      = $request->get('lng');
				$user_id  = $id_authuser;
				$comments = "";
				$valorPromoCode = "0";

				if($request->has('comments')){ $comments = $request->get('comments'); }

				//Redencion codigo de promocion
				if($request->has('valcode')){
					$valorPromoCode = $request->get('valcode');
					$valorPromoCode = str_replace(",", "", $valorPromoCode);
				}elseif ($is_new_user == true) {
          			//ws para imagenes de pop-up de promoción
			        $imgPopUp = new Client;

			        $resimgPopUp = $imgPopUp->get($this->urlBackend."/api/popup");

			        $arrimgPopUp = json_decode($resimgPopUp->getBody());

			        $porcentajePromoCode = ($arrimgPopUp->discount)*1;
			        $porcentajePromoCode = $porcentajePromoCode/100;

			        $valorPromoCode = $valort_trans*$porcentajePromoCode;
				}

				if($metodo == '1'){
					$url=Session::get('urlcardsuser');//url tarjetas de credito
					if($estado != 'FAILED'){

						//obtención de datos para login interno
						$id_user = Session::get('iduser');//\Auth::user()->id;//Usuario
						$user    = Session::get('emailuser');//\Auth::user()->email;//correo usuario
						$pass    = Session::get('pass');
						//fin login interno

						$WsLogin = new Client;
						$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
							'json' => ['username' => $user, 'password' => $pass]
						]);

						$Login = json_decode($ResponseLogin->getBody());

						$estado2 = $Login->status;
						$cards   = $Login->cards;
						$cards01 = sizeof($cards);
						$UrlAnadirTarjeta    = $Login->cc_add_form;

						if($cards01 == '0'){
							Session::put('agendacards','OK');
							Session::put('num',$cantserv);
							Session::put('style',$style);
							Session::put('val_trans',$valort_trans);
							Session::put('fecha',$request->get('fecha'));
							Session::put('direccion',$request->get('direccion'));
							Session::put('lat',$request->get('lat'));
              				Session::put('lng',$request->get('lng'));
							Session::put('hora',$request->get('hora'));
							Session::put('metodo',$metodo);
							Session::put('telefono',$request->get('telefono'));
							Session::put('comments',$request->get('comments'));
							Session::put('discount',$valorPromoCode);
							Session::put('agendalogout','OK');

							return view('transacciones.addcard',compact('UrlAnadirTarjeta'));
						}else{
							Session::put('agendacards','OK');
							Session::put('num',$cantserv);
							Session::put('style',$style);
							Session::put('val_trans',$valort_trans);
							Session::put('fecha',$request->get('fecha'));
							Session::put('direccion',$request->get('direccion'));
							Session::put('lat',$request->get('lat'));
              				Session::put('lng',$request->get('lng'));
							Session::put('hora',$request->get('hora'));
							Session::put('metodo',$metodo);
							Session::put('telefono',$request->get('telefono'));
							Session::put('comments',$request->get('comments'));
							Session::put('discount',$valorPromoCode);
							Session::put('agendalogout','OK');

							if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
								return view('mobile.agendamiento.pagos', compact('lat','lng','valort_trans','cards','url','address','style','hour','user_id','metodo','tel','comments','valorPromoCode'));
							}else{
								return view('agendamiento.pagos', compact('lat','lng','valort_trans','cards','url','address','style','hour','user_id','metodo','tel','comments','valorPromoCode'));
							}
						}

					}else{
						if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
							Session::put('agendamientoMobile',false);
							$mensaje4 = $DisponibilidadCitas->message;
							flash($mensaje4, 'danger');
							return redirect('/mobile');
						}else{
							$mensaje4 = $DisponibilidadCitas->message;
							flash($mensaje4, 'danger');

							return view('agendamiento.agenda');
						}
					}
				}elseif($metodo == '2' || $metodo == '3'){

					if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
						$device=1;
					}else{
						$device=0;
					}

					$WsAgendamiento = new Client;
					$ResponseAgendamiento = $WsAgendamiento->post($this->urlBackend."/api/book",
						[
							'json' =>
								[
									'address' => $address,
									'styles' => $style,
									'hour' => $hour,
									"user_id" => $user_id,
									"phone" => $tel,
									"card_reference" => '',
									'payment_way' => $metodo,
									'comments' => $comments,
									'discount' => $valorPromoCode,
									'latitude' => $lat,
                  					'longitude' => $lng,
									'quotes' => 1,
									'guest' => 0,
									'points' => 0,
									'device' => $device
								],
							'headers' => [
								'Content-type' => 'application/json'
						]
					]);

					$Agendamiento = json_decode($ResponseAgendamiento->getBody(), true);

					if($Agendamiento['status'] != 'FAILED'){
						$request->session()->forget('cuponutilizado');
						//correo de confirmacion
							$mail=Session::get('emailuser');
							$name=Session::get('nombreuserC');
							//dd($hour);

							//info para google calendar
							$hour = str_replace('/', '-', $hour);
							$newHour = Carbon::parse($hour)->addHour();

							$hora=explode(" ", $hour);
							$hora_prefinal=explode(":", $hora[1]);
							$hora_clean = str_replace(":", "", $hora[1]);

							$hora2=explode(" ", $newHour);
							$hora2_prefinal=explode(":", $hora2[1]);
							$hora2_clean = str_replace(":", "", $hora2[1]);
							//dd($hora);

							$urlgcalendar="https://www.google.com/calendar/render?action=TEMPLATE&text=Agendamiento%20MOOI&dates=".$fecha[2].$fecha[1].$fecha[0]."T".$hora_clean."00/".$fecha[2].$fecha[1].$fecha[0]."T".$hora2_clean."&details=Descripcion%20personalizada%20cita%20MOOI%20en%20Google%20Calendar&location=Bogota&trp=false#eventpage_6";


							$fecha=explode("-", $hora[0]);

								\Mail::send('auth.emails.confiragendamiento', compact('name','mail','urlgcalendar'), function ($message) use ($mail,$name) {
				                    $message->from('info@mooibelleza.com', 'MOOI');
				                    $message->to($mail, $name)->subject('Recibimos tu pedido - pendiente de confirmación')
						        			->bcc('catalina@mooibelleza.com','Catalina')
						        			->bcc('silvia@mooibelleza.com','Silvia')
						        			->bcc('laura.zuluaga@creardigital.com','Laura')
						        			->bcc('jaos1991@gmail.com','JAOS');
				                });
							//fin correo de confirmacio

						if(Session::get('is_mobile') && Session::get('is_mobile') == true ){

							Session::put('agendamientoMobile',true);

							\Flash::success("Gracias por elegirnos, tu solicitud esta siendo procesada. Pronto recibirás un correo de confirmación");

							return redirect('/mobile');

						}else{
							\Flash::success("Gracias por elegirnos, tu solicitud esta siendo procesada. Pronto recibirás un correo de confirmación");

							return redirect()->route('agenda_historial');
						}

					}else{

						if(Session::get('is_mobile') && Session::get('is_mobile') == true ){

							Session::put('agendamientoMobile',false);

							$mensaje5=$Agendamiento['message'];
							flash($mensaje5, 'danger');
							//\Flash::danger("El agendamiento no fue exitoso, pues no se pudo procesar su pre-pago inténtelo nuevamente");

							return redirect('/mobile');

						}else{
							$mensaje5=$Agendamiento['message'];
							flash($mensaje5, 'danger');
							//\Flash::danger("El agendamiento no fue exitoso, pues no se pudo procesar su pre-pago inténtelo nuevamente");

							return redirect()->route('agenda_listas');
						}
					}
				}
			}else{//SIN LOGIN

				$id_authuser=Session::get('iduser');
				$cantserv=Session::get('num');//validacion y distincion segun login

				$style=Session::get('style');
				$valor=Session::get('valor');

				$valort_trans = Session::get('val_trans');//Valor de la transaccion

				$fecha = Carbon::now(); //hora y fecha actual
				$fecha = $fecha->format('d-m-Y'); //fecha actual

				$hora = Carbon::now(); //hora y fecha actual
				$hora = $hora->toTimeString(); //hora actual

				$fechapet = new Carbon(Session::get('fecha'));
				$fechapet= $fechapet->format('d/m/Y');

				$prehour=new Carbon(Session::get('hora'));
				$prehour=$prehour->format('H:i');

				$hour=$fechapet." ".$prehour;

				$referencia_pago= "0";


				$WsDisponibilidadCitas = new Client;
				$ResponseDisponibilidadCitas = $WsDisponibilidadCitas->post($this->urlBackend."/api/availability",[
					'json' => ['address' => Session::get('direccion'), 'styles' => $style, 'hour' => $hour, "user_id" => $id_authuser]
				]);

				$DisponibilidadCitas = json_decode($ResponseDisponibilidadCitas->getBody());

				$estado=$DisponibilidadCitas->status;


				//variables para enviar
				$tel     = Session::get('telefono');
				$address = Session::get('direccion');
				$lat     = Session::get('lat');
        		$lng     = Session::get('lng');
				$metodo  = Session::get('metodo')*1;

				if(is_null(Session::get('comments'))){ $comments=""; }
				else{ $comments=Session::get('comments'); }

				$user_id = $id_authuser;
				$valorPromoCode="0";
				if ($is_new_user == true) {
	          		//ws para imagenes de pop-up de promoción
	              	$imgPopUp = new Client;

	              	$resimgPopUp = $imgPopUp->get($this->urlBackend."/api/popup");

	                $arrimgPopUp = json_decode($resimgPopUp->getBody());

			        $porcentajePromoCode = ($arrimgPopUp->discount)*1;
			        $porcentajePromoCode = $porcentajePromoCode/100;

			        $valorPromoCode = $valort_trans*$porcentajePromoCode;
		        }

				//Fin Redencion codigo de promocion

					if($metodo == '1'){
						$url=Session::get('urlcardsuser');//url tarjetas de credito

						$id_user=Session::get('iduser');//\Auth::user()->id;//Usuario

						$user=Session::get('emailuser');//\Auth::user()->email;//correo usuario
						$pass = Session::get('pass');
						//fin login interno

						$WsLogin = new Client;

						$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
							'json' => ['username' => $user, 'password' => $pass]
						]);

						$Login = json_decode($ResponseLogin->getBody());

						$estado2=$Login->status;
						$tel=$Login->phone;
						$cards1=$Login->cards;
						$cards11=sizeof($cards1);
						$UrlAnadirTarjeta=$Login->cc_add_form;

						if($cards11 == '0'){
							Session::put('agendacards','OK');
							return view('transacciones.addcard',compact('UrlAnadirTarjeta'));

						}else{


							if($estado != 'FAILED'){
								Session::put('agendacards','OK');
								//obtención de datos para login interno
								$id_user=Session::get('iduser');//\Auth::user()->id;//Usuario
								$user=Session::get('emailuser');//\Auth::user()->email;//correo usuario
								$pass = Session::get('pass');
								//fin login interno

								$WsLogin = new Client;

								$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
									  'json' => ['username' => $user, 'password' => $pass]
								]);

								$Login = json_decode($ResponseLogin->getBody());

								$estado2=$Login->status;
								$cards=$Login->cards;

								if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
									return view('mobile.agendamiento.pagos', compact('lat','lng','valort_trans','cards','url','address','style','hour','user_id','metodo','tel','comments','valorPromoCode'));
								}else{
									return view('agendamiento.pagos', compact('lat','lng','valort_trans','cards','url','address','style','hour','user_id','metodo','tel','comments','valorPromoCode'));
								}

							}else{

								$request->session()->forget('agendacards');
								$request->session()->forget('num');
								$request->session()->forget('style');
								$request->session()->forget('fecha');
								$request->session()->forget('direccion');
								$request->session()->forget('lat');
                				$request->session()->forget('lng');
								$request->session()->forget('hora');
								$request->session()->forget('telefono');
								$request->session()->forget('comments');
								Session::put('agendalogout','NO');
								$mensaje6=$DisponibilidadCitas->message;
								flash($mensaje6, 'danger');
								//Flash::danger("El agendamiento no fue exitoso, no tenemos disponibilidad en esta fecha u horario, intenta con una nueva");
								if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
									Session::put('agendamientoMobile',false);
									return redirect('/mobile');
								}else{
									return view('agendamiento.agenda');
								}

							}
						}
					}elseif($metodo == '2' || $metodo == '3'){
						//dd($address,$style,$hour,$user_id,$tel,$metodo,$comments,$valorPromoCode);

						if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
							$device=1;
						}else{
							$device=0;
						}

						$WsAgendamiento = new Client;

						$ResponseAgendamiento = $WsAgendamiento->post($this->urlBackend."/api/book",[
						   'json' => ["address" => $address,
									  "styles" => $style,
									  "hour" => $hour,
									  "user_id" => $user_id,
									  "phone" => $tel,
									  "card_reference" => '',
									  "payment_way" => $metodo,
									  "comments" => $comments,
									  "discount" => $valorPromoCode,
									  "latitude" => $lat,
                    				  "longitude" => $lng,
									  "quotes" => 1,
									  "guest" => 0,
									  "points" => 0,
									  "device" => $device
									  ],
								'headers' => [
									'Content-type' => 'application/json'
								]
						]);

						$Agendamiento = json_decode($ResponseAgendamiento->getBody(), true);

						if($Agendamiento['status'] != 'FAILED'){

							$request->session()->forget('num');
							$request->session()->forget('style');
							$request->session()->forget('fecha');
							$request->session()->forget('direccion');
							$request->session()->forget('hora');
							$request->session()->forget('telefono');
							$request->session()->forget('comments');
							Session::put('agendalogout','NO');

							//correo de confirmacion
								$mail=Session::get('emailuser');
								$name=Session::get('nombreuserC');

								//info para google calendar
								$hour = str_replace('/', '-', $hour);
								$newHour = Carbon::parse($hour)->addHour();

								$hora=explode(" ", $hour);
								$hora_prefinal=explode(":", $hora[1]);
								$hora_clean = str_replace(":", "", $hora[1]);

								$hora2=explode(" ", $newHour);
								$hora2_prefinal=explode(":", $hora2[1]);
								$hora2_clean = str_replace(":", "", $hora2[1]);
								//dd($hora);


								$fecha=explode("-", $hora[0]);

								$urlgcalendar="https://www.google.com/calendar/render?action=TEMPLATE&text=Agendamiento%20MOOI&dates=".$fecha[2].$fecha[1].$fecha[0]."T".$hora_clean."00/".$fecha[2].$fecha[1].$fecha[0]."T".$hora2_clean."&details=Descripcion%20personalizada%20cita%20MOOI%20en%20Google%20Calendar&location=Bogota&trp=false#eventpage_6";
								//dd($urlgcalendar);
								//fin info para google calendar

								\Mail::send('auth.emails.confiragendamiento', compact('name','mail','urlgcalendar'), function ($message) use ($mail,$name) {
				                    $message->from('info@mooibelleza.com', 'MOOI');
				                    $message->to($mail, $name)->subject('Recibimos tu pedido - pendiente de confirmación')
				                    		->bcc('catalina@mooibelleza.com','Catalina')
				                    		->bcc('silvia@mooibelleza.com','Silvia')
				                    		->bcc('laura.zuluaga@creardigital.com','Laura')
				                    		->bcc('jaos1991@gmail.com','JAOS');
				                });
							//fin correo de confirmacio

							if(Session::get('is_mobile') && Session::get('is_mobile') == true ){

								Session::put('agendamientoMobile',true);

								\Flash::success("Gracias por elegirnos, tu solicitud esta siendo procesada. Pronto recibirás un correo de confirmación");

								return redirect('/mobile');

							}else{
								\Flash::success("Gracias por elegirnos, tu solicitud esta siendo procesada. Pronto recibirás un correo de confirmación");

								return redirect()->route('agenda_historial');
							}

						}else{

							if(Session::get('is_mobile') && Session::get('is_mobile') == true ){

								Session::put('agendamientoMobile',false);

								$request->session()->forget('num');
								$request->session()->forget('style');
								$request->session()->forget('fecha');
								$request->session()->forget('direccion');
								$request->session()->forget('hora');
								$request->session()->forget('telefono');
								$request->session()->forget('comments');
								Session::put('agendalogout','NO');
								$mensaje7=$Agendamiento['message'];
								flash($mensaje7, 'danger');
								//\Flash::danger("El agendamiento no fue exitoso, pues no se pudo procesar su pre-pago inténtelo nuevamente");

								return redirect('/mobile');

							}else{
								$request->session()->forget('num');
								$request->session()->forget('style');
								$request->session()->forget('fecha');
								$request->session()->forget('direccion');
								$request->session()->forget('hora');
								$request->session()->forget('telefono');
								$request->session()->forget('comments');
								Session::put('agendalogout','NO');
								$mensaje7=$Agendamiento['message'];
								flash($mensaje7, 'danger');
								//\Flash::danger("El agendamiento no fue exitoso, pues no se pudo procesar su pre-pago inténtelo nuevamente");

								return redirect()->route('agenda_listas');
							}
						}
					}
			}

	}

	public function postAgendaLogout(Request $request){

		//Inicio tratamiento array de STYLE
		$cantservlog=$request->get('num');
		$style=unserialize($request->get('style'));

		//variables se session para enviar a la agenda normal
		Session::put('num',$request->get('num'));
		Session::put('style',$style);
		Session::put('val_trans',$request->get('valort_trans'));

		Session::put('fecha',$request->get('fecha'));
		Session::put('direccion',$request->get('direccion'));
		Session::put('lat',$request->get('lat'));
    	Session::put('lng',$request->get('lng'));
		Session::put('hora',$request->get('hora'));
		Session::put('metodo',$request->get('metodo'));
		Session::put('telefono',$request->get('telefono'));
		Session::put('comments',$request->get('comments'));
		Session::put('agendalogout','OK');

		//Mensaje de necesidad de login o registro para terminar el agendamiento
		//\Flash::success("Para terminar tu agendamiento, debes iniciar sesión o registrarte si aún no lo estás.");
		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			Session::put('agendamientoMobile',false);
			return redirect('inicio-sesion-movil');
		}else{
			return redirect('inicio-sesion');
		}
	}

	public function postPreagenda(Request $request){
		// Redencion codigo de promocion
		//dd($request->all());

		//ws para si es nuevo usuario
		      $user = Session::get('emailuser');
		      $wsNewUser = new Client;
		      $ResponseNewUser = $wsNewUser->post($this->urlBackend."/api/is_new",[
		        'json' => ['email' => $user]
		      ]);

		      $NewUser = json_decode($ResponseNewUser->getBody());

		      if($NewUser->status == "OK"){
		        $is_new_user = $NewUser->is_new_user;
		      }else{
		        $is_new_user = false;
		      }
		      //fin ws para si es nuevo usuario

		if($request->get('agendaMobile') == "false"){
			//Generacion de array para web service
			$valoryservicios=[];
			$arrIdServices = [];
			// HORRIBLE $acum_valor
			$valort_trans = 0;
			$porcentajePromoCode="0";
			foreach ($request->all() as $key => $value) {

				/*if (strpos($key, 'servicio-') !== false) {
					list($servicio, $tipoServicio, $idServicio) = explode('-', $key);
					list($id, $valor, $name) = explode('/', $value);
					$valoryservicios[] = [$id, $valor, $name,$request->get('cantidad-'.$tipoServicio.'-'.$idServicio)];
					$arrIdServices = array_merge($arrIdServices, [$id * 1, $request->get('cantidad-'.$tipoServicio.'-'.$idServicio)*1]);
					$valort_trans += $valor*$request->get('cantidad-'.$tipoServicio.'-'.$idServicio);
				}*/
				if (strpos($key, 'cantidad-') !== false) {
					if($value != "0"){
						list($servicio, $tipoServicio, $idServicio) = explode('-', $key);
						list($id, $valor, $name,$cant) = explode('/', $value);
						$valoryservicios[] = [$id, $valor, $name,$cant];
						// Para cuando se arregle backend $arrIdServices = array_merge($arrIdServices, [$id * 1, $cant*1]);
						$arrIdServices[]=$id*1;
						$valort_trans += $valor*$cant;
					}
				}
			}
			//dd($arrIdServices);
			$sessionagenda = Session::get('agendalogout');
			if($sessionagenda != 'OK'){//Con Login

				$id_authuser=Session::get('iduser');

				// WS para resucitar codigo de promocion asignado pero no utilizado
				if(Session::get('cuponutilizado') != 'null' && Session::get('cuponutilizado') != ''){

					$cuponutilizado=Session::get('cuponutilizado');

					$WsResetCupon = new Client;
					$resetCupon   = $WsResetCupon->post($this->urlBackend."/api/delete/promo_code",
						[
							'json' => ["user_id" => $id_authuser, "code" => $cuponutilizado],
							'headers' => [ 'Content-type' => 'application/json']
						]);

					$cuponUse = json_decode($resetCupon->getBody(), true);
				}
				//FIN WS para resucitar codigo de promocion asignado pero no utilizado


				if(is_null($request->get('cupon')) || $request->get('cupon') == ""){
					$cupon="NADA";
					$valorPromoCode="0";
					$msj_promocode_done="NO";
				}else{
					$cupon=$request->get('cupon');
					$WsCuponPromocion = new Client;
					$ResponseCuponPromocion = $WsCuponPromocion->post($this->urlBackend."/api/promo_code",[
							'json' => ["user_id" => $id_authuser,
									   "code" => $cupon,
									   "styles" => $arrIdServices
									],
							'headers' => ['Content-type' => 'application/json']
					]);

					$CuponPromocion   = json_decode($ResponseCuponPromocion->getBody(), true);
					$estadoPromoCode  = $CuponPromocion['status'];
					$mensajePromoCode = $CuponPromocion['message'];

					if($estadoPromoCode != 'FAILED'){
						$msj_promocode_done=$mensajePromoCode;
						$valorPromoCode=$CuponPromocion['ammount_discount'];
					}else{
						$msj_promocode_done=$mensajePromoCode;
						$valorPromoCode="0";
						flash($mensajePromoCode, 'danger');
						return redirect()->route('agenda_listas')->withInput();
					}
				}

				//descuento inicial para usuario nuevo
				if($is_new_user){

					$imgPopUp = new Client;

			        $resimgPopUp = $imgPopUp->get(webservice(true)."/api/popup");

			        $arrimgPopUp = json_decode($resimgPopUp->getBody());

			        foreach ($arrimgPopUp as $key => $value) {
			            if ($key == "discount") {
			                $array[$key]=$arrimgPopUp->$key;
			            }else{
			                $array[$key]= str_replace('./','/',$arrimgPopUp->$key);
			            }

			        }

					$msj_promocode_done="Si";
					$valorPromoCode=(($array['discount']*1)/100)*$valort_trans;
					$porcentajePromoCode = $array['discount'];
				}
			}else{

				if($is_new_user){
					$msj_promocode_done="Si";
					$valorPromoCode=(($array['discount']*1)/100)*$valort_trans;
					$porcentajePromoCode = $array['discount'];
				}else{
					$porcentajePromoCode="0";
					$msj_promocode_done="NO";
					$valorPromoCode="0";
					$id_authuser=0;
				}


			}

			//definir si agendamiento es festivo o no
			$arrFecha1 = explode(" ", $request->get('fecha'));
			$arrFecha2 = str_replace("-", "/", $arrFecha1[0]);

			$wsFestivo = new Client;
			$ResponseFestivo = $wsFestivo->post($this->urlBackend."/api/holiday",
				[
					'json' => ["date" => $arrFecha2],
					'headers' => ['Content-type' => 'application/json']
				]);

			// Saldo neto
			$valortotalcondescuento = $valort_trans < $valorPromoCode * 1? 0: $valort_trans - ($valorPromoCode * 1);

			//fin Saldo neto

			//"date": "(string) date format dd/mm/yyyy"
			$Festivo = json_decode($ResponseFestivo->getBody(), true);
			$festivo=$Festivo['isHoliday'];
			$recargo=$Festivo['ammount'];
			//fin definir festivo

			$num       = $request->get('num');
			$hora      = $request->get('hora');
			$fecha     = $request->get('fecha');
			$metodo    = $request->get('metodo');
			$comments  = $request->get('comments');
			$telefono  = $request->get('telefono');
			$direccion = $request->get('direccion');
			$lat      = $request->get('lat');
        	$lng      = $request->get('lng');
			$servidor  = $this->urlBackend;

			return view('agendamiento.infopago',compact('porcentajePromoCode','lng','lat','is_new_user','servidor','direccion','metodo','telefono','fecha','hora','comments','num','valort_trans','arrIdServices','valoryservicios','msj_promocode_done','valorPromoCode','valortotalcondescuento','cupon','festivo','recargo','id_authuser'));
		}else{
			$arrIdServices = unserialize($request->get('arrIdServices'));
			$valoryservicios = unserialize($request->get('valoryservicios'));
			$valort_trans = $request->get('valort_trans');

			$sessionagenda = Session::get('agendalogout');
			if($sessionagenda != 'OK'){//Con Login

				$id_authuser=Session::get('iduser');

				// WS para resucitar codigo de promocion asignado pero no utilizado
				if(Session::get('cuponutilizado') != 'null' && Session::get('cuponutilizado') != ''){

					$cuponutilizado=Session::get('cuponutilizado');

					$WsResetCupon = new Client;
					$resetCupon   = $WsResetCupon->post($this->urlBackend."/api/delete/promo_code",
						[
							'json' => ["user_id" => $id_authuser, "code" => $cuponutilizado],
							'headers' => [ 'Content-type' => 'application/json']
						]);

					$cuponUse = json_decode($resetCupon->getBody(), true);
				}
				//FIN WS para resucitar codigo de promocion asignado pero no utilizado


				if(is_null($request->get('cupon')) || $request->get('cupon') == ""){
					$cupon="NADA";
					$valorPromoCode="0";
					$msj_promocode_done="NO";
				}else{
					$cupon=$request->get('cupon');
					$WsCuponPromocion = new Client;
					$ResponseCuponPromocion = $WsCuponPromocion->post($this->urlBackend."/api/promo_code",[
							'json' => ["user_id" => $id_authuser,
									   "code" => $cupon,
									   "styles" => $arrIdServices
									],
							'headers' => ['Content-type' => 'application/json']
					]);

					$CuponPromocion   = json_decode($ResponseCuponPromocion->getBody(), true);
					$estadoPromoCode  = $CuponPromocion['status'];
					$mensajePromoCode = $CuponPromocion['message'];

					if($estadoPromoCode != 'FAILED'){
						$msj_promocode_done=$mensajePromoCode;
						$valorPromoCode=$CuponPromocion['ammount_discount'];
					}else{
						$msj_promocode_done=$mensajePromoCode;
						$valorPromoCode="0";
						flash($mensajePromoCode, 'danger');
						return redirect()->route('agenda_listas')->withInput();
					}
				}

				//descuento inicial para usuario nuevo
				if($is_new_user){

					$imgPopUp = new Client;

			        $resimgPopUp = $imgPopUp->get(webservice(true)."/api/popup");

			        $arrimgPopUp = json_decode($resimgPopUp->getBody());

			        foreach ($arrimgPopUp as $key => $value) {
			            if ($key == "discount") {
			                $array[$key]=$arrimgPopUp->$key;
			            }else{
			                $array[$key]= str_replace('./','/',$arrimgPopUp->$key);
			            }

			        }

					$msj_promocode_done="Si";
					$valorPromoCode=(($array['discount']*1)/100)*$valort_trans;
					$porcentajePromoCode = $array['discount'];
				}

			}else{
				if($is_new_user){
					$msj_promocode_done="Si";
					$valorPromoCode=(($array['discount']*1)/100)*$valort_trans;
					$porcentajePromoCode = $array['discount'];
				}else{
					$porcentajePromoCode="0";
					$msj_promocode_done="NO";
					$valorPromoCode="0";
					$id_authuser=0;
				}
			}

			//definir si agendamiento es festivo o no
			$arrFecha1 = explode(" ", $request->get('fecha'));
			$arrFecha2 = str_replace("-", "/", $arrFecha1[0]);

			$wsFestivo = new Client;
			$ResponseFestivo = $wsFestivo->post($this->urlBackend."/api/holiday",
				[
					'json' => ["date" => $arrFecha2],
					'headers' => ['Content-type' => 'application/json']
				]);

			// Saldo neto
			$valortotalcondescuento = $valort_trans < $valorPromoCode * 1? 0: $valort_trans - ($valorPromoCode * 1);

			//fin Saldo neto

			//"date": "(string) date format dd/mm/yyyy"
			$Festivo = json_decode($ResponseFestivo->getBody(), true);
			$festivo=$Festivo['isHoliday'];
			$recargo=$Festivo['ammount'];
			//fin definir festivo

			$num       = $request->get('num');
			$hora      = $request->get('hora');
			$fecha     = $request->get('fecha');
			$metodo    = $request->get('metodo');
			$comments  = $request->get('comments');
			$telefono  = $request->get('telefono');
			$direccion = $request->get('direccion');
			$lat      = $request->get('lat');
        	$lng      = $request->get('lng');
			$servidor  = $this->urlBackend;

			return view('mobile.agendamiento.infopago',compact('porcentajePromoCode','lng','lat','is_new_user','servidor','direccion','metodo','telefono','fecha','hora','comments','num','valort_trans','arrIdServices','valoryservicios','msj_promocode_done','valorPromoCode','valortotalcondescuento','cupon','festivo','recargo','id_authuser'));
		}
	}

	public function postPagos(Request $request){

		$arrIdServices = unserialize($request->get('style'));
		$cuotas = $request->get('cuotas');

		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			$device=1;
		}else{
			$device=0;
		}

		$WsAgendamiento = new Client;
		$ResponseAgendamiento = $WsAgendamiento->post($this->urlBackend."/api/book",
			[
			'json' =>
				[
					"address" => $request->get('address'),
					"styles" => $arrIdServices,
					"hour" => $request->get('hour'),
					"user_id" => $request->get('user_id'),
					"phone" => $request->get('tel'),
					"card_reference" => $request->get('cards'),
					"payment_way" => $request->get('metodo'),
					"comments" => $request->get('comments'),
					"discount" => $request->get('valorcupon'),
					"latitude" => $request->get('lat'),
          				"longitude" => $request->get('lng'),
					"quotes" => $cuotas,
					"guest" => 0,
					"points" => 0,
					"device" => $device
				],
			"headers" => [
					'Content-type' => 'application/json'
				]
			]);

		$Agendamiento = json_decode($ResponseAgendamiento->getBody(), true);

		$estado3=$Agendamiento['status'];

		if($estado3 != 'FAILED'){
			$request->session()->forget('cuponutilizado');

			//correo de confirmacion
			$mail = Session::get('emailuser');
			$name = Session::get('nombreuserC');

			//info para google calendar
			$hour = str_replace('/', '-', $request->get('hour'));
			$newHour = Carbon::parse($hour)->addHour();

			$hora=explode(" ", $hour);
			$hora_prefinal=explode(":", $hora[1]);
			$hora_clean = str_replace(":", "", $hora[1]);

			$hora2=explode(" ", $newHour);
			$hora2_prefinal=explode(":", $hora2[1]);
			$hora2_clean = str_replace(":", "", $hora2[1]);


			$fecha=explode("-", $hora[0]);

			$urlgcalendar="https://www.google.com/calendar/render?action=TEMPLATE&text=Agendamiento%20MOOI&dates=".$fecha[2].$fecha[1].$fecha[0]."T".$hora_clean."00/".$fecha[2].$fecha[1].$fecha[0]."T".$hora2_clean."&details=Descripcion%20personalizada%20cita%20MOOI%20en%20Google%20Calendar&location=Bogota&trp=false#eventpage_6";


			\Mail::send('auth.emails.confiragendamiento', compact('name','mail','urlgcalendar'), function ($message) use ($mail,$name) {
                $message->from('info@mooibelleza.com', 'MOOI');
                $message->to($mail, $name)->subject('Recibimos tu pedido - pendiente de confirmación')
                		->bcc('catalina@mooibelleza.com','Catalina')
                		->bcc('silvia@mooibelleza.com','Silvia')
                		->bcc('laura.zuluaga@creardigital.com','Laura')
                		->bcc('jaos1991@gmail.com','JAOS');
            });
			//fin correo de confirmacio

			if(Session::get('is_mobile') && Session::get('is_mobile') == true ){

				Session::put('agendamientoMobile',true);

				\Flash::success("Gracias por elegirnos, tu solicitud esta siendo procesada. Pronto recibirás un correo de confirmación");

				return redirect('/mobile');

			}else{
				\Flash::success("Gracias por elegirnos, tu solicitud esta siendo procesada. Pronto recibirás un correo de confirmación");

				return redirect()->route('agenda_historial');
			}

		}else{

			if(Session::get('is_mobile') && Session::get('is_mobile') == true ){

				Session::put('agendamientoMobile',false);

				\Flash::danger("No pudimos procesar el pago. Inténtalo nuevamente");

				return redirect('/mobile');

			}else{
				\Flash::danger("No pudimos procesar el pago. Inténtalo nuevamente");
				return redirect()->route('agenda_listas');
			}
		}
	}
}