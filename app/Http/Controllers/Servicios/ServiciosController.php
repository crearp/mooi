<?php

namespace App\Http\Controllers\Servicios;

use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

use App\User;
use App\servicios;
use App\Encryption;

use Illuminate\Support\Collection as Collection;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Jenssegers\Agent\Agent;

use DB;
use View;
use Mail;
use Validator;

class ServiciosController extends Controller
{
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->urlBackend = webservice();
	}

	public function getAutoview($i){
		$WsEstilos = new Client;
		$agent     = new Agent;

		$servidor  = $this->urlBackend;
		$isDesktop = $agent->isDesktop();

		// dd($isDesktop);
		$isDesktop = false;

		$ResponseEstilos = $WsEstilos->get($this->urlBackend."/api/detailed_styles");

		$Estilos = json_decode($ResponseEstilos->getBody());

		$contenido = $Estilos->detailed_styles;
		$datos     = array();

		foreach ($contenido as $y => $style) {
			$datos[$style->category_name][$y] = $style;
		}

		$count   = count($datos);
		$content = $datos[$i];

		return view('servicios.autoviewmobile',compact('count','content','i','isDesktop','servidor'));
		// if($isDesktop){
		// 	return view('servicios.autoview',compact('count','content','i','isDesktop','servidor'));
		// }
		// else{
		// 	return view('servicios.autoviewmobile',compact('count','content','i','isDesktop','servidor'));
		// }
	}

	public function getAutobook($name,$id,$ammount,$namestyle,$cant){
		//dd($name,$id,$ammount,$namestyle,$cant);
		$sessionuser = Session::get('sessionuserC');

		//ws para si es nuevo usuario
		$user = Session::get('emailuser');
		$wsNewUser = new Client;
		$ResponseNewUser = $wsNewUser->post($this->urlBackend."/api/is_new",[
			'json' => ['email' => $user]
		]);

		$NewUser = json_decode($ResponseNewUser->getBody());

		if($NewUser->status == "OK"){
			$is_new_user = $NewUser->is_new_user;
		}else{
			$is_new_user = false;
		}
		//fin ws para si es nuevo usuario

		if ($sessionuser == 'OK'){

		$user = Session::get('emailuser');//\Auth::user()->email;//correo usuario
		$pass = Session::get('pass');

		$WsLogin = new Client;
		$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
		'json' => ['username' => $user, 'password' => $pass]
		]);

		$Login            = json_decode($ResponseLogin->getBody());
		$tel              = $Login->phone;
		$tipousuario      = $Login->type;
		$UrlAnadirTarjeta = $Login->cc_add_form;

		if ($sessionuser == 'OK')
		{

		$direccion = Session::get('direccion1user');

		// formas de pago
		$WsFormasPago = new Client;
		$ResponseFormasPago = $WsFormasPago->get($this->urlBackend."/api/get_payments");

		$FormasPago = json_decode($ResponseFormasPago->getBody());
		$forms=$FormasPago->payment_types;

		// Servicios
		$WsEstilos = new Client;
		$ResponseEstilos = $WsEstilos->get($this->urlBackend."/api/detailed_styles");

		$Estilos = json_decode($ResponseEstilos->getBody());

				foreach ($Estilos->detailed_styles as $key => $value) {
					$arrayStyle[$value->category_name][] = $value;
				}

				$styleDefault = 0;
				foreach ($arrayStyle as $categoryName => $value) {
					$styleDefault++;
					if($categoryName == $name) break;
				}

				$autobook='Si'; //indicar que se viene de autobook
				return view('agendamiento.agenda', compact('is_new_user','cant','arrayStyle','direccion','forms','count','tel','id','name','ammount','namestyle','autobook', 'styleDefault'));
			}

		return view('auth.login');
		}

		// formas de pago
		$WsFormasPago = new Client;
		$ResponseFormasPago = $WsFormasPago->get($this->urlBackend."/api/get_payments");

		$FormasPago = json_decode($ResponseFormasPago->getBody());
		$forms=$FormasPago->payment_types;

		$WsEstilos = new Client;
		$ResponseEstilos = $WsEstilos->get($this->urlBackend."/api/detailed_styles");

		$Estilos = json_decode($ResponseEstilos->getBody());

		foreach ($Estilos->detailed_styles as $key => $value) {
			$arrayStyle[$value->category_name][] = $value;
		}

		$styleDefault = 0;
		foreach ($arrayStyle as $categoryName => $value) {
			$styleDefault++;
			if($categoryName == $name) break;
		}

		$direccion = "";
		$autobook='Si'; // indicar que se viene de autobook
		return view('agendamiento.agenda', compact('is_new_user','cant','arrayStyle','direccion','forms','count','id','name', 'ammount','namestyle','autobook', 'styleDefault'));
	}
}