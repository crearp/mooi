<?php

namespace App\Http\Controllers\Api;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\servicios;
use DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
use App\Encryption;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use View;
use Mail;
use App\Http\Requests\cuponRequest;

class cuponController extends Controller
{
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->urlBackend = webservice();

		// $url = url()->current();
		// if(strpos($url, 'localhost') === false) $this->urlBackend = 'http://localhost:'.ENV('WEBSERVICE_PORT');
		// $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}

	public function show(cuponRequest $request){

		//tratamiento estilos
		$estilos=$request->estilos;
		$estilos=str_replace("[", "", $estilos);
		$estilos=str_replace("]", "", $estilos);
		$estilos=explode(",", $estilos);

		foreach ($estilos as $key) {
			$arrEstilos[]=$key*1;
		}
		//fin tratamiento estilos

		//tratamiento id user
		$intIdUser=$request->id*1;
		//fin tratamiento id user

		//tratamiento fecha
		$fecha_cupon=$request->fecha;
		$fecha_cupon=str_replace("-", "/", $fecha_cupon);
		//fin tratamiento fecha

		$WsCuponPromocion = new Client;
		$ResponseCuponPromocion = $WsCuponPromocion->post($this->urlBackend."/api/promo_code",[
			  	'json' => ["user_id" => $intIdUser,
			  			   "code" => $request->cupon,
			  			   "styles" => $arrEstilos,
			  			   "date" => $fecha_cupon
			  			],
				'headers' => [
						'Content-type' => 'application/json'
						]
		]);

		$CuponPromocion = json_decode($ResponseCuponPromocion->getBody(), true);
		$estadoPromoCode=$CuponPromocion['status'];
		$mensajePromoCode=$CuponPromocion['message'];
		//dd($ResponseCuponPromocion);
		if($estadoPromoCode != "FAILED"){

			$descuento=number_format($CuponPromocion['ammount_discount']);

			$totalPagar=number_format($request->totalPagar-$CuponPromocion['ammount_discount']);

			Session::put('cuponutilizado',$request->cupon);

			return response()->json([
                "status" => "success",
                "data" => $mensajePromoCode,
                "descuento" => $descuento,
                "total" => $totalPagar,
                "nombreCupon" => $request->cupon
            ], 200);
		}else{

			$request->session()->forget('cuponutilizado');

			return response()->json([
                "status" => "failed",
                "data" => $mensajePromoCode
            ], 200);
		}

	}
}