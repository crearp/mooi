<?php

namespace App\Http\Controllers\Api;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\servicios;
use DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
use App\Encryption;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use View;
use Mail;
use App\Http\Requests\festivosRequest;

class festivosController extends Controller
{
	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->urlBackend = webservice();

		// $url = url()->current();
		// if(strpos($url, 'localhost') === false) $this->urlBackend = 'http://localhost:'.ENV('WEBSERVICE_PORT');
		// $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}

	public function show(festivosRequest $request){

		$fecha_cupon = $request->fecha;
		$fecha_cupon = str_replace("-", "/", $fecha_cupon);

		$WsFestivos = new Client;
		$ResponseFestivos = $WsFestivos->post($this->urlBackend."/api/holiday",[
			  	'json' => [ "date" => $fecha_cupon ],
				'headers' => [ 'Content-type' => 'application/json' ]
		]);

		$Festivos = json_decode($ResponseFestivos->getBody(), true);
		$estadoFestivos=$Festivos['status'];
		$mensajeFestivos=$Festivos['isHoliday'];
		//dd($ResponseCuponPromocion);
		if($mensajeFestivos != false){

			return response()->json([
                "status" => "success",
                "data" => "Si es día festivo",
                "festivo" => true
            ], 200);
		}else{

			$request->session()->forget('cuponutilizado');

			return response()->json([
                "status" => "succes",
                "data" => "No es día festivo",
                "festivo" => false
            ], 200);
		}

	}
}