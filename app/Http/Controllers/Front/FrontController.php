<?php

namespace App\Http\Controllers\Front;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\servicios;
use DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
use App\Encryption;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use View;
use Mail;

class FrontController extends Controller
{

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->urlBackend = webservice();

		// $url = url()->current();
		// if(strpos($url, 'localhost') === false) $this->urlBackend = 'http://localhost:'.ENV('WEBSERVICE_PORT');
		// $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}

    public function getPerfilUsuario(){

		//informacion de perfil
		$id_user = Session::get('iduser');//\Auth::user()->id;//Usuario
		$user    = Session::get('emailuser');//\Auth::user()->email;//correo usuario
		$pass    = Session::get('pass');

		$WsLogin = new Client;
		$ResponseLogin= $WsLogin->post($this->urlBackend."/api/login",[
			'json' => ['username' => $user, 'password' => $pass]
		]);

		$Login = json_decode($ResponseLogin->getBody());
		$nombre=$Login->name;
		$apellido=$Login->lastname;
		$telefono=$Login->phone;
		$correo=$Login->email;
		$direccion=$Login->address_one;
		$tipousuario=$Login->type;
		$StrMedComuni=$Login->communications_string_value;
		$IntMedComuni=$Login->communications_int_value;
		//informacion de perfil

		//Informacion para eliminar tarjeta
		$user = Session::get('iduser');//\Auth::user()->id;//Usuario
		$user = Session::get('emailuser');//\Auth::user()->email;//correo usuario
		$pass = Session::get('pass');

		$WsLogin = new Client;

		$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
			'json' => ['username' => $user, 'password' => $pass]
		]);

		$Login = json_decode($ResponseLogin->getBody());

		$estado21=$Login->status;
		$cards=$Login->cards;
		$id=$Login->id;

		if(Session::get('is_mobile') && Session::get('is_mobile') == true){
			return view('mobile.front.perfilUsuario', compact('nombre','apellido','telefono','correo','direccion','id','cards','StrMedComuni','IntMedComuni'));
		}else{
			return view('front.perfilUsuario', compact('nombre','apellido','telefono','correo','direccion','id','cards','StrMedComuni','IntMedComuni'));
		}
	}

	public function getPerfilProfessional(){

		//informacion de perfil
		$id_user_professional=Session::get('iduser');//\Auth::user()->id;//Usuario

		$correo = Session::get('emailuser');//\Auth::user()->email;//correo usuario
		$pass   = Session::get('pass');
		$foto   = Session::get('foto');

		$WsLogin = new Client;
		$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
			'json' => ['username' => $correo, 'password' => $pass]
		]);

		$Login = json_decode($ResponseLogin->getBody());

		//informacion de perfil
		$nombre      = $Login->name;
		$correo      = $Login->email;
		$apellido    = $Login->lastname;
		$telefono    = $Login->phone;
		$tipousuario = $Login->type;

		//informacion para historial
		$id_authuser = Session::get('iduser');

		$WsAgendaProfesional = new Client;
		$ResponseAgendaProfesional = $WsAgendaProfesional->post($this->urlBackend."/api/professional_book",[
			 'json' => ['user_id' => $id_user_professional]
		]);

		$AgendaProfesional = json_decode($ResponseAgendaProfesional->getBody());
		$cantserv = 0;

		if(isset($AgendaProfesional->status)){
			$serviciossinordenar=$AgendaProfesional->status;
			$servicios=array_reverse($serviciossinordenar);
			\Flash::success("Actualmente no tienes servicios agendados");
		}else{
			$serviciossinordenar=$AgendaProfesional;
			$servicios=array_reverse($serviciossinordenar);
			$cantserv=sizeof($servicios);

		}
		//fin informacion para historial
		return view('front.perfilProfessional', compact('servicios','cantserv','nombre','apellido','telefono','correo','foto'));
	}

	public function postActualizarPerfil(Request $request){

		$nombre         = $request->get('nombre');
		$apellido       = $request->get('apellido');
		$telefono       = $request->get('telefono');
		$direccion      = $request->get('direccion');
		$comunicaciones = $request->get('comunicaciones')*1;

		$pass    = Session::get('pass');
		$correo  = Session::get('emailuser');
		$id_user = Session::get('iduser');

		$WsActualizarPerfil = new Client;
		$ResponseActualizarPerfil = $WsActualizarPerfil->post($this->urlBackend."/api/update",[
			 'json' => ["id" => $id_user,
						"name" => $nombre,
						"lastname" => $apellido,
						"password" => $pass,
						"phone" => $telefono,
						"email" => $correo,
						"address_one" => $direccion,
						"address_two" => "no",
						"city" => "no",
						"comunications" => $comunicaciones,
			 			]
		]);

		$ActualizarPerfil = json_decode($ResponseActualizarPerfil->getBody());

		$estadoActualizarPerfil=$ActualizarPerfil->status;
		if(Session::get('is_mobile') && Session::get('is_mobile') == true){
			if($estadoActualizarPerfil != 'FAILED"'){
				flash('Tu información se guardó correctamente', 'success');
				return redirect()->route('perfil_usuario_mobile');
			}else{
				$MensajeEstadoBackend=$ActualizarPerfil->message;
				flash($MensajeEstadoBackend, 'danger');
				return redirect()->route('perfil_usuario_mobile');
			}
		}else{
			if($estadoActualizarPerfil != 'FAILED"'){
				flash('Tu información se guardó correctamente', 'success');
				return redirect()->route('perfil_usuario');
			}else{
				$MensajeEstadoBackend=$ActualizarPerfil->message;
				flash($MensajeEstadoBackend, 'danger');
				return redirect()->route('perfil_usuario');
			}
		}

	}

	public function postCorreoContactenos(Request $request){
		$correo=$request->get('correo');
		$asunto=$request->get('asunto');
		$cuerpo=$request->get('cuerpo');

		$WsCorreoContactenos = new Client;
		$ResponseCorreoContactenos = $WsCorreoContactenos->post("http://mooi.com.co:8000/api/contact_email",[
			'json' => ['email' => $correo, 'message' => $cuerpo, 'subject' => $asunto]
		]);

		$CorreoContactenos = json_decode($ResponseCorreoContactenos->getBody());
		$estado=$CorreoContactenos->status;

		flash($estado, 'info');
		return view('front.contact');

	}

	public function postCorreoTrabaja(Request $request){

		$areas=$request->get('areas');

		$this->file=$request->file('imagen1');
		$this->request = $request;

		if($request->file('imagen2') != NULL){
			$this->file2=$request->file('imagen2');
			$this->request = $request;
		}
		if($request->file('imagen3') != NULL){
			$this->file3=$request->file('imagen3');
			$this->request = $request;
		}

		Mail::send('auth.emails.emailtrabaje',compact('request','areas'), function ($message) {
			$message->from('info@mooibelleza.com', 'USUARIO');
			$message->to('info@mooibelleza.com', 'MOOI')->subject('Formulario para nuev@ Profesional Mooi');
			$message->cc('jaos1991@gmail.com', 'MOOI');
			$message->cc('bryan.canon@creardigital.com', 'MOOI');
			$message->cc('jhon.marroquin@creardigital.com', 'MOOI');

			//$message->attach($request->file('imagen1')->getRealPath(), array($request->file('imagen1')->getClientOriginalExtension(),
		//'mime' => $request->file('imagen1')->getMimeType())//);

			$message->attach($this->request->file('imagen1'), array($this->request->file('imagen1')->getClientOriginalExtension(),
			   'mime' => $this->request->file('imagen1')->getMimeType())
		   );

			if($this->request->file('imagen2') != NULL){
				$message->attach($this->request->file('imagen2'), array($this->request->file('imagen2')->getClientOriginalExtension(),
			   'mime' => $this->request->file('imagen2')->getMimeType())
		   );
			}
			if($this->request->file('imagen3') != NULL){
				$message->attach($this->request->file('imagen3'), array($this->request->file('imagen3')->getClientOriginalExtension(),
			   'mime' => $this->request->file('imagen3')->getMimeType())
		   );
			}

		   /* if(isset($request->file('imagen2'))){
				$file2=$request->file('imagen2');
				$message->attach($file2);
			}elseif (isset($request->file('imagen3'))) {
				$file3=$request->file('imagen3');
				$message->attach($file3);
			}*/
		});

		if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
			flash('Formulario enviado. Pronto nos contactaremos contigo', 'info');
			return view('mobile.front.contactwor');
		}else{
			flash('Formulario enviado. Pronto nos contactaremos contigo', 'info');
			return view('front.contactwor');
		}
	}

	public function getNoticias(){

		$WsNoticia = new Client;
		$servidor  = $this->urlBackend;

		$ResponseNoticia = $WsNoticia->get($this->urlBackend."/api/blog");
		$Noticia = json_decode($ResponseNoticia->getBody());

		foreach ($Noticia as $key) {
			if($key->type == "noticia"){
				$key->photo=str_replace("./", "/", $key->photo);
				$noticia[]=$key;
			}
		}

		return view('front.noticias',compact('noticia','servidor'));
	}

	public function getBlog(){

		$WsBlog   = new Client;
		$servidor = $this->urlBackend;

		$ResponseBlog = $WsBlog->get($this->urlBackend."/api/blog");
		$Blog = json_decode($ResponseBlog->getBody());

		foreach ($Blog as $key) {
			if($key->type == "blog"){
				$key->photo=str_replace("./", "/", $key->photo);
				$blog[]=$key;
			}
		}

		return view('front.blog',compact('blog','servidor'));
	}
}