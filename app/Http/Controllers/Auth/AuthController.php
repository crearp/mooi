<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\servicios;
use DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
use GuzzleHttp\Client;
use Illuminate\Support\Collection as Collection;
use App\Encryption;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use View;
use Mail;

class AuthController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Where to redirect users after login / registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/';
	protected $urlBackend;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        $this->urlBackend = webservice();

		// $url = url()->current();
		// if(strpos($url, 'localhost') === false) $this->urlBackend = 'http://localhost:'.ENV('WEBSERVICE_PORT');
		// $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'first_name' => 'required|max:255',
			'first_lastname' => 'required|max:255',
			'phone' => 'required|max:15',
			'direccion' => 'required',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:6|confirmed',
			'terminos' => 'required',

		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	/*protected function create(array $data)
	{
		Session::put('pass',$data['password']);
		/*Base de datos Jose
		$WsRegistro = new Client;

		$ResponseRegistro = $WsRegistro->post($this->urlBackend."/api/register",[
			  'json' => ['name' => $data['first_name'].' '.$data['second_name'], 'lastname' => $data['first_lastname'].' '.$data['second_lastname'], 'phone' => $data['phone'], 'password' => $data['password'], 'email' => $data['email'], 'address_one' => $data['direccion'], 'address_two' => "no", 'city' => "Default"]
		]);

		$Registro = json_decode($ResponseRegistro->getBody());

		//dd($Registro);

		if($Registro->status != 'OK'){

		$id=$Registro->id;

		/*Base de datos Jhonatan
			return User::create([
				'nombre1' => $data['first_name'],
				'nombre2' => $data['second_name'],
				'apellido1' => $data['first_lastname'],
				'apellido2' => $data['second_lastname'],
				'telefono' => $data['phone'],
				'direccion1' => $data['direccion'],
				'email' => $data['email'],
				'password' => bcrypt($data['password']),
				'password1' => bcrypt($data['password_confirmation']),
				//'password' => Crypt::encrypt($data['password']),
				//'password1' => Crypt::encrypt($data['password_confirmation']),
				'confirmacion' => 'Si',
				'medio' => $data['medio'],
				'tipo' => $data['tipo_usuario'],
				'estado' => 'en_proceso',
				'acepto' => 'No',
				'remember_token' => '',
				'confirmacion_token' => '',
				'token' => '',
				'id_userpython' => $id,
				'remember_token' => '{{ csrf_field() }}',
			]);
		}else{
			 echo 'error';
		}

	}*/

	public function getRegPromoCode(Request $request){
		//generacion de promocode
		$promoCode="PromoCodePrueba123";
		$email = $request->get('email_promoCode');
		return view('auth.register', compact('email','promoCode'));
	}

	public function postRegister(Request $request){

		//Session::put('pass',$request->get('password'));
		/*Base de datos Jose*/

		$comunicaciones   = $request->get('medio')*1;
		$WsRegistro       = new Client;
		$ResponseRegistro = $WsRegistro->post($this->urlBackend."/api/register",[
			  'json' => ['name' => $request->get('first_name').' '.$request->get('second_name'),
			  			 'lastname' => $request->get('first_lastname').' '.$request->get('second_lastname'),
			  			 'phone' => $request->get('phone'),
			  			 'password' => $request->get('password'),
			  			 'email' => $request->get('email'),
			  			 'address_one' => $request->get('direccion'),
			  			 'address_two' => "no",
			  			 'city' => "Default",
			  			 'comunications' => $comunicaciones
			  			]
		]);

		$Registro = json_decode($ResponseRegistro->getBody());
		$estado   = $Registro->status;

		if($estado != "FAILED"){

			$WsLogin = new Client;
			$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
				'json' => ['username' => $request->get('email'), 'password' => $request->get('password')]
			]);

			$Login  = json_decode($ResponseLogin->getBody());
			$estado = $Login->status;

			if($estado != "FAILED"){
				Session::put('pass',$request->get('password'));
				Session::put('direccion1user',$Login->address_one);
				Session::put('direccion2user',$Login->address_two);
				Session::put('urlcardsuser',$Login->cc_add_form);
				Session::put('iduser',$Login->id);
				Session::put('telefonouser',$Login->phone);
				Session::put('emailuser',$Login->email);
				Session::put('cardsuser',$Login->cards);
				Session::put('apellidouser',$Login->lastname);
				Session::put('nombreuserC',$Login->name);
				Session::put('tipo',$Login->type);
				View::share('nombreuserV', $Login->name);
				Session::put('sessionuserC','OK');
				View::share('sessionuserV', 'OK');

				$sessionagenda=Session::get('agendalogout');
				if($sessionagenda != 'OK'){
					if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
						return redirect('/mobile');
					}else{
						return redirect('/');
					}
				}else{
					return redirect()->route('agenda_servicioslogout2');
				}

			}else{
				if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
					$mensaje2=$Login->message;
					Session::put('sessionuserC','NO');
					View::share('sessionuserV', 'NO');
					//Flash::message('{{ $mensaje2 }}');
					flash($mensaje2, 'danger');
					return redirect('inicio-sesion-movil');
				}else{
					$mensaje2=$Login->message;
					Session::put('sessionuserC','NO');
					View::share('sessionuserV', 'NO');
					//Flash::message('{{ $mensaje2 }}');
					flash($mensaje2, 'danger');
					return redirect('inicio-sesion');
				}
			}

		}else{
			if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
				$mensaje1=$Registro->message;
				//Flash::message('{{ $mensaje1 }}');
				flash($mensaje1, 'danger');
				return redirect()->route('get-registro-movil')->withInput();/*
				return view('auth.register')->withInput();*/
			}else{
				$mensaje1=$Registro->message;
				//Flash::message('{{ $mensaje1 }}');
				flash($mensaje1, 'danger');
				return redirect()->route('get-registro')->withInput();/*
				return view('auth.register')->withInput();*/
			}
		}
	}

	public function postLogin(Request $request){

		$WsLogin = new Client;
		$ResponseLogin = $WsLogin->post($this->urlBackend."/api/login",[
			  'json' => ['username' => $request->get('email'), 'password' => $request->get('password')]
		]);

		$Login = json_decode($ResponseLogin->getBody());

		$estado=$Login->status;

		if($estado == "OK"){
			$type_user=$Login->type;

			if($type_user != "PROFESSIONAL"){
				Session::put('pass',$request->get('password'));
				Session::put('direccion1user',$Login->address_one);
				Session::put('direccion2user',$Login->address_two);
				Session::put('urlcardsuser',$Login->cc_add_form);
				Session::put('iduser',$Login->id);
				Session::put('telefonouser',$Login->phone);
				Session::put('emailuser',$Login->email);
				Session::put('cardsuser',$Login->cards);
				Session::put('apellidouser',$Login->lastname);
				Session::put('nombreuserC',$Login->name);
				Session::put('tipo',$Login->type);
				View::share('nombreuserV', $Login->name);
				Session::put('sessionuserC','OK');
				Session::put('is_new_user',$Login->is_new_user);
			}else{
				Session::put('pass',$request->get('password'));
				Session::put('iduser',$Login->id);
				Session::put('telefonouser',$Login->phone);
				Session::put('tipo',$Login->type);
				Session::put('emailuser',$Login->email);
				Session::put('apellidouser',$Login->lastname);
				Session::put('nombreuserC',$Login->name);
				View::share('nombreuserV', $Login->name);
				Session::put('cedula',$Login->cc);
				Session::put('foto',$Login->photo);
				Session::put('sessionuserC','OK');
			}

			$sessionagenda=Session::get('agendalogout');
			$tipo_usuario=Session::get('tipo');

			if($sessionagenda != 'OK'){
				if($tipo_usuario != "PROFESSIONAL"){
					if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
						return redirect('/mobile');
					}else{
						return redirect('/');
					}
				}else{
					return redirect("perfilprofessional");
				}

			}else{
				return redirect()->route('agenda_servicioslogout2');
			}

		}else{
			if(Session::get('is_mobile') && Session::get('is_mobile') == true ){
				$mensaje3=$Login->message;
				Session::put('sessionuserC','NO');
				View::share('sessionuserV', 'NO');
				//Flash::message('Su correo o contraseña son incorrectos, verifique y vuelva a intentarlo');
				flash($mensaje3, 'danger');
				return redirect('inicio-sesion-movil');
			}else{
				$mensaje3=$Login->message;
				Session::put('sessionuserC','NO');
				View::share('sessionuserV', 'NO');
				//Flash::message('Su correo o contraseña son incorrectos, verifique y vuelva a intentarlo');
				flash($mensaje3, 'danger');
				return redirect('inicio-sesion');
			}
		}

	}

	public function getLogout(){
		if(Session::get('is_mobile') && Session::get('is_mobile') == true){
			Session::flush();
			return redirect('/mobile');
		}else{
			Session::flush();
			return redirect('/');
		}
	}

	public function sendResetLinkEmail(Request $request){ //Aqui se recibe el correo de la vista reset y se envia el link al mismo
		$correo=$request->get('email');

		$WsResetPassword = new Client;

		$ResponseResetPassword = $WsResetPassword->post($this->urlBackend."/api/passsord/reset",[
			'json' => ['email' => $correo]
		]);

		$ResetPassword = json_decode($ResponseResetPassword->getBody());
		$estado=$ResetPassword->message;

		flash($estado, 'info');
		return view('auth.passwords.email');
	}

	public function showResetForm($info){ //aqui se recibe y procesa la URL para reset de la contraseña
		$preinfo = preg_split("~&~", $info);
		$id=$preinfo[1];

		return view('auth.passwords.reset', compact('id'));
	}

	public function resetpass(Request $request){
		$id=$request->get('id');
		$pass=$request->get('password');
		$WsConfirmacionResetPassword = new Client;

		$ResponseConfirmacionResetPassword = $WsConfirmacionResetPassword->post($this->urlBackend."/api/passsord/done",[
			'json' => ["app_secret" => "5abfnJ5eIrOB3rkqy7gtGgk6DVTZkN6i","user_id" => $id, "new_password" => $pass]
		]);

		$ConfirmacionResetPassword = json_decode($ResponseConfirmacionResetPassword->getBody());
		$estado=$ConfirmacionResetPassword->status;

		if($estado != 'FAILED'){
			$msj=$ConfirmacionResetPassword->message;
			flash($msj, 'info');
			 return redirect()->route('inicio-sesion');
		}else{
			$msj=$ConfirmacionResetPassword->message;
			flash($msj, 'info');
			return view('auth.passwords.email');
		}



	}
}