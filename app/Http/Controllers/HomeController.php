<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
use GuzzleHttp\Client;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		/*//obtención de datos para loguin interno
		$user=\Auth::user()->id;//Usuario

		$correo=\Auth::user()->email;//correo usuario
		$pass = Session::get('pass');
		//fin loguin interno

		$userl=$correo;
		$passl=$pass;

		$client2 = new Client;

		$res2 = $client2->post("http://159.203.42.145:8000/api/login",[
			'json' => ['username' => $userl, 'password' => $passl]
		]);

		$arr2 = json_decode($res2->getBody());

		$estado2=$arr2->status;
		$cards1=$arr2->cards;
		$cards11=sizeof($cards1);
		$UrlAnadirTarjeta=$arr2->cc_add_form;

		if($cards11 == '0'){
			return view('transacciones.addcard',compact('UrlAnadirTarjeta'));
		}else{*/
			return view('front.home');
		/*}*/

	}

	public function loginmant(Request $request){

		$nombre = $request->get('login');
		$pass   = $request->get('password');

		if($nombre == 'admin128CD' || $nombre == 'bryan.canon' || $nombre == 'luis.jaramillo' || $nombre == 'adminCD987' || $nombre == 'userSUadminCD345'){

			if($pass == '123*456*98' || $pass == 'CD*mooi*0' || $pass == 'CD*bryan*0' || $pass == 'CD*luis*0'){
				return redirect('inicio');
			}else{
				return redirect('/')->with('succes','contraseña incorrecta');
			}

		}else{
			return redirect('/')->with('succes','usuario de ingreso incorrecto');
		}
	}
}