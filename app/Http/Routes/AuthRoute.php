<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Rutas para Autorizacion

//inicio-sesion
Route::get('inicio-sesion', function () {
	if(is_null(Session::get('sessionuserC')) || Session::get('sessionuserC') != 'OK'){
		return view('auth.login');
	}else{
		return view('front.home');
	}
});
Route::post('inicio-sesion', ['as'=> 'inicio-sesion','uses'=>'Auth\AuthController@postlogin']);

//Registro
Route::get('registro', ['as' => 'get-registro', function () { $email=""; $promoCode=""; return view('auth.register', compact('email','promoCode'));}]);
Route::post('registro', ['as'=> 'post-registro','uses'=>'Auth\AuthController@postRegister']);

//Registro con PromoCode
Route::post('regPromoCode', ['as'=> 'post-regPromo','uses'=>'Auth\AuthController@getRegPromoCode']);

//Reset Contraseña
Route::get('contrasena/cambiar', function () {return view('auth.passwords.email');});//password/reset
Route::post('password/email', 'Auth\AuthController@sendResetLinkEmail');
Route::get('change_password/{info}', 'Auth\AuthController@showResetForm');
Route::post('password/reset', 'Auth\AuthController@resetpass');

//if(!is_null(Session::get('iduser'))){
	//Logout
	Route::get('logout', ['as'=> 'logout','uses'=>'Auth\AuthController@getLogout']);
	Route::post('send', ['as' => 'send', 'uses' => 'MailController@postSend'] );
//}



/*Route::get('password-reset', ['as' => 'password_reset', 'uses' => 'Front\PasswordController@getEmail']);
Route::post('password-reset', ['as' => 'password_reset', 'uses' => 'Front\PasswordController@postEmail']);
Route::get('change_password/{info}', ['as' => 'change_password', 'uses' => 'Front\PasswordController@getPasswordChange']);
Route::post('password-change', ['as' => 'password_change', 'uses' => 'Front\PasswordController@postPasswordChange'])*/;
//Route::get('change_password/{info}', 'Auth\AuthController@showResetForm');
//Route::post('password/reset', 'Auth\AuthController@resetpass');