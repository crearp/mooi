<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//calificacion por correo electronico
Route::get('calificacion/{id}/', ['as'=> 'url_calificacion','uses' => 'Historial\HistorialController@getCalificacion']);
Route::get('calificacion/finalizacion-calificacion/{mensaje}/{estado}', ['as'=> 'url_fin_calificacion','uses' => 'Historial\HistorialController@getCalificacionRedirect']);
Route::get('calificacionMail/print-comments/', ['as'=> 'url_calificacion_mail','uses' => 'Historial\HistorialController@postCalificacion']);

//if(!is_null(Session::get('iduser'))){
	//Rutas para historial
	Route::get('/historial', ['as'=> 'agenda_historial','uses'=>'Historial\HistorialController@getHistorial']);
	Route::get('/tiempo/{dia}/{mes}/{ano}/{hora}/{estado}', ['as'=> 'tiempo_cita','uses'=>'Historial\HistorialController@getTiempo']);

	Route::post('/cancelar', ['as'=> 'cancelar_cita','uses'=>'Historial\HistorialController@postCancelar']);
	Route::post('/cancelarProfessional', ['as'=> 'cancelar_citaProfessional','uses'=>'Historial\HistorialController@postCancelarProfessional']);
	//Route::post('/cancelarconfir', ['as'=> 'cancelar_citaconfir','uses'=>'Historial\HistorialController@postCancelarConfirmacion']);
	//Route::post('/iniciar', ['as'=> 'iniciar_cita','uses'=>'Historial\HistorialController@postIniciar']);
	//Route::post('/incumplir', ['as'=> 'incumplilr_cita','uses'=>'Historial\HistorialController@postIncumplir']);
	Route::post('/terminar', ['as'=> 'terminar_cita','uses'=>'Historial\HistorialController@postTerminar']);
	Route::post('/reprogramar', function(){ return redirect()->route('agenda_listas'); });
	Route::post('/calificar', ['as'=> 'calificar_cita','uses'=>'Historial\HistorialController@postCalificar']);
	Route::post('/calificarfinal', ['as'=> 'calificarfinal_cita','uses'=>'Historial\HistorialController@postCalificarFinal']);
//}