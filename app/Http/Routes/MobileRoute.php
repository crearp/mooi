<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Rutas para Front

Route::get('aliados', function () {
    return view('mobile.front.aliados');
});

//Correo Contactenos
Route::get('contacto-movil', function () {return view('mobile.front.contacto');});

//Cancelacion
Route::get('cancelacion', function () {return view('mobile.front.cancelacion');});

//donde encontrarnos
Route::get('donde-estamos', function () {return view('mobile.front.ciudades');});

//terminos y condiciones
Route::get('terminos-y-condiciones-mobile', function () {return view('mobile.front.terms');});

//porque nosotros
Route::get('porque-nosotros-mobile', function () {return view('mobile.front.why_us');});

//comofunciona
Route::get('como-funciona-mobile', function () {return view('mobile.front.comofunciona');});

//privacidad
Route::get('privacidad-mobile', function () {return view('mobile.front.private');});

//trabaja con nosotros
Route::get('trabaja-con-nosotros-mobile', function () {return view('mobile.front.contactwor');});

Route::post('/agendaMobile', ['as' => 'agenda_Mobile', 'uses' => 'Mobile\Agendamiento\AgendaController@agendaEstilosMobile']);

//inicio-sesion
Route::get('inicio-sesion-movil', function () {
	if(is_null(Session::get('sessionuserC')) || Session::get('sessionuserC') != 'OK'){
		return view('mobile.auth.login');
	}else{
		return view('mobile.front.home');
	}
});

//Registro
Route::get('registro-movil', ['as' => 'get-registro-movil', function () {return view('mobile.auth.register');}]);

//logout
Route::get('logout-mobile', ['as'=> 'logout_mobile','uses'=>'Auth\AuthController@getLogout']);

//Reset Contraseña
Route::get('contrasena/cambiar-mobile', function () {return view('mobile.auth.passwords.email');});//password/reset

//historial
Route::get('/historial-mobile', ['as'=> 'agenda_historial_mobile','uses'=>'Historial\HistorialController@getHistorial']);

//perfil
Route::get('perfil-mobile', ['as'=> 'perfil_usuario_mobile','uses' => 'Front\FrontController@getPerfilUsuario']);

//reprogramar mobile
Route::post('/reprogramar-mobile', function(){ return redirect('/mobile'); });

// Route::get('error', function(){
//     abort(500);
// });

// Route::get('como_funciona', function () {
//     return view('front.comofunciona');
// });
// Route::get('porque-nosotros', function () {
//     return view('front.why_us');
// });
// Route::get('servicios', function () {
//     return view('front.servicios');
// });
// Route::get('terminos-y-condiciones', function () {
//     return view('front.terms');
// });
// Route::get('politicas-privacidad', function () {
//     return view('front.private');
// });
// Route::get('clientes', function () {
//     return view('front.clientes');
// });
// Route::get('preguntas', function () {
//     return view('front.preguntas');
// });
// Route::get('novias', function () {
//     return view('front.novias');
// });

// //Landing de lanzamiento mooi
// Route::get('lanzamiento', function () {
//     return redirect()->to('http://unbouncepages.com/corte/');
// });

// //prueba calificacion
// //Route::post('calificacion', function(){ dd('si ingresa'); });

// //NOTICIAS
// Route::get('noticias', ['as'=> 'noticias_mooi','uses' => 'Front\FrontController@getNoticias']);

// //BLOG
// Route::get('blog', ['as'=> 'blog_mooi','uses' => 'Front\FrontController@getBlog']);

// //Correo Contactenos
// Route::get('contacto', function () {return view('front.contact');});
// Route::post('contacmail', ['as' => 'contacto_mail', 'uses' => 'Front\FrontController@postCorreoContactenos'] );

// //Correo Trabaja con nosotros
// Route::get('trabaja-con-nosotros', function () {return view('front.contactwor');});
// Route::post('contacmailtra', ['as' => 'contacto_mailtra', 'uses' => 'Front\FrontController@postCorreoTrabaja'] );

// //if(!is_null(Session::get('iduser'))){
//     //Perfiles
//     Route::get('perfil', ['as'=> 'perfil_usuario','uses' => 'Front\FrontController@getPerfilUsuario']);
//     Route::get('perfilprofessional', ['as'=> 'perfil_professional','uses' => 'Front\FrontController@getPerfilProfessional']);
//     Route::post('/actualizarPerfil', ['as'=> 'actualizar_perfil','uses'=>'Front\FrontController@postActualizarPerfil']);
// //}