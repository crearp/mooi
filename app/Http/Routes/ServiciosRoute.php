<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Rutas para Servicios
//vista Inspirate de servicios automaticos
//llamada de la vista
Route::get('/servicios/{i}', 'Servicios\ServiciosController@getAutoview');
//llamada del servicio para agendar
Route::get('servicios/autoservicios/{name}/{id}/{ammount}/{namestyle}/{cant}', array('as'=> 'auto_book','uses'=>'Servicios\ServiciosController@getAutobook'));
