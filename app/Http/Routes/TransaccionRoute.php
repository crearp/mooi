<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Rutas para transaccion

//Validacion si esta logout o no
Route::get('/kjhsuy43uy78x', 'Transacciones\TransaccionesController@getValidAddCardLogout');

//if(!is_null(Session::get('iduser'))){
	//eliminar tarjetas
	Route::get('/deletecards', ['as'=> 'eliminar_cards','uses'=>'Transacciones\TransaccionesController@getEliminarTarjetas']);
	Route::post('/postdeletecards', ['as'=> 'posteliminar_cards','uses'=>'Transacciones\TransaccionesController@postEliminarTarjetas']);

	//agregar tarjetas
	Route::get('/changecard', ['as'=> 'cambiar_cards','uses'=>'Transacciones\TransaccionesController@getAgregarTarjeta']);
//}