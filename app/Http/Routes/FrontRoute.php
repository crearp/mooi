<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Rutas para Front

//Cancelacion
Route::get('politicas-cancelacion', function () {return view('front.cancelacion');});

Route::get('error', function(){
    abort(500);
});

Route::get('como_funciona', function () {
    return view('front.comofunciona');
});
Route::get('porque-nosotros', function () {
    return view('front.why_us');
});
Route::get('servicios', function () {
    return view('front.servicios');
});
Route::get('terminos-y-condiciones', function () {
    return view('front.terms');
});
Route::get('politicas-privacidad', function () {
    return view('front.private');
});
Route::get('clientes', function () {
    return view('front.clientes');
});
Route::get('preguntas', function () {
    return view('front.preguntas');
});
Route::get('novias', function () {
    return view('front.novias');
});

//Landing de lanzamiento mooi
Route::get('lanzamiento', function () {
    return redirect()->to('http://unbouncepages.com/corte/');
});

//prueba calificacion
//Route::post('calificacion', function(){ dd('si ingresa'); });

//NOTICIAS
Route::get('noticias', ['as'=> 'noticias_mooi','uses' => 'Front\FrontController@getNoticias']);

//BLOG
Route::get('blog', ['as'=> 'blog_mooi','uses' => 'Front\FrontController@getBlog']);

//Correo Contactenos
Route::get('contacto', function () {return view('front.contact');});
Route::post('contacmail', ['as' => 'contacto_mail', 'uses' => 'Front\FrontController@postCorreoContactenos'] );

//Correo Trabaja con nosotros
Route::get('trabaja-con-nosotros', function () {return view('front.contactwor');});
Route::post('contacmailtra', ['as' => 'contacto_mailtra', 'uses' => 'Front\FrontController@postCorreoTrabaja'] );

//if(!is_null(Session::get('iduser'))){
    //Perfiles
    Route::get('perfil', ['as'=> 'perfil_usuario','uses' => 'Front\FrontController@getPerfilUsuario']);
    Route::get('perfilprofessional', ['as'=> 'perfil_professional','uses' => 'Front\FrontController@getPerfilProfessional']);
    Route::post('/actualizarPerfil', ['as'=> 'actualizar_perfil','uses'=>'Front\FrontController@postActualizarPerfil']);
//}