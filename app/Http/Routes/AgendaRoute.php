<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Rutas para agendamiento

//llamado agenda
Route::get('/agenda', ['as'=> 'agenda_listas','uses'=>'Agendamiento\AgendaController@getListas']);

//agenda en login y logout
Route::post('/agendaservicio', ['as'=> 'agenda_servicios','uses'=>'Agendamiento\AgendaController@postAgenda']);
Route::post('/agendaserviciologout', ['as'=> 'agenda_servicioslogout','uses'=>'Agendamiento\AgendaController@postAgendaLogout']);
Route::get('/agendaserviciologout2', ['as'=> 'agenda_servicioslogout2','uses'=>'Agendamiento\AgendaController@postAgenda']);
//resumen de pago
	Route::post('/infopago', ['as'=> 'info_pago','uses'=>'Agendamiento\AgendaController@postPreagenda']);
	/*Route::get('infopago', ['as' => 'getinfo_pago', function () {return view('agendamiento.infopago');}]);*/
	
//if(!is_null(Session::get('iduser'))){
	

	//pagina de pagos con tarjeta de credito
	Route::post('/pagos', ['as'=> 'pagos_cita','uses'=>'Agendamiento\AgendaController@postPagos']);
//}
