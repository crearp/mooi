<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
include 'Routes/AuthRoute.php';
include 'Routes/AgendaRoute.php';
include 'Routes/HistorialRoute.php';
include 'Routes/FrontRoute.php';
include 'Routes/ServiciosRoute.php';
include 'Routes/TransaccionRoute.php';
include 'Routes/PruebasRoute.php';
include 'Routes/MobileRoute.php';
//Ws redencion de cupon
Route::get('/cupon/{cupon}/{id}/{estilos}/{totalPagar}/{fecha}', ['as'=> 'redencion_cupon','uses'=>'Api\cuponController@show']);

//Ws revision de festivos
Route::get('/festivos/{fecha}', ['as'=> 'festivos_agenda','uses'=>'Api\festivosController@show']);

//Route::resource('cupon', 'Api\cuponController',['only' => ['index', 'store', 'update', 'destroy', 'show']]);

Route::get('/{param?}',['as' => '/{param?}', function ($param = false) {

 	$sessionuser=Session::get('sessionuserC');

	if($param && $param == 'mobile'){

		Session::put('is_mobile',true);
		// Servicios
		$WsEstilos = new Client;
		$ResponseEstilos = $WsEstilos->get(webservice()."/api/detailed_styles");

		$Estilos  = json_decode($ResponseEstilos->getBody());
		$jsonData = json_encode(["specify_style_name" => "null", "specify_style_photo" => "null", "specify_style_description" => "null"]);

		foreach ($Estilos->detailed_styles as $key => $value) {
			//$name = str_ireplace([" ", "ñ"], ["","n"], $value->category_name);
			$name = $value->category_name;

			if(COUNT($value->specify_styles) == 0){
				$value->specify_styles[0] = json_decode($jsonData);
			}

			$arrayStyle[$name][] = $value;
		}

		$agendamientoMobile = false;

		$calificar=false;

		if ($sessionuser == 'OK'){

			$id_authuser=Session::get('iduser');

			$WsHistorial = new Client;

			$ResponseHistorial = $WsHistorial->post(webservice()."/api/history",[
				 'json' => ['user_id' => $id_authuser]
			]);

			$Historial = json_decode($ResponseHistorial->getBody());

			if(isset($Historial->status)){
				$serviciossinordenar=$Historial->services;
				$servicios=array_reverse($serviciossinordenar);
				$cantserv=sizeof($servicios);
				$calificar = false;
				$agendamientoMobile = false;
				return view('mobile.front.home', compact('arrayStyle','calificar','agendamientoMobile'));
			}else{
				$serviciossinordenar=$Historial;
				$servicios=array_reverse($serviciossinordenar);
				$cantserv=sizeof($servicios);
				//dd($servicios);
				foreach ($servicios as $key => $value) {
					if($value->state == 'Finalizado' && $value->score == null){
						$calificar = true;
						//servicios para enviar a calificar
						$id_servicio = $servicios[$key]->id;
						$hora_servicio = $servicios[$key]->hour;
						$fecha_servicio = $servicios[$key]->date;
						$serviciosCal[] = $servicios[$key]->services;
						$profesional_servicio = $servicios[$key]->professional;
						//fin servicios para enviar a calificar
						break;
					}
				}
				//dd($id_servicio,$hora_servicio,$fecha_servicio,$serviciosCal,$profesional_servicio);
				if($calificar == true){
					$agendamientoMobile = false; //Para que no aparezca popUp de notificacion de agendamiento.
					return view('mobile.front.calificacionMobile', compact('arrayStyle','serviciosCal','calificar','id_servicio','hora_servicio','fecha_servicio','profesional_servicio','agendamientoMobile'));
				}else{

					if(Session::get('agendamientoMobile') !== null){
						if(Session::get('agendamientoMobile') == true){
							$agendamientoMobile = true;
						}else{
							$agendamientoMobile = false;
						}

						Session::put('agendamientoMobile',false);

					}else{
						$agendamientoMobile = false;
					}

					$calificar = false;

					return view('mobile.front.home', compact('arrayStyle','calificar','agendamientoMobile'));
				}
			}

		}else{
			$agendamientoMobile = false; //Para que no aparezca popUp de notificacion de agendamiento.
			return view('mobile.front.home', compact('arrayStyle','calificar','agendamientoMobile'));

		}
	}else{
		return view('front.home');
	}
}]);

Route::get('/inicio', ['as' => 'inicio','uses' => 'HomeController@index']);
Route::get('/mapa', function(){ return view('pruebas.mapa');});

if(is_null(Session::get('iduser'))){
	Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
}