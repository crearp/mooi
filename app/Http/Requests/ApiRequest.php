<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

abstract class ApiRequest extends FormRequest
{
	protected function formatErrors(Validator $validator)
	{
		return [
            "success" => false,
            "error" => [
                "message" => "Existen errores en algunos campos",
                "data" => $validator->errors()->all()
            ]
        ];
	}

    public function response(array $errors)
    {
        return response()->json($errors, 400);
    }
}
