<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'usuario';
    protected $fillable = [
        'nombre1', 'nombre2', 'apellido1','apellido2','telefono','direccion1','direccion2','id_departamento','id_ciudad','email', 'password1', 'password2',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password1', 'password2', 'remember_token',
    ];

    public $timestamps = false;
}
