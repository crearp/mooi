<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use Mail;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            //Traer la informacion de tiempo base
            $fecha = Carbon::now()->toDateString(); //fecha actual
            $time = Carbon::now()->toTimeString();//hora actual
            //Fin traer la informacion de tiempo base

            //Tiempo actual
            $arrayTime1=explode(':',$time);
            $arrayTime1[2] = "00";
            $time1=implode(':',$arrayTime1);
            //Fin Tiempo actual

            //Tiempo limite
            $arrayTime2 = explode(':', $time);
            $arrayTime2[0] += "02";
            $arrayTime2[2] = "00";
            $time2 = implode(':', $arrayTime2);
            //Fin Tiempo limite

            //Traer informacion de la base de datos
            $request = DB::table('api_book')
                            ->select('api_userapp.mail', 'api_book.date','api_book.hour','api_userapp.name','api_book.professional_id')
                            ->join('api_userapp', 'api_userapp.id', '=', 'api_book.user_id')
                            ->where('api_book.date','=', $fecha)
                            /*->whereBetween('api_book.hour', [$time1, $time2])*/
                            ->where('api_book.hour','=', $time2)
                            ->where('api_book.state',1)
                            ->get();
            //Fin Traer informacion de la base de datos

            \Log::info($request);

            foreach ($request as $req) {
            //mail de recordatorio para usuario

                $fecha=$req->date;
                $hora=$req->hour;
                $mail=$req->mail;
                $profesional_id=$req->professional_id;

                //mail para la professional del servicio
                $request2 = \DB::table('api_professional')
                            ->select('mail','name')
                            ->where('id','=',$profesional_id)
                            ->get();

                \Log::info($request2);

                foreach ($request2 as $req2){
                    $nombre=$req2->name;
                    \Mail::send('auth.emails.emailreminder', compact('nombre','hora','fecha'), function ($message) use ($req2) {
                        $message->from('info@mooi.com.co', 'MOOI');
                        $message->to($req2->mail, $req2->name)->subject('Recordatorio servicio MOOI');
                        $message->cc('jaos1991@gmail.com','JAOS')->subject('Recordatorio para profesional MOOI');
                    });
                }
                $nombre=$req->name;
                //fin mail para la professional del servicio
                //mail para la usuario del servicio
                \Mail::send('auth.emails.emailreminder', compact('nombre','hora','fecha'), function ($message) use ($req) {
                    $message->from('info@mooi.com.co', 'MOOI');
                    $message->to($req->mail, $req->name)->subject('Recordatorio MOOI');
                    $message->cc('jaos1991@gmail.com','JAOS')->subject('Recordatorio para usuari@ MOOI');
                });
            //FIN mail de recordatorio para usuario
            }

        })->cron('00,30 * * * * *');
    }
}
