<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //ws para traer los estilos del menu
        $client28 = new Client;

        $res28 = $client28->get(webservice(true)."/api/styles");

        $arr28 = json_decode($res28->getBody());

        View::share('menustyles', $arr28);

        //ws para imagenes de pop-up de promoción
        $imgPopUp = new Client;

        $resimgPopUp = $imgPopUp->get(webservice()."/api/popup");

        $arrimgPopUp = json_decode($resimgPopUp->getBody());

        foreach ($arrimgPopUp as $key => $value) {
            if ($key == "discount") {
                $array[$key]=$arrimgPopUp->$key;
            }else{
                $array[$key]= str_replace('./','/',$arrimgPopUp->$key);
            }

        }

        View::share('promoPopUp', $array);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
