<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsuarioProfess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_profess', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre1', 20);
            $table->string('nombre2', 20);
            $table->string('apellido1', 20);
            $table->string('apellido2', 20);
            $table->mediumInteger('telefono');
            $table->string('direccion1', 40);
            $table->string('direccion2', 40);
            $table->integer('id_departamento')->unsigned();
            $table->integer('id_ciudad')->unsigned();
            $table->string('email')->unique();
            $table->string('password1',100);
            $table->string('password2',100);
            $table->integer('confirmacion');
            $table->integer('medio');
            $table->enum('tipo', ['admin', 'user','profesional']);
            $table->enum('estado', ['activo', 'inactivo','en_proceso']);
            $table->tinyInteger('acepto');
            $table->rememberToken();
            $table->string('confirmacion_token',100);
            $table->string('token',100);
            $table->enum('tipo_cuenta', ['corriente', 'ahorros','credito']);
            $table->bigInteger('cuenta');

           $table->foreign('id_departamento')
                ->references('id')
                ->on('departamento')
                ->onDelete('cascade');

            $table->foreign('id_ciudad')
                ->references('id')
                ->on('departamento_ciudad')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuario_profess');
    }
}
