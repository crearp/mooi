<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoTransacciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_transacciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_servicio')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_profesional')->unsigned();
            $table->integer('valor_total');
            $table->date('fecha_inicio');
            $table->time('hora_inicio');
            $table->date('fecha_vence');
            $table->time('hora_vence');
            $table->integer('numeros_tarjeta');
            $table->integer('id_transaccion');
            $table->mediumInteger('referencia');
            $table->enum('estado', ['espera', 'terminado','cancelado','incumplido','en curso']);
            $table->string('token',200);
            $table->string('ccumplimiento',30);
            $table->integer('cservicio');
            $table->text('ccomentario');


           $table->foreign('id_servicio')
                ->references('id')
                ->on('servicios')
                ->onDelete('cascade');

            $table->foreign('id_usuario')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('id_profesional')
                ->references('id')
                ->on('usuario_profess')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('info_transacciones');
    }
}
