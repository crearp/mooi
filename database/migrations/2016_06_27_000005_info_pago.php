<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InfoPago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_uso');
            $table->integer('id_usuario')->unsigned();
            $table->string('nombre', 30);
            $table->string('apellido', 30);
            $table->mediumInteger('numero');
            $table->date('fecha');
            $table->integer('codsecu');

            $table->foreign('id_usuario')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('info_pago');
    }
}
