<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Servicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('categoria',20);
            $table->string('nombre_servicio', 150);
            $table->text('descripcion_servicio');
            $table->integer('valor');
            $table->integer('iva');
            $table->integer('valor_total');
            $table->string('imagen1', 100);
            $table->string('imagen2', 100);
            $table->string('imagen3', 100);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicios');
    }
}
