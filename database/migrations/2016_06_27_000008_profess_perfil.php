<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfessPerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profess_perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuprofess')->unsigned();
            $table->enum('genero', ['m','f']);
            $table->date('fecha_nacimiento');
            $table->string('imagen', 100);

            $table->foreign('id_usuprofess')
                ->references('id')
                ->on('usuario_profess')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profess_perfil');
    }
}
